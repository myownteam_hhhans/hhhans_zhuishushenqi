//
//  LSYReadPageViewController.h
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSYReadModel.h"
#import "ZY_DataDownloader.h"
#import "DZMATViewController.h"

@interface LSYReadPageViewController : DZMATViewController<ZY_DataDownloaderDelegate,UIAlertViewDelegate>
    
@property (nonatomic,strong) NSURL *resourceURL;
@property (nonatomic,strong) LSYReadModel *model;
@property (nonatomic,strong) ZY_DataDownloader *downLoader;
@property (nonatomic,assign) BOOL isNeedReadProgress;

-(void)changeSourceContent:(NSString *)txt;
@end

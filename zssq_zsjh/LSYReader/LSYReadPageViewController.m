//
//  LSYReadPageViewController.m
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYReadPageViewController.h"
#import "LSYReadViewController.h"
#import "LSYChapterModel.h"
#import "LSYMenuView.h"
#import "LSYCatalogViewController.h"
#import "UIImage+ImageEffects.h"
#import "LSYMarkModel.h"
#import <objc/runtime.h>
#import "NSString+HTML.h"
#import "ZY_BookHelp.h"
#import "SVProgressHUD.h"
#import "ZY_NovelDownLoadOperation.h"
#import "ZY_DBHelper.h"
#import "LSYWebViewController.h"
#import "HWWeakTimer.h"
#import "ZY_Common.h"
#import "ZY_NeedFiveStarView.h"
#import "ZY_ADHelper.h"
#import "ZY_JsonParser.h"
#import "CFT_parameterOnline.h"
#import "ZY_NovelProgress.h"
#import "LSNovelSourceViewController.h"
#import "Common.h"
#import "LSYChapterADView.h"
#import "GDTNativeAd.h"
#import "LS_DownloadInfoManager.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "LSYMoreInfoView.h"
#import "NWS_httpClient.h"

#define AnimationDelay 0.3

BOOL NIIsPad(void) {
    static NSInteger isPad = -1;
    if (isPad < 0) {
        isPad = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) ? 1 : 0;
    }
    return isPad > 0;
}

@interface LSYReadPageViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource,LSYMenuViewDelegate,UIGestureRecognizerDelegate,LSYCatalogViewControllerDelegate,LSYReadViewControllerDelegate,UIActionSheetDelegate,GDTNativeAdDelegate>
{
    NSUInteger _chapter;    //当前显示的章节
    NSUInteger _page;       //当前显示的页数
    NSUInteger _chapterChange;  //将要变化的章节
    NSUInteger _pageChange;     //将要变化的页数
    BOOL _isTransition;     //是否开始翻页
    NSUInteger downloadChapter; //正在下载的章节
}
@property (nonatomic,strong) UIPageViewController *pageViewController;
@property (nonatomic,getter=isShowBar) BOOL showBar; //是否显示状态栏
@property (nonatomic,strong) LSYMenuView *menuView; //菜单栏
@property (nonatomic,strong) LSYCatalogViewController *catalogVC;   //侧边栏
@property (nonatomic,strong) UIView * catalogView;  //侧边栏背景
@property (nonatomic,strong) LSYReadViewController *readView;   //当前阅读视图
@property (nonatomic,strong) UIView * bgView;  //背景亮度
@property (nonatomic,weak) NSTimer *timer;

@property (nonatomic,strong) ADModel *adEntity;
// m广告的数据数组，视频广告
@property (nonatomic,strong) NSArray *MADArray;

@property (nonatomic, assign) NSInteger showZeroPageCount;

@property (nonatomic, strong) GDTNativeAd *GDTAd;
@property (nonatomic, strong) NSArray *GDTDataArray;

@property (nonatomic, strong) GADBannerView *bannerView; // 谷歌banner

@property (nonatomic, strong) LSYMoreInfoView *moreInfo;

@property (nonatomic, assign) BOOL showedClickChapterFirstPage;

@property (nonatomic, weak) UIViewController *frontVC;
@property (nonatomic, weak) UIViewController *afterVC;

@property (nonatomic, assign) BOOL pageDirection; // 翻页方向 0 往前 1 往后

@property (nonatomic, strong) LSYReadViewController *downloadedPage;

@property (nonatomic, strong) NSMutableArray *downLoadedPages;

@end

@implementation LSYReadPageViewController
#pragma mark - ZY_DataDownloaderDelegate
-(void)dealloc{
    
    [_timer invalidate];
    _timer = nil;

}
-(void)downloadNavError:(NSError*)error downloadErrorType:(downloadErrorType)type{
    if(type == downloadErrorType_noKnow){
        _isTransition = NO;
    }
    _isTransition = NO;
    [SVProgressHUD showErrorWithStatus:@"加载失败!"];
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8/*延迟执行时间*/ * NSEC_PER_SEC));
    
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
         [SVProgressHUD dismiss];
    });
}
-(void)htmlDataFinish:(NSData *)htmlData{
    NSMutableArray *newChapters = [[[ZY_JsonParser alloc]init]getChapterListByData:htmlData];
    NSInteger oldCount = _model.chapters.count;
    NSInteger count = newChapters.count - _model.chapters.count;
    if (count > 0) {
        for (int i = 0; i < count; i++) {
            [_model.chapters addObject:[newChapters objectAtIndex:oldCount+i]];
        }
        _model.record.chapterCount = _model.chapters.count;
        [LSYReadModel updateLocalModel:_model];
    }
}
-(void)ContentDataFinish:(NSData *)htmlData{

    _isTransition = NO;
    if (htmlData) {
        NSString *newContent = [[[ZY_JsonParser alloc]init] getNovelContent:htmlData];
        if (newContent && ![newContent isEqualToString:@""]) {
            [SVProgressHUD dismiss];
            
            
            NSArray *arr = self.pageViewController.viewControllers;
            NSLog(@"pageView:%ld",arr.count);
            
            
            newContent = [self prefixSpaceWithStr:newContent  withTitle:_model.chapters[downloadChapter].title];
            _model.chapters[downloadChapter].content = newContent;
            
            
            if (self.downloadedPage) {
                self.downloadedPage.authur = self.model.novelInfo.novelAuthor;
                self.downloadedPage.bookName = self.model.novelInfo.novelName;
                [self.downloadedPage reSetContent:newContent withTitle:_model.chapters[downloadChapter].title];
            }
            
            [LSYReadModel updateLocalModel:_model];
        }
        else{
            [SVProgressHUD showErrorWithStatus:@"加载失败!"];
        }
    }else{
        [SVProgressHUD showErrorWithStatus:@"加载失败!"];
    }
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0/*延迟执行时间*/ * NSEC_PER_SEC));
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    
    ZY_NovelProgress *progress = [[ZY_NovelProgress alloc]init];
    progress.bookID = _model.novelInfo.novelId;
    progress.chapterID = [[_model.chapters objectAtIndex:downloadChapter] chapterID];
    progress.chapterIndex = [NSString stringWithFormat:@"%zd",downloadChapter];
    [[ZY_DBHelper shareDBHelper] updateBookProgress:progress];
}

-(ZY_DataDownloader *)downLoader{
    if (!_downLoader) {
        _downLoader = [[ZY_DataDownloader alloc] init];
        [_downLoader setDelegate:self];
    }
    return _downLoader;
}

-(void)updateChapter{
    //更新章节
    [self.downLoader getNovelHtmlDataByUrl:_model.novelInfo.novelListUrl];
}

- (void)resetChapters:(NSNotification *)not {
    NSArray *chapterArr = not.object;
    if (!(chapterArr.count > 0)) {
        return;
    }
    [_model.chapters removeAllObjects];
    _model.chapters = [NSMutableArray arrayWithArray:chapterArr[0]];
    _model.record.chapterCount = _model.chapters.count;
    _model.novelInfo.novelListUrl = chapterArr[1];
    [LSYReadModel updateLocalModel:_model];
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self catalog:nil didSelectChapter:0 page:0];
    });

}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //[self.navigationController setToolbarHidden:YES animated:NO];
    self.navigationController.navigationBar.translucent = NO;
    //self.tabBarController.tabBar.hidden = YES;
}


- (void)viewWillDisappear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.showZeroPageCount = 0;
    
    
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // 监听换源通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetChapters:) name:@"resetChapterList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GDTShowUpload:) name:@"bottomADShow" object:nil];
    
    // 洛米或者广点通广告
    NSString *adtype = [ZY_ADHelper shareADHelper].bottomADType;
    AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:adtype];
    if ([Common shareCommon].zs_Open && [adtype isEqualToString:@"5"]) {
        self.adEntity =  [[ZY_ADHelper shareADHelper] obtainADInstance:adType_Gif adPlatform:platform];
    }
    if ([Common shareCommon].zs_Open && [adtype isEqualToString:@"3"]) {
        NSDictionary *gdtKeyAndID = [[ZY_ADHelper shareADHelper] getGDTKeyWithADPlace:@"readerbottom"];
        self.GDTAd = [[GDTNativeAd alloc] initWithAppkey:gdtKeyAndID[@"ID"] placementId:gdtKeyAndID[@"KEY"]];
        self.GDTAd.delegate = self;
        self.GDTAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
        [self.GDTAd loadAd:3];
    }

    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    if (_model.chapters.count > 0) {
        [self performSelector:@selector(updateChapter) withObject:nil afterDelay:3];
    }

    [self addChildViewController:self.pageViewController];
    _chapter = _model.record.chapter;
    _page = _model.record.page;
    if (_model.record.chapterModel.ChapterUrl) {
        self.menuView.lbl_orgLink.text = _model.record.chapterModel.ChapterUrl;
    }else if(_model.chapters.count > 0){
        self.menuView.lbl_orgLink.text = [_model.chapters objectAtIndex:0].ChapterUrl;
        //初始化浏览记录
        _model.record.chapterModel = _model.chapters.firstObject;
        _model.record.chapterCount = _model.chapters.count;
        _model.record.page = 0;
       
    }
    BOOL readProgress = NO;
    if (!self.isNeedReadProgress) {
        NSArray *array = [[ZY_DBHelper shareDBHelper] queryProgressWithID:_model.novelInfo.novelId];
        if (array.count > 0) {
            ZY_NovelProgress *progress = [array objectAtIndex:0];
            if (![progress.chapterID isEqualToString:@"0"]) {
                if (_model.record.page == 0) {
                    NSInteger chapter = [progress.chapterIndex integerValue];
                    if (chapter != 0 && _model.chapters.count > chapter) {
                        _model.record.chapter = chapter;
                        _chapter = chapter;
                        //谨慎
                        [LSYReadModel updateLocalModel:_model];
                        self.showZeroPageCount = 1;
                        
                        LSYReadViewController *readVC = [self readViewWithChapter:chapter page:0];
                        _readView = readVC;
                        
                        [self.pageViewController setViewControllers:@[readVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
                        readProgress = YES;
                    }
                }
            }
        }
    }
    
    if (readProgress == NO) {
        //谨慎
        [LSYReadModel updateLocalModel:_model];
        if(_model.record.page == 0) {
            self.showZeroPageCount = 1;
        }
        LSYReadViewController *readVC = [self readViewWithChapter:_model.record.chapter page:_model.record.page];
        _readView = readVC;
        [self.pageViewController setViewControllers:@[readVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    }
    
    [self.view addGestureRecognizer:({
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showToolMenu:)];
        tap.delegate = self;
        tap;
    })];
    
    // 加载谷歌广告
    if ([Common shareCommon].zs_Open && [[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookbanner"] && [[ZY_ADHelper shareADHelper].bannerADType isEqualToString:@"1"]) {
        self.bannerView = [[GADBannerView alloc]
                           initWithAdSize:kGADAdSizeBanner];
        CGFloat bottomFix = 49;
        if (isIPhoneX) {
            bottomFix = 83;
        }
        [self.view addSubview:self.bannerView];
        [self.bannerView setFrame:CGRectMake(k_ScreenWidth/2 - 160, k_ScreenHeight - bottomFix, 320, 50)];
        self.bannerView.adUnitID = @"ca-app-pub-9664827263749384/8608386404";
        self.bannerView.rootViewController = self;
        [self.bannerView loadRequest:[GADRequest request]];
        [MobClick event:@"Google_show" label:@"展示_阅读器banner"];
    }
    
    [self.view addSubview:self.menuView];
    
    [self addChildViewController:self.catalogVC];
    [self.view addSubview:self.catalogView];
    [self.catalogView addSubview:self.catalogVC.view];
    
    [self.view addSubview:self.bgView];
    
    if (!_timer) {
        _timer = [HWWeakTimer scheduledTimerWithTimeInterval:60.0f block:^(id userInfo) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"HH:mm"];
            NSString *timestamp = [formatter stringFromDate:[NSDate date]];
            _readView.lbl_time.text = timestamp;
        } userInfo:@"Fire" repeats:YES];
        [_timer fire];
    }
    
    
}

-(BOOL)prefersStatusBarHidden
{
    return !_showBar;
}

-(void)showToolMenu:(UITapGestureRecognizer *)tap
{
    
    if (self.readView.isNeedAD) {
        CGPoint point = [tap locationInView:self.readView.readView];
        
        if (self.readView.readView.isFinishAd && point.y > self.readView.readView.frame.size.height - _readView.adModel.height*(CGRectGetWidth(_readView.readView.frame) / _readView.adModel.width)) {
  
            [[ZY_ADHelper shareADHelper] tapAD:_readView.adModel CurrentVC:self];
            
            if ([[ZY_ADHelper shareADHelper].bottomADType isEqualToString:@"3"]) {
               
            } else if ([[ZY_ADHelper shareADHelper].bottomADType isEqualToString:@"5"]) {
                
            }
            return;
        }
        
    }
    
    // 往前翻个页
    CGPoint point = [tap locationInView:self.view];
    if (point.x < 80) {

        NSInteger _pageChange = _page;
        NSInteger _chapterChange = _chapter;

        if (_chapterChange == 0 &&_pageChange == 0) {
            return;
        }
        if (_pageChange == 0) {
            _chapterChange--;
            _pageChange = _model.chapters[_chapterChange].pageCount-1;
            if (_model.chapters[_chapterChange].pageCount==0) {
                _pageChange=0;
            }
        }
        else{
            _pageChange--;
        }
        

        [self updateReadModelWithChapter:_chapterChange page:_pageChange];
        if (self.frontVC) {
            _readView = (LSYReadViewController *)self.frontVC;
            [self.pageViewController setViewControllers:@[self.frontVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        } else {
            LSYReadViewController *readVC = [self readViewWithChapter:_chapterChange page:_pageChange];
            _readView = readVC;
            [self.pageViewController setViewControllers:@[readVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        }
        return;

    }
    // 往后翻个页
    if (point.x > k_ScreenWidth - 80) {
        NSInteger _pageChange = _page;
        NSInteger _chapterChange = _chapter;
        
        if (_page == _model.chapters.lastObject.pageCount-1 && _chapter == _model.chapters.count-1) {
            return;
        }
        
        if (_pageChange == _model.chapters[_chapterChange].pageCount-1 || _model.chapters[_chapterChange].pageCount == 0 || _model.chapters[_chapterChange].content.length ==0) {
            _chapterChange++;
            _pageChange = 0;
            self.showZeroPageCount = 0;
        }
        else{
            _pageChange++;
        }
        
        if (_pageChange == 1 && self.showZeroPageCount < 2) {
            _pageChange -= 1;
        }
        
        self.showZeroPageCount += 1;
        
        if (self.showZeroPageCount == 2 && _pageChange == 1) {
            _pageChange = 0;
        }

        if (self.afterVC) {
            _readView = (LSYReadViewController *)self.afterVC;
            [self updateReadModelWithChapter:_readView.recordModel.chapter page:_readView.recordModel.page];
            [self.pageViewController setViewControllers:@[self.afterVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        } else {
            LSYReadViewController *readVC = [self readViewWithChapter:_chapterChange page:_pageChange];
            _readView = readVC;
            [self updateReadModelWithChapter:_chapterChange page:_pageChange];
            [self.pageViewController setViewControllers:@[readVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        }
        
        return;

    }
    
    [_readView.readView cancelSelected];
    NSString * key = [NSString stringWithFormat:@"%d_%d",(int)_model.record.chapter,(int)_model.record.page];
    
    id state = _model.marksRecord[key];
    state?(_menuView.topView.state=1): (_menuView.topView.state=0);
    [self.menuView showAnimation:YES];
}

- (void)loadMad:(NSNotification *)not {
    self.MADArray = (NSArray *)not.object;
}

#pragma mark - init
-(LSYMenuView *)menuView
{
    if (!_menuView) {
        _menuView = [[LSYMenuView alloc] init];
        _menuView.inTheShelfNow = [[ZY_DBHelper shareDBHelper] queryBookIsOnTheBookShelf:_model.novelInfo.novelId];
        _menuView.hidden = YES;
        _menuView.delegate = self;
        _menuView.recordModel = _model.record;
    }
    return _menuView;
}

-(UIPageViewController *)pageViewController
{
    if (!_pageViewController) {
        
        _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        //_pageViewController.doubleSided = YES;
        _pageViewController.delegate = self;
        _pageViewController.dataSource = self;
        [self.view addSubview:_pageViewController.view];
    }
    return _pageViewController;
}
-(LSYCatalogViewController *)catalogVC
{
    if (!_catalogVC) {
        _catalogVC = [[LSYCatalogViewController alloc] init];
        _catalogVC.readModel = _model;
        _catalogVC.catalogDelegate = self;
    }
    return _catalogVC;
}
-(UIView *)catalogView
{
    if (!_catalogView) {
        _catalogView = [[UIView alloc] init];
        _catalogView.backgroundColor = [UIColor clearColor];
        _catalogView.hidden = YES;
        [_catalogView addGestureRecognizer:({
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenCatalog)];
            tap.delegate = self;
            tap;
        })];
    }
    return _catalogView;
}
#pragma mark - CatalogViewController Delegate
-(void)catalog:(LSYCatalogViewController *)catalog didSelectChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    [_menuView hiddenAnimation:YES];
    self.showZeroPageCount = 1;
    LSYReadViewController *tmpRead = [self readViewWithChapter:chapter page:page];
    if(tmpRead){
        _readView = tmpRead;
        [self updateReadModelWithChapter:chapter page:page];
        [self.pageViewController setViewControllers:@[tmpRead] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        _readView = tmpRead;
    }
    [self hiddenCatalog];
}

#pragma mark - 广点通代理
- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray {
    self.GDTDataArray = nativeAdDataArray;
    self.adEntity =  [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_Feed];
}
- (void)GDTShowUpload:(NSNotification *)not {
    if ([not.object isKindOfClass:[UIView class]]) {
        [self.GDTAd attachAd:self.adEntity.data toView:(UIView *)not.object];
    }
}

#pragma mark -  UIGestureRecognizer Delegate
//解决TabView与Tap手势冲突
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    
    if (touch.view == self.menuView.v_setting  || [self.menuView.v_setting.subviews containsObject:touch.view]) {
        return NO;
    }
    
    CGPoint point = [touch locationInView:  self.view];
    CGFloat bottomFix = 0;
    if (isIPhoneX) {
        bottomFix = 39;
    }
    if (point.y > k_ScreenHeight - 49 - bottomFix) {
        return NO;
    }
//    if (point.x < 100) {
//        return NO;
//    }
    
    return  YES;
}
#pragma mark - Privite Method
-(void)catalogShowState:(BOOL)show
{
    show?({
        [[NSNotificationCenter defaultCenter]postNotificationName:@"reSetChapterFirstDisplay" object:nil];
        _catalogView.hidden = !show;
        [UIView animateWithDuration:AnimationDelay animations:^{
            _catalogView.frame = CGRectMake(0, 0,2*ViewSize(self.view).width, ViewSize(self.view).height);
            
        } completion:^(BOOL finished) {
            [_catalogView insertSubview:[[UIImageView alloc] initWithImage:[self blurredSnapshot]] atIndex:0];
        }];
    }):({
        if ([_catalogView.subviews.firstObject isKindOfClass:[UIImageView class]]) {
            [_catalogView.subviews.firstObject removeFromSuperview];
        }
        [UIView animateWithDuration:AnimationDelay animations:^{
            _catalogView.frame = CGRectMake(-ViewSize(self.view).width, 0, 2*ViewSize(self.view).width, ViewSize(self.view).height);
        } completion:^(BOOL finished) {
            _catalogView.hidden = !show;
        }];
    });
}
-(void)hiddenCatalog
{
    [self catalogShowState:NO];
}
- (UIImage *)blurredSnapshot {
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)), NO, 1.0f);
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)) afterScreenUpdates:NO];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImage *blurredSnapshotImage = [snapshotImage applyLightEffect];
    UIGraphicsEndImageContext();
    return blurredSnapshotImage;
}
#pragma mark - Menu View Delegate

- (void)menuViewShowMoreInfo {
    
    [self.menuView hiddenAnimation:YES];
    
    if (!self.moreInfo) {
        self.moreInfo = [[LSYMoreInfoView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight)];
        [self.moreInfo bindDataWithInfo:self.model.novelInfo];
        [[UIApplication sharedApplication].keyWindow addSubview:self.moreInfo];
    } else {
        self.moreInfo.hidden = NO;
    }
}

- (void)menuViewAddToShelf {
    if (![[ZY_DBHelper shareDBHelper] queryBookIsOnTheBookShelf:_model.novelInfo.novelId]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addToBookShelf" object:nil];
        ZY_NovelProgress *progress = [[ZY_NovelProgress alloc]init];
        progress.bookID = _model.novelInfo.novelId;
        LSYReadModel *model = [LSYReadModel getLocalModelWithNovel:_model.novelInfo];
        progress.chapterID = model.chapterID;
        progress.chapterIndex = [NSString stringWithFormat:@"%zd",model.record.chapter];
        [[ZY_DBHelper shareDBHelper] addBookProgress:progress];
        [[ZY_DBHelper shareDBHelper] addBookToShelf:_model.novelInfo];
    }else{
//        [LSYReadModel delLocalModelWithNovel:_model.novelInfo];//取消加入 删除阅读历史
//        [[ZY_DBHelper shareDBHelper] removeBookProgressWithNovelId:_model.novelInfo.novelId];
//        [[ZY_DBHelper shareDBHelper] removeBookIsOnTheBookShelfWithNovelId:_model.novelInfo.novelId];
//        [SVProgressHUD setMinimumDismissTimeInterval:2];
//        [SVProgressHUD showSuccessWithStatus:@"删除成功"];
        // 下载
        UIActionSheet *sheet = [[UIActionSheet alloc] init];
        sheet.tag = 1000601;
        [self actionSheet:sheet clickedButtonAtIndex:2];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 10086){ //加入书架
        if (buttonIndex > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"addToBookShelf" object:nil];
            ZY_NovelProgress *progress = [[ZY_NovelProgress alloc]init];
            progress.bookID = _model.novelInfo.novelId;
            LSYReadModel *model = [LSYReadModel getLocalModelWithNovel:_model.novelInfo];
            progress.chapterID = model.chapterID;
            progress.chapterIndex = [NSString stringWithFormat:@"%zd",model.record.chapter];
            [[ZY_DBHelper shareDBHelper] addBookProgress:progress];
            [[ZY_DBHelper shareDBHelper] addBookToShelf:_model.novelInfo];
        }else{
            [LSYReadModel delLocalModelWithNovel:_model.novelInfo];//取消加入 删除阅读历史
        }
        [self dismissViewControllerAnimated:YES completion:^{
            [[NSNotificationCenter defaultCenter]postNotificationName:ZY_CheckBookUpdateDidFinishedNotification object:nil];
        }];
    }else{ //去网页阅读
        if (buttonIndex > 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_model.record.chapterModel.ChapterUrl]];
        }
    }
}

-(void)menuViewBack{
    [SVProgressHUD dismiss];
    [_timer invalidate];
    _timer = nil;
    [[UIApplication sharedApplication].keyWindow viewWithTag:9999].hidden = YES;
//    if (self.isBeingPresented) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    } else {
        [self.navigationController popViewControllerAnimated:YES];
//    }
    
    
//    [self dismissViewControllerAnimated:YES completion:^{
//         [[NSNotificationCenter defaultCenter]postNotificationName:ZY_CheckBookUpdateDidFinishedNotification object:nil];
//    }];
    
}

-(void)menuViewOrgClick{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"阅读器已经对原页面通过转码技术转码，但未对源网站内容进行任何实质性的修改和编辑。若页面存在问题，您可以去原页面浏览。" delegate:self cancelButtonTitle:@"继续阅读" otherButtonTitles:@"查看原网页", nil];
    alert.tag = 10087;
    [alert show];
}
-(void)menuViewShowDownLoad{
   
    UIActionSheet *alertSheet = [[UIActionSheet alloc] initWithTitle:@"缓存章节" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"缓存后50章",@"缓存剩余章节",@"缓存全本", nil];
    alertSheet.tag=1000601;
    [alertSheet showInView:self.view];
}

-(void)menuViewDidHidden:(LSYMenuView *)menu
{
    _showBar = NO;
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)menuViewDidAppear:(LSYMenuView *)menu
{
    _showBar = YES;
    
    [self setNeedsStatusBarAppearanceUpdate];
}



-(void)menuViewInvokeCatalog:(LSYBottomMenuView *)bottomMenu
{
    [self catalogShowState:YES];
}

-(void)menuViewChangeTheme:(UIColor *)bgColor{
    
    [_readView.view setBackgroundColor:bgColor];
}

-(void)menuViewJumpChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    self.showZeroPageCount = 1;
    LSYReadViewController *tmpRead = [self readViewWithChapter:chapter page:page];
    if(tmpRead){
        _readView = tmpRead;
        [self.pageViewController setViewControllers:@[tmpRead] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [self updateReadModelWithChapter:chapter page:page];
    }
}

-(void)menuViewFontSize:(LSYBottomMenuView *)bottomMenu
{
    LSYReadViewController *tmpRead = [self readViewWithChapter:_model.record.chapter page:(_model.record.page>_model.record.chapterModel.pageCount-1)?_model.record.chapterModel.pageCount-1:_model.record.page];
    if(tmpRead){
        _readView = tmpRead;
        [_model.record.chapterModel updateFont];
        
        [self.pageViewController setViewControllers:@[tmpRead] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        [self updateReadModelWithChapter:_model.record.chapter page:(_model.record.page>_model.record.chapterModel.pageCount-1)?_model.record.chapterModel.pageCount-1:_model.record.page];
    }
}
-(void)menuViewchangeSource:(LSYTopMenuView *)topMenu{
    
    LSNovelSourceViewController *NovelSource = [[LSNovelSourceViewController alloc]init];
    NovelSource.novel = _model.novelInfo;
    NovelSource.chapterTitle = _model.chapters[_chapter].title;
    NovelSource.chapterUrl = _model.chapters[_chapter].ChapterUrl;
    NovelSource.pageView = self;
   
    [self presentViewController:NovelSource animated:YES completion:^{

    }];
}
-(void)menuViewMark:(LSYTopMenuView *)topMenu
{
   
    NSString * key = [NSString stringWithFormat:@"%d_%d",(int)_model.record.chapter,(int)_model.record.page];
    id state = _model.marksRecord[key];
    if (state) {
        //如果存在移除书签信息
        [_model.marksRecord removeObjectForKey:key];
        [[_model mutableArrayValueForKey:@"marks"] removeObject:state];
    }
    else{
        //记录书签信息
        LSYMarkModel *model = [[LSYMarkModel alloc] init];
        model.date = [NSDate date];
        model.recordModel = [_model.record copy];
        [[_model mutableArrayValueForKey:@"marks"] addObject:model];
        [_model.marksRecord setObject:model forKey:key];
    }
    [LSYReadModel updateLocalModel:_model];
    _menuView.topView.state = !state;
}

- (void)menuViewChangeMode {
    //    if (_pageViewController) {
    //        [_pageViewController.view removeFromSuperview];
    //        [_pageViewController removeFromParentViewController];
    //        _pageViewController = nil;
    //    }
    //    _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:([LSYReadConfig shareInstance].readMode == 0)?UIPageViewControllerTransitionStylePageCurl:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    //
    //    _pageViewController.delegate = self;
    //    _pageViewController.dataSource = self;
    //    [self addChildViewController:_pageViewController];
    //    [self.view insertSubview:_pageViewController.view belowSubview:self.menuView];
}

-(void)menuViewLight:(float)light{
    self.bgView.alpha  = -light;
    [LSYReadConfig shareInstance].light = -light;
}
- (void)menuViewChangeProgress:(CGFloat)index {
    
    if (index > 98) {
        [self menuViewJumpChapter:(self.model.chapters.count - 1) page:0];
        return;
    }
    [self menuViewJumpChapter:(self.model.chapters.count-1) / 100 * index page:0];
}
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex != 3) {
        [[LS_DownloadInfoManager shareDownloadInfoManager] addItemToDownLoadingArray:self.model];
    }
    
    //下载
    if (actionSheet.tag==1000601) {
        if (buttonIndex==0) {
       
            ZY_NovelContentDownLoadInfo *downLoadInfo = [[ZY_NovelContentDownLoadInfo alloc] init];
            [downLoadInfo setDownloadType:downloadTypeThe50Chapter];
            [downLoadInfo setReadModel:self.model];
            downLoadInfo.StartDownloadIndex = self.model.record.chapter;
            ZY_NovelDownLoadOperation *downLoadOperation = [[ZY_NovelDownLoadOperation alloc] init];
            [downLoadOperation setDownloadInfo:downLoadInfo];
            [[ZY_BookHelp shareBookHelp].novelDownLoadQueue addOperation:downLoadOperation];
            [SVProgressHUD setMinimumDismissTimeInterval:2];
        }else if (buttonIndex==1){
        
            ZY_NovelContentDownLoadInfo *downLoadInfo = [[ZY_NovelContentDownLoadInfo alloc] init];
            [downLoadInfo setDownloadType:downloadTypeShengYuChapter];
            [downLoadInfo setReadModel:self.model];
            downLoadInfo.StartDownloadIndex = self.model.record.chapter;
            ZY_NovelDownLoadOperation *downLoadOperation = [[ZY_NovelDownLoadOperation alloc] init];
            [downLoadOperation setDownloadInfo:downLoadInfo];
            [[ZY_BookHelp shareBookHelp].novelDownLoadQueue addOperation:downLoadOperation];
            [SVProgressHUD setMinimumDismissTimeInterval:2];
        }else if(buttonIndex==2){

            ZY_NovelContentDownLoadInfo *downLoadInfo = [[ZY_NovelContentDownLoadInfo alloc] init];
            [downLoadInfo setDownloadType:downloadTypeAll];
            [downLoadInfo setReadModel:self.model];
            ZY_NovelDownLoadOperation *downLoadOperation = [[ZY_NovelDownLoadOperation alloc] init];
            [downLoadOperation setDownloadInfo:downLoadInfo];
            [[ZY_BookHelp shareBookHelp].novelDownLoadQueue addOperation:downLoadOperation];
            [SVProgressHUD setMinimumDismissTimeInterval:2];
        }
        
        if (buttonIndex!=3) {
            [SVProgressHUD showSuccessWithStatus:@"加入缓存管理，正在下载中!"];
            if (![[ZY_DBHelper shareDBHelper] queryBookIsOnTheBookShelf:_model.novelInfo.novelId]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"addToBookShelf" object:nil];
                ZY_NovelProgress *progress = [[ZY_NovelProgress alloc]init];
                progress.bookID = _model.novelInfo.novelId;
                LSYReadModel *model = [LSYReadModel getLocalModelWithNovel:_model.novelInfo];
                progress.chapterID = model.chapterID;
                progress.chapterIndex = [NSString stringWithFormat:@"%zd",model.record.chapter];
                [[ZY_DBHelper shareDBHelper] addBookProgress:progress];
                [[ZY_DBHelper shareDBHelper] addBookToShelf:_model.novelInfo];
            }
        }
        
    }
    
}
-(void)changeSourceContent:(NSString *)txt{
    _readView = [[LSYReadViewController alloc] init];
    _readView.authur = self.model.novelInfo.novelAuthor;
    _readView.bookName = self.model.novelInfo.novelName;
    _readView.recordModel = _model.record;
    _readView.type = ReaderTxt;
    [_model.record.chapterModel updateFont];
    _model.chapters[_model.record.chapter].content = [self prefixSpaceWithStr:txt withTitle:_model.chapters[_model.record.chapter].title];
    [_readView reSetContent:txt withTitle:_model.chapters[_model.record.chapter].title];
    
    _readView.delegate = self;
    
    [self.pageViewController setViewControllers:@[_readView] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}
#pragma mark - Create Read View Controller

- (void)continueReadClick {
    
    self.showZeroPageCount = 2;
    LSYReadViewController *tmpRead = [self readViewWithChapter:_chapter page:0];
    if (tmpRead.content && tmpRead.content.length > 0) {
        _readView = tmpRead;
        [self.pageViewController setViewControllers:@[tmpRead] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    }
    
}

-(LSYReadViewController *)readViewWithChapter:(NSUInteger)chapter page:(NSUInteger)page{

    bool isNewChapter = (self.showZeroPageCount == 1 && page == 0) ? YES : NO;
    //BOOL isNewChapter = NO;
    
    if (_model.record.chapter != chapter) {//该切换章节了
        [_model.record.chapterModel updateFont];
    }
    
    __block LSYReadViewController *readVC = [[LSYReadViewController alloc] init];
    readVC.isNewChapter = isNewChapter;
   
    NSLog(@"阅读器生成了一页%@：chapter:%ld page:%ld",readVC,chapter,page);
    
    readVC.recordModel = [_model.record copy];
    readVC.recordModel.page = page;
    readVC.recordModel.chapter = chapter;
    readVC.type = ReaderTxt;
    //_readView = readVC;
    
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"_iapManager"]) {
        
        if ([Common shareCommon].zs_Open) {
            NSString *adtype = [ZY_ADHelper shareADHelper].bottomADType;
            AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:adtype];

            
            if (platform != adplatform_Google && _model.chapters[chapter].pageCount-1 == page) {
                if (_model.record.chapter != chapter) {
                    if ([Common shareCommon].zs_Open && [adtype isEqualToString:@"5"]) {
                        readVC.adModel =  [[ZY_ADHelper shareADHelper] obtainADInstance:adType_Gif adPlatform:platform];
                    }
                    if ([Common shareCommon].zs_Open && [adtype isEqualToString:@"3"]) {
                        readVC.adModel = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_Feed];
                    }
                    self.adEntity = readVC.adModel;
                    
                }
                else{
                    readVC.adModel = self.adEntity ;
                }
            }
        }
    }
    if (_model.chapters.count > 0) {
        if (_model.chapters[chapter].content == nil || [_model.chapters[chapter].content isEqualToString:@""])
        {
            [SVProgressHUD showWithStatus:@"加载中..."];
            _isTransition = YES;
            downloadChapter = chapter;
            //异步下载内容
            NSLog(@"异步下载内容%@",_model.chapters[chapter].ChapterUrl);
            self.downloadedPage = readVC;
            //[self.downLoader getNovelContentDataByUrl:_model.chapters[chapter].ChapterUrl];
            __block downloadChapter = chapter;
            // 改成用afn下载
            [[NWS_httpClient sharedInstance] getWithURLString:_model.chapters[chapter].ChapterUrl parameters:nil success:^(NSDictionary * responseObject) {
                
                __block NSString *content = @"";
                
                if (responseObject) {
                    if ([[responseObject allKeys] containsObject:@"chapter"]) {
                        NSDictionary *dic_data = [responseObject objectForKey:@"chapter"];
                        
                        if ([[dic_data allKeys] containsObject:@"body"]) {
                            NSString *tmp_content = [dic_data objectForKey:@"body"];
                            if (![ZY_Common isBlankString:tmp_content]) {
                                content = tmp_content;
                            }
                        }
                        if ([[dic_data allKeys] containsObject:@"cpContent"]) {
                            NSString *tmp_content = [dic_data objectForKey:@"cpContent"];
                            if (![ZY_Common isBlankString:tmp_content]) {
                                content = tmp_content;
                            }
                        }
                        
                        
                    }
                }
                
                readVC.authur = self.model.novelInfo.novelAuthor;
                readVC.bookName = self.model.novelInfo.novelName;
                [readVC reSetContent:content withTitle:_model.chapters[downloadChapter].title];
                _model.chapters[downloadChapter].content = content;
                [LSYReadModel updateLocalModel:_model];
                [SVProgressHUD dismiss];
                
                if (readVC.recordModel.chapter == _chapter && readVC.recordModel.page == page) {
                    [self.pageViewController setViewControllers:@[readVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
                }
                
            } failure:^(NSError *error) {
                
            }];
            
            
        
        }else
        {
            _model.chapters[chapter].content = [self prefixSpaceWithStr:_model.chapters[chapter].content withTitle:_model.chapters[chapter].title];
            //if (_readView.adModel.height >0 &&_readView.adModel.width >0  ) {
            readVC.isNeedAD = [_model.chapters[chapter] isShowAD:page adImageHeight:readVC.adModel.height*(CGRectGetWidth(readVC.readView.frame) / readVC.adModel.width)];
            
            if ([[ZY_ADHelper shareADHelper].bottomADType isEqualToString:@"4"]) {
                readVC.isNeedAD = [_model.chapters[chapter] isShowAD:page adImageHeight:400*(CGRectGetWidth(readVC.readView.frame) / 600)];
            }
//            }else{
//                _readView.isNeedAD = NO;
//
//            }
            
            readVC.authur = self.model.novelInfo.novelAuthor;
            readVC.bookName = self.model.novelInfo.novelName;
            [readVC reSetContent:[_model.chapters[chapter] stringOfPage:page] withTitle:_model.chapters[chapter].title];
            
            
        }
    }
    readVC.delegate = self;
    NSLog(@"_readGreate");
    
    
    return readVC;
    
    
    
}

-(NSString *)prefixSpaceWithStr:(NSString *)content withTitle:(NSString *)title{
    NSString *newContent = content;
    
    if (![content containsString:title]) {
        newContent = [[NSString stringWithFormat:@"%@",content] stringByReplacingOccurrencesOfString:@"<br /><br />" withString:@"{zhanweifu}"];
        
        newContent = [newContent stringByReplacingOccurrencesOfString:@"<br />" withString:@"{zhanweifu}"];
        
        newContent = [newContent stringByReplacingOccurrencesOfString:@"<br/>" withString:@"{zhanweifu}"];
        
        newContent = [newContent stringByReplacingOccurrencesOfString:@"<br>" withString:@"{zhanweifu}"];
        
        newContent = [newContent stringByReplacingOccurrencesOfString:@"\n" withString:@"{zhanweifu}"];
        
        newContent = [self replaceImageHtml:newContent withRegText:@"\\s" withReplaceText:@""];
        
        newContent = [newContent stringByReplacingOccurrencesOfString:@"{zhanweifu}" withString:@"\n"];
        
        NSMutableArray *contentArr = [[NSMutableArray alloc]init];
        NSArray *array = [newContent componentsSeparatedByString:@"\n"];
        for (int i = 0; i < array.count; i ++) {
            NSString *str = [array objectAtIndex:i];
            if ([str length]>0) {
                [contentArr addObject:str];
            }
            
        }
        
        NSMutableArray *mutArr = [[NSMutableArray alloc]init];
        for (int i = 0; i < contentArr.count; i ++) {
            NSString *str = [contentArr objectAtIndex:i];
            if (![str hasPrefix:@"　　"]) {
                str = [NSString stringWithFormat:@"　　%@",str];
            }
            // str = [self replaceImageHtml:str withRegText:@"\\s" withReplaceText:@"[][]"];
            [mutArr addObject:str];
        }
        newContent = [mutArr componentsJoinedByString:@"\n"];
        if (![newContent containsString:title]) {
            newContent = [NSString stringWithFormat:@"%@\n%@",title,newContent];
        }
    }
    
    return newContent;
}

- (NSString *)replaceImageHtml:(NSString *)oldHtml withRegText:(NSString *)regex withReplaceText:(NSString *)replaceText{
    
    NSRange r;
    NSMutableString *newHtml = [NSMutableString stringWithString:oldHtml];
    
    BOOL flag = false;
    
    while (flag == false) {
        
        r = [newHtml rangeOfString:regex options:NSRegularExpressionSearch];
        if (r.location != NSNotFound) {
            [newHtml replaceCharactersInRange:r withString:replaceText];
        } else {
            flag = true;
        }
        
    };
    return newHtml;
}

-(void)updateReadModelWithChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    NSLog(@"阅读器 更新chapter:%ld, page:%ld",chapter,page);
    _chapter = chapter;
    _page = page;
    _model.record.chapterModel = _model.chapters[chapter];
    _model.record.chapter = chapter;
    _model.record.page = page;
    _readView.recordModel = [_model.record copy];
    [LSYReadModel updateLocalModel:_model];
}
#pragma mark - Read View Controller Delegate
-(void)readViewEndEdit:(LSYReadViewController *)readView
{
    for (UIGestureRecognizer *ges in self.pageViewController.view.gestureRecognizers) {
        if ([ges isKindOfClass:[UIPanGestureRecognizer class]]) {
            ges.enabled = YES;
            break;
        }
    }
}
-(void)readViewEditeding:(LSYReadViewController *)readView
{
    for (UIGestureRecognizer *ges in self.pageViewController.view.gestureRecognizers) {
        if ([ges isKindOfClass:[UIPanGestureRecognizer class]]) {
            ges.enabled = NO;
            break;
        }
    }
}
#pragma mark -PageViewController DataSource
//往前翻页
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{

//    if (_isTransition) {
//        return nil;
//    }
    NSLog(@"阅读器 --  前");
    
    _pageChange = _page;
    _chapterChange = _chapter;
    
    if (_chapterChange == 0 &&_pageChange == 0) {
        return nil;
    }
    if (_pageChange == 0) {
        _chapterChange--;
        _pageChange = _model.chapters[_chapterChange].pageCount-1;
        
        if (_model.chapters[_chapterChange].pageCount==0) {
            _pageChange=0;
        }
    }
    else{
        _pageChange--;
    }
    
    self.frontVC = [self readViewWithChapter:_chapterChange page:_pageChange];

    return self.frontVC;
    
}
//往后翻页
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
//    if (_isTransition) {
//        return nil;
//    }
    
    NSLog(@"阅读器 --  后");
    
    _pageChange = _page;
    _chapterChange = _chapter;
    if (_pageChange == _model.chapters.lastObject.pageCount-1 && _chapterChange == _model.chapters.count-1) {
        SVProgressHUD.minimumDismissTimeInterval = 3;
        [SVProgressHUD showInfoWithStatus:@"已经是最后一页了"];
        return nil;
    }
    
    // 快换章节的时候请求新的广告
    if (_pageChange == _model.chapters[_chapterChange].pageCount-2 || _model.chapters[_chapterChange].pageCount == 0) {
       
        [ZY_ADHelper shareADHelper].chapterADType = [[Common shareCommon] randomADTypeWithKey:@"readpage"];
        [[ZY_ADHelper shareADHelper] requestChapterAD];
    }
    
    if (_pageChange == _model.chapters[_chapterChange].pageCount-1 || _model.chapters[_chapterChange].pageCount == 0 || _model.chapters[_chapterChange].content.length ==0) {
        
        // 新章节请求广告
        [ZY_ADHelper shareADHelper].bottomADType = [[Common shareCommon] randomADTypeWithKey:@"readerbottom"];
        [ZY_ADHelper shareADHelper].bannerADType = [[Common shareCommon] randomADTypeWithKey:@"bookbanner"];
        [[ZY_ADHelper shareADHelper] requestBanner];
        
        _chapterChange++;
        _pageChange = 0;
        self.showZeroPageCount = 0;
    }
    else{
        _pageChange++;
    }
    
    if (_pageChange == 1 && self.showZeroPageCount < 2) {
        _pageChange -= 1;
    }
    self.showZeroPageCount += 1;
    
    self.afterVC = [self readViewWithChapter:_chapterChange page:_pageChange];

    return self.afterVC;
}

#pragma mark -PageViewController Delegate
//翻页动画执行完成后回调的方法
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    NSLog(@"阅读器 --  翻页完");
    if (!completed) {
        LSYReadViewController *readView = previousViewControllers.firstObject;
        _readView = readView;
        _page = readView.recordModel.page;
        _chapter = readView.recordModel.chapter;
    }
    else{
        
        LSYReadViewController *readVC = self.pageViewController.viewControllers[0];
        LSYReadViewController *previousVC = previousViewControllers[0];
        
        if (readVC.recordModel.chapter == previousVC.recordModel.chapter && readVC.recordModel.page >= previousVC.recordModel.page) {
            self.pageDirection = YES;
        } else if (readVC.recordModel.chapter > previousVC.recordModel.chapter ){
            self.pageDirection = YES;
        } else {
            self.pageDirection = NO;
        }
        //NSLog(@"翻页完成，方向：%ld",self.pageDirection);
        
        if (self.pageDirection) {
            if (_page == _model.chapters[_chapter].pageCount-1 || _model.chapters[_chapter].pageCount == 0 || _model.chapters[_chapter].content.length ==0) {
                _chapter++;
                _page = 0;
                //self.showZeroPageCount = 0;
            }
            else{
                _page++;
            }
            
            if (self.showZeroPageCount == 2 && _page == 1) {
                _page = 0;
            }
            
        } else {
            if (_page == 0) {
                _chapter--;
                _page = _model.chapters[_chapter].pageCount-1;
                if (_model.chapters[_chapter].pageCount==0) {
                    _page=0;
                }
            }
            else{
                _page--;
            }
        }

        NSLog(@"翻页--  翻页完成%ld, showzerocount%ld",_page,self.showZeroPageCount);
        [self updateReadModelWithChapter:_chapter page:_page];
        //  翻页完 如果没有内容
        
    }
}
//将要翻页时执行的方法
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers
{

    LSYReadViewController *readVC = pendingViewControllers[0];
    NSLog(@"翻页-- 将要翻页page %ld chapter%ld   下一页page：%ldchapter%ld",_page,_chapter,readVC.recordModel.page,readVC.recordModel.chapter);
    
    _readView = readVC;
    
    if (readVC.recordModel.chapter == _chapter && readVC.recordModel.page > _page) {
        self.pageDirection = YES;
    } else if (readVC.recordModel.chapter > _chapter ){
        self.pageDirection = YES;
    } else {
        self.pageDirection = NO;
    }
    
    NSLog(@"阅读器 --  将要翻页  方向：%d",self.pageDirection);

}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageViewController.view.frame = self.view.frame;
    _menuView.frame = self.view.frame;
    _catalogView.frame = CGRectMake(-ViewSize(self.view).width, 0, 2*ViewSize(self.view).width, ViewSize(self.view).height);
    _catalogVC.view.frame = CGRectMake(0, 0, ViewSize(self.view).width-[Common get414RadiusByMap:87], ViewSize(self.view).height);
    [_catalogVC reload];
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.frame = self.view.frame;
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = [LSYReadConfig shareInstance].light;
        //_bgView.alpha = 1;
        _bgView.userInteractionEnabled = NO;
    }
    return _bgView;
}

- (NSMutableArray *)downLoadedPages {
    if (!_downLoadedPages) {
        _downLoadedPages = [NSMutableArray array];
    }
    return _downLoadedPages;
}

#pragma mark - 懒加载

@end

//
//  LSYReadViewController.m
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYReadViewController.h"

#import "LSYReadParser.h"
#import "LSYReadConfig.h"
#import "MyBattery.h"
#import "ZY_AdEntity.h"
#import "ZY_Common.h"
#import "Common.h"
#import <UIImageView+WebCache.h>
#import "LSYChapterADView.h"
#import "LSYReadPageViewController.h"
#import <BUAdSDK/BUBannerAdView.h>
#import <BUAdSDK/BUAdSDK.h>

@interface LSYReadViewController ()<LSYReadViewControllerDelegate, ChapterADDelegate,BUBannerAdViewDelegate>


@property (nonatomic,strong) UILabel * lbl_progress;
@property (nonatomic,strong) UILabel * lbl_chapterTitle;
@property (nonatomic,strong) UILabel * lbl_battery;
@property (nonatomic,strong) UIImageView * img_battery;
@property (nonatomic,strong) UILabel * lbl_batteryProgress;
@property (nonatomic,strong) ZY_AdEntity *adEntity;
@property (nonatomic,strong) UIView *areaView;

@property (nonatomic,strong) LSYChapterADView *adView;


@end

@implementation LSYReadViewController
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [self removeObserver:self forKeyPath:@"recordModel"];
}

- (instancetype)init{
    self = [super init];
    if (self) {
        
        
        
        [self addObserver:self forKeyPath:@"recordModel" options:NSKeyValueObservingOptionNew context:NULL];
        [self.view addSubview:self.areaView];
        [self.areaView addSubview:self.readView];
        [self.areaView addSubview:self.lbl_chapterTitle];
        [self.areaView addSubview:self.lbl_time];
        [self.areaView addSubview:self.lbl_progress];
        [self.areaView addSubview:self.img_battery];
        [self.areaView addSubview:self.lbl_battery];
        [self.img_battery addSubview:self.lbl_batteryProgress];
        //[self.view addSubview:self.bgView];
        
        if ([[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookbanner"]) {
            [self.view addSubview:self.bannerView];
        }
        
        
        
    }
    return self;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    self.lbl_chapterTitle.text = self.recordModel.chapterModel.title;
    //当前章节阅读进度
    float progress = (self.recordModel.page*1.0/self.recordModel.chapterModel.pageArray.count*1.0)*100;
    if (!isnan(progress)) {
        _lbl_progress.text = [NSString stringWithFormat:@"%.1f%%" ,progress];
    }else{
        _lbl_progress.text = @"0.0%";
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prefersStatusBarHidden];
    UIColor *color = [Common colorWithHexString:@"#e9e9eb" alpha:1.0];;
    
    NSNumber *themeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
    switch (themeNumber.integerValue) {
        case 1:
            color = [Common colorWithHexString:@"#e9e9eb" alpha:1.0];
            break;
        case 2:
            color = [Common colorWithHexString:@"#f4efdc" alpha:1.0];
            break;
        case 3:
            color = [Common colorWithHexString:@"#c9ebc9" alpha:1.0];
            break;
        case 4:
            color = RGB(15, 15, 15);
            break;
            
        default:
            break;
    }
    
    if ([LSYReadConfig shareInstance].isNightMode) {
        color = RGB(15, 15, 15);
    }
    [self.view setBackgroundColor:color];
    
    [self reSetBatteryProgress];
    [self reSetChargState];
    __weak LSYReadViewController *weakSelf = self;
    [[MyBattery sharedIntance] setChargeLevelChange:^(UIDevice *dev){
        [weakSelf reSetBatteryProgress];
    }];
    [[MyBattery sharedIntance] setChargeStateChange:^(UIDevice *dev){
        [weakSelf reSetChargState];
    }];


}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

     //延迟再次试图加载广告
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.bannerView.hidden == YES && [Common shareCommon].zs_Open) {
            self.bannerView.hidden = NO;
            [self addBnnerAD];



            if (self.isNewChapter) {
                [[ZY_ADHelper shareADHelper] uploadBannerShow:self.bannerView];
            }
        }
    });


    if (self.isNewChapter) {
        [[ZY_ADHelper shareADHelper] uploadBannerShow:self.bannerView];
    }
    
    // 底部广告
    if (self.isNeedAD && [[ZY_ADHelper shareADHelper].bottomADType isEqualToString:@"4"]) {
        CGFloat bottom = 80;
        if (isIPhoneX) {
            bottom = 138;
        }
        
        BUSize *size = [BUSize sizeBy:BUProposalSize_Banner600_300];
        BUBannerAdView *banner = [[BUBannerAdView alloc] initWithSlotID:TouTiao_LastPage size:size rootViewController:self];
        [banner loadAdData];
        const CGFloat screenWidth = self.readView.bounds.size.width;
       
        CGFloat bannerHeight = screenWidth * size.height / size.width;
        banner.frame = CGRectMake(0, k_ScreenHeight - bannerHeight - bottom - 45, screenWidth, bannerHeight);
        banner.delegate = self;
        [self.readView addSubview:banner];
    }

     self.adView.backgroundColor = self.view.backgroundColor;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_readView viewWillAppear];
    
    [Common showDebugHUD:[NSString stringWithFormat:@"章节间：%@\n章节末：%@\nbanner：%@",[ZY_ADHelper shareADHelper].chapterADType,[ZY_ADHelper shareADHelper].bottomADType,[ZY_ADHelper shareADHelper].bannerADType]];
    

     //在这里添加子控件会无法翻页 原因不明

    if ([Common shareCommon].zs_Open) {
        self.bannerView.hidden = NO;
        [self addBnnerAD];
    }

    // 章节间广告
    if ([[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"readpage"] && self.isNewChapter) {
        CGFloat bottomFix = 0;
        if (isIPhoneX) {
            bottomFix = 39;
        }
        
        CGFloat bannerHeight = 47;
        if (![[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookbanner"] || ![Common shareCommon].zs_Open) {
            bannerHeight = 0;
        }

        self.adView = nil;
        //self.adView = [[LSYChapterADView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight - bannerHeight - bottomFix)];
        self.adView = [[LSYChapterADView alloc] init];
        self.adView.superViewController = self;
        [self.adView setFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight - bannerHeight - bottomFix)];
        self.adView.delegate  = self;
        self.adView.backgroundColor = self.view.backgroundColor;
        [self.view addSubview:self.adView];
        
        [self.view bringSubviewToFront:self.bgView];
    }

    if (self.bookName) {
        [self.adView bindTitle:self.bookName AuthurName:[NSString stringWithFormat:@"作者：%@",self.authur] ChapterName:[NSString stringWithFormat:@"下一章：%@",self.topTitle]];
    }



}

- (void)addBnnerAD {
    
    if (![Common shareCommon].zs_Open || ![[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookbanner"]) {
        return;
    }
    
    for (UIView *subView in self.bannerView.subviews) {
        [subView removeFromSuperview];
    }
    
    // 头条穿山甲广告
    if ([[ZY_ADHelper shareADHelper].bannerADType isEqualToString:@"4"]) {
        BUSize *size = [[BUSize alloc] init];
        size.width = k_ScreenWidth;
        size.height = 47;
        
        
        BUBannerAdView *banner = [[BUBannerAdView alloc] initWithSlotID:TouTiao_ReaderBottomBanner size:size rootViewController:self];
        [banner loadAdData];
        const CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
        
        CGFloat bannerHeight = screenWidth * size.height / size.width;
        banner.frame = CGRectMake(0, 0, screenWidth, bannerHeight);
        banner.delegate = self;
        [self.bannerView addSubview:banner];
        [self.bannerView setBackgroundColor:[UIColor clearColor]];
        // 头条穿山甲广告
        
       
        //[self.bannerView setBackgroundColor:[UIColor whiteColor]];
        return;
    }
    
    if (![[ZY_ADHelper shareADHelper].bannerADType isEqualToString:@"3"]) {
        return;
    }
    
    UIColor *backColor = [LSYReadConfig shareInstance].isNightMode ? RGB(11, 11, 11) : [UIColor whiteColor];
    
    [self.bannerView setBackgroundColor:backColor];
    
    UIImageView *bannerImageView = [[UIImageView alloc] initWithFrame:self.bannerView.bounds];
    [bannerImageView sd_setImageWithURL:[NSURL URLWithString:[ZY_ADHelper shareADHelper].bannerADModel.bannerURL]];
    [self.bannerView addSubview:bannerImageView];
    
    UIImageView *bannerIcon = [[UIImageView alloc] initWithFrame:CGRectMake(19, 3.5, 40, 40)];
    [bannerIcon sd_setImageWithURL:[NSURL URLWithString:[ZY_ADHelper shareADHelper].bannerADModel.iconURL]];
    [self.bannerView addSubview:bannerIcon];
    
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [titleLabel setText:[ZY_ADHelper shareADHelper].bannerADModel.title];
    [titleLabel setFont:[UIFont systemFontOfSize:12]];
    [titleLabel sizeToFit];
    [titleLabel setFrame:CGRectMake(63, 5, k_ScreenWidth - 80, titleLabel.frame.size.height)];
    [titleLabel setTextColor:[LSYReadConfig shareInstance].isNightMode ? RGB(111, 111, 111) : [UIColor blackColor]];
    [self.bannerView addSubview:titleLabel];
    
    UILabel *desLabel = [[UILabel alloc] init];
    [desLabel setText:[ZY_ADHelper shareADHelper].bannerADModel.desc];
    [desLabel setTextColor:[Common colorWithHexString:@"#AFAFAF" alpha:1.0]];
    [desLabel setFont:[UIFont systemFontOfSize:10]];
    [desLabel sizeToFit];
    [desLabel setFrame:CGRectMake(63, 31, k_ScreenWidth - 80, desLabel.frame.size.height)];
    [self.bannerView addSubview:desLabel];
    
    if (self.isNewChapter) {
        
        if ([[ZY_ADHelper shareADHelper].bannerADType isEqualToString:@"3"]) {
           
            [[ZY_ADHelper shareADHelper].nativeAd attachAd:[ZY_ADHelper shareADHelper].bannerADModel.gdtData toView:self.bannerView];
        } else if ([[ZY_ADHelper shareADHelper].bannerADType isEqualToString:@"3"]) {
           
        }
        
    }
    [[ZY_ADHelper shareADHelper] registClickWithBannerView:self.bannerView Controller:self];
    
    
    
}

- (void)bannerAdViewDidClick:(BUBannerAdView *)bannerAdView WithAdmodel:(BUNativeAd *)nativeAd {
   
}

- (void)adClose {
    if ([self.delegate respondsToSelector:@selector(continueReadClick)]) {
        [self.delegate continueReadClick];
    }
}

-(void)reSetBatteryProgress{
    float li = [MyBattery sharedIntance].currentBatteryDL/100.0;
    self.lbl_batteryProgress.frame = CGRectMake(1.5, 1.5, 14.5*li, 6);
    self.lbl_battery.text = [NSString stringWithFormat:@"%.0f%%",li*100];
}

-(void)reSetChargState{
    if([MyBattery sharedIntance].isChargState){
        self.img_battery.image = [UIImage imageNamed:@"充电中"];
        self.lbl_batteryProgress.alpha = 0;
    }else{
        self.lbl_batteryProgress.alpha = 1;
        self.img_battery.image = [UIImage imageNamed:@"未充电"];
    }
}

-(UIImageView *)img_battery
{
    if (!_img_battery) {
        _img_battery = [[UIImageView alloc] init];
        _img_battery.frame = CGRectMake(ScreenSize.width-70, ScreenSize.height-9-8.5, 20, 9);
    }
    return _img_battery;
}
-(UILabel *)lbl_chapterTitle
{
    if (!_lbl_chapterTitle) {
        _lbl_chapterTitle = [[UILabel alloc] init];
        _lbl_chapterTitle.frame = CGRectMake(LeftSpacing, 6, self.readView.frame.size.width, 20);
        _lbl_chapterTitle.font = [UIFont systemFontOfSize:12];
        _lbl_chapterTitle.textColor = RGB(105, 97, 78);
    }
    return _lbl_chapterTitle;
}

-(UILabel *)lbl_time
{
    if (!_lbl_time) {
        _lbl_time = [[UILabel alloc] init];
        _lbl_time.frame = CGRectMake(LeftSpacing, ScreenSize.height-20-3, 50, 20);
        _lbl_time.font = [UIFont systemFontOfSize:11];
        _lbl_time.textColor = RGB(105, 97, 78);
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        _lbl_time.text = [formatter stringFromDate:[NSDate date]];
    }
    return _lbl_time;
}

-(UILabel *)lbl_battery
{
    if (!_lbl_battery) {
        _lbl_battery = [[UILabel alloc] init];
        _lbl_battery.frame = CGRectMake(ScreenSize.width-50, ScreenSize.height-20-3, 30, 20);
        _lbl_battery.font = [UIFont systemFontOfSize:11];
        _lbl_battery.textColor = RGB(105, 97, 78);
        _lbl_battery.textAlignment = NSTextAlignmentRight;
    }
    return _lbl_battery;
}

-(UILabel *)lbl_batteryProgress
{
    if (!_lbl_batteryProgress) {
        _lbl_batteryProgress = [[UILabel alloc] init];
        _lbl_batteryProgress.backgroundColor = RGB(105, 97, 78);
    }
    return _lbl_batteryProgress;
}

-(UILabel *)lbl_progress
{
    if (!_lbl_progress) {
        _lbl_progress = [[UILabel alloc] init];
        _lbl_progress.frame = CGRectMake(ScreenSize.width/2-20, ScreenSize.height-20-3, 40, 20);
        _lbl_progress.font = [UIFont systemFontOfSize:11];
        _lbl_progress.textColor = RGB(105, 97, 78);
        _lbl_progress.textAlignment = NSTextAlignmentCenter;
    }
    return _lbl_progress;
}

-(UIView *)bgView
{
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
        _bgView.frame = self.view.frame;
        _bgView.backgroundColor = [UIColor blackColor];
        _bgView.alpha = [LSYReadConfig shareInstance].light;
        //_bgView.alpha = 1;
    }
    return _bgView;
}

-(LSYReadView *)readView
{
    if (!_readView) {
        _readView = [[LSYReadView alloc] initWithFrame:CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.view.frame.size.height-TopSpacing-BottomSpacing)];
    }
    return _readView;
}

-(void)reSetContent:(NSString *)content withTitle:(NSString *)title{
    self.readView.adModel = self.adModel;
    
    self.topTitle = title;
    [self reSetContent:content];
    
    
}

-(void)reSetContent:(NSString *)content
{
    
    
    if (content && ![content isEqualToString:@""]) {
        _content = content;
        if (self.isNeedAD) {
            NSMutableArray *array = [[NSMutableArray alloc]init];
            LSYImageData *imageData = [[LSYImageData alloc] init];
            ADModel *model = self.adModel;
           _readView.MADArray = @[@"ad"];
            if (model.adImgPath.length >0) {
                imageData.url = model.adImgPath;
                imageData.imageRect = CGRectMake(0, 0, CGRectGetWidth(self.readView.frame), model.height*(CGRectGetWidth(self.readView.frame) / model.width));
                imageData.position = 0;
                [array addObject:imageData];
                _readView.imageArray = array;
                _readView.MADArray = @[@"ad"];
            }
            
        } 
        _readView.frameRef = [LSYReadParser parserContent:_content config:[LSYReadConfig shareInstance] bouds:CGRectMake(0,0, _readView.frame.size.width, _readView.frame.size.height) withTitle:self.topTitle];
        _readView.content = _content;
        [self.adView bindTitle:self.bookName AuthurName:[NSString stringWithFormat:@"作者：%@",self.authur] ChapterName:[NSString stringWithFormat:@"下一章：%@",self.topTitle]];
    }
}
-(void)readViewEditeding:(LSYReadViewController *)readView
{
    if ([self.delegate respondsToSelector:@selector(readViewEditeding:)]) {
        [self.delegate readViewEditeding:self];
    }
}
-(void)readViewEndEdit:(LSYReadViewController *)readView
{
    if ([self.delegate respondsToSelector:@selector(readViewEndEdit:)]) {
        [self.delegate readViewEndEdit:self];
    }
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if (@available(iOS 11.0,*)) {
        
        CGRect rect = self.view.safeAreaLayoutGuide.layoutFrame;
        //CGFloat offH = [UIApplication sharedApplication].statusBarFrame.size.height;

        rect.origin.y = 20;
        if (isIPhoneX) {
            rect.origin.y = 44;
        }
        
        rect.size.height = rect.size.height - ([[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookbanner"] ? 47 : 0);
        self.areaView.frame = rect;
        self.lbl_chapterTitle.frame = CGRectMake(LeftSpacing, 6, self.areaView.frame.size.width, 20);
        self.lbl_time.frame = CGRectMake(LeftSpacing, self.areaView.frame.size.height-20-3, 50, 20);
        self.lbl_progress.frame = CGRectMake(ScreenSize.width/2-20, self.areaView.frame.size.height-20-3, 40, 20);
        self.img_battery.frame = CGRectMake(ScreenSize.width-70, self.areaView.frame.size.height-9-8.5, 20, 9);
        self.lbl_battery.frame = CGRectMake(ScreenSize.width-50, self.areaView.frame.size.height-20-3, 30, 20);
        self.readView.frame = CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.areaView.frame.size.height-TopSpacing-BottomSpacing);
        
        LSYReadConfig *config = [LSYReadConfig shareInstance];
        
        if (_content && ![_content isEqualToString:@""]) {
            _readView.frameRef = [LSYReadParser parserContent:_content config:config bouds:CGRectMake(0,0, _readView.frame.size.width, _readView.frame.size.height) withTitle:self.topTitle];
            _readView.content = _content;
        }
    }else{
        self.areaView.frame = self.view.frame;
        CGRect frame = self.areaView.frame;
        frame.size.height -= 47;
        self.areaView.frame = frame;
    }
    
}

-(UIView *)areaView{
    if (!_areaView) {
        if (@available(iOS 11.0,*)) {
            
            _areaView = [[UIView alloc]initWithFrame:self.view.safeAreaLayoutGuide.layoutFrame];
        }else{
            _areaView = [[UIView alloc]initWithFrame:self.view.frame];
        }
    }
    return _areaView;
}

- (UIView *)bannerView {
    if (_bannerView == nil) {
        _bannerView = [[UIView alloc] init];
        
        
        
        
        CGFloat bottomFix = 0;
        if (isIPhoneX) {
            bottomFix = 39;
        }
        
        [_bannerView setHidden:YES];
        
        CGFloat topFix = 20;
        if (isIPhoneX) {
            topFix = 44;
        }
        
        NSLog(@"%f",k_StatusBarHeight);
        
        [_bannerView setFrame:CGRectMake(0, k_ScreenHeight - 47 - bottomFix, k_ScreenWidth , 47)];
        
        [_bannerView setTag:4756];
    }
    return _bannerView;
}

@end

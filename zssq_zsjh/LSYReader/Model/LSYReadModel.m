//
//  LSYReadModel.m
//  LSYReader
//
//  Created by Labanotation on 16/5/31.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYReadModel.h"
#import "NSString+HTML.h"
#import "ZY_DBHelper.h"
#import "ZY_DBHelper+NovelInfo.h"
#import "ZY_NovelProgress.h"

@implementation LSYReadModel
-(instancetype)initWithContent:(NSString *)content
{
    self = [super init];
    if (self) {
        _content = content;
        NSMutableArray *charpter = [NSMutableArray array];
        [LSYReadUtilites separateChapter:&charpter content:content];
        _chapters = charpter;
        _marks = [NSMutableArray array];
        _record = [[LSYRecordModel alloc] init];
        _record.chapterModel = charpter.firstObject;
        _record.chapterCount = _chapters.count;
        _marksRecord = [NSMutableDictionary dictionary];
        _type = ReaderTxt;
    }
    return self;
}
-(instancetype)initWithNovel:(ZY_NovelInfo *)novelInfo
{
    self = [super init];
    if (self) {
        
        _bookID = novelInfo.novelId;
        _chapters = [NSMutableArray array];
        _marks = [NSMutableArray array];
        _record = [[LSYRecordModel alloc] init];
        //        _record.chapterModel = charpter.firstObject;
        _record.chapterCount = _chapters.count;
        _marksRecord = [NSMutableDictionary dictionary];
        _type = ReaderTxt;
        _novelInfo = novelInfo;
    }
    return self;
}
-(instancetype)initWithePub:(NSString *)ePubPath;
{
    self = [super init];
    if (self) {
        _chapters = [LSYReadUtilites ePubFileHandle:ePubPath];
        _marks = [NSMutableArray array];
        _record = [[LSYRecordModel alloc] init];
        _record.chapterModel = _chapters.firstObject;
        _record.chapterCount = _chapters.count;
        _marksRecord = [NSMutableDictionary dictionary];
        _type = ReaderEpub;
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.content forKey:@"content"];
    [aCoder encodeObject:self.marks forKey:@"marks"];
    [aCoder encodeObject:self.chapters forKey:@"chapters"];
    [aCoder encodeObject:self.record forKey:@"record"];
    [aCoder encodeObject:self.resource forKey:@"resource"];
    [aCoder encodeObject:self.marksRecord forKey:@"marksRecord"];
    [aCoder encodeObject:@(self.type) forKey:@"type"];
    [aCoder encodeObject:self.novelInfo forKey:@"novelInfo"];
    [aCoder encodeObject:self.bookID forKey:@"bookID"];
    [aCoder encodeObject:@(self.downloadedNumber) forKey:@"downloadedNumber"];
    
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.content = [aDecoder decodeObjectForKey:@"content"];
        self.marks = [aDecoder decodeObjectForKey:@"marks"];
        self.chapters = [aDecoder decodeObjectForKey:@"chapters"];
        self.record = [aDecoder decodeObjectForKey:@"record"];
        self.resource = [aDecoder decodeObjectForKey:@"resource"];
        self.marksRecord = [aDecoder decodeObjectForKey:@"marksRecord"];
        self.novelInfo = [aDecoder decodeObjectForKey:@"novelInfo"];
        self.type = [[aDecoder decodeObjectForKey:@"type"] integerValue];
        self.bookID = [aDecoder decodeObjectForKey:@"bookID"];
        self.downloadedNumber = [[aDecoder decodeObjectForKey:@"downloadedNumber"] integerValue];
    }
    return self;
}

+(void)delLocalModelWithNovel:(ZY_NovelInfo *)novelInfo
{
    //[[ZY_DBHelper shareDBHelper]removeNovelCacheWithNovelId:novelInfo.novelId];
    NSString *key = [NSString stringWithFormat:@"%@%@",novelInfo.novelId,novelInfo.novelSource];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

+(void)updateLocalModel:(LSYReadModel *)readModel
{
    //[[ZY_DBHelper shareDBHelper] updateLocalNovelInfo:readModel withNovelID:readModel.novelInfo.novelId];
    NSString *key = [NSString stringWithFormat:@"%@%@",readModel.novelInfo.novelId,readModel.novelInfo.novelSource];
    NSMutableData *data = [[NSMutableData alloc]init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
    [archiver encodeObject:readModel forKey:key];
    [archiver finishEncoding];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

+(id)getLocalModelWithNovel:(ZY_NovelInfo *)novelInfo
{
//    NSArray *array = [[ZY_DBHelper shareDBHelper]queryLocalBookInfoListWithNovelId:novelInfo.novelId];
//    if (array.count > 0) {
//        LSYReadModel *model = [array objectAtIndex:0];
//        return model;
//    }else{
//        LSYReadModel *model = [[LSYReadModel alloc] initWithNovel:novelInfo];
//        [[ZY_DBHelper shareDBHelper]addCacheNovels:model withNovelID:novelInfo.novelId];
//        return model;
//    }
    NSString *key = [NSString stringWithFormat:@"%@%@",novelInfo.novelId,novelInfo.novelSource];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if (!data) {
        LSYReadModel *model = [[LSYReadModel alloc] initWithNovel:novelInfo];
        [LSYReadModel updateLocalModel:model];
        return model;
    }
    NSKeyedUnarchiver *unarchive = [[NSKeyedUnarchiver alloc]initForReadingWithData:data];
    //主线程操作
    LSYReadModel *model = [unarchive decodeObjectForKey:key];
    return model;
}
+(NSString *)getReadInfo:(ZY_NovelInfo *)novelInfo{
//    //    NSString *key = [NSString stringWithFormat:@"%@%@",novelInfo.novelId,novelInfo.novelSource];
//    //    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
//    NSArray *array = [[ZY_DBHelper shareDBHelper]queryLocalBookInfoListWithNovelId:novelInfo.novelId];
//    if (!(array.count > 0)) {
//        NSArray *array = [[ZY_DBHelper shareDBHelper] queryProgressWithID:novelInfo.novelId];
//        if (array && array.count > 0) {
//            ZY_NovelProgress *progress = [array objectAtIndex:0];
//            if (![progress.chapterID isEqualToString:@"0"]) {
//                NSInteger read = [progress.chapterIndex integerValue];
//                return [NSString stringWithFormat:@"阅读至第%zd章",(read+1)];
//            }
//
//        }
//
//        return @"尚未阅读";
//    }else{
//        NSArray *array1 = [[ZY_DBHelper shareDBHelper] queryProgressWithID:novelInfo.novelId];
//        if (array1 && array1.count > 0) {
//            ZY_NovelProgress *progress = [array1 objectAtIndex:0];
//            if (![progress.chapterID isEqualToString:@"0"]) {
//                NSInteger read = [progress.chapterIndex integerValue];
//                return [NSString stringWithFormat:@"阅读至第%zd章",(read+1)];
//            }
//
//        }
//
//        //        NSKeyedUnarchiver *unarchive = [[NSKeyedUnarchiver alloc]initForReadingWithData:data];
//        //        LSYReadModel *model = [unarchive decodeObjectForKey:key];
//        LSYReadModel *model = [array objectAtIndex:0];
//        return [NSString stringWithFormat:@"阅读至第%zd章",(model.record.chapter+1)];
//
//    }
    NSString *key = [NSString stringWithFormat:@"%@%@",novelInfo.novelId,novelInfo.novelSource];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if (!data) {
        NSArray *array = [[ZY_DBHelper shareDBHelper] queryProgressWithID:novelInfo.novelId];
        if (array && array.count > 0) {
            ZY_NovelProgress *progress = [array objectAtIndex:0];
            if (![progress.chapterID isEqualToString:@"0"]) {
                NSInteger read = [progress.chapterIndex integerValue];
                return [NSString stringWithFormat:@"读至第%ld章",(long)(read+1)];
            }
            
        }
        
        return @"尚未阅读";
    }else{
        NSArray *array = [[ZY_DBHelper shareDBHelper] queryProgressWithID:novelInfo.novelId];
        if (array && array.count > 0) {
            ZY_NovelProgress *progress = [array objectAtIndex:0];
            if (![progress.chapterID isEqualToString:@"0"]) {
                NSInteger read = [progress.chapterIndex integerValue];
                return [NSString stringWithFormat:@"读至第%ld章",(long)(read+1)];
            }
        }
        
        NSKeyedUnarchiver *unarchive = [[NSKeyedUnarchiver alloc]initForReadingWithData:data];
        LSYReadModel *model = [unarchive decodeObjectForKey:key];
        return [NSString stringWithFormat:@"读至第%ld章",(long)(model.record.chapter+1)];
        
    }
}
@end

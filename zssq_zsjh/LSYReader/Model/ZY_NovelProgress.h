//
//  ZY_NovelProgress.h
//  ZYNovel
//
//

#import <Foundation/Foundation.h>

@interface ZY_NovelProgress : NSObject

@property (nonatomic,strong) NSString *bookID;

@property (nonatomic,strong) NSString *chapterID;

@property (nonatomic,strong) NSString *chapterIndex;

@end

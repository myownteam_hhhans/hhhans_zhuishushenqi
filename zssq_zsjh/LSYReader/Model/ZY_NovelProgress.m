//
//  ZY_NovelProgress.m
//  ZYNovel
//

//

#import "ZY_NovelProgress.h"
#import "NSString+Combine.h"

@implementation ZY_NovelProgress

-(NSString *)bookID{
    
    return [[NSString filterHTML:_bookID] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString *)chapterID{
    
    return [[NSString filterHTML:_chapterID] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString *)chapterIndex{
    
    return [[NSString filterHTML:_chapterIndex] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.bookID forKey:@"bookID"];
    [aCoder encodeObject:self.chapterID forKey:@"chapterID"];
    [aCoder encodeObject:self.chapterIndex forKey:@"chapterIndex"];
    
    
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.bookID = [aDecoder decodeObjectForKey:@"bookID"];
        self.chapterID = [aDecoder decodeObjectForKey:@"chapterID"];
        self.chapterIndex = [aDecoder decodeObjectForKey:@"chapterIndex"];
        
    }
    return self;
}
@end

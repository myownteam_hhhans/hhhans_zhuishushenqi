//
//  LSYChapterModel.m
//  LSYReader
//
//  Created by Labanotation on 16/5/31.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYChapterModel.h"
#import "LSYReadConfig.h"
#import "LSYReadParser.h"
#import "NSString+HTML.h"
#import "ZY_Common.h"
#import "ZY_ADHelper.h"
//#include <vector>
@interface LSYChapterModel ()

@end

@implementation LSYChapterModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        _pageArray = [NSMutableArray array];
    }
    return self;
}
+(id)chapterWithEpub:(NSString *)chapterpath title:(NSString *)title imagePath:(NSString *)path
{
    LSYChapterModel *model = [[LSYChapterModel alloc] init];
    model.title = title;
    model.epubImagePath = path;
    model.type = ReaderEpub;
    model.chapterpath = chapterpath;
    NSString* html = [[NSString alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL fileURLWithPath:chapterpath]] encoding:NSUTF8StringEncoding];
    model.html = html;
    model.content = [html stringByConvertingHTMLToPlainText];
    [model parserEpubToDictionary];
    [model paginateEpubWithBounds:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing)];
    return model;
}

-(void)parserEpubToDictionary
{
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *imageArray = [NSMutableArray array];
    NSScanner *scanner = [NSScanner scannerWithString:self.content];
    NSMutableString *newString = [[NSMutableString alloc] init];
    while (![scanner isAtEnd]) {
        if ([scanner scanString:@"<img>" intoString:NULL]) {
            NSString *img;
            [scanner scanUpToString:@"</img>" intoString:&img];
            NSString *imageString = [self.epubImagePath stringByAppendingPathComponent:img];
            UIImage *image = [UIImage imageWithContentsOfFile:imageString];
            CGSize size = CGSizeMake((ScreenSize.width-LeftSpacing-RightSpacing), (ScreenSize.width-LeftSpacing-RightSpacing)/(ScreenSize.height-TopSpacing-BottomSpacing)*image.size.width);
            if (size.height>(ScreenSize.height-TopSpacing-BottomSpacing-20)) {
                size.height = ScreenSize.height-TopSpacing-BottomSpacing-20
                ;
            }
            [array addObject:@{@"type":@"img",@"content":imageString?imageString:@"",@"width":@(size.width),@"height":@(size.height)}];
            //存储图片信息
            LSYImageData *imageData = [[LSYImageData alloc] init];
            imageData.url = imageString?imageString:@"";
            imageData.position = newString.length;
//            imageData.imageRect = CGRectMake(0, 0, size.width, size.height);
            [imageArray addObject:imageData];
            
//            [newString appendString:@" "];
            [scanner scanString:@"</img>" intoString:NULL];
        }
        else{
            NSString *content;
            if ([scanner scanUpToString:@"<img>" intoString:&content]) {
                [array addObject:@{@"type":@"txt",@"content":content?content:@""}];
                [newString appendString:content?content:@""];
            }
        }
    }
    self.epubContent = [array copy];
    self.imageArray = [imageArray copy];
//    self.content = [newString copy];
}
-(void)paginateEpubWithBounds:(CGRect)bounds
{
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] init];
    for (NSDictionary *dic in _epubContent) {
        if ([dic[@"type"] isEqualToString:@"txt"]) {
            //解析文本
            NSLog(@"--%.2f",[LSYReadConfig shareInstance].fontSize);
            NSDictionary *attr = [LSYReadParser parserAttribute:[LSYReadConfig shareInstance]];
            NSMutableAttributedString *subString = [[NSMutableAttributedString alloc] initWithString:dic[@"content"] attributes:attr];
            [attrString appendAttributedString:subString];
        }
        else if ([dic[@"type"] isEqualToString:@"img"]){
            //解析图片
            NSAttributedString *subString = [LSYReadParser parserEpubImageWithSize:dic config:[LSYReadConfig shareInstance]];
            [attrString appendAttributedString:subString];

        }
    }    
    CTFramesetterRef setterRef = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attrString);
    CGPathRef pathRef = CGPathCreateWithRect(bounds, NULL);
    CTFrameRef frameRef = CTFramesetterCreateFrame(setterRef, CFRangeMake(0, 0), pathRef, NULL);
    CFRange rang1 = CTFrameGetVisibleStringRange(frameRef);
    CFRange rang2 = CTFrameGetStringRange(frameRef);
    
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *stringArr = [NSMutableArray array];
    if (rang1.length+rang1.location == rang2.location+rang2.length) {
        CTFrameRef subFrameRef = CTFramesetterCreateFrame(setterRef, CFRangeMake(rang1.location,0), pathRef, NULL);
        CFRange range = CTFrameGetVisibleStringRange(subFrameRef);
        rang1 = CFRangeMake(range.location+range.length, 0);
        [array addObject:(__bridge id)subFrameRef];
        [stringArr addObject:[[attrString string] substringWithRange:NSMakeRange(range.location, range.length)]];
        
        CFRelease(subFrameRef);
    }
    else{
        while (rang1.length+rang1.location<rang2.location+rang2.length) {
            CTFrameRef subFrameRef = CTFramesetterCreateFrame(setterRef, CFRangeMake(rang1.location,0), pathRef, NULL);
            CFRange range = CTFrameGetVisibleStringRange(subFrameRef);
            rang1 = CFRangeMake(range.location+range.length, 0);
            [array addObject:(__bridge id)subFrameRef];
            [stringArr addObject:[[attrString string] substringWithRange:NSMakeRange(range.location, range.length)]];
            CFRelease(subFrameRef);
            
        }
    }
    
    CFRelease(setterRef);
    CFRelease(pathRef);
    _epubframeRef = [array copy];
    _epubString = [stringArr copy];
    _pageCount = _epubframeRef.count;
    _content = attrString.string;
}

/////////////////////////txt处理开始
-(void)setContent:(NSString *)content
{
    if (content && ![content isEqualToString:@""]) {
        _content = content;
        if (_type == ReaderTxt) {
            if (isIPhoneX) {
                [self paginateWithBounds:CGRectMake(LeftSpacing, TopSpacing, [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing-34-44)];
            }else{
                [self paginateWithBounds:CGRectMake(LeftSpacing, TopSpacing, [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing)];
            }
        }
    }
}
-(void)updateFont
{
    if (_type==ReaderEpub) {
        [self paginateEpubWithBounds:CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing)];
    }
    else{
        if (isIPhoneX) {
            [self paginateWithBounds:CGRectMake(LeftSpacing, TopSpacing, [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing-34-44)];
        }else{
            [self paginateWithBounds:CGRectMake(LeftSpacing, TopSpacing , [UIScreen mainScreen].bounds.size.width-LeftSpacing-RightSpacing, [UIScreen mainScreen].bounds.size.height-TopSpacing-BottomSpacing)];
        }
    }
    
}



//核心函数，处理内容分页
-(void)paginateWithBounds:(CGRect)bounds
{
    if (!self.content || [self.content isEqualToString:@""]) {
        return;
    }
    
    
    bounds.size.height -= [[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookbanner"] ? 47 : 0;
    
    [_pageArray removeAllObjects];
    NSAttributedString *attrString;
    CTFramesetterRef frameSetter;
    CGPathRef path;
    NSMutableAttributedString *attrStr;
    NSDictionary *attribute = [LSYReadParser parserAttribute:[LSYReadConfig shareInstance]];
    attrStr = [[NSMutableAttributedString alloc] initWithString:self.content attributes:attribute];
    
    //大标题
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[NSForegroundColorAttributeName] = [UIColor blackColor];
    dict[NSFontAttributeName] = [UIFont systemFontOfSize:[LSYReadConfig shareInstance].fontSize*1.4];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = [LSYReadConfig shareInstance].fontSize*1.4*[LSYReadConfig shareInstance].spaceNum;
    paragraphStyle.paragraphSpacingBefore = [LSYReadConfig shareInstance].fontSize*1.4*2;
    paragraphStyle.alignment = NSTextAlignmentJustified;
    dict[NSParagraphStyleAttributeName] = paragraphStyle;
    
    if (attrStr.length > self.title.length) {
        [attrStr addAttributes:dict range:NSMakeRange(0, self.title.length)];
    }
    
    
    //end
    
    attrString = [attrStr copy];
    frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef) attrString);
    path = CGPathCreateWithRect(bounds, NULL);
    int currentOffset = 0;
    CFRange range = CFRangeMake(0, 0);
    
    do {
        CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(currentOffset, 0), path, NULL);
        range = CTFrameGetVisibleStringRange(frame);
        currentOffset += range.length;
        [_pageArray addObject:@(range.location)];
        if (frame) {
            CFRelease(frame);
        }
    } while (range.location + range.length < attrStr.length);

    
    CGPathRelease(path);
    CFRelease(frameSetter);
    _pageCount = _pageArray.count;
}

-(BOOL)isShowAD:(NSInteger)index adImageHeight:(CGFloat)height{
    BOOL isShowAD = NO;
    if (index == _pageCount-1) { //判断章节最后一页
        NSUInteger pos = _content.length - [_pageArray[index] integerValue];//判断最后一页是否够空间展示广告
        NSLog(@"pos%ld",pos);

        if (pos < height+1 +20 && height >0) {
            isShowAD = YES;
        }
    }
    return isShowAD;
}

-(NSString *)stringOfPage:(NSUInteger)index
{
    if (_pageArray.count == 0 || _pageArray.count <= index) {
        return @"";
    }
    NSUInteger local = [_pageArray[index] integerValue];
    NSUInteger length;
    if (index<self.pageCount-1) {
        length=  [_pageArray[index+1] integerValue] - [_pageArray[index] integerValue];
    }
    else{
        length = _content.length - [_pageArray[index] integerValue];
    }
    return [_content substringWithRange:NSMakeRange(local, length)];
}
-(id)copyWithZone:(NSZone *)zone
{
    LSYChapterModel *model = [[LSYChapterModel allocWithZone:zone] init];
    model.content = self.content;
    model.ChapterUrl = self.ChapterUrl;
    model.title = self.title;
    model.pageCount = self.pageCount;
    model.pageArray = self.pageArray;
    model.epubImagePath = self.epubImagePath;
    model.type = self.type;
    model.epubString = self.epubString;
    return model;
    
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.content forKey:@"content"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.ChapterUrl forKey:@"ChapterUrl"];
    [aCoder encodeInteger:self.pageCount forKey:@"pageCount"];
    [aCoder encodeObject:self.pageArray forKey:@"pageArray"];
    [aCoder encodeObject:self.epubImagePath forKey:@"epubImagePath"];
    [aCoder encodeObject:@(self.type) forKey:@"type"];
    [aCoder encodeObject:self.epubContent forKey:@"epubContent"];
    [aCoder encodeObject:self.chapterpath forKey:@"chapterpath"];
    [aCoder encodeObject:self.html forKey:@"html"];
    [aCoder encodeObject:self.epubString forKey:@"epubString"];
    
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _content = [aDecoder decodeObjectForKey:@"content"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.ChapterUrl = [aDecoder decodeObjectForKey:@"ChapterUrl"];
        self.pageCount = [aDecoder decodeIntegerForKey:@"pageCount"];
        self.pageArray = [aDecoder decodeObjectForKey:@"pageArray"];
        self.type = [[aDecoder decodeObjectForKey:@"type"] integerValue];
        self.epubImagePath = [aDecoder decodeObjectForKey:@"epubImagePath"];
        self.epubContent = [aDecoder decodeObjectForKey:@"epubContent"];
        self.chapterpath = [aDecoder decodeObjectForKey:@"chapterpath"];
        self.html = [aDecoder decodeObjectForKey:@"html"];
        self.epubString = [aDecoder decodeObjectForKey:@"epubString"];
//        self.epubframeRef = [aDecoder decodeObjectForKey:@"epubframeRef"];
        
    }
    return self;
}
@end

@implementation LSYImageData

@end

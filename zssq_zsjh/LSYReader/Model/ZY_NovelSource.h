//
//  ZY_NovelSource.h
//  ZYNovel
//
//

#import <Foundation/Foundation.h>

@interface ZY_NovelSource : NSObject

// 原来O用的
@property (nonatomic,strong) NSString* RegRule;
@property (nonatomic,strong) NSString* ReplaceValue;
@property (nonatomic,strong) NSString* Replacement;

// zssq换源
@property (nonatomic,strong) NSString* Url;
@property (nonatomic,strong) NSString* WebName;
@property (nonatomic,strong) NSString* bookID;
@property (nonatomic,strong) NSString* lastChapter;
@property (nonatomic,strong) NSString* time;

@end

//
//  LSYReadModel.h
//  LSYReader
//
//  Created by Labanotation on 16/5/31.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LSYMarkModel.h"
#import "LSYChapterModel.h"
#import "LSYRecordModel.h"
#import "ZY_NovelInfo.h"

@interface LSYReadModel : NSObject<NSCoding>
@property (nonatomic,strong) NSURL *resource;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,assign) ReaderType type;
@property (nonatomic,copy) NSString *bookID;
@property (nonatomic,copy) NSString *chapterID;

@property (nonatomic,strong) NSMutableArray <LSYMarkModel *>*marks;
@property (nonatomic,strong) NSMutableArray <LSYChapterModel *>*chapters;
@property (nonatomic,strong) NSMutableDictionary *marksRecord;
@property (nonatomic,strong) LSYRecordModel *record;
@property (nonatomic,strong) ZY_NovelInfo *novelInfo;
@property (nonatomic, assign) NSInteger downloadedNumber;

-(instancetype)initWithNovel:(ZY_NovelInfo *)novelInfo;
//删除本地阅读记录
+(void)delLocalModelWithNovel:(ZY_NovelInfo *)novelInfo;
+(void)updateLocalModel:(LSYReadModel *)readModel;
+(id)getLocalModelWithNovel:(ZY_NovelInfo *)novelInfo;
+(NSString *)getReadInfo:(ZY_NovelInfo *)novelInfo;
@end

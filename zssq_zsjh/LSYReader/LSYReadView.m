//
//  LSYReadView.m
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYReadView.h"
#import "LSYReadConfig.h"
#import "LSYReadViewController.h"
#import "LSYMagnifierView.h"
#import "SDWebImagePrefetcher.h"
#import "SDWebImageManager.h"
#import <MTGSDK/MTGSDK.h>
#import "MTGNativeVideoView.h"
#import "Common.h"
#import <BUAdSDK/BUAdSDK.h>
#import "GDTNativeAd.h"


@interface ImageModel ()
@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong) NSString *url; //图片链接
@property (nonatomic,assign) CGRect imageRect;  //图片位置
@property (nonatomic,assign) NSInteger position;
@end

@implementation ImageModel
@end

@interface LSYReadView () <BUBannerAdViewDelegate>

@property (nonatomic,strong) LSYMagnifierView *magnifierView;
@property (nonatomic,strong) NSMutableArray *imageInfoArray;

@end

@implementation LSYReadView
{
    NSRange _selectRange;
    NSRange _calRange;
    NSArray *_pathArray;
    
    UIPanGestureRecognizer *_pan;
    //滑动手势有效区间
    CGRect _leftRect;
    CGRect _rightRect;
    
    CGRect _menuRect;
    //是否进入选择状态
    BOOL _selectState;
    BOOL _direction; //滑动方向  (0---左侧滑动 1 ---右侧滑动)
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        
        [self addGestureRecognizer:({
            UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
            pan.enabled = NO;
            _pan = pan;
            pan;
        })];

    }
    return self;
}
#pragma mark - Magnifier View
-(void)showMagnifier
{
    if (!_magnifierView) {
        self.magnifierView = [[LSYMagnifierView alloc] init];
        self.magnifierView.readView = self;
        [self addSubview:self.magnifierView];
    }
}
-(void)hiddenMagnifier
{
    if (_magnifierView) {
        [self.magnifierView removeFromSuperview];
        self.magnifierView = nil;
    }
}
#pragma -mark Gesture Recognizer

-(void)pan:(UIPanGestureRecognizer *)pan
{
    
    CGPoint point = [pan locationInView:self];
    
    if (pan.state == UIGestureRecognizerStateBegan || pan.state == UIGestureRecognizerStateChanged) {
        [self showMagnifier];
        self.magnifierView.touchPoint = point;
        if (CGRectContainsPoint(_rightRect, point)||CGRectContainsPoint(_leftRect, point)) {
            if (CGRectContainsPoint(_leftRect, point)) {
                _direction = NO;   //从左侧滑动
            }
            else{
                _direction=  YES;    //从右侧滑动
            }
            _selectState = YES;
        }
        if (_selectState) {
            
            NSArray *path = [LSYReadParser parserRectsWithPoint:point range:&_selectRange frameRef:_frameRef paths:_pathArray direction:_direction];
            _pathArray = path;
            [self setNeedsDisplay];
            
        }
        
    }
    if (pan.state == UIGestureRecognizerStateEnded) {
        [self hiddenMagnifier];
        _selectState = NO;
    }
    
}

#pragma mark - Privite Method
#pragma mark  Draw Selected Path
-(void)drawSelectedPath:(NSArray *)array LeftDot:(CGRect *)leftDot RightDot:(CGRect *)rightDot{
    if (!array.count) {
        _pan.enabled = NO;
        if ([self.delegate respondsToSelector:@selector(readViewEndEdit:)]) {
            [self.delegate readViewEndEdit:nil];
        }
        return;
    }
    if ([self.delegate respondsToSelector:@selector(readViewEditeding:)]) {
        [self.delegate readViewEditeding:nil];
    }
    _pan.enabled = YES;
    CGMutablePathRef _path = CGPathCreateMutable();
    [[UIColor cyanColor]setFill];
    for (int i = 0; i < [array count]; i++) {
        CGRect rect = CGRectFromString([array objectAtIndex:i]);
        CGPathAddRect(_path, NULL, rect);
        if (i == 0) {
            *leftDot = rect;
            _menuRect = rect;
        }
        if (i == [array count]-1) {
            *rightDot = rect;
        }
        
    }
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextAddPath(ctx, _path);
    CGContextFillPath(ctx);
    CGPathRelease(_path);
    
}
-(void)drawDotWithLeft:(CGRect)Left right:(CGRect)right
{
    if (CGRectEqualToRect(CGRectZero, Left) || (CGRectEqualToRect(CGRectZero, right))){
        return;
    }
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGMutablePathRef _path = CGPathCreateMutable();
    [[UIColor orangeColor] setFill];
    CGPathAddRect(_path, NULL, CGRectMake(CGRectGetMinX(Left)-2, CGRectGetMinY(Left),2, CGRectGetHeight(Left)));
    CGPathAddRect(_path, NULL, CGRectMake(CGRectGetMaxX(right), CGRectGetMinY(right),2, CGRectGetHeight(right)));
    CGContextAddPath(ctx, _path);
    CGContextFillPath(ctx);
    CGPathRelease(_path);
    CGFloat dotSize = 15;
    _leftRect = CGRectMake(CGRectGetMinX(Left)-dotSize/2-10, ViewSize(self).height-(CGRectGetMaxY(Left)-dotSize/2-10)-(dotSize+20), dotSize+20, dotSize+20);
    _rightRect = CGRectMake(CGRectGetMaxX(right)-dotSize/2-10,ViewSize(self).height- (CGRectGetMinY(right)-dotSize/2-10)-(dotSize+20), dotSize+20, dotSize+20);
    CGContextDrawImage(ctx,CGRectMake(CGRectGetMinX(Left)-dotSize/2, CGRectGetMaxY(Left)-dotSize/2, dotSize, dotSize),[UIImage imageNamed:@"r_drag-dot"].CGImage);
    CGContextDrawImage(ctx,CGRectMake(CGRectGetMaxX(right)-dotSize/2, CGRectGetMinY(right)-dotSize/2, dotSize, dotSize),[UIImage imageNamed:@"r_drag-dot"].CGImage);
}
#pragma mark - Privite Method
#pragma mark Cancel Draw
-(void)cancelSelected
{
    if (_pathArray) {
        _pathArray = nil;
        [self setNeedsDisplay];
    }
}

-(void)setFrameRef:(CTFrameRef)frameRef
{
    if (_frameRef != frameRef) {
        if (_frameRef) {
            CFRelease(_frameRef);
            _frameRef = nil;
        }
        _frameRef = frameRef;
    }
}
-(void)dealloc
{
    if (_frameRef) {
        CFRelease(_frameRef);
        _frameRef = nil;
    }
}
-(void)setContent:(NSString *)content
{
    if(![content isEqualToString:@""])
    {
        _content = content;
        [self setNeedsDisplay];
    }
}

-(NSMutableArray *)imageInfoArray{
    if (!_imageInfoArray) {
        _imageInfoArray = [[NSMutableArray alloc] init];
    }
    return _imageInfoArray;
}

-(void)viewWillAppear {
    
    
    
    if (self.imageArray.count >0) {
        NSMutableArray *iconImageArray = [[NSMutableArray alloc] init];

        __weak LSYReadView *weakSelf = self;
        for (LSYImageData * imageData  in self.imageArray) {
            NSURL *url = [NSURL URLWithString:imageData.url];
            [iconImageArray addObject:url];
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imageData.url ]  options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                if (finished) {
                    
                    for (LSYImageData * imageData  in self.imageArray) {
                        if([imageData.url isEqualToString:imageURL.absoluteString]){
                            ImageModel *imgmodel = [[ImageModel alloc] init];
                            imgmodel.url = imageData.url;
                            imgmodel.position = imageData.position;
                            imgmodel.imageRect = imageData.imageRect;
                            imgmodel.image = image;
                            [weakSelf.imageInfoArray addObject:imgmodel];
                            [weakSelf setNeedsDisplay];
                            break;
                        }
                    }
                }
            }];
        }
    }
}

- (void)bannerAdViewDidClick:(BUBannerAdView *)bannerAdView WithAdmodel:(BUNativeAd *)nativeAd {
   
}

-(void)drawRect:(CGRect)rect
{
    if (!_frameRef) {
        return;
    }

    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetTextMatrix(ctx, CGAffineTransformIdentity);
    CGContextTranslateCTM(ctx, 0, self.bounds.size.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    CGRect leftDot,rightDot = CGRectZero;
    _menuRect = CGRectZero;
    [self drawSelectedPath:_pathArray LeftDot:&leftDot RightDot:&rightDot];
    CTFrameDraw(_frameRef, ctx);
    //    [self fillImagePosition];
    if (_imageArray.count) {
        for (ImageModel * imageData in self.imageInfoArray) {
            
            if (imageData.imageRect.origin.x == 0 && imageData.imageRect.origin.y == 0) {
                CGSize size =  [self measureFrame:_frameRef];
                
                if (rect.size.height - size.height <= imageData.imageRect.size.height) {
                    self.isFinishAd = NO;
                    continue;
                }
            }
            
            UIImage *image = imageData.image;
            
            if (image) {
                
                if (imageData.imageRect.origin.x == 0&&imageData.imageRect.origin.y == 0) {
                    CGSize size =  [self measureFrame:_frameRef];
                    
                    if (rect.size.height - size.height > imageData.imageRect.size.height) {
                        
                        self.isFinishAd = YES;
                    }
                }
                
                
                CFRange range = CTFrameGetVisibleStringRange(_frameRef);
                
                if (image&&(range.location<=imageData.position&&imageData.position<=(range.length + range.location))) {
                    
                    [self fillImagePosition:imageData];
                    
                    if ([[ZY_ADHelper shareADHelper].bottomADType isEqualToString:@"5"]) {
                        // 展示汇报
                        NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiGif;
                        [[ZY_ADHelper shareADHelper] upLoadLomiShowWithURL:kaipingDic[@"count_url"]];
                       
                    } else if ([[ZY_ADHelper shareADHelper].bottomADType isEqualToString:@"3"]) {
                       
                    }
                    
                    if (imageData.position==(range.length + range.location)) {
                        if ([self showImage]) {
                            CGContextDrawImage(ctx, imageData.imageRect, image.CGImage);
                        }
                        else{
                            
                        }
                    }
                    else{
                        CGContextDrawImage(ctx, imageData.imageRect, image.CGImage);
                    }
                    
                    if (self.isFinishAd && imageData.imageRect.origin.x == 0 && imageData.imageRect.origin.y == 0) {
                        
                        UIImage *guanggaoImage = [UIImage imageNamed:@"YD广告"];
                        CGContextDrawImage(ctx, CGRectMake(CGRectGetMaxX(imageData.imageRect) -25, CGRectGetMaxY(imageData.imageRect) -15, 22, 13) , guanggaoImage.CGImage);
                        if ([self.adModel.data isKindOfClass:[ZY_AdEntity class]]) {
                            [[ZY_Common shareCommon] advertisingDisplayReportWithModel:self.adModel.data];
                        }
                        else if ([self.adModel.data isKindOfClass:[GDTNativeAdData class]]) {
                        
                            // 点击事件上报
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"bottomADShow" object:self];
                            //[[ZY_ADHelper shareADHelper].nativeAd  attachAd:self.adModel.data toView:self];
                        }
                    }
                }
            }
        }
    }
    
    [self drawDotWithLeft:leftDot right:rightDot];
}

- (CGSize)measureFrame:(CTFrameRef)frame
{
    CGPathRef framePath = CTFrameGetPath(frame);
    CGRect frameRect = CGPathGetBoundingBox(framePath);
    CFArrayRef lines = CTFrameGetLines(frame);
    CFIndex numLines = CFArrayGetCount(lines);
    CGFloat maxWidth = 0;
    CGFloat textHeight = 0;
    CFIndex lastLineIndex = numLines - 1;
    
    for(CFIndex index = 0; index < numLines; index++)
    {
        CGFloat ascent, descent, leading, width;
        CTLineRef line = (CTLineRef) CFArrayGetValueAtIndex(lines, index);
        width = CTLineGetTypographicBounds(line, &ascent,  &descent, &leading);
        if (width > maxWidth) { maxWidth = width; }
        if (index == lastLineIndex) {
            CGPoint lastLineOrigin;
            CTFrameGetLineOrigins(frame, CFRangeMake(lastLineIndex, 1), &lastLineOrigin);
            textHeight =  CGRectGetMaxY(frameRect) - lastLineOrigin.y + descent;
        }
    }
    return CGSizeMake(ceil(maxWidth), ceil(textHeight));
}

-(BOOL)showImage
{
    NSArray *lines = (NSArray *)CTFrameGetLines(self.frameRef);
    NSUInteger lineCount = [lines count];
    CGPoint lineOrigins[lineCount];
    CTFrameGetLineOrigins(self.frameRef, CFRangeMake(0, 0), lineOrigins);
    
    CTLineRef line = (__bridge CTLineRef)lines[lineCount-1];
    
    NSArray * runObjArray = (NSArray *)CTLineGetGlyphRuns(line);
    CTRunRef run = (__bridge CTRunRef)runObjArray.lastObject;
    NSDictionary *runAttributes = (NSDictionary *)CTRunGetAttributes(run);
    CTRunDelegateRef delegate = (__bridge CTRunDelegateRef)[runAttributes valueForKey:(id)kCTRunDelegateAttributeName];
    if (delegate == nil) {
        return NO;
    }
    else{
        return YES;
    }
}
- (void)fillImagePosition:(ImageModel *)imageData;
{
    if (self.imageArray.count == 0) {
        return;
    }
    NSArray *lines = (NSArray *)CTFrameGetLines(self.frameRef);
    NSUInteger lineCount = [lines count];
    CGPoint lineOrigins[lineCount];
    CTFrameGetLineOrigins(self.frameRef, CFRangeMake(0, 0), lineOrigins);
    for (int i = 0; i < lineCount; ++i) {
        if (imageData == nil) {
            break;
        }
        CTLineRef line = (__bridge CTLineRef)lines[i];
        NSArray * runObjArray = (NSArray *)CTLineGetGlyphRuns(line);
        for (id runObj in runObjArray) {
            CTRunRef run = (__bridge CTRunRef)runObj;
            NSDictionary *runAttributes = (NSDictionary *)CTRunGetAttributes(run);
            CTRunDelegateRef delegate = (__bridge CTRunDelegateRef)[runAttributes valueForKey:(id)kCTRunDelegateAttributeName];
            if (delegate == nil) {
                continue;
            }
            
            NSDictionary * metaDic = CTRunDelegateGetRefCon(delegate);
            if (![metaDic isKindOfClass:[NSDictionary class]]) {
                continue;
            }
            
            CGRect runBounds;
            CGFloat ascent;
            CGFloat descent;
            runBounds.size.width = CTRunGetTypographicBounds(run, CFRangeMake(0, 0), &ascent, &descent, NULL);
            runBounds.size.height = ascent + descent;
            
            CGFloat xOffset = CTLineGetOffsetForStringIndex(line, CTRunGetStringRange(run).location, NULL);
            runBounds.origin.x = lineOrigins[i].x + xOffset;
            runBounds.origin.y = lineOrigins[i].y;
            runBounds.origin.y -= descent;
            
            CGPathRef pathRef = CTFrameGetPath(self.frameRef);
            CGRect colRect = CGPathGetBoundingBox(pathRef);
            
            CGRect delegateBounds = CGRectOffset(runBounds, colRect.origin.x, colRect.origin.y);
            NSLog(@"delegateBounds %@",NSStringFromCGRect(delegateBounds));
            imageData.imageRect = delegateBounds;
            break;
        }
    }
}

@end

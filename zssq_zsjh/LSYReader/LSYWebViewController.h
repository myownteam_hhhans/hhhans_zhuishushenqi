//
//  LSYWebViewController.h
//  ZYNovel
////

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface LSYWebViewController : UIViewController<WKNavigationDelegate>

@property(nonatomic,strong) NSString *linkUrl;
@end

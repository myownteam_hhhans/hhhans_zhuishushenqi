//
//  LSYReadConfig.h
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LSYReadConfig : NSObject<NSCoding>
+(instancetype)shareInstance;
@property (nonatomic) BOOL isNightMode;
@property (nonatomic) CGFloat fontSize;
@property (nonatomic) CGFloat lineSpace;
@property (nonatomic) CGFloat spaceNum;

@property (nonatomic,strong) UIColor *fontColor;
@property (nonatomic) int theme;
@property (nonatomic) int readMode;
@property (nonatomic) float light;

-(UIColor *)getBgColor;
-(UIColor *)getColorByTheme:(int)t;
-(NSMutableArray *)getThemeArray;
@end

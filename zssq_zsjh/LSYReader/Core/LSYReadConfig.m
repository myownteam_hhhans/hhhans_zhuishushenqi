//
//  LSYReadConfig.m
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYReadConfig.h"

@implementation LSYReadConfig

static NSMutableArray *arr_ThemeList;

+(instancetype)shareInstance
{
    static LSYReadConfig *readConfig = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        readConfig = [[self alloc] init];
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"ThemeList" ofType:@"plist"];
        arr_ThemeList = [[NSMutableArray alloc] initWithContentsOfFile:plistPath];
    });
    return readConfig;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"ReadConfig"];
        if (data) {
            NSKeyedUnarchiver *unarchive = [[NSKeyedUnarchiver alloc]initForReadingWithData:data];
            LSYReadConfig *config = [unarchive decodeObjectForKey:@"ReadConfig"];
            [config addObserver:config forKeyPath:@"fontSize" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"lineSpace" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"fontColor" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"theme" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"isNightMode" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"readMode" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"light" options:NSKeyValueObservingOptionNew context:NULL];
            [config addObserver:config forKeyPath:@"spaceNum" options:NSKeyValueObservingOptionNew context:NULL];
            return config;
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            _fontSize = 22.0f;
            _spaceNum = 1.6f;
            _lineSpace = _fontSize * _spaceNum;
        }else{
            _fontSize = 20.0f;
            _spaceNum = 0.8f;

            _lineSpace = _fontSize * _spaceNum;
        }
        _fontColor = RGB(56, 51, 44);
        _theme = 0;
        _light = 0;
        [self addObserver:self forKeyPath:@"fontSize" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"lineSpace" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"fontColor" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"theme" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"isNightMode" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"readMode" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"light" options:NSKeyValueObservingOptionNew context:NULL];
        [self addObserver:self forKeyPath:@"spaceNum" options:NSKeyValueObservingOptionNew context:NULL];
        [LSYReadConfig updateLocalConfig:self];
        
    }
    return self;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    [LSYReadConfig updateLocalConfig:self];
}
+(void)updateLocalConfig:(LSYReadConfig *)config
{
    NSMutableData *data=[[NSMutableData alloc]init];
    NSKeyedArchiver *archiver=[[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
    [archiver encodeObject:config forKey:@"ReadConfig"];
    [archiver finishEncoding];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"ReadConfig"];
}
-(NSMutableArray *)getThemeArray{
    return arr_ThemeList;
}
-(UIColor *)getBgColor{
    return [self getColorByTheme:_theme];
}
-(UIColor *)getColorByTheme:(int)t{
    if (arr_ThemeList.count > t) {
        NSArray *arr = [arr_ThemeList objectAtIndex:t];
        NSString *bgImgName = [arr objectAtIndex:0];//背景图名字
        NSString *bgColor = [arr objectAtIndex:2];//背景图颜色
        if (![bgImgName isEqualToString:@""]) {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                NSString *tmp_bgImgName = [bgImgName stringByReplacingOccurrencesOfString:@".jpg" withString:@""];
                tmp_bgImgName = [tmp_bgImgName stringByAppendingString:@"-ipad.jpg"];
                if([UIImage imageNamed:tmp_bgImgName]){
                    bgImgName = tmp_bgImgName;
                }
            }
            else{
                 BOOL isIphoneX = ([UIScreen mainScreen].bounds.size.height >= 812);
                if (isIphoneX) {
                    NSString *tmp_bgImgName = [bgImgName stringByReplacingOccurrencesOfString:@".jpg" withString:@"-X.jpg"];
                    if([UIImage imageNamed:tmp_bgImgName]){
                        bgImgName = tmp_bgImgName;
                    }
                    
                }
            }
            return [UIColor colorWithPatternImage:[UIImage imageNamed:bgImgName]];
        }else{
            NSArray *arr_color = [bgColor componentsSeparatedByString:@","];
            if (arr_color.count == 3) {
                return RGB([[arr_color objectAtIndex:0] intValue], [[arr_color objectAtIndex:1] intValue], [[arr_color objectAtIndex:2] intValue]);
            }
        }
    }
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"阅读3.jpg"]];
}
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeDouble:self.fontSize forKey:@"fontSize"];
    [aCoder encodeDouble:self.lineSpace forKey:@"lineSpace"];
    [aCoder encodeObject:self.fontColor forKey:@"fontColor"];
    [aCoder encodeInt:self.theme forKey:@"theme"];
    [aCoder encodeInt:self.readMode forKey:@"readMode"];
    [aCoder encodeBool:self.isNightMode forKey:@"isNightMode"];
    [aCoder encodeFloat:self.light forKey:@"light"];
    [aCoder encodeFloat:self.spaceNum forKey:@"spaceNum"];
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.fontSize = [aDecoder decodeDoubleForKey:@"fontSize"];
        self.lineSpace = [aDecoder decodeDoubleForKey:@"lineSpace"];
        self.fontColor = [aDecoder decodeObjectForKey:@"fontColor"];
        self.theme = [aDecoder decodeIntForKey:@"theme"];
        self.readMode = [aDecoder decodeIntForKey:@"readMode"];
        self.isNightMode = [aDecoder decodeBoolForKey:@"isNightMode"];
        self.light = [aDecoder decodeFloatForKey:@"light"];
        self.spaceNum = [aDecoder decodeFloatForKey:@"spaceNum"];
    }
    return self;
}
@end

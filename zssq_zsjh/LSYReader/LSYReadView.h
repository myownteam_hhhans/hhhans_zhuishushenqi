//
//  LSYReadView.h
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSYChapterModel.h"
#import "ZY_ADHelper.h"
#import "ZY_Common.h"

@interface ImageModel :NSObject

@end

@protocol LSYReadViewControllerDelegate;
@interface LSYReadView : UIView
@property (nonatomic,assign) CTFrameRef frameRef;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSArray *imageArray;
@property (nonatomic,strong) id<LSYReadViewControllerDelegate>delegate;
@property (nonatomic,assign) BOOL isFinishAd; //view上加载了广告图片
@property (nonatomic,strong) ADModel *adModel;
@property (nonatomic,strong) NSArray *MADArray;
-(void)cancelSelected;

-(void)viewWillAppear;
@end

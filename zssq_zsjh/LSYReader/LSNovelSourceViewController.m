//
//  LSNovelSourceViewController.m
//  ZYNovel
////

#import "LSNovelSourceViewController.h"
#import "LSYReadUtilites.h"
#import "ZY_NovelSource.h"
#import "SVProgressHUD.h"
#import "ZY_Common.h"
#import "LSYReadPageViewController.h"
#import "ZY_XPathParser.h"
#import "ZY_JsonParser.h"
#import "ZY_NovelDownLoadOperation.h"
#import "ZY_BookHelp.h"

@interface LSNovelSourceViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) UIButton *back;
@property (nonatomic,strong) UITableView *tableView;
@end

@implementation LSNovelSourceViewController
-(UIButton *)back
{
    if (!_back) {
        _back = [LSYReadUtilites commonButtonSEL:@selector(backView) target:self];
        [_back setImage:[UIImage imageNamed:@"阅读器返回"] forState:UIControlStateNormal];
    }
    return _back;
}
-(void)backView{
    [SVProgressHUD dismiss];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.dataArr = [NSMutableArray array];
    ZY_NovelSource *Source = [[ZY_NovelSource alloc]init];
    Source.Url = self.chapterUrl;
    Source.WebName = @"默认源";
    [self.dataArr addObject:Source];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSize.width, JH_Navigation_Height)];
    topView.backgroundColor = RGB(29, 29, 29);
    [self.view addSubview:topView];
    [self.view addSubview:self.tableView];
    [self.tableView setTableHeaderView:[self tableViewHeadView]];
    [topView addSubview:self.back];
    _back.frame = CGRectMake(0, JH_Navigation_Height-40, 40, 40);
    
    UILabel *lbl_title = [[UILabel alloc]initWithFrame:CGRectMake(100, JH_Navigation_Height-40, ScreenSize.width-200, 40)];
    lbl_title.text = @"选择来源";
    lbl_title.textColor = [UIColor whiteColor];
    lbl_title.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:lbl_title];
    
    NSString *url = [NSString stringWithFormat:@"https://api.zhuishushenqi.com/toc?view=summary&book=%@",self.novel.novelId];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSString *tmpContent = [LSYReadUtilites encodeWithURL:[NSURL URLWithString:url]];
            if (tmpContent) {
                NSArray *dict = [NSJSONSerialization JSONObjectWithData:[tmpContent dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                if (dict) {
                    if ( dict.count > 0) {
                        NSArray *arr_data = dict;
                        for (NSDictionary *itemdic in arr_data) {
                            ZY_NovelSource *Source = [[ZY_NovelSource alloc]init];
                            Source.RegRule = [itemdic objectForKey:@"RegRule"];
                            Source.ReplaceValue = [itemdic objectForKey:@"ReplaceValue"];
                            Source.Replacement = [itemdic objectForKey:@"Replacement"];
                            // 追书
                            Source.bookID = [itemdic objectForKey:@"_id"];
                            Source.Url = [itemdic objectForKey:@"link"];
                            Source.WebName = [itemdic objectForKey:@"name"];
                            Source.lastChapter = [itemdic objectForKey:@"lastChapter"];
                            Source.time =[itemdic objectForKey:@"updated"];
                            Source.time = [Source.time substringToIndex:10];
                            [self.dataArr addObject:Source];
                        }
                    }
                }
            }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    });
}
- (UILabel *)tableViewHeadView
{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, self.view.frame.size.width-30, 60)];
    titleLabel.text = @"内容均来源于网络，本阅读器仅提供源网站的结果展示";
    titleLabel.textColor = [UIColor lightGrayColor];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    return titleLabel;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, JH_Navigation_Height,kCScreenWidth, kCScreenHeight-JH_Navigation_Height) style:UITableViewStylePlain];
        if(@available(iOS 11.0, *)) {
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        [_tableView setBackgroundColor:[UIColor whiteColor]];
    }
    return _tableView;
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZY_NovelSource *source = [self.dataArr objectAtIndex:indexPath.row];
    static NSString *identifier = @"identifier";
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!tableViewCell) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    tableViewCell.textLabel.textColor = [UIColor blackColor];
    tableViewCell.textLabel.text = source.WebName;
    if (source.lastChapter && source.lastChapter,source.time) {
        tableViewCell.detailTextLabel.text =  [NSString stringWithFormat:@"%@  %@",source.time,source.lastChapter] ;
    }
    
    tableViewCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return tableViewCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // 取消正在下载的任务
    for (ZY_NovelDownLoadOperation *downOP in [ZY_BookHelp shareBookHelp].novelDownLoadQueue.operations) {
        if (downOP.downloadInfo.readModel == self.pageView.model) {
            downOP.isPaused = NO;
            [downOP cancel];
        }
    }
    
    // 清理cookie
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
    
    ZY_NovelSource *Source = [self.dataArr objectAtIndex:indexPath.row];
    
    [SVProgressHUD showWithStatus:@"正在请求源站"];
    
    if (indexPath.row == 0) {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            BOOL isNeedShowError = YES;
            NSString *chapterURL = [NSString stringWithFormat:@"https://api.zhuishushenqi.com/mix-toc/%@?view=chapters",self.novel.novelId];
            NSString *tmpContent = [LSYReadUtilites encodeWithURL:[NSURL URLWithString:chapterURL]];
            NSData *data = [tmpContent dataUsingEncoding:NSUTF8StringEncoding];
            if (![ZY_Common isBlankString:tmpContent]) {
                // NSString *content = @"";
                NSArray *chapterList = [[[ZY_JsonParser alloc] init] getChapterListByData:data];
                // LSYChapterModel *model = chapterList[0];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetChapterList" object:@[chapterList,chapterURL]];
                
                
                isNeedShowError = NO;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self backView];
                });
            }
        });
        
    } else {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            BOOL isNeedShowError = YES;
            NSString *chapterURL = [NSString stringWithFormat:@"https://api.zhuishushenqi.com/toc/%@?view=chapters",Source.bookID];
            NSString *tmpContent = [LSYReadUtilites encodeWithURL:[NSURL URLWithString:chapterURL]];
            NSData *data = [tmpContent dataUsingEncoding:NSUTF8StringEncoding];
            if (![ZY_Common isBlankString:tmpContent]) {
               // NSString *content = @"";
                NSArray *chapterList = [[[ZY_JsonParser alloc] init] getChapterListByData:data];
               // LSYChapterModel *model = chapterList[0];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resetChapterList" object:@[chapterList,chapterURL]];
                

                isNeedShowError = NO;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self backView];
                    
                });

            }

        });
    }
    
    
    
    
    
}
@end

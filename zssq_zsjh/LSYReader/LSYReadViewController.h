//
//  LSYReadViewController.h
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSYRecordModel.h"
#import "LSYReadView.h"
#import "LSYReadModel.h"
#import "ZY_ADHelper.h"
@class LSYReadViewController;
@protocol LSYReadViewControllerDelegate <NSObject>
-(void)readViewEditeding:(LSYReadViewController *)readView;
-(void)readViewEndEdit:(LSYReadViewController *)readView;
-(void)continueReadClick;
@optional
-(void)updateReadModel;
-(void)menuBack;
-(void)scrollPageForward;
-(void)scrollPageBack;
-(void)menuViewChangeMode;
@end


@interface LSYReadViewController : UIViewController
@property (nonatomic,strong) NSString *content; //显示的内容
@property (nonatomic,strong) NSString *topTitle; //标题
@property (nonatomic,strong) NSString *authur;
@property (nonatomic,strong) NSString *bookName;

@property (nonatomic,strong) id epubFrameRef;  //epub显示内容
@property (nonatomic,strong) NSArray *imageArray;  //epub显示的图片
@property (nonatomic,assign) ReaderType type;   //文本类型
@property (nonatomic,strong) LSYRecordModel *recordModel;   //阅读进度
@property (nonatomic,strong) LSYReadView *readView;
@property (nonatomic,strong) UIView * bgView;  //背景亮度
@property (nonatomic,strong) UILabel * lbl_time;
@property (nonatomic,weak) id<LSYReadViewControllerDelegate>delegate;

@property (nonatomic) BOOL isNeedAD;
@property (nonatomic,strong) ADModel *adModel;

@property (nonatomic,strong) NSArray *MADArray;

// 底部bannerView
@property (nonatomic,strong) UIView *bannerView;

@property (nonatomic,assign) BOOL isNewChapter;

-(void)reSetContent:(NSString *)content withTitle:(NSString *)title;

-(void)reSetContent:(NSString *)content;
@end

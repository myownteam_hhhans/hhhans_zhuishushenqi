//
//  LSNovelSourceViewController.h
//  ZYNovel
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@class LSYReadPageViewController;
@interface LSNovelSourceViewController : UIViewController

@property (nonatomic,strong) ZY_NovelInfo *novel;
@property (nonatomic,strong) NSString *chapterTitle;
@property (nonatomic,strong) NSString *chapterUrl;
@property (nonatomic,strong) NSMutableArray *dataArr;
@property (nonatomic,weak) LSYReadPageViewController *pageView;

@end

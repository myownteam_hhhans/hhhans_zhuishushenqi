//
//  LSYBottomMenuView.m
//  LSYReader
//
//  Created by Labanotation on 16/6/1.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYBottomMenuView.h"
#import "LSYMenuView.h"
#import "LSYReadConfig.h"
#import "Common.h"

@interface LSYBottomMenuView ()



@end
@implementation LSYBottomMenuView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)setup{
    
    
    UIColor *color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
    NSNumber *themeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
    switch (themeNumber.integerValue) {
        case 1:
            color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
            break;
        case 2:
            color = [Common colorWithHexString:@"#fbf6e8" alpha:1.0];
            break;
        case 3:
            color = [Common colorWithHexString:@"#d1eecf" alpha:1.0];
            break;
        case 4:
            color = RGB(29, 29, 29);
            break;
            
        default:
            break;
    }
    
    [self setBackgroundColor:color];
    
    int imgWidth = ScreenSize.width/4;

    
    UIButton *img_Chapter = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, imgWidth, 56)];
    [img_Chapter setImage:[UIImage imageNamed:@"chapterList"] forState:UIControlStateNormal];
    [img_Chapter setImageEdgeInsets:UIEdgeInsetsMake(-12, 0, 0, 0)];
    [img_Chapter addTarget:self action:@selector(showCatalog) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:img_Chapter];
    
    UIButton *btn_Chapter = [[UIButton alloc]initWithFrame:CGRectMake(0, 28, imgWidth, 21)];
    
    btn_Chapter.titleLabel.font = [UIFont systemFontOfSize:10];
    [btn_Chapter addTarget:self action:@selector(showCatalog) forControlEvents:UIControlEventTouchUpInside];
    //[self addSubview:btn_Chapter];
    
    UIButton *img_Down = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth, 0, imgWidth, 56)];
    [img_Down setImage:[UIImage imageNamed:@"readProgress"]  forState:UIControlStateNormal];
    [img_Down setImage:[UIImage imageNamed:@"readProgressSelected"]  forState:UIControlStateSelected];
    [img_Down setImageEdgeInsets:UIEdgeInsetsMake(-12, 0, 0, 0)];
    [img_Down addTarget:self action:@selector(showDownLoad:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:img_Down];
    self.progressButton = img_Down;
    //img_Down.hidden = ![Common shareCommon].zs_Open;
    
    UIButton *btn_Down = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth, 28, imgWidth, 21)];
 
    btn_Down.titleLabel.font = [UIFont systemFontOfSize:10];
    [btn_Down addTarget:self action:@selector(showDownLoad:) forControlEvents:UIControlEventTouchUpInside];
    //[self addSubview:btn_Down];
    //btn_Down.hidden = ![Common shareCommon].zs_Open;
    
    self.img_White = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth*2, 0, imgWidth, 56)];
    [self.img_White addTarget:self action:@selector(showThemeChange) forControlEvents:UIControlEventTouchUpInside];
    [self.img_White setImageEdgeInsets:UIEdgeInsetsMake(-12, 0, 0, 0)];
    [self addSubview:self.img_White];
    
    self.btn_White = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth*2, 28, imgWidth, 21)];
    self.btn_White.titleLabel.font = [UIFont systemFontOfSize:10];
    [self.btn_White addTarget:self action:@selector(changeReadModel) forControlEvents:UIControlEventTouchUpInside];
    //[self addSubview:self.btn_White];
    
    //NSString *nightModeName = [LSYReadConfig shareInstance].isNightMode?@"白天":@"夜间";
    
    [self.img_White setImage:[UIImage imageNamed:@"changeTheme"] forState:UIControlStateNormal];
    [self.img_White setImage:[UIImage imageNamed:@"changeThemeSelected"] forState:UIControlStateSelected];
    NSInteger settingIndex = 3;
    
    UIButton *img_Setting = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth*settingIndex, 0, imgWidth, 56)];
    [img_Setting setImage:[UIImage imageNamed:@"changeFont"]  forState:UIControlStateNormal];
    [img_Setting setImageEdgeInsets:UIEdgeInsetsMake(-12, 0, 0, 0)];
    [img_Setting addTarget:self action:@selector(showSetting) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:img_Setting];
    
    UIButton *btn_Setting = [[UIButton alloc]initWithFrame:CGRectMake(imgWidth*settingIndex, 28, imgWidth, 21)];
   
    [btn_Setting addTarget:self action:@selector(showSetting) forControlEvents:UIControlEventTouchUpInside];
    btn_Setting.titleLabel.font = [UIFont systemFontOfSize:10];
    //[self addSubview:btn_Setting];
    
    
    [[LSYReadConfig shareInstance] addObserver:self forKeyPath:@"fontSize" options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionOld context:NULL];
}
///////tabBar点击//////*********************************

//字体按钮点击   原设置点击
-(void)showSetting{
    self.progressButton.selected = NO;
    self.img_White.selected = NO;
    if ([self.delegate respondsToSelector:@selector(menuViewSetting)]) {
        UIView *v_setting = [self.delegate menuViewSetting];
        v_setting.hidden = NO;
    }
}

-(void)showCatalog
{
    [MobClick event:@"Reader" label:@"目录"];
    self.progressButton.selected = NO;
    self.img_White.selected = NO;
  
    if ([self.delegate respondsToSelector:@selector(menuViewSetting)]) {
        UIView *v_setting = [self.delegate menuViewSetting];
        v_setting.hidden = YES;
    }
    if ([self.delegate respondsToSelector:@selector(menuViewInvokeCatalog:)]) {
        [self.delegate menuViewInvokeCatalog:self];
    }
}
-(void)showDownLoad:(UIButton *)sender
{
    [MobClick event:@"Reader" label:@"进度"];
    sender.selected = YES;
    self.img_White.selected = NO;
//    if ([self.delegate respondsToSelector:@selector(menuViewSetting)]) {
//        UIView *v_setting = [self.delegate menuViewSetting];
//        v_setting.hidden = YES;
//    }
    if ([self.delegate respondsToSelector:@selector(menuViewShowDownLoad)]) {
        UIView *v_setting = [self.delegate menuViewSetting];
        v_setting.hidden = NO;
        [self.delegate menuViewShowDownLoad];
    }
}

- (void)showThemeChange {
    [MobClick event:@"Reader" label:@"亮度"];
    self.img_White.selected = YES;
    self.progressButton.selected = NO;
    
    if ([self.delegate respondsToSelector:@selector(menuViewShowThemeChange)]) {
        [self.delegate menuViewShowThemeChange];
    }
}

//夜间模式切换
-(void)changeReadModel{
  
    if ([LSYReadConfig shareInstance].isNightMode) {
        
        [self.img_White setImage:[UIImage imageNamed:@"白天"] forState:UIControlStateNormal];
        
        [LSYReadConfig shareInstance].isNightMode = NO;
        [LSYReadConfig shareInstance].fontColor = RGB(56, 52, 44);
        
        if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
            [self.delegate menuViewFontSize:self];
        }
        if ([self.delegate respondsToSelector:@selector(menuViewChangeTheme:)]) {
            [self.delegate menuViewChangeTheme:[[LSYReadConfig shareInstance]getColorByTheme:[LSYReadConfig shareInstance].theme]];
           
        }
    }else
    {
       
        [self.img_White setImage:[UIImage imageNamed:@"夜间"] forState:UIControlStateNormal];
        
        [LSYReadConfig shareInstance].isNightMode = YES;
        [LSYReadConfig shareInstance].fontColor = [Common colorWithHexString:@"#6b6e71" alpha:1.0];
        if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
            [self.delegate menuViewFontSize:self];
        }
        if ([self.delegate respondsToSelector:@selector(menuViewChangeTheme:)]) {
            [self.delegate menuViewChangeTheme:RGB(15, 15, 15)];
        }
    }
}

/////////////*********************************

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if([keyPath isEqualToString:@"frame"]){
        LSYBottomMenuView *obj = (LSYBottomMenuView *)object;
        CGRect rect = obj.frame;
        if (rect.origin.y >= ScreenSize.height) {
            if ([self.delegate respondsToSelector:@selector(menuViewSetting)]) {
                UIView *v_setting = [self.delegate menuViewSetting];
                v_setting.hidden = YES;
            }
        }
    }
}

-(void)dealloc
{
    [self removeObserver:self forKeyPath:@"frame"];
    [[LSYReadConfig shareInstance] removeObserver:self forKeyPath:@"fontSize"];
}

@end


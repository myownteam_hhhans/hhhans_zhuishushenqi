//
//  LSYChapterADView.m
//  zssq_zsjh
//
//  Created by 包涵 on 2018/11/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "LSYChapterADView.h"
#import "Common.h"
#import "ZY_Common.h"
#import <MTGSDK/MTGSDK.h>
#import <UIImageView+WebCache.h>
#import "ZY_ADHelper.h"
#import "MTGNativeVideoView.h"
#import <BUAdSDK/BUAdSDK.h>
#import "GDTNativeAd.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

@interface LSYChapterADView() <BUBannerAdViewDelegate,GDTNativeAdDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *authurLabel;
@property (nonatomic, strong) UILabel *chapterLabel;

@property (nonatomic, strong) UIButton *continueButton;

@property (nonatomic, strong) UIImageView *bannerImage;  // 普通图片广告
 // 洛米视频广告
@property (nonatomic, strong) MTGNativeVideoView *mediaView;

@property (nonatomic, strong) GDTNativeAd *GDTAd;
@property (nonatomic, strong) NSArray *GDTDataArray;

@property (nonatomic, strong) ADModel *GDTAdModel;

// 谷歌广告
@property(nonatomic, strong) GADBannerView *midBannerView;

@end

@implementation LSYChapterADView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        

        
        [self addSubview:self.continueButton];
        [self addSubview:self.chapterLabel];
        [self addSubview:self.authurLabel];
        [self addSubview:self.titleLabel];
        
        
        if ([Common shareCommon].zs_Open) {
            if ([ [ZY_ADHelper shareADHelper].chapterADType isEqualToString:@"4"]) {
                
            } else if ([ZY_ADHelper shareADHelper].ChapterChangeAD) {
                [[ZY_ADHelper shareADHelper] registClickWithChapterView:self.bannerImage Controller:[UIApplication sharedApplication].keyWindow.rootViewController];
                [self addSubview:self.bannerImage];
            }
            
            if ([ [ZY_ADHelper shareADHelper].chapterADType isEqualToString:@"3"]) {
               
                NSDictionary *gdtKeyAndID = [[ZY_ADHelper shareADHelper] getGDTKeyWithADPlace:@"readpage"];
                self.GDTAd = [[GDTNativeAd alloc] initWithAppkey:gdtKeyAndID[@"ID"] placementId:gdtKeyAndID[@"KEY"]];
                self.GDTAd.delegate = self;
                self.GDTAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
                [self.GDTAd loadAd:1];
            }
            if ([ [ZY_ADHelper shareADHelper].chapterADType isEqualToString:@"5"]) {
               
            }
            if ([ [ZY_ADHelper shareADHelper].chapterADType isEqualToString:@"1"]) {
                self.midBannerView = [[GADBannerView alloc]
                                   initWithAdSize:kGADAdSizeMediumRectangle];
                [self addSubview:self.midBannerView];
                [self.midBannerView setFrame:CGRectMake(k_ScreenWidth/2 - 150, k_ScreenHeight/2 - 125, 300, 250)];
                self.midBannerView.adUnitID = @"ca-app-pub-9664827263749384/8416814713";
                self.midBannerView.rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
                [self.midBannerView loadRequest:[GADRequest request]];
                [MobClick event:@"Google_show" label:@"展示_翻页广告"];
            }
        }

    }
    return self;
}


- (void)hideSelf {
    
    if ([self.delegate respondsToSelector:@selector(adClose)]) {
        [self.delegate adClose];
    }
    //[[ZY_ADHelper shareADHelper] requestChapterAD];
}

- (void)bindTitle:(NSString *)title AuthurName:(NSString *)authur ChapterName:(NSString *)chapter {
    [self.titleLabel setText:title];
    [self.authurLabel setText:authur];
    [self.chapterLabel setText:chapter];
    
    if ([ [ZY_ADHelper shareADHelper].chapterADType isEqualToString:@"4"]) {
        
        BUSize *size = [BUSize sizeBy:BUProposalSize_Banner600_400];
        BUBannerAdView *banner = [[BUBannerAdView alloc] initWithSlotID:TouTiao_ChapterHead size:size rootViewController:self.superViewController];
        [banner loadAdData];
        const CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
        
        CGFloat bannerHeight = screenWidth * size.height / size.width;
        banner.frame = CGRectMake(0, k_ScreenHeight/2 - bannerHeight/2, screenWidth, bannerHeight);
        banner.delegate = self;
        [self addSubview:banner];
       
    }
}

- (void)bannerAdViewDidClick:(BUBannerAdView *)bannerAdView WithAdmodel:(BUNativeAd *)nativeAd {
   
}

#pragma mark - 广点通回调
- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray {
    self.GDTDataArray = nativeAdDataArray;
    self.GDTAdModel = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_Feed];
    [self.bannerImage sd_setImageWithURL:[NSURL URLWithString:self.GDTAdModel.adImgPath]];
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.GDTAd attachAd:self.GDTAdModel.data toView:self.bannerImage];
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GDTBannerClick)];
        [self.bannerImage addGestureRecognizer:tapGes];
    });
    
    
}
- (void)nativeAdFailToLoad:(NSError *)error {
    
}

- (void)GDTBannerClick {
    self.GDTAd.controller = self.superViewController.parentViewController;
    [self.GDTAd clickAd:self.GDTAdModel.data];
}

#pragma mark - 懒加载
- (UIButton *)continueButton {
    if (_continueButton == nil) {
        
        _continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_continueButton setFrame:CGRectMake(20, self.frame.size.height - 41 - 27, k_ScreenWidth - 40, 41)];
        [_continueButton setBackgroundColor:[Common colorWithHexString:@"#F1C65D" alpha:1.0]];
        [_continueButton setTitle:@"开始阅读" forState:UIControlStateNormal];
        [_continueButton setTitleColor:[Common colorWithHexString:@"#886409" alpha:1.0] forState:UIControlStateNormal];
        [_continueButton.layer setCornerRadius:41/2];
        [_continueButton addTarget:self action:@selector(hideSelf) forControlEvents:UIControlEventTouchUpInside];
    }
    return _continueButton;
}

- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setFrame:CGRectMake(0, 48, k_ScreenWidth, 17)];
        [_titleLabel setFont:[UIFont systemFontOfSize:17]];
        [_titleLabel setText:@"武道至尊"];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setTextColor:RGB(111, 111, 111)];
    }
    return _titleLabel;
}
- (UILabel *)authurLabel {
    if (_authurLabel == nil) {
        _authurLabel = [[UILabel alloc] init];
        [_authurLabel setFrame:CGRectMake(0, 90, k_ScreenWidth, 15)];
        [_authurLabel setTextColor:[Common colorWithHexString:@"#98928E" alpha:1.0]];
        [_authurLabel setFont:[UIFont systemFontOfSize:15]];
        [_authurLabel setText:@"作者：幽兰之上"];
        [_authurLabel setTextAlignment:NSTextAlignmentCenter];
    }
    return _authurLabel;
}
- (UILabel *)chapterLabel {
    if (_chapterLabel == nil) {
        _chapterLabel = [[UILabel alloc] init];
        [_chapterLabel setFrame:CGRectMake(0, 128, k_ScreenWidth, 14)];
        [_chapterLabel setFont:[UIFont systemFontOfSize:14]];
        [_chapterLabel setText:@"下一章：武道至尊"];
        [_chapterLabel setTextAlignment:NSTextAlignmentCenter];
        [_chapterLabel setTextColor:RGB(111, 111, 111)];
    }
    return _chapterLabel;
}

- (MTGNativeVideoView *)mediaView {
    if (_mediaView == nil) {
        UINib *nib = [UINib nibWithNibName:@"MTGNativeVideoView" bundle:nil];
        _mediaView = [[nib instantiateWithOwner:nil options:nil] firstObject];
        
        [_mediaView updateCellWithCampaign:[ZY_ADHelper shareADHelper].ChapterChangeAD.campaign unitId:M_UnitID_Chapter];
        
        CGFloat bottom = 47;
        if (isIPhoneX) {
            bottom = 47+34;
        }
        
        [_mediaView setFrame: CGRectMake(0, 0, k_ScreenWidth, k_ScreenWidth/16*9)];
        [_mediaView.MTGMediaView setFrame:_mediaView.bounds];
        [_mediaView setCenter:CGPointMake(k_ScreenWidth/2, k_ScreenHeight/2)];
        //[VideoView setCenter:CGPointMake(kCScreenWidth/2, VideoView.center.y)];
        
        _mediaView.appDescLabel.hidden = YES;
        _mediaView.appNameLabel.hidden = YES;
        _mediaView.iconImageView.hidden = YES;
        _mediaView.adCallButton.hidden = YES;
    }
    return _mediaView;
}

- (UIImageView *)bannerImage {
    if (_bannerImage == nil) {
        _bannerImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 414, 264)]];
        if (![[ZY_ADHelper shareADHelper].chapterADType isEqualToString:@"3"] && [ZY_ADHelper shareADHelper].ChapterChangeAD.bannerURL) {
            [_bannerImage sd_setImageWithURL:[NSURL URLWithString:[ZY_ADHelper shareADHelper].ChapterChangeAD.bannerURL]];
        }
        [_bannerImage setCenter:CGPointMake(k_ScreenWidth/2, k_ScreenHeight/2)];
        _bannerImage.userInteractionEnabled = YES;
    }
    return _bannerImage;
}

@end

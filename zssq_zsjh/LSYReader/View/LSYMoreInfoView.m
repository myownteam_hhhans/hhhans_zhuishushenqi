//
//  LSYMoreInfoView.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/22.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "LSYMoreInfoView.h"
#import "Common.h"
#import <UIImageView+WebCache.h>

@interface LSYMoreInfoView ()

@property (nonatomic, strong) UIView *halfWhiteView;

@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *authurLabel;

@property (nonatomic, strong) UIView *lineViewTop;
@property (nonatomic, strong) UILabel *rateLabel;

@property (nonatomic, strong) UIButton *shareFriendButton;
@property (nonatomic, strong) UIButton *shareMomentsButton;
@property (nonatomic, strong) UIButton *downloadButton;

@property (nonatomic, strong) UILabel *shareFriendLabel;
@property (nonatomic, strong) UILabel *shareMomentsLabel;
@property (nonatomic, strong) UILabel *downloadLabel;

@property (nonatomic, strong) UIView *lineViewBottom;
@property (nonatomic, strong) UIButton *closeButton;

@end

@implementation LSYMoreInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
        
        [self addSubview:self.halfWhiteView];
        [self.halfWhiteView addSubview:self.coverImageView];
        [self.halfWhiteView addSubview:self.titleLabel];
        [self.halfWhiteView addSubview:self.authurLabel];
        [self.halfWhiteView addSubview:self.lineViewTop];
        [self.halfWhiteView addSubview:self.rateLabel];
        [self.halfWhiteView addSubview:self.shareFriendButton];
        [self.halfWhiteView addSubview:self.shareMomentsButton];
        [self.halfWhiteView addSubview:self.downloadButton];
        [self.halfWhiteView addSubview:self.shareFriendLabel];
        [self.halfWhiteView addSubview:self.shareMomentsLabel];
        [self.halfWhiteView addSubview:self.downloadLabel];
        [self.halfWhiteView addSubview:self.lineViewBottom];
        [self.halfWhiteView addSubview:self.closeButton];
    }
    return self;
}

- (void)bindDataWithInfo:(ZY_NovelInfo *)info {
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:info.novelUrl]];
    [self.titleLabel setText:info.novelName];
    [self.authurLabel setText:info.novelAuthor];
}
- (void)hideSelf {
    self.hidden = YES;
}

#pragma mark - lazyLoad
- (UIView *)halfWhiteView {
    if (!_halfWhiteView) {
        _halfWhiteView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 414, 475)]];
        CGRect frame = _halfWhiteView.frame;
        frame.origin.y = k_ScreenHeight - _halfWhiteView.frame.size.height;
        _halfWhiteView.frame = frame;
        [_halfWhiteView setBackgroundColor:[UIColor whiteColor]];
    }
    return _halfWhiteView;
}
- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        _coverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(25, 25, 57, 82)];
    }
    return _coverImageView;
}
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setFrame:[Common get414FrameByMap:CGRectMake(101, 33, 276, 18)]];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:18]]];
        [_titleLabel setTextColor:[Common colorWithHexString:@"#1F262F" alpha:1.0]];
    }
    return _titleLabel;
}
- (UILabel *)authurLabel {
    if (!_authurLabel) {
        _authurLabel = [[UILabel alloc] init];
        [_authurLabel setFrame:[Common get414FrameByMap:CGRectMake(101, 63, 276, 13)]];
        [_authurLabel setTextAlignment:NSTextAlignmentLeft];
        [_authurLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
        [_authurLabel setTextColor:[Common colorWithHexString:@"#585E67" alpha:1.0]];
    }
    return _authurLabel;
}
- (UIView *)lineViewTop {
    if (!_lineViewTop) {
        _lineViewTop = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 132, 414, 0.7)]];
        [_lineViewTop setBackgroundColor:[Common colorWithHexString:@"#D1D3D5" alpha:1.0]];
    }
    return _lineViewTop;
}
- (UILabel *)rateLabel {
    if (!_rateLabel) {
        _rateLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(25, 158, 65, 14)]];
        [_rateLabel setTextColor:[Common colorWithHexString:@"#9097A0" alpha:1.0]];
        [_rateLabel setText:@"轻点评分"];
        [_rateLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:14]]];
        [_rateLabel setTextAlignment:NSTextAlignmentLeft];
    }
    return _rateLabel;
}
- (UIButton *)shareFriendButton {
    if (!_shareFriendButton) {
        _shareFriendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareFriendButton setFrame:[Common get414FrameByMap:CGRectMake(30, 211, 56, 56)]];
        [_shareFriendButton setBackgroundImage:[UIImage imageNamed:@"readerShareFriend"] forState:UIControlStateNormal];
    }
    return _shareFriendButton;
}
- (UIButton *)shareMomentsButton {
    if (!_shareMomentsButton) {
        _shareMomentsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareMomentsButton setFrame:[Common get414FrameByMap:CGRectMake(120, 211, 56, 56)]];
        [_shareMomentsButton setBackgroundImage:[UIImage imageNamed:@"readerShareMomment"] forState:UIControlStateNormal];
    }
    return _shareMomentsButton;
}
- (UIButton *)downloadButton {
    if (!_downloadButton) {
        _downloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_downloadButton setFrame:[Common get414FrameByMap:CGRectMake(30, 315, 56, 56)]];
        [_downloadButton setBackgroundImage:[UIImage imageNamed:@"readerDownload"] forState:UIControlStateNormal];
    }
    return _downloadButton;
}
- (UIView *)lineViewBottom {
    if (!_lineViewBottom) {
        _lineViewBottom = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 420.3, 414, 0.7)]];
        [_lineViewBottom setBackgroundColor:[Common colorWithHexString:@"#D1D3D5" alpha:1.0]];
    }
    return _lineViewBottom;
}
- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setFrame:CGRectMake(0, self.halfWhiteView.frame.size.height - 54, k_ScreenWidth, 54)];
        [_closeButton setTitle:@"关闭" forState:UIControlStateNormal];
        [_closeButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:15]]];
        [_closeButton setTitleColor:[Common colorWithHexString:@"#454B55" alpha:1.0] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(hideSelf) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}
- (UILabel *)shareFriendLabel {
    if (!_shareFriendLabel) {
        _shareFriendLabel = [[UILabel alloc] init];
        [_shareFriendLabel setText:@"分享给好友"];
        [_shareFriendLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:11]]];
        [_shareFriendLabel setTextColor:[Common colorWithHexString:@"#585E67" alpha:1.0]];
        [_shareFriendLabel sizeToFit];
        [_shareFriendLabel setCenter:CGPointMake(self.shareFriendButton.center.x, CGRectGetMaxY(self.shareFriendButton.frame) + [Common get414RadiusByMap:13])];
    }
    return _shareFriendLabel;
}
- (UILabel *)shareMomentsLabel {
    if (!_shareMomentsLabel) {
        _shareMomentsLabel = [[UILabel alloc] init];
        [_shareMomentsLabel setText:@"分享到朋友圈"];
        [_shareMomentsLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:11]]];
        [_shareMomentsLabel setTextColor:[Common colorWithHexString:@"#585E67" alpha:1.0]];
        [_shareMomentsLabel sizeToFit];
        [_shareMomentsLabel setCenter:CGPointMake(self.shareMomentsButton.center.x, CGRectGetMaxY(self.shareFriendButton.frame) + [Common get414RadiusByMap:13])];
    }
    return _shareMomentsLabel;
}
- (UILabel *)downloadLabel {
    if (!_downloadLabel) {
        _downloadLabel = [[UILabel alloc] init];
        [_downloadLabel setText:@"离线阅读"];
        [_downloadLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:11]]];
        [_downloadLabel setTextColor:[Common colorWithHexString:@"#585E67" alpha:1.0]];
        [_downloadLabel sizeToFit];
        [_downloadLabel setCenter:CGPointMake(self.shareFriendButton.center.x, CGRectGetMaxY(self.downloadButton.frame) + [Common get414RadiusByMap:13])];
    }
    return _downloadLabel;
}

@end

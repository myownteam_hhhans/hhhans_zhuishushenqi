//
//  LSYChapterADView.h
//  zssq_zsjh
//
//  Created by 包涵 on 2018/11/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_ADHelper.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ChapterADDelegate <NSObject>

- (void)adClose;

@end

@interface LSYChapterADView : UIView

@property (nonatomic, weak) id<ChapterADDelegate> delegate;

@property (nonatomic, weak) UIViewController *superViewController;

- (void)bindTitle:(NSString *)title AuthurName:(NSString *)authur ChapterName:(NSString *)chapter;


@end

NS_ASSUME_NONNULL_END

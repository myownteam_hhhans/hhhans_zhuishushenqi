//
//  LSYMoreInfoView.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/22.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

NS_ASSUME_NONNULL_BEGIN

@interface LSYMoreInfoView : UIView

- (void)bindDataWithInfo:(ZY_NovelInfo *)info;

@end

NS_ASSUME_NONNULL_END

//
//  LSYBottomMenuView.h
//  LSYReader
//
//  Created by Labanotation on 16/6/1.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LSYMenuViewDelegate;

@interface LSYBottomMenuView : UIView

@property (nonatomic,strong) UIButton *btn_White;
@property (nonatomic,strong) UIButton *img_White;
@property (nonatomic, strong) UIButton *progressButton;

@property (nonatomic,weak) id<LSYMenuViewDelegate>delegate;
@end




//
//  LSYMenuView.m
//  LSYReader
//
//  Created by Labanotation on 16/6/1.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYMenuView.h"
#import "LSYTopMenuView.h"
#import "LSYBottomMenuView.h"
#import "ZY_Common.h"
#import "Common.h"

#define AnimationDelay 0.3f
#define TopViewHeight (isIPhoneX?84.0f:64.0f)
#define BottomViewHeight (isIPhoneX?83.0f + 7.0f:49.0f + 7.0f)
@interface LSYMenuView ()<LSYMenuViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic,strong) UISlider *slider;
@property (nonatomic,strong) UIImageView *img_nlight_left;
@property (nonatomic,strong) UIImageView *img_nlight_right;
@property (nonatomic,strong) UIButton *increaseFont;
@property (nonatomic,strong) UIButton *decreaseFont;
@property (nonatomic,strong) UIButton *btn_fangzhen;
@property (nonatomic,strong) UIButton *btn_pingyi;
@property (nonatomic,strong) UIScrollView *scroll_theme;
@property (nonatomic,strong) UIView *oldWebView;
@property (nonatomic,strong) UILabel *lbl_orgTitle;

// 背景色和亮度
@property (nonatomic , strong) UIButton *themeButton1;
@property (nonatomic , strong) UIButton *themeButton2;
@property (nonatomic , strong) UIButton *themeButton3;
@property (nonatomic , strong) UIButton *themeButtonDark;

// 调整字体大小
@property (nonatomic, strong) UISlider *fontSlider;
@property (nonatomic, strong) UIImageView *leftFontImage;
@property (nonatomic, strong) UIImageView *rightFontImage;
@property (nonatomic, strong) NSMutableArray *lineArray;

// 调整进度
@property (nonatomic, strong) UISlider *progressSlider;
@property (nonatomic, strong) UILabel *readTimeLabel;
@property (nonatomic, strong) UILabel *chapterNameLabel;
@property (nonatomic, strong) UIButton *preChapterButton;
@property (nonatomic, strong) UIButton *nextChapterButton;

@end
@implementation LSYMenuView



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)setup
{
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.topView];
    [self.oldWebView addSubview:self.lbl_orgLink];
    [self.oldWebView addSubview:self.lbl_orgTitle];
    [self addSubview:self.v_setting];
    self.v_setting.hidden = YES;
    [self addSubview:self.bottomView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenSelf)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    
    [self.v_setting addSubview:self.slider];
    [self.v_setting addSubview:self.img_nlight_left];
    [self.v_setting addSubview:self.img_nlight_right];
    [self.v_setting addSubview:self.increaseFont];
    [self.v_setting addSubview:self.decreaseFont];
    [self.v_setting addSubview:self.scroll_theme];
    
    [self.v_setting addSubview:self.themeButton1];
    [self.v_setting addSubview:self.themeButton2];
    [self.v_setting addSubview:self.themeButton3];
    [self.v_setting addSubview:self.themeButtonDark];
    
    // 增加字体滑动条上的七条竖线
    CGFloat space = 46.f;
    for (int i=0; i<7; i++) {
        UIView *lineView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(69 + space * i, 43, 1, 6)]];
        [lineView setBackgroundColor:[Common colorWithHexString:@"#D6D6D6" alpha:1.0]];
        [self.lineArray addObject:lineView];
        [self.v_setting addSubview:lineView];
    }
    [self.v_setting addSubview:self.leftFontImage];
    [self.v_setting addSubview:self.rightFontImage];
    [self.v_setting addSubview:self.fontSlider];
    
    [self.v_setting addSubview:self.progressSlider];
    [self.v_setting addSubview:self.preChapterButton];
    [self.v_setting addSubview:self.nextChapterButton];
    [self.v_setting addSubview:self.readTimeLabel];
    [self.v_setting addSubview:self.nextChapterButton];
    
    [self.v_setting addSubview:self.readTimeLabel];
    [self.v_setting addSubview:self.chapterNameLabel];
    [self.v_setting addSubview:self.progressSlider];
    [self.v_setting addSubview:self.preChapterButton];
    [self.v_setting addSubview:self.nextChapterButton];
    
    // 设定选中的主题
    NSNumber *tag = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
    switch (tag.integerValue) {
        case 1:
            self.themeButton1.layer.borderWidth = 1;
            self.themeButton1.layer.borderColor = [Common colorWithHexString:@"#6A717A" alpha:1.0].CGColor;
            break;
        case 2:
            self.themeButton2.layer.borderWidth = 1;
            self.themeButton2.layer.borderColor = [Common colorWithHexString:@"#6A717A" alpha:1.0].CGColor;
            break;
        case 3:
            self.themeButton3.layer.borderWidth = 1;
            self.themeButton3.layer.borderColor = [Common colorWithHexString:@"#6A717A" alpha:1.0].CGColor;
            break;
        case 4:
            self.themeButtonDark.layer.borderWidth = 1;
            self.themeButtonDark.layer.borderColor = [Common colorWithHexString:@"#6A717A" alpha:1.0].CGColor;
            break;
            
        default:
            self.themeButton1.layer.borderWidth = 1;
            self.themeButton1.layer.borderColor = [Common colorWithHexString:@"#6A717A" alpha:1.0].CGColor;
            break;
    }
}
-(void)gotoWebView{
    [self hiddenAnimation:YES];
    [self menuViewOrgClick];
}

-(void)setRecordModel:(LSYRecordModel *)recordModel
{
    _recordModel = recordModel;
}
- (void)dealloc {

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (touch.view == self.v_setting || touch.view == self.progressSlider || touch.view == self.slider) {
        return NO;
    }
    
    return YES;
    
}

#pragma mark - LSYMenuViewDelegate
-(void)menuViewShowMoreInfo {
    if ([self.delegate respondsToSelector:@selector(menuViewShowMoreInfo)]) {
        [self.delegate menuViewShowMoreInfo];
    }
}
- (void)menuViewAddToShelf {
    if ([self.delegate respondsToSelector:@selector(menuViewAddToShelf)]) {
        [self.delegate menuViewAddToShelf];
    }
}

-(void)nextOrPreChapterButtonClick:(UIButton *)sender {
    NSInteger chapter = self.recordModel.chapter + sender.tag;
    if ([self.delegate respondsToSelector:@selector(menuViewJumpChapter:page:)]) {
        [self.delegate menuViewJumpChapter:chapter page:0];
    }
    [self.chapterNameLabel setText:self.recordModel.chapterModel.title];
}

-(void)menuViewInvokeCatalog:(LSYBottomMenuView *)bottomMenu
{
    if ([self.delegate respondsToSelector:@selector(menuViewInvokeCatalog:)]) {
        [self.delegate menuViewInvokeCatalog:bottomMenu];
    }
}
-(void)menuViewJumpChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    if ([self.delegate respondsToSelector:@selector(menuViewJumpChapter:page:)]) {
        [self.delegate menuViewJumpChapter:chapter page:page];
    }
    [self.chapterNameLabel setText:self.recordModel.chapterModel.title];
}
-(void)menuViewFontSize:(LSYBottomMenuView *)bottomMenu
{
    if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
        [self.delegate menuViewFontSize:bottomMenu];
    }
}
-(void)menuViewchangeSource:(LSYTopMenuView *)topMenu{
    if ([self.delegate respondsToSelector:@selector(menuViewchangeSource:)]) {
        [self.delegate menuViewchangeSource:topMenu];
    }
}
-(void)menuViewMark:(LSYTopMenuView *)topMenu
{
    if ([self.delegate respondsToSelector:@selector(menuViewMark:)]) {
        [self.delegate menuViewMark:topMenu];
    }
}
-(void)menuViewChangeMode
{
    if ([self.delegate respondsToSelector:@selector(menuViewChangeMode)]) {
        [self.delegate menuViewChangeMode];
    }
}
-(void)menuViewChangeTheme:(UIColor *)bgColor
{
    if ([self.delegate respondsToSelector:@selector(menuViewChangeTheme:)]) {
        [self.delegate menuViewChangeTheme:bgColor];
    }
}

// 原下载回调，现弹出进度调整
-(void)menuViewShowDownLoad
{
//    if ([self.delegate respondsToSelector:@selector(menuViewShowDownLoad)]) {
//        [self.delegate menuViewShowDownLoad];
//    }
    self.chapterNameLabel.text = self.recordModel.chapterModel.title;
    
    for (UIView *view in self.v_setting.subviews) {
        [view setHidden:YES];
    }
    self.chapterNameLabel.hidden = NO;
    self.preChapterButton.hidden = NO;
    self.nextChapterButton.hidden = NO;
    self.readTimeLabel.hidden = NO;
    self.progressSlider.hidden = NO;
    
}
-(void)menuViewBack
{
    if ([self.delegate respondsToSelector:@selector(menuViewBack)]) {
        [self.delegate menuViewBack];
    }
}
-(void)menuViewOrgClick
{
    if ([self.delegate respondsToSelector:@selector(menuViewOrgClick)]) {
        [self.delegate menuViewOrgClick];
    }
}

-(UIView *)menuViewSetting{
    
    for (UIView *view in self.v_setting.subviews) {
        [view setHidden:YES];
    }
    for (UIView *view in self.lineArray) {
        view.hidden = NO;
    }
    self.leftFontImage.hidden = NO;
    self.rightFontImage.hidden = NO;
    self.fontSlider.hidden = NO;
    
    
    return _v_setting;
}
-(void)menuViewShowThemeChange {
    
    for (UIView *view in self.v_setting.subviews) {
        [view setHidden:YES];
    }
    
    self.img_nlight_left.hidden = NO;
    self.img_nlight_right.hidden = NO;
    self.slider.hidden = NO;
    self.themeButton1.hidden = NO;
    self.themeButton2.hidden = NO;
    self.themeButton3.hidden = NO;
    self.themeButtonDark.hidden = NO;
    
    self.v_setting.hidden = NO;
}

-(void)menuViewLight:(float)light
{
    if ([self.delegate respondsToSelector:@selector(menuViewLight:)]) {
        [self.delegate menuViewLight:light];
    }
}



#pragma mark -
-(void)hiddenSelf
{
    [self hiddenAnimation:YES];
}
-(void)showAnimation:(BOOL)animation
{
    if(self.inTheShelfNow) {
        [self.topView.addShelfButton setImage:[UIImage imageNamed:@"readerRemoveShelf"] forState:UIControlStateNormal];
        [self.topView.addShelfButton setTitle:@"  缓存全本" forState:UIControlStateNormal];
    } else {
        [self.topView.addShelfButton setImage:[UIImage imageNamed:@"readerAddShelf"] forState:UIControlStateNormal];
        [self.topView.addShelfButton setTitle:@"  加入书架" forState:UIControlStateNormal];
    }
    
    if (self.topView.addShelfButton.selected) {
        [self.topView.addShelfButton setImage:[UIImage imageNamed:@"readerRemoveShelf"] forState:UIControlStateNormal];
    }
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    self.hidden = NO;
    _topView.frame = CGRectMake(0, -TopViewHeight, ViewSize(self).width,TopViewHeight);
    _bottomView.frame = CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomViewHeight);
    _oldWebView.frame = CGRectMake(0, -TopViewHeight-21, ViewSize(self).width,21);
    [UIView animateWithDuration:animation?AnimationDelay:0 animations:^{
        _topView.frame = CGRectMake(0, 0, ViewSize(self).width, TopViewHeight);
        _oldWebView.frame = CGRectMake(0, TopViewHeight, ViewSize(self).width,21);
        _bottomView.frame = CGRectMake(0, ViewSize(self).height-BottomViewHeight, ViewSize(self).width,BottomViewHeight);
    } completion:^(BOOL finished) {
        
    }];
    if ([self.delegate respondsToSelector:@selector(menuViewDidAppear:)]) {
        [self.delegate menuViewDidAppear:self];
    }
}
-(void)hiddenAnimation:(BOOL)animation
{
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    [UIView animateWithDuration:animation?AnimationDelay:0 animations:^{
        _topView.frame = CGRectMake(0, -TopViewHeight, ViewSize(self).width, TopViewHeight);
        _oldWebView.frame = CGRectMake(0, -TopViewHeight-21, ViewSize(self).width,21);
         _bottomView.frame = CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomViewHeight);
    } completion:^(BOOL finished) {
        self.hidden = YES;
        self.v_setting.hidden = YES;
        
        self.bottomView.progressButton.selected = NO;
        self.bottomView.img_White.selected = NO;
        
    }];
    if ([self.delegate respondsToSelector:@selector(menuViewDidHidden:)]) {
        [self.delegate menuViewDidHidden:self];
    }
}
-(void)layoutSubviews
{
    [super layoutSubviews];
//    _topView.frame = CGRectMake(0, -TopViewHeight, ViewSize(self).width,TopViewHeight);
//    _oldWebView.frame = CGRectMake(0, -TopViewHeight-21, ViewSize(self).width,21);
    _v_setting.frame = CGRectMake(0, ScreenSize.height-[Common get414RadiusByMap:113]-BottomViewHeight, ViewSize(self).width, [Common get414RadiusByMap:113]);
//    _bottomView.frame = CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomViewHeight);
    
    _slider.frame = [Common get414FrameByMap:CGRectMake(62, 27, 290, 13)];
    _img_nlight_left.frame = [Common get414FrameByMap:CGRectMake(24, 25, 20, 20)];
    _img_nlight_right.frame = [Common get414FrameByMap:CGRectMake(369, 23, 20, 20)];
    _decreaseFont.frame = CGRectMake((ScreenSize.width-102*2)/3, DistanceFromTopGuiden(_slider)+10, 102, 27);
    _increaseFont.frame = CGRectMake(ScreenSize.width-DistanceFromLeftGuiden(_decreaseFont), DistanceFromTopGuiden(_slider)+10,  102, 27);
    //    _btn_fangzhen.frame = CGRectMake((ScreenSize.width-102*2)/3, DistanceFromTopGuiden(_decreaseFont)+18, 102, 27);
    //    _btn_pingyi.frame = CGRectMake(ScreenSize.width-DistanceFromLeftGuiden(_btn_fangzhen), DistanceFromTopGuiden(_decreaseFont)+18,  102, 27);
    _scroll_theme.frame = CGRectMake(0, DistanceFromTopGuiden(_decreaseFont)+18, ViewSize(self).width, 40);
}



#pragma mark - Button Click



- (void)changeFontSlider {
    
    NSInteger font = (NSInteger)(((NSInteger)self.fontSlider.value*2 + 1)/2);
    self.fontSlider.value = (CGFloat)font;
    
    [LSYReadConfig shareInstance].fontSize = 17 + font;
    if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
        [self.delegate menuViewFontSize:nil];
    }
    
}

- (void)changeProgress {
    if ([self.delegate respondsToSelector:@selector(menuViewChangeProgress:)]) {
        [self.delegate menuViewChangeProgress:self.progressSlider.value];
    }
    [self.chapterNameLabel setText:self.recordModel.chapterModel.title];
}

-(void)changeReadMode:(UIButton *)sender{
    if (sender == _btn_pingyi) {
        [LSYReadConfig shareInstance].readMode = 1;
    }
    if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
        [self.delegate menuViewFontSize:nil];
    }
    if ([self.delegate respondsToSelector:@selector(menuViewChangeMode)]) {
        [self.delegate menuViewChangeMode];
    }
    
}
-(void)changeFont:(UIButton *)sender
{
    [MobClick endEvent:@"Reader" label:[NSString stringWithFormat:@"字体%ld",self.fontSlider.value]];
    
    if (sender == _increaseFont) {
        if (floor([LSYReadConfig shareInstance].fontSize) == floor(MaxFontSize)) {
            return;
        }
        [LSYReadConfig shareInstance].fontSize++;
    }
    else{
   
        if (floor([LSYReadConfig shareInstance].fontSize) == floor(MinFontSize)){
            return;
        }
        [LSYReadConfig shareInstance].fontSize--;
    }
    
    if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
        [self.delegate menuViewFontSize:nil];
    }
}

- (void)actionTapGesture:(UITapGestureRecognizer *)sender {
    CGPoint touchPoint = [sender locationInView:_fontSlider];
    CGFloat value = (_fontSlider.maximumValue - _fontSlider.minimumValue) * (touchPoint.x / _slider.frame.size.width );
    [_fontSlider setValue:value animated:YES];
    [self changeFontSlider];
}

-(void)changeMsg:(UISlider *)sender
{
    if ([self.delegate respondsToSelector:@selector(menuViewLight:)]) {
        [self.delegate menuViewLight:sender.value];
    }
}

-(void)changeTheme:(id)tap
{
    // 去掉之前的选中框
    self.themeButton1.layer.borderWidth = 0;
    self.themeButton2.layer.borderWidth = 0;
    self.themeButton3.layer.borderWidth = 0;
    self.themeButtonDark.layer.borderWidth = 0;
    
    //  先加一个选中框
    UIButton *themeButton = (UIButton *)tap;
    themeButton.layer.borderWidth = 1;
    themeButton.layer.borderColor = [Common colorWithHexString:@"#6A717A" alpha:1.0].CGColor;
    [MobClick event:@"Reader" label:[NSString stringWithFormat:@"背景%ld",themeButton.tag]];
    
    // 设置各种主题的颜色
    UIColor *pageViewColor;
    UIColor *toolBarColor;
    UIColor *chapterListColor;
    [LSYReadConfig shareInstance].fontColor = RGB(56, 52, 44);
    [LSYReadConfig shareInstance].isNightMode = NO;
    switch (themeButton.tag) {
        case 1:
            pageViewColor = [Common colorWithHexString:@"#e9e9eb" alpha:1.0];
            toolBarColor = [Common colorWithHexString:@"#efeff0" alpha:1.0];
            chapterListColor = [Common colorWithHexString:@"F0F0F0" alpha:1.0];
            break;
        case 2:
            pageViewColor = [Common colorWithHexString:@"#f4efdc" alpha:1.0];
            toolBarColor = [Common colorWithHexString:@"#fbf6e8" alpha:1.0];
            chapterListColor = [Common colorWithHexString:@"#fbf7e9" alpha:1.0];
            break;
        case 3:
            pageViewColor = [Common colorWithHexString:@"#c9ebc9" alpha:1.0];
            toolBarColor = [Common colorWithHexString:@"#d1eecf" alpha:1.0];
            chapterListColor = [Common colorWithHexString:@"#d3eed1" alpha:1.0];
            break;
        case 4:
            // 进入夜间模式
            [LSYReadConfig shareInstance].isNightMode = YES;
            [LSYReadConfig shareInstance].fontColor = [Common colorWithHexString:@"#6b6e71" alpha:1.0];
            pageViewColor = RGB(15, 15, 15);
            toolBarColor = RGB(29, 29, 29);
            
            break;
            
        default:
            break;
    }
    
    //  切换menuView的颜色
    self.v_setting.backgroundColor = toolBarColor;
    self.bottomView.backgroundColor = toolBarColor;
    self.topView.backgroundColor = toolBarColor;
    
    //  通知pageVC切换背景色、目录色
    if ([self.delegate respondsToSelector:@selector(menuViewChangeTheme:)]) {
        if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
            [self.delegate menuViewFontSize:nil];
        }
        [self.delegate menuViewChangeTheme:pageViewColor];
    }
    //  本地存储主题
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:themeButton.tag] forKey:@"readerThemeColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // 发送通知
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeTheme" object:nil];

}

-(UIImage *)thumbImage
{
    CGRect rect = CGRectMake(0, 0, 18,18);
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineWidth = 5;
    [path addArcWithCenter:CGPointMake(rect.size.width/2, rect.size.height/2) radius:9 startAngle:0 endAngle:2*M_PI clockwise:YES];
    
    UIImage *image = nil;
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    {
        [[UIColor whiteColor] setFill];
        [path fill];
        image = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    return image;
}


#pragma mark - lazyload
-(UIView *)oldWebView
{
    if (!_oldWebView) {
        _oldWebView = [[UIView alloc] init];
        _oldWebView.backgroundColor = RGB(41, 41, 41);
        [_oldWebView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoWebView)]];
    }
    return _oldWebView;
}
-(UILabel *)lbl_orgLink
{
    if (!_lbl_orgLink) {
        _lbl_orgLink = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, ScreenSize.width-60-30, 21)];
        _lbl_orgLink.textColor = RGB(90, 90, 90);
        _lbl_orgLink.font = [UIFont systemFontOfSize:10];
        _lbl_orgLink.textAlignment = NSTextAlignmentCenter;
    }
    return _lbl_orgLink;
}
-(UILabel *)lbl_orgTitle
{
    if (!_lbl_orgTitle) {
        _lbl_orgTitle = [[UILabel alloc] initWithFrame:CGRectMake(ScreenSize.width-75, 0, 60, 21)];
        _lbl_orgTitle.textColor = RGB(90, 90, 90);
        _lbl_orgTitle.text = @"原网页阅读";
        _lbl_orgTitle.font = [UIFont systemFontOfSize:11];
    }
    return _lbl_orgTitle;
}
-(LSYTopMenuView *)topView
{
    if (!_topView) {
        _topView = [[LSYTopMenuView alloc] init];
        _topView.delegate = self;

    }
    return _topView;
}
-(LSYBottomMenuView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[LSYBottomMenuView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.frame), CGRectGetWidth(self.frame), BottomViewHeight)];
        _bottomView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _bottomView.delegate = self;
    }
    return _bottomView;
}
-(UIView *)v_setting{
    if (!_v_setting) {
        _v_setting = [[UIView alloc]init];
        UIColor *color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
        NSNumber *themeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
        switch (themeNumber.integerValue) {
            case 1:
                color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
                break;
            case 2:
                color = [Common colorWithHexString:@"#fbf6e8" alpha:1.0];
                break;
            case 3:
                color = [Common colorWithHexString:@"#d1eecf" alpha:1.0];
                break;
            case 4:
                color = RGB(29, 29, 29);
                break;
                
            default:
                break;
        }
        
        [_v_setting setBackgroundColor:color];
    }
    return _v_setting;
}
-(UIScrollView *)scroll_theme{
    if (!_scroll_theme) {
        _scroll_theme = [[UIScrollView alloc]init];
        _scroll_theme.contentSize = CGSizeMake(520, 40);
        _scroll_theme.showsVerticalScrollIndicator = NO;
        _scroll_theme.showsHorizontalScrollIndicator = NO;
        [_scroll_theme setBackgroundColor:[UIColor clearColor]];
    }
    return _scroll_theme;
}
-(UISlider *)slider
{
    if (!_slider) {
        _slider = [[UISlider alloc] init];
        _slider.minimumValue = -0.7;
        _slider.maximumValue = 0;
        _slider.minimumTrackTintColor = RGB(106, 113, 121);
        _slider.maximumTrackTintColor = RGB(106, 113, 121);
        [_slider setThumbImage:[self thumbImage] forState:UIControlStateNormal];
        [_slider setThumbImage:[self thumbImage] forState:UIControlStateHighlighted];
        [_slider addTarget:self action:@selector(changeMsg:) forControlEvents:UIControlEventValueChanged];
        _slider.value = -[LSYReadConfig shareInstance].light;
    }
    return _slider;
}
-(UIImageView *)img_nlight_right
{
    if (!_img_nlight_right) {
        _img_nlight_right = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"lightMax"]];
    }
    return _img_nlight_right;
}
-(UIImageView *)img_nlight_left
{
    if (!_img_nlight_left) {
        _img_nlight_left = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"lightMin"]];
    }
    return _img_nlight_left;
}
-(UIButton *)increaseFont
{
    if (!_increaseFont) {
        _increaseFont = [LSYReadUtilites commonButtonSEL:@selector(changeFont:) target:self];
        [_increaseFont setTitle:@"Aa+" forState:UIControlStateNormal];
        [_increaseFont.titleLabel setFont:[UIFont systemFontOfSize:14]];
        _increaseFont.layer.borderWidth = 1;
        _increaseFont.layer.cornerRadius = 6;
        _increaseFont.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    return _increaseFont;
}
-(UIButton *)decreaseFont
{
    if (!_decreaseFont) {
        _decreaseFont = [LSYReadUtilites commonButtonSEL:@selector(changeFont:) target:self];
        [_decreaseFont setTitle:@"Aa-" forState:UIControlStateNormal];
        [_decreaseFont.titleLabel setFont:[UIFont systemFontOfSize:14]];
        _decreaseFont.layer.borderWidth = 1;
        _decreaseFont.layer.cornerRadius = 6;
        _decreaseFont.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    return _decreaseFont;
}
-(UIButton *)btn_fangzhen
{
    if (!_btn_fangzhen) {
        _btn_fangzhen = [LSYReadUtilites commonButtonSEL:@selector(changeFont:) target:self];
        [_btn_fangzhen setTitle:@"仿真" forState:UIControlStateNormal];
        [_btn_fangzhen.titleLabel setFont:[UIFont systemFontOfSize:14]];
        _btn_fangzhen.layer.borderWidth = 1;
        _btn_fangzhen.layer.cornerRadius = 6;
        [_btn_fangzhen setTitleColor:RGB(229, 20, 13) forState:UIControlStateNormal];
        _btn_fangzhen.layer.borderColor = RGB(229, 20, 13).CGColor;
    }
    return _btn_fangzhen;
}
-(UIButton *)btn_pingyi
{
    if (!_btn_pingyi) {
        _btn_pingyi = [LSYReadUtilites commonButtonSEL:@selector(changeReadMode:) target:self];
        [_btn_pingyi setTitle:@"平移" forState:UIControlStateNormal];
        [_btn_pingyi.titleLabel setFont:[UIFont systemFontOfSize:14]];
        _btn_pingyi.layer.borderWidth = 1;
        _btn_pingyi.layer.cornerRadius = 6;
        _btn_pingyi.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    return _btn_pingyi;
}
- (UIButton *)themeButton1 {
    if (_themeButton1 == nil) {
        _themeButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
        _themeButton1.tag = 1;
        [_themeButton1 setFrame:[Common get414FrameByMap:CGRectMake(22, 69, 88, 24)]];
        [_themeButton1 setBackgroundColor:[Common colorWithHexString:@"#F0F0F0" alpha:1.0]];
        [_themeButton1 addTarget:self action:@selector(changeTheme:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _themeButton1;
}
- (UIButton *)themeButton2 {
    if (_themeButton2 == nil) {
        _themeButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
        _themeButton2.tag = 2;
        [_themeButton2 setFrame:[Common get414FrameByMap:CGRectMake(117, 69, 88, 24)]];
        [_themeButton2 setBackgroundColor:[Common colorWithHexString:@"#EDE8DA" alpha:1.0]];
        [_themeButton2 addTarget:self action:@selector(changeTheme:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _themeButton2;
}
- (UIButton *)themeButton3 {
    if (_themeButton3 == nil) {
        _themeButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
        _themeButton3.tag = 3;
        [_themeButton3 setFrame:[Common get414FrameByMap:CGRectMake(210, 69, 88, 24)]];
        [_themeButton3 setBackgroundColor:[Common colorWithHexString:@"#C0E2C3" alpha:1.0]];
        [_themeButton3 addTarget:self action:@selector(changeTheme:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _themeButton3;
}
- (UIButton *)themeButtonDark {
    if (_themeButtonDark == nil) {
        _themeButtonDark = [UIButton buttonWithType:UIButtonTypeCustom];
        _themeButtonDark.tag = 4;
        [_themeButtonDark setFrame:[Common get414FrameByMap:CGRectMake(304, 69, 88, 24)]];
        [_themeButtonDark setBackgroundColor:[Common colorWithHexString:@"#0F1317" alpha:1.0]];
        [_themeButtonDark addTarget:self action:@selector(changeTheme:) forControlEvents:UIControlEventTouchUpInside];
        [_themeButtonDark addSubview:({
            UIImageView *moonImage = [[UIImageView alloc] init];
            [moonImage setFrame:[Common get414FrameByMap:CGRectMake(36, 4, 16, 16)]];
            [moonImage setImage:[UIImage imageNamed:@"darkMoon"]];
            moonImage;
        })];
    }
    return _themeButtonDark;
}
- (UISlider *)fontSlider {
    if (_fontSlider == nil) {
        _fontSlider = [[UISlider alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(67, 36, 281, 21)]];
        _fontSlider.minimumValue = 0;
        _fontSlider.maximumValue = 6;
        _fontSlider.minimumTrackTintColor = RGB(214, 214, 214);
        _fontSlider.maximumTrackTintColor = RGB(214, 214, 214);
        [_fontSlider setThumbImage:[self thumbImage] forState:UIControlStateNormal];
        [_fontSlider setThumbImage:[self thumbImage] forState:UIControlStateHighlighted];
        [_fontSlider addTarget:self action:@selector(changeFontSlider) forControlEvents:UIControlEventTouchUpInside];
        _fontSlider.value = [LSYReadConfig shareInstance].fontSize - 17;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTapGesture:)];
        tapGesture.delegate = self;
        [_fontSlider addGestureRecognizer:tapGesture];
        
    }
    return _fontSlider;
}

- (UIImageView *)leftFontImage {
    if (_leftFontImage == nil) {
        _leftFontImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(26, 39, 10, 13)]];
        [_leftFontImage setImage:[UIImage imageNamed:@"minFont"]];
    }
    return _leftFontImage;
}
- (UIImageView *)rightFontImage {
    if (_rightFontImage == nil) {
        _rightFontImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(375, 36, 15, 19)]];
        [_rightFontImage setImage:[UIImage imageNamed:@"maxFont"]];
    }
    return _rightFontImage;
}
- (UISlider *)progressSlider {
    if (_progressSlider == nil) {
        _progressSlider = [[UISlider alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(67, 53, 281, 13)]];
        _progressSlider.minimumValue = 0;
        _progressSlider.maximumValue = 99;
        _progressSlider.minimumTrackTintColor = RGB(106, 113, 121);
        _progressSlider.maximumTrackTintColor = RGB(214, 214, 214);
        [_progressSlider setThumbImage:[self thumbImage] forState:UIControlStateNormal];
        [_progressSlider setThumbImage:[self thumbImage] forState:UIControlStateHighlighted];
        [_progressSlider addTarget:self action:@selector(changeProgress) forControlEvents:UIControlEventTouchUpInside];
    }
    return _progressSlider;
}
- (UILabel *)readTimeLabel {
    if (_readTimeLabel == nil) {
        _readTimeLabel = [[UILabel alloc] init];
        [_readTimeLabel setFrame:[Common get414FrameByMap:CGRectMake(0, 20, 414, 12)]];
        [_readTimeLabel setText:@"已阅读0分钟"];
        [_readTimeLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:12]]];
        [_readTimeLabel setTextAlignment:NSTextAlignmentCenter];
        [_readTimeLabel setTextColor:[Common colorWithHexString:@"#585E67" alpha:1.0]];
    }
    return _readTimeLabel;
}
- (UILabel *)chapterNameLabel {
    if (_chapterNameLabel == nil) {
        _chapterNameLabel = [[UILabel alloc] init];
        [_chapterNameLabel setFrame:[Common get414FrameByMap:CGRectMake(0, 88, 414, 12)]];
        [_chapterNameLabel setText:@"第一章 色诱小姨子"];
        [_chapterNameLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:12]]];
        [_chapterNameLabel setTextAlignment:NSTextAlignmentCenter];
        [_chapterNameLabel setTextColor:[Common colorWithHexString:@"#585E67" alpha:1.0]];
    }
    return _chapterNameLabel;
}
- (UIButton *)preChapterButton {
    if (_preChapterButton == nil) {
        _preChapterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_preChapterButton setFrame:[Common get414FrameByMap:CGRectMake(19.5, 51, 17, 17)]];
        [_preChapterButton setImage:[UIImage imageNamed:@"preChapter"] forState:UIControlStateNormal];
        [_preChapterButton setTag:-1];
        [_preChapterButton addTarget:self action:@selector(nextOrPreChapterButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _preChapterButton;
}
- (UIButton *)nextChapterButton {
    if (_nextChapterButton == nil) {
        _nextChapterButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_nextChapterButton setFrame:[Common get414FrameByMap:CGRectMake(378.5, 51, 17, 17)]];
        [_nextChapterButton setImage:[UIImage imageNamed:@"nextChapter"] forState:UIControlStateNormal];
        [_nextChapterButton setTag:1];
        [_nextChapterButton addTarget:self action:@selector(nextOrPreChapterButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _nextChapterButton;
}
- (NSMutableArray *)lineArray {
    if (_lineArray == nil) {
        _lineArray = [NSMutableArray array];
    }
    return _lineArray;
}

@end

//
//  LSYTopMenuView.m
//  LSYReader
//
//  Created by Labanotation on 16/6/1.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYTopMenuView.h"
#import "LSYMenuView.h"
#import "ZY_Common.h"
#import "Common.h"

#define fixTop (isIPhoneX?20.0f:0.0f)


@interface LSYTopMenuView ()
@property (nonatomic,strong) UIButton *back;
@property (nonatomic,strong) UIButton *more;  // 原书签按钮 现已隐藏
@property (nonatomic,strong) UIButton *changeSource;

@property (nonatomic, strong) UIButton *moreButton;


@end
@implementation LSYTopMenuView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)setup
{
    UIColor *color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
    NSNumber *themeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
    switch (themeNumber.integerValue) {
        case 1:
            color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
            break;
        case 2:
            color = [Common colorWithHexString:@"#fbf6e8" alpha:1.0];
            break;
        case 3:
            color = [Common colorWithHexString:@"#d1eecf" alpha:1.0];
            break;
        case 4:
            color = color = RGB(29, 29, 29);
            break;
            
        default:
            break;
    }
    
    
    [self setBackgroundColor:color];
    
    
    [self addSubview:self.back];
    //[self addSubview:self.more];
    [self addSubview:self.changeSource];
    //[self addSubview:self.moreButton];
    [self addSubview:self.addShelfButton];
}

// delegate
- (void)addShelfButtonClick {
    
    [MobClick event:@"Reader" label:self.addShelfButton.titleLabel.text];
    
    self.addShelfButton.selected = YES;
    
    [self.addShelfButton setTitle:@"  缓存全本" forState:UIControlStateNormal];
    [self.addShelfButton setImage:[UIImage imageNamed:@"readerRemoveShelf"] forState:UIControlStateNormal];
    
    if ([self.delegate respondsToSelector:@selector(menuViewAddToShelf)]) {
        [self.delegate menuViewAddToShelf];
    }
}
- (void)moreButtonClick {
    if ([self.delegate respondsToSelector:@selector(menuViewShowMoreInfo)]) {
        [self.delegate menuViewShowMoreInfo];
    }
}

-(void)setState:(BOOL)state
{
    _state = state;
    if (state) {
        [_more setImage:[UIImage imageNamed:@"已加书签"] forState:UIControlStateNormal];
        return;
    }
    [_more setImage:[UIImage imageNamed:@"书签"] forState:UIControlStateNormal];
}
-(UIButton *)back
{
    if (!_back) {
        
        _back = [UIButton buttonWithType:UIButtonTypeCustom];
        [_back addTarget:self action:@selector(backView) forControlEvents:UIControlEventTouchUpInside];
        [_back setImage:[UIImage imageNamed:@"readerBack"] forState:UIControlStateNormal];
        
    }
    return _back;
}
-(UIButton *)more
{
    if (!_more) {
        _more = [[UIButton alloc]init];
        [_more addTarget:self action:@selector(moreOption) forControlEvents:UIControlEventTouchUpInside];
        [_more setImage:[UIImage imageNamed:@"书签"] forState:UIControlStateNormal];
    }
    return _more;
}
-(UIButton *)changeSource
{
    if (!_changeSource) {
        _changeSource = [UIButton buttonWithType:UIButtonTypeCustom];
        [_changeSource addTarget:self action:@selector(changeSourceOption) forControlEvents:UIControlEventTouchUpInside];
        [_changeSource setTitle:@"换源" forState:UIControlStateNormal];
        _changeSource.titleLabel.font = [UIFont systemFontOfSize:13];
        //_changeSource.titleLabel.textColor = [Common colorWithHexString:@"#585E67" alpha:1.0];
        [_changeSource setTitleColor:[Common colorWithHexString:@"#0090FA" alpha:1.0] forState:UIControlStateNormal];
    }
    return _changeSource;
}
-(void)changeSourceOption{
    [MobClick event:@"Reader" label:@"换源"];
    if ([self.delegate respondsToSelector:@selector(menuViewchangeSource:)]) {
        [self.delegate menuViewchangeSource:self];
    }
}
-(void)moreOption
{
    if ([self.delegate respondsToSelector:@selector(menuViewMark:)]) {
        [self.delegate menuViewMark:self];
    }
}
-(void)backView
{
    if ([self.delegate respondsToSelector:@selector(menuViewBack)]) {
        [self.delegate menuViewBack];
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    _back.frame = CGRectMake(0, 24 + fixTop, 40, 40);
    _more.frame = CGRectMake(ViewSize(self).width-50, 24 + fixTop, 40, 40);
    _changeSource.frame = CGRectMake(ViewSize(self).width-90, 24, 40, 40);
    [_changeSource setCenter:CGPointMake([Common get414RadiusByMap:249+25/2.0], _back.center.y)];
    //[_moreButton setCenter:CGPointMake([Common get414RadiusByMap:384], _back.center.y)];
    [_addShelfButton setCenter:CGPointMake([Common get414RadiusByMap:287+17+35], _back.center.y)];
}

#pragma mark - lazyload
- (UIButton *)addShelfButton {
    if (_addShelfButton == nil) {
        _addShelfButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addShelfButton setFrame:[Common get414FrameByMap:CGRectMake(0, 0 + fixTop, 85, 20)]];
        [_addShelfButton setTitle:@"  加入书架" forState:UIControlStateNormal];
        [_addShelfButton setImage:[UIImage imageNamed:@"readerAddShelf"] forState:UIControlStateNormal];
        [_addShelfButton setTitleColor:[Common colorWithHexString:@"#585E67" alpha:1.0] forState:UIControlStateNormal];
        [_addShelfButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
        [_addShelfButton addTarget:self action:@selector(addShelfButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addShelfButton;
}
- (UIButton *)moreButton {
    if (_moreButton == nil) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton setFrame:CGRectMake(0, 0 + fixTop, 10, 16)];
        [_moreButton setImage:[UIImage imageNamed:@"readerMore"] forState:UIControlStateNormal];
        [_moreButton addTarget:self action:@selector(moreButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}

@end

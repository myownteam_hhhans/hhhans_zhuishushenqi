//
//  LYSMarkTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/4/5.
//  Copyright © 2017年 S. All rights reserved.
//

#import "LSYMarkTableViewCell.h"
#import "LSYMarkVC.h"
#import "LSYCatalogViewController.h"
#import "LSYDottedLine.h"

@interface LSYMarkTableViewCell()

@property(nonatomic,strong)UIImageView *img_mark;

@property(nonatomic,strong)UILabel *v_title;

@property(nonatomic,strong)UILabel *v_subTitle;

@property(nonatomic,strong)UILabel *v_time;

@property(nonatomic,strong)LSYDottedLine *img_xian;


@end

@implementation LSYMarkTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self.contentView addSubview:self.img_mark];
        [self.contentView addSubview:self.v_title];
        [self.contentView addSubview:self.v_subTitle];
        [self.contentView addSubview:self.v_time];
        [self.contentView addSubview:self.img_xian];
    }
    
    return self;
}

#pragma mark - Getter or Setter

-(LSYDottedLine *)img_xian
{
    if (!_img_xian) {
        _img_xian= [[LSYDottedLine alloc]initWithFrame:CGRectMake(24, 47, ScreenSize.width-48-65, 1)];
    }
    return _img_xian;
}

-(UIImageView *)img_mark
{
    if (!_img_mark) {
        _img_mark = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bookmark"]];
        _img_mark.frame = CGRectMake(9, 18, 7, 12);
    }
    return _img_mark;
}

-(UILabel *)v_title
{
    if (!_v_title) {
        _v_title= [[UILabel alloc]initWithFrame:CGRectMake(24, 2, ScreenSize.width-48-65, 24)];
        _v_title.font = [UIFont systemFontOfSize:12];
        _v_title.textColor = RGB(51, 51, 51);
    }
    return _v_title;
    
}

-(UILabel *)v_subTitle
{
    if (!_v_subTitle) {
        _v_subTitle= [[UILabel alloc]initWithFrame:CGRectMake(24, 24, ScreenSize.width-108-65, 20)];
        _v_subTitle.font = [UIFont systemFontOfSize:11];
        _v_subTitle.textColor = RGB(102, 102, 102);
    }
    return _v_subTitle;
}

-(UILabel *)v_time
{
    if (!_v_time) {
        _v_time= [[UILabel alloc]initWithFrame:CGRectMake(DistanceFromLeftGuiden(self.v_subTitle), 24, 58, 20)];
        _v_time.font = [UIFont systemFontOfSize:11];
        _v_time.textColor = RGB(102, 102, 102);
        _v_time.textAlignment = NSTextAlignmentRight;
    }
    
    return _v_time;
}

-(void)bindItemWithtimeM:(NSString*)timecurrMark withv_titlet:(NSString*)v_titletext withv_subTitlet:(NSString*)v_subTitletext{
    self.v_title.text=v_titletext;
    self.v_subTitle.text=[v_subTitletext stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.v_time.text=timecurrMark;
}


@end

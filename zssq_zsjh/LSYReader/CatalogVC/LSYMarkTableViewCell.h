//
//  LYSMarkTableViewCell.h
//  ZYNovel
//
//  Created by S on 2017/4/5.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSYMarkTableViewCell : UITableViewCell

-(void)bindItemWithtimeM:(NSString*)timecurrMark withv_titlet:(NSString*)v_titletext withv_subTitlet:(NSString*)v_subTitletext;

@end

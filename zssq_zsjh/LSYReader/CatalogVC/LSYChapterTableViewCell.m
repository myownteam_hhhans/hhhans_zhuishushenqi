//
//  LYSChapterTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/4/1.
//  Copyright © 2017年 S. All rights reserved.
//

#import "LSYChapterTableViewCell.h"
#import "LSYDottedLine.h"
#import "Common.h"

@interface LSYChapterTableViewCell()

@property(nonatomic,strong) UIView *v_yuan;

@property(nonatomic,strong) UILabel *v_title;

@property(nonatomic,strong) UIView *img_xian;

@property(nonatomic,strong) UIImageView *image_downloadInfo;




@end

@implementation LSYChapterTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        //[self.contentView addSubview:self.v_yuan];
        [self.contentView addSubview:self.v_title];
        [self.contentView addSubview:self.img_xian];
        [self.contentView addSubview:self.image_downloadInfo];
        [self.contentView addSubview:self.indexLabel];
        
    }
    
    return self;
}

#pragma mark - Getter or Setter

-(UIView *)v_yuan{
    if (!_v_yuan) {
         _v_yuan = [[UIView alloc]initWithFrame:CGRectMake(14, 20, 4, 4)];
         _v_yuan.backgroundColor = RGB(153, 153, 153);
         _v_yuan.layer.cornerRadius = 2;
         _v_yuan.backgroundColor = RGB(229, 20, 13);
    }
    
    return _v_yuan;
}

-(UILabel *)v_title{
    if (!_v_title) {
         _v_title = [[UILabel alloc]initWithFrame:CGRectMake([Common get414RadiusByMap:44], 0, self.frame.size.width-50-65, [Common get414RadiusByMap:56])];
         _v_title.textColor = RGB(229, 20, 13);
         _v_title.font = [UIFont systemFontOfSize:[Common get414RadiusByMap:14]];
        [_v_title setTextAlignment:NSTextAlignmentLeft];
    }
    
    return _v_title;
}

-(UIView *)img_xian{
    if (!_img_xian) {
        _img_xian = [[UIView alloc] initWithFrame:CGRectMake([Common get414RadiusByMap:19], 0, [Common get414RadiusByMap:287], 0.5)];
        [_img_xian setBackgroundColor:[Common colorWithHexString:@"#C7C9CB" alpha:1.0]];
    }
    
    return _img_xian;
}

-(void)bindItem:(UIColor*)vYuanColor withTitle:(NSString*)title withTitleColor:(UIColor*)titleColor{
    // 有可能有一个空格或两个空格
    if ([title containsString:@"章"]) {
        NSRange range = [title rangeOfString:@"章"];
        title = [title substringFromIndex:range.location];
    }
    title = [title stringByReplacingOccurrencesOfString:@"章" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    self.backgroundColor = [UIColor clearColor];
    self.v_title.textColor = titleColor;
    self.v_yuan.backgroundColor = vYuanColor;
    [self.v_title setText:title];
    if (self.isFullScreen) {
        _v_title.frame = CGRectMake([Common get414RadiusByMap:44], 0, ScreenSize.width, [Common get414RadiusByMap:56]);
        _img_xian.hidden = YES;
    }
    [self.indexLabel setText:[NSString stringWithFormat:@"%ld",self.tag]];
    
    if (!vYuanColor) {
        self.image_downloadInfo.hidden = NO;
    } else {
        self.image_downloadInfo.hidden = YES;
    }
    
}

- (UIImageView *)image_downloadInfo {
    if (_image_downloadInfo == nil) {
        _image_downloadInfo = [[UIImageView alloc] init];
        [_image_downloadInfo setImage:[UIImage imageNamed:@"chapterListDownloadInfo"]];
        [_image_downloadInfo sizeToFit];
        [_image_downloadInfo setFrame:[Common get414FrameByMap:CGRectMake(20, 20, 17, 15)]];
        [_image_downloadInfo setHidden:YES];
    }
    return _image_downloadInfo;
}
- (UILabel *)indexLabel {
    if (_indexLabel == nil) {
        _indexLabel = [[UILabel alloc] init];
        [_indexLabel setText:[NSString stringWithFormat:@"%ld",self.tag]];
        [_indexLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:12]]];
        [_indexLabel setTextColor:[Common colorWithHexString:@"#9097A0" alpha:1.0]];
        [_indexLabel setFrame:[Common get414FrameByMap:CGRectMake(326 - 36 - 19, 22, 36, 12)]];
    }
    return _indexLabel;
}

@end

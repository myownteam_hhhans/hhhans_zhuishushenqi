//
//  LYSChapterTableViewCell.h
//  ZYNovel
//
//  Created by S on 2017/4/1.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSYChapterTableViewCell : UITableViewCell

@property(nonatomic,assign) BOOL isFullScreen;


@property(nonatomic,strong) UILabel *indexLabel;

-(void)bindItem:(UIColor*)vYuanColor withTitle:(NSString*)title withTitleColor:(UIColor*)titleColor;

@end

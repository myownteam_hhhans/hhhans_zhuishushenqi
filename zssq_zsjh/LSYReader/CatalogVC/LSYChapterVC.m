
//
//  LSYChapterVC.m
//  LSYReader
//

#import "LSYChapterVC.h"
#import "LSYCatalogViewController.h"
#import "LSYDottedLine.h"
#import "LSYChapterTableViewCell.h"
#import "ZY_Common.h"
#import "Common.h"
#import <UIImageView+WebCache.h>



static  NSString *chapterCell = @"chapterCell";
@interface LSYChapterVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
@property (nonatomic,strong) UITableView *tabView;
@property (nonatomic) NSUInteger readChapter;
@property (nonatomic,strong) UIButton *btnQuick;

@property (nonatomic, strong) UIImageView *novelCover;
@property (nonatomic, strong) UILabel *novelTitleLabel;
@property (nonatomic, strong) UILabel *authorNameLabel;

@property (nonatomic, strong) UIView *bottomView;

@property (nonatomic,assign) BOOL isScrollToEnd;



@end



@implementation LSYChapterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tabView];
    [self.view addSubview:self.btnQuick];
    [self.view addSubview:self.novelCover];
    [self.view addSubview:self.authorNameLabel];
    [self.view addSubview:self.novelTitleLabel];
    [self.view addSubview:self.bottomView];
     self.isScrollToEnd=YES;
    
    UIColor *color = [Common colorWithHexString:@"#e9e9eb" alpha:1.0];
    NSNumber *themeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
    switch (themeNumber.integerValue) {
        case 1:
            color = [Common colorWithHexString:@"#e9e9eb" alpha:1.0];
            break;
        case 2:
            color = [Common colorWithHexString:@"#f4efdc" alpha:1.0];
            break;
        case 3:
            color = [Common colorWithHexString:@"#c9ebc9" alpha:1.0];
            break;
        case 4:
            color = RGB(15, 15, 15);
            break;
            
        default:
            break;
    }
    [self.view setBackgroundColor:color];
    
    [self addObserver:self forKeyPath:@"readModel.record.chapter" options:NSKeyValueObservingOptionNew context:NULL];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reSetFirstDisplay) name:@"reSetChapterFirstDisplay" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeTheme) name:@"changeTheme" object:nil];
    

}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    [_tabView reloadData];
    
}

- (void)changeTheme {
    UIColor *color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
    UIColor *viewColor = [Common colorWithHexString:@"#e9e9eb" alpha:1.0];
    NSNumber *themeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
    switch (themeNumber.integerValue) {
        case 1:
            color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
            viewColor = [Common colorWithHexString:@"#e9e9eb" alpha:1.0];
            break;
        case 2:
            color = [Common colorWithHexString:@"#fbf6e8" alpha:1.0];
            viewColor = [Common colorWithHexString:@"#f4efdc" alpha:1.0];
            break;
        case 3:
            color = [Common colorWithHexString:@"#d1eecf" alpha:1.0];
            viewColor = [Common colorWithHexString:@"#c9ebc9" alpha:1.0];
            break;
        case 4:
            color = RGB(29, 29, 29);
            viewColor = RGB(15, 15, 15);
            break;
            
        default:
            break;
    }
    [_bottomView setBackgroundColor:color];
    [self.view setBackgroundColor:viewColor];
}

-(UIButton *)btnQuick{
    if (!_btnQuick) {
         _btnQuick = [[UIButton alloc] initWithFrame:CGRectZero]; 
        [_btnQuick setImage:[UIImage imageNamed:@"向下"] forState:UIControlStateNormal];
        [_btnQuick addTarget:self action:@selector(clickEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _btnQuick;
}

-(UITableView *)tabView
{
    if (!_tabView) {
        _tabView = [[UITableView alloc] init];
        _tabView.delegate = self;
        _tabView.dataSource = self;
        _tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
       [_tabView registerClass:[LSYChapterTableViewCell class] forCellReuseIdentifier:chapterCell];
        
        
        
        [_tabView setBackgroundColor:[UIColor clearColor]];
        
    }
    return _tabView;
}
- (void)reSetFirstDisplay{
    _firstDisplay = YES;
    [_tabView reloadData];
}

#pragma mark -PrivateFunction

-(void)clickEvent:(id)sender{
    if (self.isScrollToEnd)
    {
       [self.tabView setContentOffset:CGPointMake(0, self.tabView.contentSize.height - self.tabView.bounds.size.height) animated:YES];
    }else{
       [self.tabView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

- (void)fixStatusHeight:(UIView *)view {
    CGFloat fix = 20;
    if (isIPhoneX) {
        fix = 44;
    }
    CGRect frame = view.frame;
    frame.origin.y += fix;
    view.frame = frame;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

    CGPoint contentOffsetPoint = self.tabView.contentOffset;
    CGRect frame = self.tabView.frame;
    if (contentOffsetPoint.y == self.tabView.contentSize.height - frame.size.height || self.tabView.contentSize.height < frame.size.height)
    {
        [self.btnQuick setImage:[UIImage imageNamed:@"向上"] forState:UIControlStateNormal];
         self.isScrollToEnd=NO;
    }else{
        [self.btnQuick setImage:[UIImage imageNamed:@"向下"] forState:UIControlStateNormal];
         self.isScrollToEnd=YES;
    }
}

#pragma mark - UITableView Delagete DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (_firstDisplay) {
        _firstDisplay = NO;
        CGFloat offsetY = _readModel.record.chapter * [Common get414RadiusByMap:56] - tableView.frame.size.height/2;
        if (offsetY > _readModel.chapters.count * [Common get414RadiusByMap:56] - tableView.frame.size.height) {
            offsetY = _readModel.chapters.count * [Common get414RadiusByMap:56] - tableView.frame.size.height;
        } else if (offsetY < 0) {
            offsetY = 0;
        }
        tableView.contentOffset = CGPointMake(0,offsetY);
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _readModel.chapters.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:chapterCell];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *nsstrTitle =_readModel.chapters[indexPath.row].title;
    NSString *content = _readModel.chapters[indexPath.row].content;
    UIColor *bgColor =RGB(153, 153, 153);
    UIColor *titleColor =RGB(102, 102, 102);
    
    if (content && ![content isEqualToString:@""]) {
         bgColor= nil;
    }
    
    if (indexPath.row == _readModel.record.chapter) {
        titleColor = RGB(229, 20, 13);
        bgColor = RGB(229, 20, 13);
    }
    cell.tag = indexPath.row + 1;
    [(LSYChapterTableViewCell*)cell bindItem:bgColor withTitle:nsstrTitle withTitleColor:titleColor];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  [Common get414RadiusByMap:56];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    if ([self.delegate respondsToSelector:@selector(catalog:didSelectChapter:page:)]) {
        [self.delegate catalog:nil didSelectChapter:indexPath.row page:0];
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [self removeObserver:self forKeyPath:@"readModel.record.chapter"];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _tabView.frame = CGRectMake(0, [Common get414RadiusByMap:119], ViewSize(self.view).width, ViewSize(self.view).height - [Common get414RadiusByMap:119] - self.bottomView.frame.size.height - (isIPhoneX ? 44 : 20));
    self.btnQuick.frame=CGRectMake(ViewSize(self.view).width-46, ViewSize(self.view).height-36, 36,36);
    [self fixStatusHeight:_tabView];
}

#pragma mark - lazyload
- (UIImageView *)novelCover {
    if (_novelCover == nil) {
        _novelCover = [[UIImageView alloc] init];
        [_novelCover setFrame:[Common get414FrameByMap:CGRectMake(19, 30, 48, 69)]];
        if (self.readModel.novelInfo.novelCoverUrl && self.readModel.novelInfo.novelCoverUrl.length >0) {
            [_novelCover sd_setImageWithURL:[NSURL URLWithString:self.readModel.novelInfo.novelCoverUrl]];
        }
        [self fixStatusHeight:_novelCover];
    }
    return _novelCover;
}
- (UILabel *)novelTitleLabel {
    if (_novelTitleLabel == nil) {
        _novelTitleLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(84, 43, 414 - 84, 17)]];
        [_novelTitleLabel setText:self.readModel.novelInfo.novelName];
        [_novelTitleLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:17]]];
        [_novelTitleLabel setTextAlignment:NSTextAlignmentLeft];
        [_novelTitleLabel setTextColor:[Common colorWithHexString:@"#1F262F" alpha:1.0]];
        [self fixStatusHeight:_novelTitleLabel];
    }
    return _novelTitleLabel;
}
- (UILabel *)authorNameLabel {
    if (_authorNameLabel == nil) {
        _authorNameLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(84, 72, 414 - 84, 14)]];
        [_authorNameLabel setText:self.readModel.novelInfo.novelAuthor];
        [_authorNameLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:14]]];
        [_authorNameLabel setTextAlignment:NSTextAlignmentLeft];
        [_authorNameLabel setTextColor:[Common colorWithHexString:@"#7D848D" alpha:1.0]];
        [self fixStatusHeight:_authorNameLabel];
    }
    return _authorNameLabel;
}
- (UIView *)bottomView {
    if (_bottomView == nil) {
        
        CGFloat bottomHeight = [Common get414RadiusByMap:55];
        if (isIPhoneX) {
            bottomHeight = [Common get414RadiusByMap:55] + 34;
        }
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height  - bottomHeight, self.view.frame.size.width, bottomHeight)];
        
        UIColor *color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
        NSNumber *themeNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"readerThemeColor"];
        switch (themeNumber.integerValue) {
            case 1:
                color = [Common colorWithHexString:@"#efeff0" alpha:1.0];
                break;
            case 2:
                color = [Common colorWithHexString:@"#fbf6e8" alpha:1.0];
                break;
            case 3:
                color = [Common colorWithHexString:@"#d1eecf" alpha:1.0];
                break;
            case 4:
                color = RGB(29, 29, 29);
                break;
                
            default:
                break;
        }
        [_bottomView setBackgroundColor:color];
        
        [_bottomView addSubview:({
            UILabel *catListLabel = [[UILabel alloc] init];
            [catListLabel setText:@"目录"];
            [catListLabel setFont:[UIFont systemFontOfSize:15]];
            [catListLabel setTextColor:[Common colorWithHexString:@"#1A80E0" alpha:1.0]];
            [catListLabel sizeToFit];
            [catListLabel setCenter:CGPointMake([Common get414RadiusByMap:326]/2, [Common get414RadiusByMap:22+6.5f])];
            catListLabel;
        })];
    }
    return _bottomView;
}

@end

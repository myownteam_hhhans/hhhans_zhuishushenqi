//
//  UIImage+ImgSize.h
//  ZYNovel
////

#import <UIKit/UIKit.h>

@interface UIImage (ImgSize)
+ (CGSize)getImageSizeWithURL:(id)URL;
@end

//
//  AppDelegate.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "AppDelegate.h"
#import "ZY_XPathParserConfig.h"
#import "ZY_SettingViewController.h"
#import "ZY_BookShelfViewController.h"
#import "ZY_SettingViewController.h"
#import "ZY_BookDetailsViewController.h"
#import "ZY_Common.h"
#import "ZY_DBHelper.h"
#import "SHW_ADInterstitial.h"
#import "CFT_parameterOnline.h"
#import "SHW_ADBombBox.h"
#import "ZY_BookHelp.h"
#import <objc/runtime.h>
#import "SVProgressHUD.h"
#import "LSYReadPageViewController.h"
#import "ZY_XPathParser.h"
#import "CFT_parameterOnline.h"
#import "Reachability.h"
#import "ZY_ADHelper.h"
#import "ZY_JsonParser.h"
#import "ZY_BookStoreViewController.h"
#import "ZY_BookCategoryViewController.h"
#import "ZY_BookCatalogViewController.h"
#import "ZY_SecondCategoryController.h"
#import "UIImageView+WebCache.h"
#import <MJRefresh.h>
#import <MTGSDK/MTGSDK.h>
#import "MTGNativeVideoView.h"
#import "NWS_httpClient.h"
#import "Common.h"
#import "ABP_GuideController.h"
#import "BS_MainBridgeController.h"
#import <IIViewDeckController.h>
#import <BUAdSDK/BUAdSDK.h>
#import "GDTNativeAd.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "CustumTabbarController.h"
#import "UMOnlineConfig.h"


@interface AppDelegate ()<UITabBarControllerDelegate,ABP_GuideControllerDelegate,BUSplashAdDelegate,GDTNativeAdDelegate,GADInterstitialDelegate>

@property(nonatomic,strong) ZY_BookShelfViewController *bookShlfVC;

@property(nonatomic,strong) SHW_ADBombBox *shw_AdBomBox;

@property(nonatomic,strong) UIImageView *img_qidong;
@property(nonatomic,strong) UIImageView *M_img_qidong;
@property (nonatomic, strong) UIImageView *nuomi_img_qidong;
@property (nonatomic, strong) UIImageView *google_img_qidong; // 谷歌开屏

// 谷歌广告模板view
@property(nonatomic, strong) GADBannerView *bannerView;
@property(nonatomic, strong) GADInterstitial *interstitial; // 谷歌插屏

@property(nonatomic,strong) ADModel *model;

@property(nonatomic,assign) BOOL isShowAd;

@property (nonatomic, strong) MTGNativeAdManager *kaipingManager;
@property (nonatomic, strong) MTGCampaign *kaipingCampaign;
@property (nonatomic, strong) MTGNativeVideoView *kaipingVideoView;

@property (nonatomic, strong) GDTNativeAd *GDTAd;
@property (nonatomic, strong) NSArray *GDTDataArray;

@end

@implementation AppDelegate

// 谷歌插屏代理
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self.window.rootViewController];
    }
}

// 根据在线参数加载开屏广告
- (void)loadAllSplashWithGreenData:(NSString*)greenData {
    
    if (![Common shareCommon].zs_Open) {
        return;
    }

    if (![[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"screen"]) {
        return;
    }

    int splashAdType = greenData.intValue;
    self.isShowAd = YES;
    
    splashAdType = 1;
    
    if (splashAdType == 0) {
        //自营广告 APPID
        [SHW_ADInterstitial showAdsWithAppId:kAdId withIsAsdMobInterstitial:NO withWindow:self.window];
    }else if (splashAdType == 1) {
        //GoogleAdMob
        //[SHW_ADInterstitial showAdsWithAppId:kAdId withIsAsdMobInterstitial:YES withWindow:self.window];
//        self.bannerView = [[GADBannerView alloc]
//                           initWithAdSize:kGADAdSizeBanner];
//        [self.bannerView setFrame:[Common get414FrameByMap:CGRectMake(55, 175, 300, 250)]];
//        [self.google_img_qidong addSubview:self.bannerView];
//        self.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
//        self.bannerView.rootViewController = self.window.rootViewController;
//        [self.bannerView loadRequest:[GADRequest request]];
        //[self.window addSubview:self.google_img_qidong];
        //[self performSelector:@selector(hideQiDong) withObject:nil afterDelay:4.0];
        self.interstitial = [[GADInterstitial alloc]
                             initWithAdUnitID:@"ca-app-pub-9664827263749384/5518532094"];
        self.interstitial.delegate = self;
        GADRequest *request = [GADRequest request];
        [self.interstitial loadRequest:request];
        [MobClick event:@"Google_show" label:@"展示_开屏"];
        
    }else if (splashAdType == 2) {
        //先自营，后出Google插屏
        [SHW_ADInterstitial showAdsWithAppId:kAdId withWindow:self.window];
    }
    else if (splashAdType == 3) {
        //广点通
        // 请求广点通开屏
        NSDictionary *gdtKeyAndID = [[ZY_ADHelper shareADHelper] getGDTKeyWithADPlace:@"screen"];
        self.GDTAd = [[GDTNativeAd alloc] initWithAppkey:gdtKeyAndID[@"ID"] placementId:gdtKeyAndID[@"KEY"]];
        self.GDTAd.delegate = self;
        self.GDTAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
        [self.GDTAd loadAd:3];
        
        [self.window addSubview:self.img_qidong];
        [self performSelector:@selector(hideQiDong) withObject:nil afterDelay:4.0];
        
    } else if (splashAdType == 4) {
        //头条
       
        CGRect frame = [UIScreen mainScreen].bounds;
        BUSplashAdView *splashView = [[BUSplashAdView alloc] initWithSlotID:TouTiao_Splash frame:frame];
        splashView.delegate = self;
        UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
        [splashView loadAdData];
        [keyWindow.rootViewController.view addSubview:splashView];
        splashView.rootViewController = keyWindow.rootViewController;

    } else if (splashAdType == 5) {
        [self.window addSubview:self.nuomi_img_qidong];
        [self performSelector:@selector(hideQiDong) withObject:nil afterDelay:5.0];
        // 展示汇报
        NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiKaiPing;
        [[ZY_ADHelper shareADHelper] upLoadLomiShowWithURL:kaipingDic[@"count_url"]];
    }
    
}

-(void)hideQiDong{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.img_qidong.hidden = YES;
        self.M_img_qidong.hidden = YES;
        self.nuomi_img_qidong.hidden = YES;
        self.google_img_qidong.hidden = YES;
    });
}


-(void)addMonetizationAD:(NSNotification *)not {
    
    [self.window bringSubviewToFront:self.M_img_qidong];
    
    for (UIView *subView in self.M_img_qidong.subviews) {
        [subView removeFromSuperview];
    }
    
    self.kaipingManager = ((NSArray *)not.object)[1];
    self.kaipingCampaign = ((NSArray *)not.object)[0];
    
    UINib *nib = [UINib nibWithNibName:@"MTGNativeVideoView" bundle:nil];
    self.kaipingVideoView = [[nib instantiateWithOwner:nil options:nil] firstObject];
    [self.kaipingVideoView setFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight)];
    [self.kaipingVideoView updateCellWithCampaign:self.kaipingCampaign unitId:M_UnitID_Open];
    
    CGFloat scale = kCScreenWidth / self.kaipingVideoView.MTGMediaView.frame.size.width;
    [self.kaipingVideoView.MTGMediaView setFrame:CGRectMake(0, 0, self.kaipingVideoView.MTGMediaView.frame.size.width * scale, self.kaipingVideoView.MTGMediaView.frame.size.height * scale)];
    
    [self.kaipingVideoView.iconImageView setFrame:CGRectMake(kCScreenWidth/2 - 150/2, CGRectGetMaxY(self.kaipingVideoView.MTGMediaView.frame) + 30, 130, 130)];
    self.kaipingVideoView.iconImageView.layer.cornerRadius = 15;
    
    [self.kaipingVideoView.appNameLabel setFont:[UIFont systemFontOfSize:20  weight:UIFontWeightBold]];
    [self.kaipingVideoView.appNameLabel sizeToFit];
    [self.kaipingVideoView.appNameLabel setCenter:CGPointMake(kCScreenWidth/2, CGRectGetMaxY(self.kaipingVideoView.iconImageView.frame)+40)];
    
    [self.kaipingVideoView.appDescLabel setFont:[UIFont systemFontOfSize:17 ]];
    self.kaipingVideoView.appDescLabel.numberOfLines = 4;
    [self.kaipingVideoView.appDescLabel sizeToFit];
    
    [self.kaipingVideoView.appDescLabel setCenter:CGPointMake(kCScreenWidth/2, CGRectGetMaxY(self.kaipingVideoView.appNameLabel.frame)+60)];
    
    [self.kaipingVideoView.adCallButton setCenter:CGPointMake(kCScreenWidth/2, CGRectGetMaxY(self.kaipingVideoView.appDescLabel.frame)+40)];
    [self.kaipingVideoView.adCallButton setTitle:@"立即体验" forState:UIControlStateNormal];
    [self.M_img_qidong addSubview:self.kaipingVideoView];
}

-(void)addNuomiKaiping {
    
    for (UIView *subView in self.nuomi_img_qidong.subviews) {
        [subView removeFromSuperview];
    }
    NSDictionary *nuomiDic = [ZY_ADHelper shareADHelper].lomiKaiPing;

    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight/4*3)];
    [image sd_setImageWithURL:[NSURL URLWithString:nuomiDic[@"imgurl"]]];
    [self.nuomi_img_qidong addSubview:image];
    [image setUserInteractionEnabled:YES];
    
    [image addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpLuomiAD)]];
}
- (void)jumpLuomiAD {
    NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiKaiPing;
    NSString *clickURL = kaipingDic[@"click_url"];
    [[NWS_httpClient sharedInstance] getWithURLString:clickURL parameters:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kaipingDic[@"gotourl"]]];
}

-(void)addGDTKaiPing{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"_iapManager"]) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.model) {
            float imageScale = self.model.height/self.model.width;
            UIImageView *img_big = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenWidth*imageScale)];
            [img_big sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:self.model.adImgPath] placeholderImage:nil options:SDWebImageRetryFailed progress:nil completed:nil];
            [self.img_qidong addSubview:img_big];
            
            UIImageView *img_icon = [[UIImageView alloc]initWithFrame:CGRectMake((kCScreenWidth-130)/2, img_big.frame.size.height + 30, 130, 130)];
            [img_icon sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:self.model.adIconImgPath] placeholderImage:nil options:SDWebImageRetryFailed progress:nil completed:nil];
            img_icon.layer.cornerRadius = 8;
            img_icon.layer.masksToBounds = YES;
            [self.img_qidong addSubview:img_icon];
            
            UILabel *lbl_title = [[UILabel alloc]initWithFrame:CGRectMake(0, img_icon.frame.origin.y+img_icon.frame.size.height+20, kCScreenWidth, 40)];
            lbl_title.textAlignment = NSTextAlignmentCenter;
            lbl_title.text = self.model.name;
            lbl_title.font = [UIFont boldSystemFontOfSize:20];
            [self.img_qidong addSubview:lbl_title];
            
            UILabel *lbl_info = [[UILabel alloc]initWithFrame:CGRectMake(20, lbl_title.frame.origin.y+lbl_title.frame.size.height+20, kCScreenWidth-40, 60)];
            lbl_info.textAlignment = NSTextAlignmentCenter;
            lbl_info.text = self.model.appSummary;
            lbl_info.font = [UIFont systemFontOfSize:18];
            lbl_info.numberOfLines = 0;
            [self.img_qidong addSubview:lbl_info];
            
            UIButton *btn_tiyan = [[UIButton alloc]initWithFrame:CGRectMake(40, lbl_info.frame.origin.y+lbl_info.frame.size.height+10, kCScreenWidth-80, 50)];
            [btn_tiyan setTitle:@"立即体验" forState:UIControlStateNormal];
            [btn_tiyan setBackgroundColor:RGB(228, 102, 107)];
            btn_tiyan.layer.cornerRadius = 8;
            btn_tiyan.layer.masksToBounds = YES;
            [btn_tiyan addTarget:self action:@selector(clickGDTAD) forControlEvents:UIControlEventTouchUpInside];
            [self.img_qidong addSubview:btn_tiyan];
            
            UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [closeButton setFrame:[Common get414FrameByMap:CGRectMake(328, 18, 76, 30)]];
            if (isIPhoneX) {
                [closeButton setFrame:[Common get414FrameByMap:CGRectMake(328, 38, 76, 30)]];
            }
            [closeButton.layer setCornerRadius:[Common get414RadiusByMap:11]];
            [closeButton setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
            [closeButton setTitle:@"跳过" forState:UIControlStateNormal];
            [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [closeButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
            [closeButton addTarget:self action:@selector(hideQiDong) forControlEvents:UIControlEventTouchUpInside];
            [_img_qidong addSubview:closeButton];
    
            [[ZY_ADHelper shareADHelper].nativeAd  attachAd:self.model.data toView:self.img_qidong];
            
        }
    });
}

-(void)loadTabbarController{
    
    // 引导页
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"guideVCOpened"]) {
        [self loadGuideController];
        return;
    }
    
    
    bool isCloseChangLiangScreen = [[NSUserDefaults standardUserDefaults] boolForKey:kCloseChangLiang];
    
    if (!isCloseChangLiangScreen) {
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
    
    CustumTabbarController *tabController = [[CustumTabbarController alloc] init];
    [tabController setDelegate:self];
    [tabController.tabBar setBackgroundImage:[ZY_Common imageWithColor:[UIColor whiteColor] WithFrame:CGRectMake(0, 0, kCScreenWidth, 1)]];
    [tabController.tabBar setShadowImage:[ZY_Common imageWithColor:kTabbarShadowColor WithFrame:CGRectMake(0, 0, kCScreenWidth, 0.5)]];
    [tabController.tabBar setTranslucent:NO];
    tabController.tabBar.barTintColor =[UIColor whiteColor];
    tabController.tabBar.tintColor = kTabbarSelectedColor;
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSForegroundColorAttributeName :kTabbarSelectedColor
                                                        } forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSForegroundColorAttributeName :kRGBCOLOR(102, 102, 102)
                                                        } forState:UIControlStateNormal];
    
    self.bookShlfVC = [[ZY_BookShelfViewController alloc] init];
    UINavigationController *bookShlfNavController = [ZY_Common createNavTabBarController:self.bookShlfVC withNavBgColor:kNavigationBgColor withImage:@"书架" withSelectImage:@"书架_Click" withTitle:@"书架"];
    
    ZY_BookStoreViewController *bookStoreVC = [[ZY_BookStoreViewController alloc] init];
    UINavigationController *bookStoreNavController = [ZY_Common createNavTabBarController:bookStoreVC withNavBgColor:kNavigationBgColor withImage:@"书城" withSelectImage:@"书城_Click" withTitle:@"排行"];
    
    BS_MainBridgeController *bookStore = [[BS_MainBridgeController alloc] init];
    UINavigationController *bookStoreNav= [ZY_Common createNavTabBarController:bookStore withNavBgColor:[UIColor whiteColor] withImage:@"新书城" withSelectImage:@"新书城_Click" withTitle:@"书城"];

    
    // 第一种分类
    ZY_BookCategoryViewController *bookCateVC = [[ZY_BookCategoryViewController alloc] init];
    UINavigationController *bookCateNavController = [ZY_Common createNavTabBarController:bookCateVC withNavBgColor:kNavigationBgColor withImage:@"分类" withSelectImage:@"分类_Click" withTitle:@"分类"];
    
    // 第二种样式的分类
    ZY_SecondCategoryController *secondBookCateVC = [[ZY_SecondCategoryController alloc] init];
    UINavigationController *secondbookCateNavController = [ZY_Common createNavTabBarController:secondBookCateVC withNavBgColor:kNavigationBgColor withImage:@"分类" withSelectImage:@"分类_Click" withTitle:@"分类"];

    
    NSMutableArray *arrayControllers = [[NSMutableArray alloc] initWithObjects:bookShlfNavController,bookStoreNavController,bookStoreNav,secondbookCateNavController, nil];
    
    NSString *type = [[NSUserDefaults standardUserDefaults] objectForKey:@"categoryType"];
    
    if ([type isEqualToString:@"1"]) {
        arrayControllers = [[NSMutableArray alloc] initWithObjects:bookShlfNavController,bookStoreNavController,bookStoreNav,bookCateNavController, nil];
    }
    
    // 判断是否添加排行
    NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"parameterOnline"];
    NSString *paihang = [dic objectForKey:[NSString stringWithFormat:@"Ran_Type%@",kAPP_VERSION]];
    if ([paihang isEqualToString:@"0"]) {
        [arrayControllers removeObjectAtIndex:1];
    }
    
    [tabController setViewControllers:arrayControllers];
    
    // 抽屉效果
    ZY_SettingViewController *seetingVC = [[ZY_SettingViewController alloc] init];
    UINavigationController *seetingNavController = [ZY_Common createNavTabBarController:seetingVC withNavBgColor:kNavigationBgColor withImage:@"我的" withSelectImage:@"我的_Click" withTitle:@"我的"];
    [seetingNavController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    seetingNavController.preferredContentSize = CGSizeMake(k_ScreenWidth/414*332, k_ScreenHeight);
    seetingNavController.navigationBar.hidden = YES;
    IIViewDeckController *deckVC = [[IIViewDeckController alloc] initWithCenterViewController:tabController leftViewController:seetingNavController];

    [self.window setRootViewController:deckVC];

    self.bookShlfVC.navigationItem.leftBarButtonItem = [self getLeftBarButton];
    bookStore.navigationItem.leftBarButtonItem = [self getLeftBarButton];
    bookStoreVC.navigationItem.leftBarButtonItem = [self getLeftBarButton];
    bookCateVC.navigationItem.leftBarButtonItem = [self getLeftBarButton];
    secondBookCateVC.navigationItem.leftBarButtonItem = [self getLeftBarButton];
    
}

- (UIBarButtonItem *)getLeftBarButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 35, 35)];
    [button setImage:[UIImage imageNamed:@"leftSettingItem"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(showLeftVC) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    return item;
}

- (void)loadGuideController {
    ABP_GuideController  *guideVC = [[ABP_GuideController alloc] init];
    guideVC.delegate=self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:guideVC];
    [self.window setRootViewController:nav];
}

- (void)showLeftVC {
    IIViewDeckController *iivc = (IIViewDeckController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    [iivc openSide:IIViewDeckSideLeft animated:YES];
    [MobClick event:@"TABbar" label:@"我的"];
}

- (void)tuiGuang{
    NSString *updateStr = [CFT_parameterOnline getValueByKey:[NSString stringWithFormat:@"内部推送%@",kAPP_VERSION]];
    [self.shw_AdBomBox showAdBombBox:updateStr];
}

#pragma mark - Life cycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UMOnlineConfig updateOnlineConfigWithAppkey:kUMKey];
    // 初始化M广告
    [[MTGSDK sharedInstance] setAppID:M_APPID ApiKey:M_APPKey];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addMonetizationAD:) name:@"GetAMTGCampaign" object:nil];
    // 初始化洛米广告一次
    [[ZY_ADHelper shareADHelper] requestLomiPic];
    [[ZY_ADHelper shareADHelper] requestLomiGif];
    [[ZY_ADHelper shareADHelper] requestLomiKaiPing];
    // 初始化头条广告
    [BUAdSDKManager setAppID:TouTiao_AppID];
    [BUAdSDKManager setIsPaidApp:NO];
    [BUAdSDKManager setLoglevel:BUAdSDKLogLevelDebug];
    // 初始化谷歌
    //[GADMobileAds configureWithApplicationID:@"ca-app-pub-3940256099942544~1458002511"];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"lastGDTAdSuccessToLoad"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addGDTKaiPing) name:@"nativeAdSuccessToLoad" object:nil];
    //[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hideQiDong) name:@"nativeAdErrorToLoad" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNuomiKaiping) name:@"getNuomiKaiping" object:nil];
    
    // Override point for customization after application launch.
    [ZY_XPathParserConfig shareXPathParserConfig];
    [ZY_DBHelper shareDBHelper];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    self.shw_AdBomBox = [[SHW_ADBombBox alloc] init];
    [ZY_Common settingReadBookUseTime];
    
    [UMConfigure initWithAppkey:kUMKey channel:nil];
    if ([UIDevice currentDevice].systemVersion.floatValue < 11.0f) {
        [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-100,-100) forBarMetrics:UIBarMetricsDefault];
    }
    UIViewController *vc = [[UIViewController alloc] init];
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
    [ZY_ADHelper shareADHelper].isNeedNoti = YES;
    [[ZY_ADHelper shareADHelper] requestADdata];
    [[ZY_ADHelper shareADHelper] getCampaignWithID:M_UnitID_Open];
    __weak __typeof(&*self)weakSelf = self;
    
    NSDictionary *dict = [UMOnlineConfig getConfigParams];
    if (dict) {
        NSString *reviewOpen = [dict objectForKey:[NSString stringWithFormat:@"switch_%@",kAPP_VERSION]];
        NSString *categoryType = [dict objectForKey:[NSString stringWithFormat:@"class_Type%@",kAPP_VERSION]];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"parameterOnline"];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:reviewOpen forKey:@"isOpenReview"];
        [[NSUserDefaults standardUserDefaults] setObject:categoryType forKey:@"categoryType"];
        
        [weakSelf loadTabbarController];
        NSString *adType = [[Common shareCommon] randomADTypeWithKey:@"screen"];
        [weakSelf loadAllSplashWithGreenData:adType];
        
        [Common showDebugHUD:[NSString stringWithFormat:@"开屏：%@",adType]];
    } else {
        [weakSelf loadTabbarController];
    }

    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoReadByNovel:) name:@"gotoReadByNovel" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoReadFromCatelog:) name:@"gotoReadFromCatelog" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoCatelog:) name:@"gotoCatelog" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(downLoadNovelSuccess:) name:@"downLoadNovelSuccess" object:nil];
    
    return YES;
}


-(void)downLoadNovelSuccess:(NSNotification *)not{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"系统提示" message:[not object] delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles: nil];
    [alert show];
}
static BOOL isGotoReadNovel = NO;
-(void)gotoReadByNovel:(NSNotification *)not{
    if (isGotoReadNovel) {
        return;
    }
    [ZY_ADHelper shareADHelper].bottomADType = [[Common shareCommon] randomADTypeWithKey:@"readerbottom"];
    [ZY_ADHelper shareADHelper].chapterADType = [[Common shareCommon] randomADTypeWithKey:@"readpage"];
    [ZY_ADHelper shareADHelper].bannerADType = [[Common shareCommon] randomADTypeWithKey:@"bookbanner"];
    
    // 请求阅读器广告
    [[ZY_ADHelper shareADHelper] requestBanner];
    [[ZY_ADHelper shareADHelper] requestChapterAD];
    
    isGotoReadNovel = !isGotoReadNovel;
    [SVProgressHUD showWithStatus:@"加载中..."];

    LSYReadPageViewController *pageView = [[LSYReadPageViewController alloc] init];
    ZY_NovelInfo *NovelInfo = [not object];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        pageView.model = [LSYReadModel getLocalModelWithNovel:NovelInfo];
        if (pageView.model.chapters.count == 0) {

            NSString *tmpContent = [LSYReadUtilites encodeWithURL:[NSURL URLWithString:pageView.model.novelInfo.novelListUrl]];
            if (tmpContent) {
                NSData* xmlData = [tmpContent dataUsingEncoding:NSUTF8StringEncoding];
                pageView.model.chapters = [[[ZY_JsonParser alloc]init]getChapterListByData:xmlData];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (pageView.model.chapters.count == 0) {
                [SVProgressHUD showErrorWithStatus:@"获取信息失败!"];
            }else
            {
                [SVProgressHUD dismiss];
                //[self.window.rootViewController presentViewController:pageView animated:YES completion:nil];
                pageView.hidesBottomBarWhenPushed = YES;
                IIViewDeckController *deckVC = self.window.rootViewController;
                UITabBarController *tabVC = deckVC.centerViewController;
                UINavigationController *navVC = tabVC.selectedViewController;
                //[navVC pushViewController:pageView animated:YES];
                
                if (tabVC.selectedIndex == 0 && [navVC.topViewController isKindOfClass:[ZY_BookShelfViewController class]]) {
                    [navVC pushATViewController:pageView animated:YES];
                } else {
                    [navVC pushViewController:pageView animated:YES];
                }

                
            }
            isGotoReadNovel = !isGotoReadNovel;
        });
    });
}

static BOOL isGotoCatelog = NO;

-(void)gotoCatelog:(NSNotification *)not{
    if (isGotoCatelog) {
        return;
    }
    isGotoCatelog = !isGotoCatelog;
    [SVProgressHUD showWithStatus:@"加载中..."];
  
    ZY_BookCatalogViewController *pageView = [[ZY_BookCatalogViewController alloc] init];
    ZY_NovelInfo *NovelInfo = [not object];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        LSYReadModel *model = [LSYReadModel getLocalModelWithNovel:NovelInfo];
        if (model.chapters.count == 0) {
            
            NSString *tmpContent = [LSYReadUtilites encodeWithURL:[NSURL URLWithString:model.novelInfo.novelListUrl]];
            if (tmpContent) {
                NSData* xmlData = [tmpContent dataUsingEncoding:NSUTF8StringEncoding];
                model.chapters = [[[ZY_JsonParser alloc]init]getChapterListByData:xmlData];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (model.chapters.count == 0) {
                [SVProgressHUD showErrorWithStatus:@"获取信息失败!"];
            }else
            {
                [SVProgressHUD dismiss];
                pageView.readModel = model;
                IIViewDeckController *iivc = (IIViewDeckController *)self.window.rootViewController;
                UINavigationController *nav = ((UITabBarController *)iivc.centerViewController).selectedViewController;
                [nav pushViewController:pageView animated:YES];
            }
            isGotoCatelog = !isGotoCatelog;
        });
    });
}


-(void)gotoReadFromCatelog:(NSNotification *)not{
    
    [SVProgressHUD showWithStatus:@"加载中..."];
    LSYReadPageViewController *pageView = [[LSYReadPageViewController alloc] init];
    pageView.isNeedReadProgress = NO;
    pageView.model = [not object];
    [SVProgressHUD dismiss];
    
    IIViewDeckController *deckVC = (IIViewDeckController *)self.window.rootViewController;
    UITabBarController *tabVC = (UITabBarController *)deckVC.centerViewController;
    UINavigationController *navVC = tabVC.selectedViewController;
    //[navVC pushViewController:pageView animated:YES];
    
    if (tabVC.selectedIndex == 0 && [navVC.topViewController isKindOfClass:[ZY_BookShelfViewController class]]) {
        [navVC pushATViewController:pageView animated:YES];
    } else {
        [navVC pushViewController:pageView animated:YES];
    }

    
    
}
- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [UMOnlineConfig updateOnlineConfigWithAppkey:kUMKey];
}

-(BOOL)isNeedRequestGDTAD{
    
    NSDate *oldDate = [[NSUserDefaults standardUserDefaults]objectForKey:@"lastGDTAdSuccessToLoad"];
    NSDate *currentDate = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    unsigned int unitFlags = NSCalendarUnitMinute;
    NSDateComponents *dateComponents = [cal components:unitFlags fromDate:oldDate toDate:currentDate options:0];
    
    float sec = [dateComponents minute];
    if (sec > 30) {
        return YES;
    }
    return NO;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    if (![[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"screen"] || NO) {
        return;
    }
    
    [[ZY_ADHelper shareADHelper] getCampaignWithID:M_UnitID_Open];
    
    if([self isNeedRequestGDTAD]){
      
        [ZY_ADHelper shareADHelper].isNeedNoti = NO;
        [[ZY_ADHelper shareADHelper] requestGDTNativeAD];
    }
    
    NSString *adtype = [[Common shareCommon] randomADTypeWithKey:@"screen"];
    
    [Common showDebugHUD:[NSString stringWithFormat:@"后台进前台:%@",adtype]];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
    
    NSString *greenData = [CFT_parameterOnline getValueByKey:[NSString stringWithFormat:@"GreenData_%@", kAPP_VERSION]];
    if (greenData != nil && ![greenData isEqualToString:@"0"] && [adtype isEqualToString:@"3"]) {
        if (![self.window viewWithTag:9999]) {
            [self.window addSubview:self.img_qidong];
        }
        for (UIView *subView in self.img_qidong.subviews) {
            [subView removeFromSuperview];
        }
        self.img_qidong.hidden = NO;
        self.model = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_Feed];
        [self addGDTKaiPing];

        [self performSelector:@selector(hideQiDong) withObject:nil afterDelay:3.0];
   
    }
    else if (greenData != nil && ![greenData isEqualToString:@"0"]   && [adtype isEqualToString:@"4"]) {
        
        CGRect frame = [UIScreen mainScreen].bounds;
        BUSplashAdView *splashView = [[BUSplashAdView alloc] initWithSlotID:@"900721489" frame:frame];
        splashView.delegate = self;
        UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
        [splashView loadAdData];
        [keyWindow.rootViewController.view addSubview:splashView];
        splashView.rootViewController = keyWindow.rootViewController;
   
        
    } else if (greenData != nil && ![greenData isEqualToString:@"0"]   && [adtype isEqualToString:@"5"]) {
        if (![self.window viewWithTag:7777]) {
            [self.window addSubview:self.nuomi_img_qidong];
        }
        [[ZY_ADHelper shareADHelper] requestLomiKaiPing];
        self.nuomi_img_qidong.hidden = NO;
        [self performSelector:@selector(hideQiDong) withObject:nil afterDelay:4.0];
        
        // 展示汇报
        NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiKaiPing;
        [[ZY_ADHelper shareADHelper] upLoadLomiShowWithURL:kaipingDic[@"count_url"]];
        
    } else if (greenData != nil && ![greenData isEqualToString:@"0"]   && [adtype isEqualToString:@"1"]) {
        self.interstitial = [[GADInterstitial alloc]
                             initWithAdUnitID:@"ca-app-pub-9664827263749384/5518532094"];
        self.interstitial.delegate = self;
        GADRequest *request = [GADRequest request];
        [self.interstitial loadRequest:request];
        
    }
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
//    [[ZY_BookHelp shareBookHelp] checkBookUpdate];
    [self performSelector:@selector(tuiGuang) withObject:nil afterDelay:8];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - ControllerDelegate
- (void)gotoMainInterface {
    [self loadTabbarController];
}

#pragma mark - 头条代理 广点通代理
- (void)splashAdDidClose:(BUSplashAdView *)splashAd {
    [splashAd removeFromSuperview];
}
- (void)splashAd:(BUSplashAdView *)splashAd didFailWithError:(NSError *)error {
    [splashAd removeFromSuperview];
}
- (void)splashAdDidClick:(BUSplashAdView *)splashAd {
    
}

- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray {
    self.GDTDataArray = nativeAdDataArray;
    self.model = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_Feed];
    [self addGDTKaiPing];
}
- (void)nativeAdFailToLoad:(NSError *)error {
    
}

#pragma mark - UITabBarControllerDelegate

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (tabBarController.selectedIndex == 0) {
        [self.bookShlfVC refreshBookShelf];
    }
    
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        if ([((UINavigationController*)viewController).topViewController isKindOfClass:[ZY_BookShelfViewController class]]) {
            [MobClick event:@"TABbar" label:@"书架"];
        }else if ([((UINavigationController*)viewController).topViewController isKindOfClass:[ZY_BookStoreViewController class]]){
            [MobClick event:@"TABbar" label:@"排行"];
        }else if ([((UINavigationController*)viewController).topViewController isKindOfClass:[ZY_BookCategoryViewController class]]){
            [MobClick event:@"TABbar" label:@"分类"];
        }else if ([((UINavigationController*)viewController).topViewController isKindOfClass:[BS_MainBridgeController class]]){
            [MobClick event:@"TABbar" label:@"书城"];
        }
    }
}
-(UIImageView *)img_qidong{
    if (!_img_qidong) {
        _img_qidong = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight)];
        _img_qidong.image = [UIImage imageNamed:[self getLauchImageNameString]];
        _img_qidong.tag = 9999;
        _img_qidong.userInteractionEnabled = YES;
        [_img_qidong addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickGDTAD)]];
    }
    return _img_qidong;
}
- (UIImageView *)M_img_qidong {
    if (!_M_img_qidong) {
        _M_img_qidong = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight)];
        _M_img_qidong.image = [UIImage imageNamed:[self getLauchImageNameString]];
        _M_img_qidong.tag = 8888;
        _M_img_qidong.userInteractionEnabled = YES;

    }
    return _M_img_qidong;
}

- (UIImageView *)nuomi_img_qidong {
    if (!_nuomi_img_qidong) {
        _nuomi_img_qidong = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight)];
        _nuomi_img_qidong.image = [UIImage imageNamed:[self getLauchImageNameString]];
        _nuomi_img_qidong.tag = 7777;
        _nuomi_img_qidong.userInteractionEnabled = YES;
        
    }
    return _nuomi_img_qidong;
}
- (UIImageView *)google_img_qidong {
    if (!_google_img_qidong) {
        _google_img_qidong = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight)];
        _google_img_qidong.image = [UIImage imageNamed:[self getLauchImageNameString]];
        _google_img_qidong.tag = 1111;
        _google_img_qidong.userInteractionEnabled = YES;
        
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight - [Common get414RadiusByMap:143])];
//        [imageView setImage:[UIImage imageNamed:@"googleSplashBack"]];
//        [_google_img_qidong addSubview:imageView];
//        [imageView setContentMode:UIViewContentModeScaleAspectFill];
//
//        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [closeButton setFrame:[Common get414FrameByMap:CGRectMake(328, 18, 76, 30)]];
//        if (isIPhoneX) {
//            [closeButton setFrame:[Common get414FrameByMap:CGRectMake(328, 38, 76, 30)]];
//        }
//        [closeButton.layer setCornerRadius:[Common get414RadiusByMap:11]];
//        [closeButton setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
//        [closeButton setTitle:@"跳过" forState:UIControlStateNormal];
//        [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [closeButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:18]]];
//        [closeButton addTarget:self action:@selector(hideQiDong) forControlEvents:UIControlEventTouchUpInside];
//        [_google_img_qidong addSubview:closeButton];
        
    }
    return _google_img_qidong;
}

-(void)clickGDTAD{
    if (self.model) {
        [self hideQiDong];
       
        [[ZY_ADHelper shareADHelper]tapAD:self.model CurrentVC:self.window.rootViewController];
    }
}

- (NSString*)getLauchImageNameString{
    NSString *nsstrLauchImageName  = @"LaunchImage";
    NSString    *viewOrientation = nil;
    CGSize     viewSize  = [UIScreen mainScreen].bounds.size;
    UIInterfaceOrientation orientation  = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        viewOrientation = @"Landscape";
    } else {
        viewOrientation = @"Portrait";
    }
    
    NSArray *imagesDictionary = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for (NSDictionary *dict in imagesDictionary) {
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]]) {
            nsstrLauchImageName = dict[@"UILaunchImageName"];
        }
    }
    return nsstrLauchImageName;
}
@end

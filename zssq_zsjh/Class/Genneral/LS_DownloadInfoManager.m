//
//  LS_DownloadInfoManager.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/17.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "LS_DownloadInfoManager.h"


@interface LS_DownloadInfoManager()



@end


@implementation LS_DownloadInfoManager

static LS_DownloadInfoManager *_instance;

#pragma mark ==== 私有方法 ====
- (void)addItemToDownLoadedArray:(LSYReadModel *)bookModel {
    
    [self.downloadingArray removeObject:bookModel];
    
    BOOL hadAdded = NO;
    BOOL isEqual = NO;
    LSYReadModel *addedModel;
    for (LSYReadModel *model in self.downloadedArray) {
        if ([model.bookID isEqualToString:bookModel.bookID]) {
            hadAdded = YES;
            addedModel = model;
        }
        if (model == bookModel) {
            isEqual = YES;
        }
    }
    
    if (hadAdded) {
        addedModel.downloadedNumber  = bookModel.downloadedNumber;
    } else {
        [self.downloadedArray addObject:bookModel];
    }
    
    // 更新一下本地存储
    
    NSArray *array = [NSArray arrayWithArray:self.downloadedArray];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:array];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"downloadedArray"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chapterEndDownload" object:bookModel];
    
}
- (void)addItemToDownLoadingArray:(LSYReadModel *)bookModel {
    
    [self.downloadingArray addObject:bookModel];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chaptersStartDownload" object:bookModel];
}
- (void)cancelItemWithModel:(LSYReadModel *)bookModel {
    [self.downloadingArray removeObject:bookModel];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chapterEndDownload" object:bookModel];
}
- (void)deletaDownloadedItem:(LSYReadModel *)bookModel {
    [self.downloadedArray removeObject:bookModel];
    [LSYReadModel delLocalModelWithNovel:bookModel.novelInfo];
    
    // 更新本地存贮
    NSArray *array = [NSArray arrayWithArray:self.downloadedArray];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:array];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"downloadedArray"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"chapterEndDownload" object:bookModel];
    
}
- (void)shelfDeleteWithBookID:(NSString *)bookId {
    
    for (LSYReadModel *model in self.downloadedArray) {
        if ([model.bookID isEqualToString:bookId]) {
            [self.downloadedArray removeObject:model];
        }
    }
    
}


#pragma mark ==== 实现单例类 ====

+ (instancetype)shareDownloadInfoManager {
    static LS_DownloadInfoManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        
        manager = [[LS_DownloadInfoManager alloc] init];
        manager.downloadingArray = [NSMutableArray array];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"downloadedArray"];
        if (data) {
            
            NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            manager.downloadedArray = [NSMutableArray arrayWithArray:array];
            
        } else {
            manager.downloadedArray = [NSMutableArray array];
        }
        
    });
    return manager;
}
+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_instance == nil) {
            _instance = [super allocWithZone:zone];
        }
    });
    return _instance;
}

-(id)copyWithZone:(NSZone *)zone
{
    return _instance;
}

-(id)mutableCopyWithZone:(NSZone *)zone
{
    return _instance;
}

- (void)setNowPercent:(NSInteger)nowPercent {
    if (_nowPercent == nowPercent) {
        return;
    }
    _nowPercent = nowPercent;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgressChange" object:[NSNumber numberWithInteger:self.nowPercent]];
}

@end

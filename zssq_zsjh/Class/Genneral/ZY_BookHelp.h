//
//  ZY_BookHelp.h
//  ZYNovel
//
//  Created by S on 2017/3/17.
//  Copyright © 2017年 S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LSYChapterModel.h"

#define ZY_CheckBookUpdateDidFinishedNotification @"OnlineConfigDidFinishedNotification"
  
@interface ZY_BookHelp : NSObject

//更新的书籍字典
@property(nonatomic,strong) NSMutableDictionary *dictNovelUpdate;

@property(nonatomic,strong) NSOperationQueue *novelDownLoadQueue;

+(ZY_BookHelp *)shareBookHelp;

/************************************
 函数描述:更新XPath 在线配置文件
 作者:S
 时间:2017-03-17
 ************************************/
- (void)detectConfigurationTemplateUpdate:(NSString *)nsstrJson;

/************************************
 函数描述:检查书籍更新
 作者:S
 时间:2017-03-17
 ************************************/
- (void)checkBookUpdate;
 
@end

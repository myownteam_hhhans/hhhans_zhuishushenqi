//
//  ZY_ADHelper.m
//  ZYNovel
//

#import "ZY_ADHelper.h"
#import "GDTNativeAd.h"
#import "SHW_ADInterstitial.h"
#import "ZY_Common.h"
#import <StoreKit/StoreKit.h>
#import "SVProgressHUD.h"
#import "UIImage+ImgSize.h"
#import "CFT_parameterOnline.h"
#import "ZY_Common.h"
#import "SDWebImageManager.h"
#import "NWS_httpClient.h"
#import <AdSupport/ASIdentifierManager.h>
#import "Common.h"
#import "CFT_parameterOnline.h"


#define kAdLabelHeight [UIFont systemFontOfSize:19.0f].lineHeight *2
#define kAdDownloadHeight 30
#define DOWNLOADSPACE 10
#define kCommonMargin 10.0f
#define isNullOrNil(str) str!=nil&&(NSNull*)str!=[NSNull null]&&![str isEqualToString:@""]
#define NullOrNil(str) str==nil||(NSNull*)str==[NSNull null]||[str isEqualToString:@""]

@implementation ADModel

@end

@implementation BannerADModel

@end


@interface ZY_ADHelper()<GDTNativeAdDelegate,SKStoreProductViewControllerDelegate,MTGNativeAdManagerDelegate,BUNativeAdsManagerDelegate>
{
    
}

@property (nonatomic,assign) BOOL isRequestGDTNativeAd;
@property (nonatomic,strong) UIViewController *iTunesStoreVC;

@property (nonatomic,strong) NSArray *GDTadArray;
@property (nonatomic,strong) NSArray *TouTiaoADArray;

@property (nonatomic,strong) NSArray *GDTScreenArray;
@property (nonatomic,strong) NSArray *GDTbookcoverArray;
@property (nonatomic,strong) NSArray *GDTbookflowArray;
@property (nonatomic,strong) NSArray *GDTDetailsArray;
@property (nonatomic,strong) NSArray *GDTreaderbottomArray;
@property (nonatomic,strong) NSArray *GDTbookbannerArray;
@property (nonatomic,strong) NSArray *GDTreadpageArray;

@property (nonatomic, strong) NSArray *M_BannerArray;
@property (nonatomic, strong) MTGNativeAdManager *M_Banner_Manager;
@property (nonatomic, strong) MTGNativeAdManager *M_Chapter_Manager;
@property (nonatomic, weak) UIViewController *bannerController;

@property (nonatomic, strong) NSDictionary *gdtKeysAndID;

@end
@implementation ZY_ADHelper

+(ZY_ADHelper *)shareADHelper{
    static ZY_ADHelper *zy_adHelp = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        zy_adHelp = [[ZY_ADHelper alloc] init];
    });
    return zy_adHelp;
}

- (void)requestLomiPic {
    
    NSDictionary *parament = @{
                               @"z" : Luomi_Z_Pic,
                               @"appkey" : Luomi_key,
                               @"deviceId" : [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString],
                               @"sw" : [NSString stringWithFormat:@"%f",kCScreenWidth],
                               @"sh" : [NSString stringWithFormat:@"%f",kCScreenHeight],
                               @"osver" : [UIDevice currentDevice].systemVersion
                               };
    
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://sdk.cferw.com/iosapi.php" parameters:parament success:^(NSDictionary *responseObject) {
        
        NSNumber *succNumber = (NSNumber *)responseObject[@"succ"];
        
        if (succNumber.integerValue == 1) {
            self.lomiPic = responseObject;
        }
        
        
    } failure:^(NSError *error) {
        NSLog(@"");
    }];
}

- (void)requestLomiGif {
    
    NSDictionary *parament = @{
                               @"z" : Luomi_Z_Gif,
                               @"appkey" : Luomi_key,
                               @"deviceId" : [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString],
                               @"sw" : [NSString stringWithFormat:@"%f",kCScreenWidth],
                               @"sh" : [NSString stringWithFormat:@"%f",kCScreenHeight],
                               @"osver" : [UIDevice currentDevice].systemVersion
                               };
    
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://sdk.cferw.com/iosapi.php" parameters:parament success:^(NSDictionary *responseObject) {
        
        NSNumber *succNumber = (NSNumber *)responseObject[@"succ"];
        if (succNumber.integerValue == 1) {
            self.lomiGif = responseObject;
        }

    } failure:^(NSError *error) {
        NSLog(@"");
    }];
    
}
- (void)requestLomiKaiPing {
    
    NSDictionary *parament = @{
                               @"z" : Luomi_Z_KaiPing,
                               @"appkey" : Luomi_key,
                               @"deviceId" : [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString],
                               @"sw" : [NSString stringWithFormat:@"%f",kCScreenWidth],
                               @"sh" : [NSString stringWithFormat:@"%f",kCScreenHeight/4*3],
                               @"osver" : [UIDevice currentDevice].systemVersion
                               };
    
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://sdk.cferw.com/iosapi.php" parameters:parament success:^(NSDictionary *responseObject) {
        
        self.lomiKaiPing = responseObject;
        
        NSNumber *succNumber = (NSNumber *)responseObject[@"succ"];
        
        if (succNumber.integerValue == 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getNuomiKaiping" object:nil];
        }
    
        
    } failure:^(NSError *error) {
        NSLog(@"");
    }];
    
}

- (void)requestBanner {
    
    if ([self.bannerADType isEqualToString:@"5"]) {  // 从洛米请求广告
        NSDictionary *parament = @{
                                   @"z" : Luomi_Z_Banner,
                                   @"appkey" : Luomi_key,
                                   @"deviceId" : [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString],
                                   @"sw" : [NSString stringWithFormat:@"%f",kCScreenWidth],
                                   @"sh" : [NSString stringWithFormat:@"%f",kCScreenHeight/4*3],
                                   @"osver" : [UIDevice currentDevice].systemVersion
                                   };
        
        [[NWS_httpClient sharedInstance] getWithURLString:@"http://sdk.cferw.com/iosapi.php" parameters:parament success:^(NSDictionary *responseObject) {
            
            NSNumber *succNumber = (NSNumber *)responseObject[@"succ"];
            if (succNumber.integerValue == 1) {
                
                self.bannerADModel = nil;
                
                self.bannerADModel.bannerURL = responseObject[@"imgurl"];
                self.bannerADModel.showURL = responseObject[@"count_url"];
                self.bannerADModel.jumpURL = responseObject[@"gotourl"];
                self.bannerADModel.clickURL = responseObject[@"click_url"];
                self.bannerADModel.title = responseObject[@"wenzi"];
            }
        } failure:^(NSError *error) {
            NSLog(@"");
        }];
    } else if ([self.bannerADType isEqualToString:@"3"]) { // 从广点通请求广告
        
        if (self.GDTadArray.count <= 0) {
            [self requestGDTNativeAD];
            return;
        }
        
        GDTNativeAdData *data = [self.GDTadArray objectAtIndex:arc4random() % self.GDTadArray.count];
        self.bannerADModel.title = [data.properties objectForKey:GDTNativeAdDataKeyDesc];
        self.bannerADModel.desc = [data.properties objectForKey:GDTNativeAdDataKeyTitle];
        self.bannerADModel.iconURL = [data.properties objectForKey:GDTNativeAdDataKeyIconUrl];
        self.bannerADModel.gdtData = data;

    }
    
}

- (void)requestChapterAD {
    
    if ([self.chapterADType isEqualToString:@"5"]) {  // 从洛米请求广告
        NSDictionary *parament = @{
                                   @"z" : Luomi_Z_Gif,
                                   @"appkey" : Luomi_key,
                                   @"deviceId" : [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString],
                                   @"sw" : [NSString stringWithFormat:@"%f",kCScreenWidth],
                                   @"sh" : [NSString stringWithFormat:@"%f",kCScreenHeight],
                                   @"osver" : [UIDevice currentDevice].systemVersion
                                   };
        
        [[NWS_httpClient sharedInstance] getWithURLString:@"http://sdk.cferw.com/iosapi.php" parameters:parament success:^(NSDictionary *responseObject) {
            
            NSNumber *succNumber = (NSNumber *)responseObject[@"succ"];
            if (succNumber.integerValue == 1) {
                self.ChapterChangeAD.bannerURL = responseObject[@"imgurl"];
                self.ChapterChangeAD.showURL = responseObject[@"count_url"];
                self.ChapterChangeAD.jumpURL = responseObject[@"gotourl"];
                self.ChapterChangeAD.clickURL = responseObject[@"click_url"];
            }
            
        } failure:^(NSError *error) {
            NSLog(@"");
        }];
    }
    // 广点通广告由ADView自己获取
//    else if ([self.chapterADType isEqualToString:@"3"]) { // 从广点通请求广告
//
//        if (self.GDTadArray.count <= 0) {
//            [self requestGDTNativeAD];
//            return;
//        }
//
//        GDTNativeAdData *data = [self.GDTadArray objectAtIndex:arc4random() % self.GDTadArray.count];
//        self.ChapterChangeAD.bannerURL = [data.properties objectForKey:GDTNativeAdDataKeyImgUrl];
//        self.ChapterChangeAD.gdtData = data;
//
//    }
    
}

- (void)registClickWithBannerView:(UIView *)view Controller:(UIViewController *)controller{
    view.userInteractionEnabled = YES;
    
    if ([self.bannerADType isEqualToString:@"3"]) {   // gdt
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GDTBannerClick)];
        [view addGestureRecognizer:tapGes];
        self.bannerController = controller;
        
    } else if ([self.bannerADType isEqualToString:@"4"]) {  // M
        
        [self.M_Banner_Manager registerViewForInteraction:view withCampaign:self.bannerADModel.campaign];
        
    } else if ([self.bannerADType isEqualToString:@"5"]) {  // luomi
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(luomiBannerClick)];
        [view addGestureRecognizer:tapGes];
        if (controller) {
            self.bannerController = controller;
        }
        
    }
}

- (void)uploadBannerShow:(UIView *)bannerView {
    
    if ([self.bannerADType isEqualToString:@"5"]) {
        [self upLoadLomiShowWithURL:self.bannerADModel.showURL];
    } else if ([self.bannerADType isEqualToString:@"3"]) {
        [[ZY_ADHelper shareADHelper].nativeAd  attachAd:self.bannerADModel.gdtData toView:bannerView];
    }
    
}

- (void)registClickWithChapterView:(UIView *)view Controller:(UIViewController *)controller {
    if ([self.chapterADType isEqualToString:@"3"]) {   // gdt
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(GDTChapterClick)];
        [view addGestureRecognizer:tapGes];
        //self.bannerController = controller;
        
    } else if ([self.chapterADType isEqualToString:@"4"]) {  // M
        
        [self.M_Chapter_Manager registerViewForInteraction:view withCampaign:self.ChapterChangeAD.campaign];
        
    } else if ([self.chapterADType isEqualToString:@"5"]) {  // luomi
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(luomiChapterClick)];
        [view addGestureRecognizer:tapGes];

        
    }
    
}

- (void)GDTBannerClick {
   
    [self tapGDTNativeAD:self.bannerADModel.gdtData CurrentViewController:self.bannerController];
}
- (void)GDTChapterClick {
    [self tapGDTNativeAD:self.ChapterChangeAD.gdtData CurrentViewController:self.bannerController];
   
}

- (void)luomiBannerClick {
   
    // 汇报
    [self upLoadLomiShowWithURL:self.bannerADModel.clickURL];
    // 跳转
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.bannerADModel.jumpURL]];
}
- (void)luomiChapterClick {
   
    // 汇报
    [self upLoadLomiShowWithURL:self.ChapterChangeAD.clickURL];
    // 跳转
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.ChapterChangeAD.jumpURL]];
}

- (void)upLoadLomiShowWithURL:(NSString *)urlString {
    [[NWS_httpClient sharedInstance] getWithURLString:urlString parameters:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)getCampaignWithID:(NSString *)unitID {
    MTGNativeAdManager *adManager = [[MTGNativeAdManager alloc] initWithUnitID:unitID fbPlacementId:@"" videoSupport:YES forNumAdsRequested:1 presentingViewController:nil];
    adManager.delegate = self;
    [adManager loadAds];
}
#pragma mark AdManger delegate
- (void)nativeAdsLoaded:(NSArray *)nativeAds nativeManager:(nonnull MTGNativeAdManager *)nativeManager
{
    if (nativeAds.count == 0) {
        return;
    }
    
    if ([nativeManager.currentUnitId isEqualToString:M_UnitID_Banner]) {
        MTGCampaign *campaign = nativeAds[0];
        self.bannerADModel.iconURL = campaign.iconUrl;
        self.bannerADModel.title = campaign.appName;
        self.bannerADModel.desc = campaign.appDesc;
        self.bannerADModel.campaign = campaign;
    }  else if ([nativeManager.currentUnitId isEqualToString:M_UnitID_Chapter]) {
        MTGCampaign *campaign = nativeAds[0];
        self.ChapterChangeAD.campaign = campaign;
    } else {
        
        MTGCampaign *campain=nativeAds[0];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GetAMTGCampaign" object:@[campain,nativeManager]];
    }
    
    
}
- (void)nativeAdsFailedToLoadWithError:(NSError *)error nativeManager:(nonnull MTGNativeAdManager *)nativeManager
{
    
}

-(void)requestADdata{
    NSLog(@"requestADdata");
    [self GDTadArray];
    return;
    ZY_AdEntity *adEntity =  [ZY_AdEntity initWithSHW_AdsModel:[SHW_ADInterstitial pickUpFeedAd]];
    if (adEntity.adImgPath.length <= 0 ) {
        [SHW_ADInterstitial loadFeedAdData:kAdId];
    }
    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0/*延迟执行时间*/ * NSEC_PER_SEC));
    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
        ZY_AdEntity *adEntity =  [ZY_AdEntity initWithSHW_AdsModel:[SHW_ADInterstitial pickUpFeedAd]];
        if (adEntity.adImgPath.length <= 0 || adEntity.width<= 0 || adEntity.height <= 0) {
            if (self.GDTadArray.count <= 0) {
                [self GDTadArray];
            }
        }
    });
    
}

-(ADModel *)obtainADInstance:(ADtype)type adPlatform:(AdPlatform)platform {
    
    ADModel *model = [[ADModel alloc] init];
    model.platform = platform;

    if (platform == adplatform_GDT) {
        if (self.GDTadArray.count <= 0) {
            [self requestGDTNativeAD];
            return nil;
        }
        GDTNativeAdData *data = [self.GDTadArray objectAtIndex:arc4random() % self.GDTadArray.count];
        model.data = data;
        
        if(type == adType_Feed){
            
            model.width = 1280;
            model.height = 720;
            float imageScale = model.height/model.width;
            model.CellHeight = kCommonMargin+(kCScreenWidth-20)*imageScale+kAdLabelHeight+5+  DOWNLOADSPACE + kAdDownloadHeight +10 +kCommonMargin;
            model.adIconImgPath = [data.properties objectForKey:GDTNativeAdDataKeyIconUrl];
            __weak ADModel *weakmodel = model;
            
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[data.properties objectForKey:GDTNativeAdDataKeyImgUrl]]  options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                if (finished && image) {
                    CGSize size = image.size;
                    weakmodel.width = size.width;
                    weakmodel.height = size.height;
                    float weakimageScale = weakmodel.height/weakmodel.width;
                    weakmodel.CellHeight = kCommonMargin+(kCScreenWidth-20)*weakimageScale+kAdLabelHeight+5+ DOWNLOADSPACE + kAdDownloadHeight +10 +kCommonMargin;
                }
            }];
            
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:model.adIconImgPath]  options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }
        
        model.name = [data.properties objectForKey:GDTNativeAdDataKeyTitle];
        model.appSummary = [data.properties objectForKey:GDTNativeAdDataKeyDesc];
        model.adImgPath = [data.properties objectForKey:type == adType_Feed?GDTNativeAdDataKeyImgUrl:GDTNativeAdDataKeyIconUrl];
        
        
        if (data) {
            return model;
        }
    }
    
    // 洛米广告 信息流 书架
    if (platform == adplatform_luomi && self.lomiPic && (type == adType_Feed || type == adType_BookShelf) ) {

        model.appSummary = self.lomiPic[@"wenzi"];
        model.adImgPath = self.lomiPic[@"imgurl"];
        model.width = ((NSString *)self.lomiPic[@"width"]).floatValue;
        model.height = ((NSString *)self.lomiPic[@"height"]).floatValue;
        model.gotourl = self.lomiPic[@"gotourl"];
        model.click_url = self.lomiPic[@"click_url"];
        model.count_url = self.lomiPic[@"count_url"];
        model.name = self.lomiPic[@"wenzi"];
        return model;
    }
    // 洛米广告gif
    if (platform == adplatform_luomi && self.lomiGif && (type == adType_Gif) ) {
        model.appSummary = self.lomiGif[@"wenzi"];
        model.name = self.lomiGif[@"wenzi"];
        model.adImgPath = self.lomiGif[@"imgurl"];
        model.width = ((NSString *)self.lomiGif[@"width"]).floatValue;
        model.height = ((NSString *)self.lomiGif[@"height"]).floatValue;
        model.CellHeight = 250.0+30.0f;
        
        model.gotourl = self.lomiGif[@"gotourl"];
        model.click_url = self.lomiGif[@"click_url"];
        model.count_url = self.lomiGif[@"count_url"];
        
        model.width = 1280;
        model.height = 720;
        float imageScale = model.height/model.width;
        model.CellHeight = kCommonMargin+(kCScreenWidth-20)*imageScale+kAdLabelHeight+5+  DOWNLOADSPACE + kAdDownloadHeight +10 +kCommonMargin;
        
        return model;
    }
    
    if (platform == adplatform_TouTiao && self.TouTiaoADArray && self.TouTiaoADArray.count > 0) {
        
        BUNativeAd *toutiaoAD = self.TouTiaoADArray[rand() % self.TouTiaoADArray.count];
        model.data = toutiaoAD;
        
        model.appSummary = toutiaoAD.data.AdDescription;
        model.adImgPath = toutiaoAD.data.imageAry[0].imageURL;
        model.name = toutiaoAD.data.AdTitle;
        model.width = 600;
        model.height = 400;
        
        return model;
    }
    
    
    return nil;
}

- (ADModel *)getModelWithGDTData:(NSArray *)array type:(ADtype)type {
    if (array == nil || array.count == 0) {
        return nil;
    }
    ADModel *model = [[ADModel alloc] init];
    model.platform = adplatform_GDT;
    GDTNativeAdData *data = [array objectAtIndex:arc4random() % array.count];
    model.data = data;
    
    
        
    model.width = 1280;
    model.height = 720;
    float imageScale = model.height/model.width;
    model.CellHeight = kCommonMargin+(kCScreenWidth-20)*imageScale+kAdLabelHeight+5+  DOWNLOADSPACE + kAdDownloadHeight +10 +kCommonMargin;
    model.adIconImgPath = [data.properties objectForKey:GDTNativeAdDataKeyIconUrl];
    __weak ADModel *weakmodel = model;
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[data.properties objectForKey:GDTNativeAdDataKeyImgUrl]]  options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (finished && image) {
            CGSize size = image.size;
            weakmodel.width = size.width;
            weakmodel.height = size.height;
            float weakimageScale = weakmodel.height/weakmodel.width;
            weakmodel.CellHeight = kCommonMargin+(kCScreenWidth-20)*weakimageScale+kAdLabelHeight+5+ DOWNLOADSPACE + kAdDownloadHeight +10 +kCommonMargin;
        }
    }];
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:model.adIconImgPath]  options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
    }];
    
    
    model.name = [data.properties objectForKey:GDTNativeAdDataKeyTitle];
    model.appSummary = [data.properties objectForKey:GDTNativeAdDataKeyDesc];
    
    if (type == adType_Feed) {
        model.adImgPath = [data.properties objectForKey:GDTNativeAdDataKeyImgUrl];
    } else {
        model.adImgPath = [data.properties objectForKey:GDTNativeAdDataKeyIconUrl];
    }
    
    
    
    
    if (data) {
        return model;
    } else {
        return nil;
    }
    
}

-(NSString *)ADName:(ADModel *)data type:(ADtype)type {
    
    if ([data.data isKindOfClass:[ZY_AdEntity class]]) {
        ZY_AdEntity *ad = (ZY_AdEntity *)data;
        return ad.name;
    }else if([data.data isKindOfClass:[GDTNativeAdData class]])
    {
        GDTNativeAdData *ad = (GDTNativeAdData *)data;
        return [ad.properties objectForKey:GDTNativeAdDataKeyTitle];
    }
    return nil;
}

-(NSArray *)GDTadArray{
    if (!_GDTadArray) {
        [self requestGDTNativeAD];
    }
    return _GDTadArray;
}

-(void)requestGDTNativeAD{
    
    // 请求头条广告
    BUNativeAdsManager *nad = [BUNativeAdsManager new];
    BUAdSlot *slot1 = [[BUAdSlot alloc] init];
    slot1.ID = TouTiao_Feed;
    slot1.AdType = BUAdSlotAdTypeFeed;
    slot1.position = BUAdSlotPositionFeed;
    slot1.imgSize = [BUSize sizeBy:BUProposalSize_Banner600_500];
    slot1.isSupportDeepLink = YES;
    nad.adslot = slot1;
    
    nad.delegate = self;
    [nad loadAdDataWithCount:5];
    
    // 请求广点通广告 // 仅用于阅读器的banner广告
    if (self.isRequestGDTNativeAd) {
        return;
    }
    self.isRequestGDTNativeAd = YES;
    NSString *gdtID = [self getGDTNativeAdKey];
    NSString *placeID = [self getGDTNativeADID];
    
    _nativeAd = [[GDTNativeAd alloc] initWithAppkey:gdtID placementId:placeID];
    _nativeAd.delegate = self;
    _nativeAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    [_nativeAd loadAd:3]; //这里以一次拉取一条原生广告为例

}

- (NSMutableDictionary *)getGDTKeyWithADPlace:(NSString *)place {
    if (self.gdtKeysAndID == nil) {
        NSString *GDTKeyString = [CFT_parameterOnline getValueByKey:[NSString stringWithFormat:@"广点通配置V%@",k_APP_VERSION]];
        if (GDTKeyString == nil) {
            return nil;
        }
        NSData *data = [GDTKeyString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *GDTKeyDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        if (![NSJSONSerialization isValidJSONObject:GDTKeyDict]) {
            return nil;
        }
        self.gdtKeysAndID = GDTKeyDict;
    }
    
    NSMutableDictionary *returnDict = [NSMutableDictionary dictionary];
    [returnDict setValue:self.gdtKeysAndID[@"ID"] forKey:@"ID"];
    [returnDict setValue:self.gdtKeysAndID[@"KEY"][place] forKey:@"KEY"];
    return returnDict;
}

- (void)nativeAdsManagerSuccessToLoad:(BUNativeAdsManager *)adsManager nativeAds:(NSArray<BUNativeAd *> *)nativeAdDataArray {
    self.TouTiaoADArray = nativeAdDataArray;
}
- (void)nativeAdsManager:(BUNativeAdsManager *)adsManager didFailWithError:(NSError *)error {
    NSLog(@"%@",error);
}

-(void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray
{
    [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:@"lastGDTAdSuccessToLoad"];
    self.GDTadArray = nativeAdDataArray;
    self.isRequestGDTNativeAd = NO;
    
    if (self.isNeedNoti) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nativeAdSuccessToLoad" object:nil];
    }
}

-(void)nativeAdFailToLoad:(NSError *)error
{
    self.GDTadArray = nil;
    self.isRequestGDTNativeAd = NO;
    if (self.isNeedNoti) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"nativeAdErrorToLoad" object:nil];
    }
}

- (BOOL)getOpenStatusForAD:(NSString *)ad {
    
    return YES;
    
    NSDictionary *paramenterOnline = [[NSUserDefaults standardUserDefaults] objectForKey:@"parameterOnline"];
    if (!paramenterOnline || ![paramenterOnline objectForKey:[NSString stringWithFormat:@"广告设置%@",k_APP_VERSION]]) {
        return NO;
    }
    NSString *adOpenString = paramenterOnline[[NSString stringWithFormat:@"广告设置%@",k_APP_VERSION]];
    NSDictionary *adOpenJson = [self dictionaryWithJsonString:adOpenString];
    return ((NSString *)adOpenJson[ad]).boolValue;
    
}

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}


//获取广点通原生广告key
-(NSString *)getGDTNativeAdKey{
    NSString * strGDTKey = nil;
//#ifdef DEBUG
//    strGDTKey = @"1105344611";//@"1105344611";//
//#else
    strGDTKey = [self getGDTKeyWithADPlace:@"bookbanner"][@"ID"];;
    if (NullOrNil(strGDTKey)) {
        strGDTKey = @"1105344611";
    }
//#endif
    return strGDTKey;
}

//获取广点通原生广告ID
-(NSString *)getGDTNativeADID{
    NSString * strPlacementID =nil;
//#ifdef DEBUG
//    strPlacementID = @"1080215124193862";//@"1080215124193862";//
//#else
    strPlacementID = [self getGDTKeyWithADPlace:@"bookbanner"][@"KEY"];
    if (NullOrNil(strPlacementID)) {
        strPlacementID = @"1080215124193862";
    }
//#endif
    return strPlacementID;
}

-(void)tapAD:(ADModel *)adData CurrentVC:(UIViewController *)VC{
    if ([adData.data isKindOfClass:[GDTNativeAdData class]]) {
        [self tapGDTNativeAD:adData.data CurrentViewController:VC];
    }
    else if ([adData.data isKindOfClass:[ZY_AdEntity class]]) {
        self.iTunesStoreVC = VC;
        [self gotoiTunesInapp:self.iTunesStoreVC appId:((ZY_AdEntity *)adData.data).appId];
        ZY_AdEntity *adEntity = [[ZY_AdEntity alloc] init];
        adEntity.appSourceUrlString = @"";
        adEntity.appId = ((ZY_AdEntity *)adData.data).appId;
        adEntity.adType = ((ZY_AdEntity *)adData.data).adType;
        adEntity.adId = ((ZY_AdEntity *)adData.data).adId;
        
        [[ZY_Common shareCommon] gotoiTunes:adEntity];
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adData.gotourl]];
        [[NWS_httpClient sharedInstance] getWithURLString:adData.click_url parameters:nil success:^(id responseObject) {
            NSLog(@"%@",responseObject);
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        
    }
}

-(void)gotoiTunesInapp:(UIViewController *)currentVC appId:(NSString *)appid {
    [SVProgressHUD show];
    SKStoreProductViewController *storeProductVC = [[SKStoreProductViewController alloc] init];
    storeProductVC.delegate = self;
    NSDictionary *dic = [NSDictionary dictionaryWithObject:appid forKey:SKStoreProductParameterITunesItemIdentifier];
    [storeProductVC loadProductWithParameters:dic completionBlock:^(BOOL result, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (!error) {
            [currentVC presentViewController:storeProductVC animated:YES completion:nil];
        } else {
            NSLog(@"ERROR:%@",error);
        }
    }];
}

-(void) productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [self.iTunesStoreVC dismissViewControllerAnimated:YES completion:nil];
}

-(void)tapGDTNativeAD:(id)adData CurrentViewController:(UIViewController *)currentVC {
    _nativeAd.controller = currentVC;
    [_nativeAd clickAd:adData];
}

+(AdPlatform)platformWithOnlineParameter:(NSString *)onlineParameter {
    
    
    if ([onlineParameter isEqualToString:@"1"]) {
        return adplatform_ZY;
    }
    else if ([onlineParameter isEqualToString:@"3"]) {
        return adplatform_GDT;
    } else if ([onlineParameter isEqualToString:@"2"]) {
        return adplatform_Google;
    }else if ([onlineParameter isEqualToString:@"5"]) {
        return adplatform_luomi;
    } else if ([onlineParameter isEqualToString:@"4"]) {
        return adplatform_TouTiao;
    }
    
    return 9999;
    
}

- (BannerADModel *)bannerADModel {
    if (_bannerADModel == nil) {
        _bannerADModel = [[BannerADModel alloc] init];
    }
    return _bannerADModel;
}
- (BannerADModel *)ChapterChangeAD {
    if (_ChapterChangeAD == nil) {
        _ChapterChangeAD = [[BannerADModel alloc] init];
    }
    return _ChapterChangeAD;
}
@end

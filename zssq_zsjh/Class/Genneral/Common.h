//
//  Common.h
//  Palm_Novel
//
//  Created by shiwei on 2018/3/20.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD.h>


@interface Common : NSObject

#define k_ScreenWidth                   [UIScreen mainScreen].bounds.size.width
#define k_ScreenHeight                  [UIScreen mainScreen].bounds.size.height
#define k_StatusBarHeight               [UIApplication sharedApplication].statusBarFrame.size.height
#define k_NavigatiBarHeight             self.navigationController.navigationBar.frame.size.height
#define k_APP_VERSION                         \
[[[NSBundle mainBundle] infoDictionary] \
objectForKey:@"CFBundleShortVersionString"]


@property(nonatomic,assign,readonly) BOOL zs_Open;

+(Common *)shareCommon;

+(UIColor *)colorWithHexString:(NSString *)strColor alpha:(CGFloat)value;

+(void)settingControllerBarItemWithTitle:(NSString *)title withController:(UIViewController *)controller withImage:(NSString *)normalImage withSelectedImage:(NSString *)selectedImage;

+(CGFloat)getRadiusByMap:(CGFloat)radius;
+(CGFloat)get414RadiusByMap:(CGFloat)radius;

+(CGFloat)getFontOfSizeByMap:(CGFloat)fontSize;
+(CGFloat)get414FontOfSizeByMap:(CGFloat)fontSize;

+(CGRect)getFrameByMap:(CGRect)tmpFrame;
+(CGRect)get414FrameByMap:(CGRect)tmpFrame;

+(CGSize)getSizeByMap:(CGFloat)width withHeight:(CGFloat)height;
+(CGSize)get414SizeByMap:(CGFloat)width withHeight:(CGFloat)height;

+(CGPoint)getPointByMap:(CGFloat)x withY:(CGFloat)y;
+(CGPoint)get414PointByMap:(CGFloat)x withY:(CGFloat)y;

+(CGFloat)getLineSpacing:(CGFloat)lineSpace;

+(UIEdgeInsets)getEdgeInsetsByMap:(UIEdgeInsets)edgeInsets;
+(UIEdgeInsets)get414EdgeInsetsByMap:(UIEdgeInsets)edgeInsets;

+(UIImage*)imageWithColor:(UIColor*)color WithFrame:(CGRect)rect;

+(BOOL)isBlankString:(NSString *)string;

+(NSString*)getCurrentTimeString;

+(NSString*)getCurrentDayString;

+(NSString*)getUnixTime;

+(CGFloat)heightForString:(NSString *)text andWidth:(CGFloat)width andFontSize:(CGFloat)size;


+(NSString*)encryptReplacingString:(NSString *)nsstrOriginal;

//从数据源中随机获取指定数量的数据
+(NSMutableArray *)randomAccessData:(NSMutableArray*)dataArray WithSpecifiedNumber:(NSInteger)numberCount;

+(NSString*)getCurrentTimeStringWithFormat:(NSString*)nsstrFormat;

+(NSString *)getAppIconName;

-(NSString *)getADType;  // 老版获取广告类型

// key:
//screen：开屏广告
//bookcover：书封广告
//bookflow：信息流广告
//Details：详情页广告
//readerbottom：阅读器底部广告
//bookbanner：阅读器banner广告、
//readpage：阅读器翻页广告
-(NSString *)randomADTypeWithKey:(NSString *)key; // 新版随机获得广告类型

+(void)setLabelSpace:(UILabel*)label withValue:(NSString*)content withFont:(UIFont*)font withlineSpace:(CGFloat)lineSpace;

+(void)showDebugHUD:(NSString *)messege;

@end

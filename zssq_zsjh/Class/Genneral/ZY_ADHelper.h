//
//  ZY_ADHelper.h
//  ZYNovel
//

#import <Foundation/Foundation.h>
#import "ZY_AdEntity.h"
#import "GDTNativeAd.h"
#import <MTGSDK/MTGSDK.h>
#import <MTGSDK/MTGCampaign.h>
#import <BUAdSDK/BUAdSDK.h>

typedef NS_ENUM(NSInteger, ADtype) {
    adType_Feed,
    adType_BookShelf,
    adType_Gif
};

typedef NS_ENUM(NSInteger, AdPlatform) {
    adplatform_ZY,
    adplatform_GDT,
    adplatform_Google,
    adplatform_luomi,
    adplatform_TouTiao
};

//  广告实体（古老版本，将来替换，区分广告位）
@interface ADModel :NSObject

@property (nonatomic,strong) id data; //广告实体 nil是google广告，
@property (nonatomic,assign) CGFloat width;
@property (nonatomic,assign) CGFloat height;
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * adImgPath;
@property (nonatomic,strong) NSString * adIconImgPath;
@property (nonatomic,strong) NSString * appSummary;
@property (nonatomic,strong) NSString * gotourl;
@property (nonatomic,strong) NSString * count_url; // 展示成功回调
@property (nonatomic,strong) NSString * click_url; // 点击回调
@property (nonatomic,assign) AdPlatform platform;
@property(nonatomic,assign) CGFloat CellHeight;

@end

@interface BannerADModel :NSObject

@property (nonatomic, strong) NSString *iconURL;
@property (nonatomic, strong) NSString *bannerURL;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;

@property (nonatomic, strong) NSString *showURL;
@property (nonatomic, strong) NSString *clickURL;
@property (nonatomic, strong) NSString *jumpURL;

// M系统广告实体，用来注册点击、汇报等
@property (nonatomic, strong) MTGCampaign *campaign;
// 广点通实体，用来点击、汇报
@property (nonatomic, strong) GDTNativeAdData *gdtData;


@end


@interface ZY_ADHelper : NSObject

@property (nonatomic,strong) GDTNativeAd *nativeAd;     //原生广告实例


@property (nonatomic,assign) BOOL isNeedNoti;

// 阅读器三个广告位随机到的广告类型，临时存储起来
@property (nonatomic,strong) NSString *chapterADType;
@property (nonatomic,strong) NSString *bottomADType;
@property (nonatomic,strong) NSString *bannerADType;

@property (nonatomic, strong) NSDictionary *lomiPic;
@property (nonatomic, strong) NSDictionary *lomiGif;
@property (nonatomic, strong) NSDictionary *lomiKaiPing;

//  不同广告位的广告实体
@property (nonatomic, strong) BannerADModel *bannerADModel;
@property (nonatomic, strong) BannerADModel *ChapterChangeAD;

+(ZY_ADHelper *)shareADHelper;

//请求广告数据
-(void)requestADdata;

-(void)requestGDTNativeAD;

// 提前加载洛米广告
-(void)requestLomiPic;
-(void)requestLomiGif;
-(void)requestLomiKaiPing;
// 展示汇报洛米
-(void)upLoadLomiShowWithURL:(NSString *)urlString;


// 阅读器Banner
-(void)requestBanner;
// 阅读器章节间广告
-(void)requestChapterAD;
// 注册view点击和汇报
- (void)registClickWithBannerView:(UIView *)view Controller:(UIViewController *)controller;
- (void)registClickWithChapterView:(UIView *)view Controller:(UIViewController *)controller;
// 汇报广告位的展示
- (void)uploadBannerShow:(UIView *)bannerView;

// 获取线上的广点通key和ID
- (NSMutableDictionary *)getGDTKeyWithADPlace:(NSString *)place;
// 根据广点通Data获取一个ADModel
- (ADModel *)getModelWithGDTData:(NSArray *)array type:(ADtype)type;
// 获取一个广告
-(ADModel*)obtainADInstance:(ADtype)type adPlatform:(AdPlatform)platform;

//点击广告
-(void)tapAD:(ADModel *)adData CurrentVC:(UIViewController *)VC;

+(AdPlatform)platformWithOnlineParameter:(NSString *)onlineParameter;
// 获取一个M原生MTGCampaign.h
- (void)getCampaignWithID:(NSString *)unitID;

// 获取广告开关
//screen：开屏广告
//bookcover：书封广告
//bookflow：信息流广告
//readerbottom：阅读器底部广告
//bookbanner：阅读器banner广告、
//readpage：阅读器翻页广告
- (BOOL)getOpenStatusForAD:(NSString *)ad;

@end

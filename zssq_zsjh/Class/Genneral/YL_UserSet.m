//
//  YL_UserSet.m
//  ZYNovel
//
//  Created by niegang on 2017/11/23.
//  Copyright © 2017年 shiwei. All rights reserved.
//

#import "YL_UserSet.h"
#import "NSString+Combine.h"
static NSString *nsstrDictCacheUseTimeKey =@"nsstrCacheUseTime";

@implementation YL_UserSet
+(NSInteger)getCurrentUnixTime{
    NSDate *date = [NSDate date];
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"];//或GMT
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:date];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:date];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[NSDate date]];
    NSTimeInterval timeInterval = [destinationDateNow timeIntervalSince1970];
    long long dTime = [[NSNumber numberWithDouble:timeInterval] longLongValue]; // 将double转为long long型
    NSString *tempTime = [NSString stringWithFormat:@"%llu",dTime];
    NSInteger time = [tempTime integerValue];
    
    return time;
}

+(void)settingReadBookUseTime{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:nsstrDictCacheUseTimeKey];
    
    if (dict) {
        NSString *nsstrTime = [dict objectForKey:@"RecordTime"];
        NSString *nsstrCurrentTime =[NSString getCurrentTimeStringWithFormat:@"YYYY-MM-dd"];
        
        if (![nsstrTime isEqualToString:nsstrCurrentTime]) {
            NSInteger useTimeCount =[[dict objectForKey:@"UseTime"] integerValue];
            NSDictionary *dictTemp = [[NSDictionary alloc] initWithObjectsAndKeys:nsstrCurrentTime,@"RecordTime",[NSString stringWithFormat:@"%zd",(useTimeCount+1)],@"UseTime",nil];
            [[NSUserDefaults standardUserDefaults] setObject:dictTemp forKey:nsstrDictCacheUseTimeKey];
        }
    }else{
        NSString *nsstrCurrentTime =[NSString getCurrentTimeStringWithFormat:@"YYYY-MM-dd"];
        NSDictionary *dictTemp = [[NSDictionary alloc] initWithObjectsAndKeys:nsstrCurrentTime,@"RecordTime",@"1",@"UseTime",nil];
        [[NSUserDefaults standardUserDefaults] setObject:dictTemp forKey:nsstrDictCacheUseTimeKey];
    }
}

+(NSInteger)getReadBookUseTimeCount{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:nsstrDictCacheUseTimeKey];
    
    if (dict) {
        NSInteger useTimeCount =[[dict objectForKey:@"UseTime"] integerValue];
        
        return useTimeCount;
    }
    
    return 1;
}
@end

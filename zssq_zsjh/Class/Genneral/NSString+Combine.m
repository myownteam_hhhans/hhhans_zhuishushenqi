//
//  NSString+Combine.m
//  ZYNovel
//
//

#import "NSString+Combine.h"

@implementation NSString (Combine)

+ (NSString *)getSeparatedPhoneNumberWithString:(NSString *)phoneString {
    
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    NSString * string = phoneString;
    
    NSCharacterSet *specCharacterSet = [characterSet invertedSet];
    
    NSArray * strArr = [string componentsSeparatedByCharactersInSet:specCharacterSet];
    return [strArr componentsJoinedByString:@""];
    
}


+(NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        [scanner scanUpToString:@"<" intoString:nil];
        [scanner scanUpToString:@">" intoString:&text];
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    html = [html stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    html = [html stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    return html;
}

+(BOOL)isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
//    if ([string length] == 0) {
//        return YES;
//    }

    return NO;
}

+(NSString*)getCurrentTimeStringWithFormat:(NSString*)nsstrFormat{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:nsstrFormat];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}

+(NSString *)getLimitFreeTimeString{
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:3*24*60*60];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}

+(NSString*)getCurrentTimeString{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}

+(BOOL)getResultComparedRecord:(NSString *)recordTime withNow:(NSString *)now{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSDate *date1 = [formatter dateFromString:recordTime];
    NSDate *date2 = [formatter dateFromString:now];
    NSComparisonResult result = [date1 compare:date2];
    if (result==NSOrderedAscending) {
        return YES;
    }else{
        return NO;
    }
    
    
}

+ (CGSize)autoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize AttrButeString:(NSDictionary*)dictAttrButes{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode=NSLineBreakByWordWrapping;
    
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    if (dictAttrButes) {
        attributes=dictAttrButes;
    }
    
    CGSize size = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    size.height=ceil(size.height);
    size.width=ceil(size.width);
    
    return size;
}
@end

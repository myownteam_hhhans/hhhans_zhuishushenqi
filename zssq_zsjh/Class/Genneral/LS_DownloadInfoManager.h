//
//  LS_DownloadInfoManager.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/17.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LSYReadModel.h"

@interface LS_DownloadInfoManager : NSObject

@property (atomic, strong) NSMutableArray *downloadingArray;
@property (atomic, strong) NSMutableArray *downloadedArray;

@property (nonatomic, assign)NSInteger nowPercent; // 当前任务进行百分比
@property (atomic, assign)NSInteger downloadIndex; // 正在进行的任务序号


- (void)addItemToDownLoadedArray:(LSYReadModel *)bookModel;
- (void)addItemToDownLoadingArray:(LSYReadModel *)bookModel;

- (void)cancelItemWithModel:(LSYReadModel *)bookModel;
- (void)deletaDownloadedItem:(LSYReadModel *)bookModel;

- (void)shelfDeleteWithBookID:(NSString *)bookId;

+ (instancetype)shareDownloadInfoManager;

@end

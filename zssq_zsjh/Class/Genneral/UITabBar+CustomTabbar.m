//
//  UITabBar+CustomTabbar.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/28.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "UITabBar+CustomTabbar.h"

@implementation UITabBar (CustomTabbar)

-(id<CAAction>)actionForLayer:(CALayer *)layer forKey:(NSString *)event{
    if ([event isEqualToString:@"position"]) {
        if(layer.position.x<0){
            //show tabbar
            CATransition *pushFromTop = [[CATransition alloc] init];
            pushFromTop.duration = 0.3;
            pushFromTop.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
            pushFromTop.type = kCATransitionPush;
            pushFromTop.subtype = kCATransitionFade;
            return pushFromTop;
        }else if (layer.position.x>0&&(layer.position.y>layer.bounds.size.height)&&(layer.position.y<[UIScreen mainScreen].bounds.size.height)){
            //hide tabbar
            CATransition *pushFromBottom = [[CATransition alloc] init];
            pushFromBottom.duration = 0.2;
            pushFromBottom.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
            pushFromBottom.type = kCATransitionFade;
            pushFromBottom.subtype = kCATransitionFromBottom;
            return pushFromBottom;
            
//            CA_EXTERN CATransitionType const kCATransitionFade
//            API_AVAILABLE(macos(10.5), ios(2.0), watchos(2.0), tvos(9.0));
//            CA_EXTERN CATransitionType const kCATransitionMoveIn
//            API_AVAILABLE(macos(10.5), ios(2.0), watchos(2.0), tvos(9.0));
//            CA_EXTERN CATransitionType const kCATransitionPush
//            API_AVAILABLE(macos(10.5), ios(2.0), watchos(2.0), tvos(9.0));
//            CA_EXTERN CATransitionType const kCATransitionReveal
//            API_AVAILABLE(macos(10.5), ios(2.0), watchos(2.0), tvos(9.0));
            
        }
    }
    return (id<CAAction>)[NSNull null];
}

-(void)runActionForKey:(NSString *)event object:(id)anObject arguments:(NSDictionary *)dict{
    
}

@end

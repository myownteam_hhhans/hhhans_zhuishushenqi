//
//  DBHelper.h
//  ZYNovel
//
//  Created by S on 2017/3/14.
//  Copyright © 2017年 S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"
#import "ZY_NovelInfo.h"
#import "ZY_NovelProgress.h"

@interface ZY_DBHelper : NSObject

@property(nonatomic,strong) FMDatabase *db;

@property (nonatomic, strong) FMDatabaseQueue *queue;

- (BOOL)isTableOK:(NSString *)tableName;

+(ZY_DBHelper *)shareDBHelper;

/************************************
 函数描述:查询书架最近阅读的书
 作者:C
 时间:2018-03-06
 ************************************/
-(ZY_NovelInfo*)queryBookReadLasted;

/************************************
 函数描述:替换最近阅读的书
 作者:C
 时间:2018-03-06
 ************************************/
-(void)ReplaceBookToReadLasted:(ZY_NovelInfo*)novelInfo;


/************************************
 函数描述:查询书架中的所有书籍
 作者:S
 时间:2017-03-14
 ************************************/
-(NSMutableArray*)queryBookShelfInfoList;

-(NSMutableArray*)queryProgressInfoList;

//查询某本书的进度
-(NSMutableArray*)queryProgressWithID:(NSString *)bookid;


/************************************
 函数描述:查询所有热词
 作者:S
 时间:2017-03-14
 ************************************/
- (NSMutableArray*)queryHotWordList;

/************************************
 函数描述:清空关键词
 作者:S
 时间:2017-03-14
 ************************************/
-(void)removeHotWords;

/************************************
 函数描述:清空书架进度
 作者:C
 时间:2018-03-21
 ************************************/
-(void)removeBookShelves;

-(void)removeBooksProgress;

/************************************
 函数描述:添加一本书到书架
 作者:S
 时间:2017-03-14
 ************************************/
-(void)addBookToShelf:(ZY_NovelInfo*)novelInfo;

-(void)addBookProgress:(ZY_NovelProgress *)novelProgress;

/************************************
 函数描述:添加热词
 作者:S
 时间:2017-03-14
 ************************************/
-(void)addHotWord:(NSString*)nsstrHotWord;

/************************************
 函数描述:更新书籍状态
 作者:S
 时间:2017-03-17
 ************************************/
-(void)updateBookToShelf:(ZY_NovelInfo*)novelInfo;

-(void)updateBookProgress:(ZY_NovelProgress *)novelProgress;

/************************************
 函数描述:更新阅读时间
 作者:S
 时间:2017-03-17
 ************************************/
-(void)updateBookToShelfAndReadTime:(ZY_NovelInfo*)novelInfo;

/************************************
 函数描述:根据书籍唯一标示查询这本书是否已经在书架中
 作者:S
 时间:2017-03-14
 ************************************/
-(BOOL)queryBookIsOnTheBookShelf:(NSString*)novelId;

/************************************
 函数描述:删除书架中的书籍
 作者:S
 时间:2017-03-14
 ************************************/
-(void)removeBookIsOnTheBookShelfWithNovelId:(NSString*)novelId;

-(void)removeBookProgressWithNovelId:(NSString*)novelId;

@end

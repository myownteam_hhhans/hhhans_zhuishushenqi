//
//  Common.m
//  Joke
//
//  Created by S on 2016/10/31.
//  Copyright © 2016年 ___S___. All rights reserved.
//

#import "ZY_Common.h"
#import "SDImageCache.h"

static NSString *nsstrDictCacheUseTimeKey =@"nsstrCacheUseTime";

void invokeSelector(id object, SEL selector, NSArray *arguments)
{
    NSMethodSignature *signature = [object methodSignatureForSelector:selector];
    
    if (signature.numberOfArguments == 0) {
        return; //@selector未找到
    }
    
    if (signature.numberOfArguments > [arguments count]+2) {
        return; //传入arguments参数不足。signature至少有两个参数，self和_cmd
    }
    
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    [invocation setTarget:object];
    [invocation setSelector:selector];
    
    for(int i=0; i<[arguments count]; i++)
    {
        id arg = [arguments objectAtIndex:i];
        [invocation setArgument:&arg atIndex:i+2]; // The first two arguments are the hidden arguments self and _cmd
    }
    
    [invocation invoke]; // Invoke the selector
}

@interface ZY_Common()


@end

@implementation ZY_Common

+(ZY_Common *)shareCommon{
    static ZY_Common *common = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        common = [[ZY_Common alloc] init];
    });
    
    return common;
}

-(void)gotoiTunes:(id)model{
    
    //获取模型里的跳转网址和appid
    SEL selGetAppSourceUrlString = NSSelectorFromString(@"appSourceUrlString");
    SEL selGetAppId = NSSelectorFromString(@"appId");
    SEL selGetAdType = NSSelectorFromString(@"adType");
    SEL selGetAdid = NSSelectorFromString(@"adId");
    
    NSString *nsstrAppSourceUrlString;
    SuppressPerformSelectorLeakWarning(
                                       nsstrAppSourceUrlString = [model performSelector:selGetAppSourceUrlString];
                                       );
    
    NSString* nsstrAppId;
    SuppressPerformSelectorLeakWarning(
                                       nsstrAppId= [model performSelector:selGetAppId];
                                       );
    
    NSString* nsstrAdType;
    SuppressPerformSelectorLeakWarning(
                                       nsstrAdType= [model performSelector:selGetAdType];
                                       
                                       );
    NSString* nsstrAdid;
    SuppressPerformSelectorLeakWarning(
                                       nsstrAdid= [model performSelector:selGetAdid];
                                       );
    //用反射调用跳转方法
    Class classSHW_AdCommon = NSClassFromString(@"SHW_AdCommon");
    SEL selShareCommon = NSSelectorFromString(@"shareCommon");
    IMP impShareCommon = [classSHW_AdCommon methodForSelector:selShareCommon];
    id instance = impShareCommon(classSHW_AdCommon,selShareCommon);
    
    SEL selClick = NSSelectorFromString(@"clickApplicationWithAppid:withAppSourceString:withAdId:WithAdType:");
    
    invokeSelector(instance, selClick, @[nsstrAppId,nsstrAppSourceUrlString,nsstrAdid,nsstrAdType]);
}

- (void)advertisingDisplayReportWithModel:(id)model{
    SEL selGetAppId = NSSelectorFromString(@"appId");
    SEL selGetAdType = NSSelectorFromString(@"adType");
    SEL selGetAdid = NSSelectorFromString(@"adId");
    NSString *nsstrAppId;
    SuppressPerformSelectorLeakWarning(
                                       nsstrAppId = [model performSelector:selGetAppId];
                                       );
    
    NSString* nsstrAdType;
    SuppressPerformSelectorLeakWarning(
                                       nsstrAdType= [model performSelector:selGetAdType];
 
                                       );
    NSString* nsstrAdid;
    SuppressPerformSelectorLeakWarning(
                                       nsstrAdid= [model performSelector:selGetAdid];
                                       );
    
    Class classSHW_AdCommon = NSClassFromString(@"SHW_AdCommon");
    SEL selShareCommon = NSSelectorFromString(@"shareCommon");
    IMP impShareCommon = [classSHW_AdCommon methodForSelector:selShareCommon];
    id instance = impShareCommon(classSHW_AdCommon,selShareCommon);
    
    SEL selClick = NSSelectorFromString(@"advertisingDisplayReportWithAdAppId:withAdType:withAdId:");
    
    invokeSelector(instance, selClick, @[nsstrAppId,nsstrAdType,nsstrAdid]);
    
}

-(void)clearImageCache{
    CGFloat tmpSize = [[SDImageCache sharedImageCache] getSize]/1024/1024;
    if (tmpSize>150) {
        DebugLog(@"缓存大于50M，准备自动清理");
        [[SDImageCache sharedImageCache] clearDisk];
    }
}

+(BOOL)isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

-(UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 {
    
    UIGraphicsBeginImageContext(CGSizeMake(90, 90));
    
    // Draw image1
    [image1 drawInRect:CGRectMake(0, 0, 90, 90)];
    
    // Draw image2
    [image2 drawInRect:CGRectMake(20,20, 50,50)];
    
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resultingImage;
}

+(UIImage*)imageWithColor:(UIColor*)color WithFrame:(CGRect)rect
{
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (CGSize)autoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize AttrButeString:(NSDictionary*)dictAttrButes{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode=NSLineBreakByWordWrapping;
    
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    if (dictAttrButes) {
        attributes=dictAttrButes;
    }
    
    CGSize size = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    size.height=ceil(size.height);
    size.width=ceil(size.width);
    
    return size;
}

+(UIColor *)colorWithHexString:(NSString *)strColor alpha:(CGFloat)a
{
    if ((strColor == nil) && (![strColor isEqualToString:@""])) return nil;
    NSString *strProcess = [[strColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    
    if ([strProcess length] < 6)
        return [UIColor whiteColor];
    if ([strProcess hasPrefix:@"#"])
        strProcess = [strProcess substringFromIndex:1];
    if ([strProcess length] != 6)
        return [UIColor whiteColor];
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *strRed = [strProcess substringWithRange:range];
    
    range.location = 2;
    NSString *strGreen = [strProcess substringWithRange:range];
    
    range.location = 4;
    NSString *strBlue = [strProcess substringWithRange:range];
    
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:strRed] scanHexInt:&r];
    [[NSScanner scannerWithString:strGreen] scanHexInt:&g];
    [[NSScanner scannerWithString:strBlue] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:a];
    
}

+(UINavigationController*)createNavTabBarController:(UIViewController*)viewController withNavBgColor:(UIColor*)navBgColor withImage:(NSString*)image withSelectImage:(NSString*)selImage withTitle:(NSString*)title{
    UIColor *titleColor = [ZY_Common colorWithHexString:@"#333333" alpha:1.0];
    UINavigationController *navHomeController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [navHomeController.navigationBar setTranslucent:NO];
    [navHomeController.navigationBar setTintColor:titleColor];
    viewController.title = title;
    UITabBarItem *homeBarItem = [[UITabBarItem alloc] init];
    [homeBarItem setTitle:title];
    homeBarItem.image = [[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    homeBarItem.selectedImage = [[UIImage imageNamed:selImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [navHomeController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : titleColor}];
    navHomeController.navigationBar.barTintColor=navBgColor;
    [viewController setTabBarItem:homeBarItem];
    
    
    
    return navHomeController;
}

-(NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        [scanner scanUpToString:@"<" intoString:nil];
        [scanner scanUpToString:@">" intoString:&text];
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
   
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; 
    html = [html stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    html = [html stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    return html;
}

+(NSMutableDictionary*)convertNSDictionaryToNSMutableDictionary:(NSDictionary*)dict{
    
    NSMutableDictionary *mutableDict = [[NSMutableDictionary alloc] init];
    
    if (dict) {
        for (NSInteger i =0; i<[dict allKeys].count; i++) {
            NSString *nsstrDictKey = [[dict allKeys] objectAtIndex:i];
            [mutableDict setObject:[[dict objectForKey:nsstrDictKey] mutableCopy] forKey:nsstrDictKey];
        }
    }
    
    return mutableDict;
}

+(UIView *)createLoadingErrorViewWithFrame:(CGRect)frame withTarget:(nullable id)target action:(SEL)action{
    UIView *errorView = [[UIView alloc] initWithFrame:frame];
    errorView.backgroundColor = [UIColor whiteColor];
    UIImage *image = [UIImage imageNamed:@"error"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((kCScreenWidth-image.size.width)/2,(kCScreenHeight-64-49-image.size.height)/2,image.size.width,image.size.height)];
    [imageView setImage:image];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake((kCScreenWidth-120)/2,CGRectGetMaxY(imageView.frame)+20, 120,40)];
    [btn setTitle:@"重新加载" forState:UIControlStateNormal];
    [btn setTitleColor:kRGBCOLOR(229, 20, 13) forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:20]];
     btn.layer.borderColor = kRGBCOLOR(229, 20, 13).CGColor;
    [btn.layer setCornerRadius:6.0];
     btn.layer.borderWidth=2.0;
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [errorView addSubview:imageView];
    [errorView addSubview:btn];
    
    return errorView;
}

+(NSData*)converDataToUtf8Data:(NSData*)currentData{
    NSData *tempData =currentData;
    
    NSString *temputf8Str =[[NSString alloc] initWithData:tempData encoding:NSUTF8StringEncoding];
    if (![ZY_Common isBlankString:temputf8Str]) {
        return tempData;
    }
    
    NSString *tempGb2312Str =[[NSString alloc] initWithData:tempData encoding:0x80000632];
    
    if (![ZY_Common isBlankString:tempGb2312Str]) {
        return [tempGb2312Str dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    NSString *tempGbkStr =[[NSString alloc] initWithData:tempData encoding:0x80000631];
    
    if (![ZY_Common isBlankString:tempGbkStr]) {
        return [tempGbkStr dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    return tempData;
}

+(NSString*)getCurrentTimeString{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}

+(NSString*)getCurrentTimeStringWithFormat:(NSString*)nsstrFormat{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:nsstrFormat];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}


+(void)settingReadBookUseTime{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:nsstrDictCacheUseTimeKey];
    
    if (dict) {
        NSString *nsstrTime = [dict objectForKey:@"RecordTime"];
        NSString *nsstrCurrentTime =[ZY_Common getCurrentTimeStringWithFormat:@"YYYY-MM-dd"];
        
        if (![nsstrTime isEqualToString:nsstrCurrentTime]) {
            NSInteger useTimeCount =[[dict objectForKey:@"UseTime"] integerValue];
            NSDictionary *dictTemp = [[NSDictionary alloc] initWithObjectsAndKeys:nsstrCurrentTime,@"RecordTime",[NSString stringWithFormat:@"%zd",(useTimeCount+1)],@"UseTime",nil];
            [[NSUserDefaults standardUserDefaults] setObject:dictTemp forKey:nsstrDictCacheUseTimeKey];
        }
    }else{
        NSString *nsstrCurrentTime =[ZY_Common getCurrentTimeStringWithFormat:@"YYYY-MM-dd"];
        NSDictionary *dictTemp = [[NSDictionary alloc] initWithObjectsAndKeys:nsstrCurrentTime,@"RecordTime",@"1",@"UseTime",nil];
        [[NSUserDefaults standardUserDefaults] setObject:dictTemp forKey:nsstrDictCacheUseTimeKey];
    }
}

+(NSInteger)getReadBookUseTimeCount{
    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:nsstrDictCacheUseTimeKey];
    
    if (dict) {
        NSInteger useTimeCount =[[dict objectForKey:@"UseTime"] integerValue];
        
        return useTimeCount;
    }
    
    return 1;
}

+(UIColor *)getCateColor:(NSString *)novelCategory{
    if ([novelCategory isEqualToString:@"都市言情"]) {
        return RGB(180, 158, 214);
    }else if ([novelCategory isEqualToString:@"玄幻奇幻"]) {
        return RGB(233, 201, 147);
    }else if ([novelCategory isEqualToString:@"武侠仙侠"]) {
        return RGB(150, 210, 196);
    }else if ([novelCategory isEqualToString:@"科幻灵异"]) {
        return RGB(156, 209, 159);
    }else if ([novelCategory isEqualToString:@"历史军事"]) {
        return RGB(123, 159, 216);
    }else if ([novelCategory isEqualToString:@"网游竞技"]) {
        return RGB(203, 199, 146);
    }else if ([novelCategory isEqualToString:@"同人小说"]) {
        return RGB(149, 184, 110);
    }else{
        return RGB(253, 106, 131);
    }
}
@end

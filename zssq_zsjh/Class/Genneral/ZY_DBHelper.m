//
//  DBHelper.m
//  ZYNovel
//
//  Created by S on 2017/3/14.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_DBHelper.h"
#import "ZY_Common.h"
#import "NSString+Combine.h"
#import "ZY_DBHelper+NovelInfo.h"

@interface ZY_DBHelper()


@end

@implementation ZY_DBHelper

+(ZY_DBHelper *)shareDBHelper{
    static ZY_DBHelper *dbHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dbHelper = [[ZY_DBHelper alloc] init];
        [dbHelper initDataBase];
        [dbHelper initNovelInfo];
    });
    
    return dbHelper;
}

- (BOOL)isTableOK:(NSString *)tableName
{
    [self.db open];
    
    FMResultSet *rs = [self.db executeQuery:@"select count(*) as 'count' from sqlite_master where type ='table' and name = ?", tableName];
    while ([rs next])
    {
        // just print out what we've got in a number of formats.
        NSInteger count = [rs intForColumn:@"count"];
        
        if (0 == count)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    [self.db close];
    
    return NO;
}

-(void)initDataBase{
    
    [self.db open];
    // 初始化数据表
    NSString *createBookShelfSql = @"CREATE TABLE BookShelf ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,'novelId' VARCHAR(255),'novelUrl' VARCHAR(500),'novelCoverUrl' VARCHAR(500),'novelName' VARCHAR(300),'novelAuthor' VARCHAR(200),'novelCategory' VARCHAR(200),'novelStatus' VARCHAR(300),'novelLatestUpdateTime' VARCHAR(300),'novelLatestChapter' VARCHAR(300),'NovelDes' VARCHAR(500),'novelListUrl' VARCHAR(500),'novelSource' VARCHAR(500),'NovelCollectCreateTime' VARCHAR(100),'limitFreeState' VARCHAR(100))";
    
    NSString *createReadProgressSql = @"CREATE TABLE ReadProgress ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,'novelId' VARCHAR(255),'chapterId' VARCHAR(100),'chapterIndex' VARCHAR(100))";
    
    NSString *createLastReadSql = @"CREATE TABLE LastReadBook ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,'novelId' VARCHAR(255),'novelUrl' VARCHAR(500),'novelCoverUrl' VARCHAR(500),'novelName' VARCHAR(300),'novelAuthor' VARCHAR(200),'novelCategory' VARCHAR(200),'novelStatus' VARCHAR(300),'novelLatestUpdateTime' VARCHAR(300),'novelLatestChapter' VARCHAR(300),'NovelDes' VARCHAR(500),'novelListUrl' VARCHAR(500),'novelSource' VARCHAR(500),'NovelCollectCreateTime' VARCHAR(100),'limitFreeState' VARCHAR(100))";
    
    
    NSString *createHotWordSql =@"CREATE TABLE HotWord ('id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'Word' VARCHAR(255),'CreateTime' VARCHAR(255))";
    
    if (![self isTableOK:@"BookShelf"]) {
        [self.db executeUpdate:createBookShelfSql];
    }else{
        if (![self checkAddColumnExistsWithColumnName:@"NovelCollectCreateTime"]) {
            NSString *nsstrAlterBookShelfSql=@"ALTER TABLE BookShelf ADD COLUMN NovelCollectCreateTime VarChar(100);";
            [self.db executeUpdate:nsstrAlterBookShelfSql];
        }
    }
    
    if (![self isTableOK:@"ReadProgress"]) {
        [self.db executeUpdate:createReadProgressSql];
    }
    
    if (![self isTableOK:@"LastReadBook"]) {
        [self.db executeUpdate:createLastReadSql];
    }
    
    if (![self isTableOK:@"HotWord"]) {
        [self.db executeUpdate:createHotWordSql];
    }
    
    [self.db close];
    
}

-(FMDatabase *)db{
    if (!_db) {
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"zyNovel.sqlite"];
        DebugLog(@"数据库路径:%@",filePath);
        _db = [FMDatabase databaseWithPath:filePath];
    }
    
    return _db;
}

- (FMDatabaseQueue *)queue {
    if (_queue == nil) {
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        // 文件路径
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"zyNovel.sqlite"];
        _queue = [FMDatabaseQueue databaseQueueWithPath:filePath];
    }
    return _queue;
}

-(BOOL)checkAddColumnExistsWithColumnName:(NSString*)nsstrColumnName{
    [self.db open];
    
    FMResultSet *rs = [self.db executeQuery:@"select count(1) from sqlite_master  where tbl_name='BookShelf' and sql like '%?%';", nsstrColumnName];
    while ([rs next])
    {
        NSInteger count = [rs intForColumn:@"count"];
        
        if (0 == count)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    [self.db close];
    
    
    return NO;
}

-(void)addBookProgress:(ZY_NovelProgress *)novelProgress{
    [self.db open];
    if (novelProgress.chapterID == nil) {
        novelProgress.chapterID = @"-1";
    }
    NSString *nsstrSql =[NSString stringWithFormat:@"insert into ReadProgress(novelId,chapterId,chapterIndex)values('%@','%@','%@')",novelProgress.bookID,novelProgress.chapterID,novelProgress.chapterIndex];
    
    if ([self isTableOK:@"ReadProgress"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}

-(void)addBookToShelf:(ZY_NovelInfo*)novelInfo{
    [self.db open];
    
    NSString *nsstrSql =[NSString stringWithFormat:@"insert into BookShelf(novelId,novelUrl,novelCoverUrl,novelName,novelAuthor,novelCategory,novelStatus,novelLatestUpdateTime,novelLatestChapter,NovelDes,novelListUrl,novelSource,NovelCollectCreateTime)values('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",novelInfo.novelId,novelInfo.novelUrl,novelInfo.novelCoverUrl,novelInfo.novelName,novelInfo.novelAuthor,novelInfo.novelCategory,novelInfo.novelStatus,novelInfo.novelLatestUpdateTime,novelInfo.novelLatestChapter,novelInfo.novelDes,novelInfo.novelListUrl,novelInfo.novelSource,[NSString getCurrentTimeString]];
    
    if ([self isTableOK:@"BookShelf"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}

-(void)addHotWord:(NSString*)nsstrHotWord{
    
    if ([NSString isBlankString:nsstrHotWord]) {
        return;
    }
    
    
    [self.db open];
    
    NSString *nsstrSql =[NSString stringWithFormat:@"insert into HotWord('Word','CreateTime')values('%@','%@')",nsstrHotWord,[NSString getCurrentTimeString]];
    
    if ([self isTableOK:@"HotWord"]) {
        
        if (![self queryHotWord:nsstrHotWord]) {
            [self.db executeUpdate:nsstrSql];
        }else{
            NSString *nsstrdeSql =[NSString stringWithFormat:@"delete from  HotWord where Word='%@'",nsstrHotWord];
            [self.db executeUpdate:nsstrdeSql];
            [self.db executeUpdate:nsstrSql];
        }
    }
    
    [self.db close];
}

-(void)updateBookProgress:(ZY_NovelProgress *)novelProgress{
    [self.db open];
    
    NSString *nsstrSql1 =[NSString stringWithFormat:@"update  ReadProgress  set chapterId='%@' Where novelId='%@'",novelProgress.chapterID,novelProgress.bookID];
    
    NSString *nsstrSql2 =[NSString stringWithFormat:@"update  ReadProgress  set chapterIndex='%@' Where novelId='%@'",novelProgress.chapterIndex,novelProgress.bookID];
    if ([self isTableOK:@"ReadProgress"]) {
        [self.db executeUpdate:nsstrSql1];
        [self.db executeUpdate:nsstrSql2];

    }
    
    [self.db close];
}

-(void)updateBookToShelf:(ZY_NovelInfo*)novelInfo{
    [self.db open];
    
    NSString *nsstrSql =[NSString stringWithFormat:@"update  BookShelf  set novelLatestChapter='%@' Where novelId='%@'",novelInfo.novelLatestChapter,novelInfo.novelId];
    
    if ([self isTableOK:@"BookShelf"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}



-(void)updateBookToShelfAndReadTime:(ZY_NovelInfo*)novelInfo{
    [self.db open];
    
    NSString *nsstrSql =[NSString stringWithFormat:@"update  BookShelf  set NovelCollectCreateTime='%@' Where novelId='%@'",[NSString getCurrentTimeString],novelInfo.novelId];
    
    if ([self isTableOK:@"BookShelf"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}

-(BOOL)queryHotWord:(NSString*)nsstrHotWord{
    [self.db open];
    
    FMResultSet *rs = [self.db executeQuery:@"select count(*) as 'count' from HotWord where Word=?", nsstrHotWord];
    while ([rs next])
    {
        // just print out what we've got in a number of formats.
        NSInteger count = [rs intForColumn:@"count"];
        
        if (0 == count)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    [self.db close];
    return NO;
}

-(BOOL)queryBookIsOnTheBookShelf:(NSString*)novelId{
    [self.db open];
    
    FMResultSet *rs = [self.db executeQuery:@"select count(*) as 'count' from BookShelf where novelId=?", novelId];
    while ([rs next])
    {
        // just print out what we've got in a number of formats.
        NSInteger count = [rs intForColumn:@"count"];
        
        if (0 == count)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    [self.db close];
    
    return NO;
}

-(void)removeBookProgressWithNovelId:(NSString*)novelId{
    [self.db open];
    
    NSString *nsstrSql =[NSString stringWithFormat:@"delete from  ReadProgress where novelId='%@'",novelId];
    if ([self isTableOK:@"ReadProgress"]) {
        [self.db executeUpdate:nsstrSql];
    }
    [self.db close];
}

-(void)removeBookIsOnTheBookShelfWithNovelId:(NSString*)novelId{
    [self.db open];
    
    NSString *nsstrSql =[NSString stringWithFormat:@"delete from  BookShelf where novelId='%@'",novelId];
    
    if ([self isTableOK:@"BookShelf"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}

-(NSMutableArray*)queryProgressInfoList{
    NSString *nsstrSql =@"select * from ReadProgress";
    [self.db open];
    FMResultSet *rs = [self.db executeQuery:nsstrSql];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next])
    {
        ZY_NovelProgress *progress = [[ZY_NovelProgress alloc] init];
        
        progress.bookID = [rs stringForColumn:@"novelId"];
        progress.chapterID = [rs stringForColumn:@"chapterId"];
        progress.chapterIndex = [rs stringForColumn:@"chapterIndex"];

        [array addObject:progress];
    }
    
    [self.db close];
    
    return array;
}

-(NSMutableArray*)queryProgressWithID:(NSString *)bookid{
    
    NSString *nsstrSql = [NSString stringWithFormat:@"select * from ReadProgress where novelId = %@",bookid];
    [self.db open];
    FMResultSet *rs = [self.db executeQuery:nsstrSql];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next])
    {
        ZY_NovelProgress *progress = [[ZY_NovelProgress alloc] init];
        
        progress.bookID = [rs stringForColumn:@"novelId"];
        progress.chapterID = [rs stringForColumn:@"chapterId"];
        progress.chapterIndex = [rs stringForColumn:@"chapterIndex"];
        
        [array addObject:progress];
    }
    
    [self.db close];
    
    return array;
}

- (NSMutableArray*)queryBookShelfInfoList{
    NSString *nsstrSql =@"select * from BookShelf order By NovelCollectCreateTime desc";
    [self.db open];
    FMResultSet *rs = [self.db executeQuery:nsstrSql];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next])
    {
        ZY_NovelInfo *novelInfo = [[ZY_NovelInfo alloc] init];
        novelInfo.novelId=[rs stringForColumn:@"novelId"];
        novelInfo.novelUrl=[rs stringForColumn:@"novelUrl"];
        novelInfo.novelCoverUrl=[rs stringForColumn:@"novelCoverUrl"];
        novelInfo.novelName=[rs stringForColumn:@"novelName"];
        novelInfo.novelAuthor=[rs stringForColumn:@"novelAuthor"];
        novelInfo.novelCategory=[rs stringForColumn:@"novelCategory"];
        novelInfo.novelStatus=[rs stringForColumn:@"novelStatus"];
        novelInfo.novelLatestUpdateTime=[rs stringForColumn:@"novelLatestUpdateTime"];
        novelInfo.novelLatestChapter=[rs stringForColumn:@"novelLatestChapter"];
        novelInfo.novelDes=[rs stringForColumn:@"NovelDes"];
        novelInfo.novelListUrl=[rs stringForColumn:@"novelListUrl"];
        novelInfo.novelSource=[rs stringForColumn:@"novelSource"];
        
        [array addObject:novelInfo];
    }
    
    [self.db close];
    
    return array;
}

- (NSMutableArray*)queryHotWordList{
    NSString *nsstrSql =@"select * from HotWord order By CreateTime desc limit 0,7;";
    [self.db open];
    FMResultSet *rs = [self.db executeQuery:nsstrSql];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next])
    {
        NSString *nsstrKeyword=[rs stringForColumn:@"Word"];
        [array addObject:nsstrKeyword];
    }
    
    [self.db close];
    
    return array;
}

-(void)removeHotWords{
    [self.db open];
    
    NSString *nsstrSql =@"delete from  HotWord ";
    
    if ([self isTableOK:@"HotWord"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}

-(void)removeBookShelves{
    [self.db open];
    
    NSString *nsstrSql =@"delete from  BookShelf ";
    
    if ([self isTableOK:@"BookShelf"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}

-(void)removeBooksProgress{
    [self.db open];
    
    NSString *nsstrSql =@"delete from  ReadProgress ";
    if ([self isTableOK:@"ReadProgress"]) {
        [self.db executeUpdate:nsstrSql];
    }
    [self.db close];
}

-(void)ReplaceBookToReadLasted:(ZY_NovelInfo*)novelInfo{
    [self.db open];
    
    NSString *nsstrSql =@"delete from  LastReadBook ";
    
    if ([self isTableOK:@"LastReadBook"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    
    NSString *nsstrSql1 =[NSString stringWithFormat:@"insert into LastReadBook(novelId,novelUrl,novelCoverUrl,novelName,novelAuthor,novelCategory,novelStatus,novelLatestUpdateTime,novelLatestChapter,NovelDes,novelListUrl,novelSource,NovelCollectCreateTime)values('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",novelInfo.novelId,novelInfo.novelUrl,novelInfo.novelCoverUrl,novelInfo.novelName,novelInfo.novelAuthor,novelInfo.novelCategory,novelInfo.novelStatus,novelInfo.novelLatestUpdateTime,novelInfo.novelLatestChapter,novelInfo.novelDes,novelInfo.novelListUrl,novelInfo.novelSource,[NSString getCurrentTimeString]];
    
    if ([self isTableOK:@"LastReadBook"]) {
        [self.db executeUpdate:nsstrSql1];
    }
    
    
    
    [self.db close];
}

-(ZY_NovelInfo*)queryBookReadLasted{
    NSString *nsstrSql =@"select * from LastReadBook";
    [self.db open];
    FMResultSet *rs = [self.db executeQuery:nsstrSql];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next])
    {
        ZY_NovelInfo *novelInfo = [[ZY_NovelInfo alloc] init];
        novelInfo.novelId=[rs stringForColumn:@"novelId"];
        novelInfo.novelUrl=[rs stringForColumn:@"novelUrl"];
        novelInfo.novelCoverUrl=[rs stringForColumn:@"novelCoverUrl"];
        novelInfo.novelName=[rs stringForColumn:@"novelName"];
        novelInfo.novelAuthor=[rs stringForColumn:@"novelAuthor"];
        novelInfo.novelCategory=[rs stringForColumn:@"novelCategory"];
        novelInfo.novelStatus=[rs stringForColumn:@"novelStatus"];
        novelInfo.novelLatestUpdateTime=[rs stringForColumn:@"novelLatestUpdateTime"];
        novelInfo.novelLatestChapter=[rs stringForColumn:@"novelLatestChapter"];
        novelInfo.novelDes=[rs stringForColumn:@"NovelDes"];
        novelInfo.novelListUrl=[rs stringForColumn:@"novelListUrl"];
        novelInfo.novelSource=[rs stringForColumn:@"novelSource"];
        [array addObject:novelInfo];
        
    }
    
    [self.db close];
    return array.firstObject ;
}

@end

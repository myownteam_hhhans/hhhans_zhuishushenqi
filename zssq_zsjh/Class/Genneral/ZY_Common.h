//
//  Common.h
//  Joke
//
//  Created by S on 2016/10/31.
//  Copyright © 2016年 ___S___. All rights reserved.
//

// APPID、友盟key
#define kAppId @"1456209784"
#define kUMKey @"57d62cc5e0f55ab4280025a3"

#ifdef DEBUG
#define kAdId @"888888888"
#else
#define kAdId kAppId
#endif

//导航栏背景色和Tabbar 选中字体颜色 #48BEFE
#define kNavigationBgColor kRGBCOLOR(255,255,255)

#define kTabbarSelectedColor kRGBCOLOR(237,101,116)

// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ 头条广告 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
#define TouTiao_AppID @"5007895"

#define TouTiao_Splash @"807895932"
#define TouTiao_ReaderBottomBanner @"907895770"
#define TouTiao_ChapterHead @"907895888"
#define TouTiao_LastPage @"907895145"
#define TouTiao_detail @"907895405"
#define TouTiao_Feed @"907895415"  // 信息流
#define TouTiao_Reward @"907895885"
// ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ 头条广告 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ M广告系统 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
#define M_APPKey @"7a06cfa8c2fad9b3c4e2dbde597f199b"
#define M_APPID @"106781"
// 广告位的UnitID
#define M_UnitID_DetailReward @"59273"   // 激励视频          开始阅读
#define M_UnitID_Shelf @"59258"          // 原生图片          书架
#define M_UnitID_LSYReader @"59165"      // 原生图片、视频     阅读器
#define M_UnitID_Open @"59163"           // 原生图片、视频     开屏幕
#define M_UnitID_TotalList @"59156"      // 原生图片          总榜列表
#define M_UnitID_CateList0 @"59155"      // 原生图片          分榜列表0
#define M_UnitID_CateList1 @"59154"      // 原生图片          分榜列表1
#define M_UnitID_CateList2 @"59153"      // 原生图片          分榜列表2
#define M_UnitID_CateList3 @"59114"      // 原生图片          分榜列表3
#define M_UnitID_DetailVideo @"59066"    // 原生图片、视频     详情页
#define M_UnitID_Banner @"60892"         // 原生图片          阅读器Banner
#define M_UnitID_Chapter @"59066"        // 原生图片、视频     阅读器翻页

// ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ M广告系统 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ 洛米广告 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
#define Luomi_key @"3f59b03c0c18cffda711fb2be96c3e69"   // 洛米广告Key
#define Luomi_Z_KaiPing @"ios-21995"                    // 洛米开屏广告
#define Luomi_Z_Pic @"ios-22531"                        // 洛米小图文
#define Luomi_Z_Gif @"ios-22532"                        // 洛米新视频（gif）
#define Luomi_Z_Banner @"ios-22045"                     // 洛米阅读器banner
// ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ 洛米广告 ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

#ifdef DEBUG
#define DebugLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define DebugLog(...)
#endif

#define dispatch_main_async(block) \
dispatch_async(dispatch_get_main_queue(), block)

#define kAPP_VERSION                         \
[[[NSBundle mainBundle] infoDictionary] \
objectForKey:@"CFBundleShortVersionString"]

#define kAPP_Name                         \
[[[NSBundle mainBundle] infoDictionary] \
objectForKey:@"CFBundleDisplayName"]

#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

//#define isIPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? [[UIScreen mainScreen] currentMode].size.height >= 2436 : NO)
#define isIPhoneX ([UIScreen mainScreen].bounds.size.height >= 812)


#define JH_Navigation_Height (isIPhoneX ? 88 : 64)
#define JH_TabbarHome_Height (isIPhoneX ? 24 : 0)

//tabbar 阴影线颜色
#define kTabbarShadowColor kRGBCOLOR(207, 207, 207)

//使用时长和阅读几本书的背景颜色
#define kSettingBottomViewBgColor [[UIColor whiteColor]colorWithAlphaComponent:0.1]

#define kCScreenWidth                   [UIScreen mainScreen].bounds.size.width
#define kCScreenHeight                  [UIScreen mainScreen].bounds.size.height
#define kBookShelfListHeight  82
#define kBookShelfListAdHeight  65
#define k_StatusBarHeight                [UIApplication sharedApplication].statusBarFrame.size.height
#define k_NavigatiBarHeight            self.navigationController.navigationBar.frame.size.height
#define k_TabBarHeight  (isIPhoneX ? 83 : 49)

#define kRGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

#define kCloseChangLiang  @"OpenChangliang"

///////////////////书籍详情页//////////////////////

#define kNovelCoverImgWidth 92
#define kNovelCoverImgHeight 115
#define kLineSpacing 5

#define ZY_WS(weakSelf) __weak __typeof(&*self) weakSelf = self


#import <UIKit/UIKit.h>

static NSString *nsstrInfoStreamAdadUnitID =@"ca-app-pub-6732371368416028/7129168738";

static NSString *nsstrBookIdentifier =@"BookCellIdentifier";

static NSString *nsstrReadingModel =@"ReadingModel";

static inline CGFloat scaleValue(CGFloat value) {
        CGFloat scale = [UIScreen mainScreen].bounds.size.width /750.0;
        return value * scale;
}
static inline UIEdgeInsets sgm_safeAreaInset(UIView *view) {
    if (@available(iOS 11.0, *)) {
        return view.safeAreaInsets;
    }
    return UIEdgeInsetsZero;
}


@interface ZY_Common : NSObject
 
@property(nonatomic,strong) NSMutableArray *bookStoreDataArray;


+(ZY_Common *)shareCommon;
 
/************************************
 函数描述:汇报点击
 作者:S
 时间:2016-11-01
 ************************************/
- (void)gotoiTunes:(id)model;

/************************************
 函数描述:展示汇报
 作者:S
 时间:2016-11-01
 ************************************/
- (void)advertisingDisplayReportWithModel:(id)model;

/************************************
 函数描述:清除图片缓存
 作者:S
 时间:2016-11-01
 ************************************/
-(void)clearImageCache;

/************************************
 函数描述:判断是否是空字符串
 作者:S
 时间:2016-11-12
 ************************************/
+(BOOL)isBlankString:(NSString *)string;

/************************************
 函数描述:合成图片
 作者:S
 时间:2016-11-12
 ************************************/
-(UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2;

/************************************
 函数描述:动态计算文字高度
 作者:S
 时间:2017-03-14
 ************************************/
+ (CGSize)autoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize AttrButeString:(NSDictionary*)dictAttrButes;

/************************************
 函数描述:根据颜色值和矩形区域生成图片
 作者:S
 时间:2016-11-12
 ************************************/
+(UIImage*)imageWithColor:(UIColor*)color WithFrame:(CGRect)rect;

/************************************
 函数描述:根据#颜色值转换颜色
 作者:S
 时间:2016-11-12
 ************************************/
+(UIColor *)colorWithHexString:(NSString *)strColor alpha:(CGFloat)a;
/************************************
 *  设置导航栏主控制器
 *
 *  @param viewController 控制器
 *  @param navBgColor     导航栏颜色
 *  @param image          默认图片名称
 *  @param selImage       选中图片的名称
 *  @param title          标题
 *
 *  @return 返回设置好的导航栏
 ************************************/
+(UINavigationController*)createNavTabBarController:(UIViewController*)viewController withNavBgColor:(UIColor*)navBgColor withImage:(NSString*)image withSelectImage:(NSString*)selImage withTitle:(NSString*)title;
/************************************
 *  过滤字符串中的HTML标签
 *
 *  @param html html字符串
 *
 *  @return 返回过滤完标签的HTML字符串
 ************************************/
-(NSString *)filterHTML:(NSString *)html;
/************************************
 *  转换不可变字典为可变长度字典
 *
 *  @param dict 不可变字典
 *
 *  @return 转换定长字典为可变长度字典
 ************************************/
+(NSMutableDictionary*)convertNSDictionaryToNSMutableDictionary:(NSDictionary*)dict;
/************************************
 *  创建加载失败视图
 *
 *  @param frame 坐标
 *  @param target 事件响应
 *  @param action 事件
 *  @return 转换定长字典为可变长度字典
 ************************************/
+(UIView *)createLoadingErrorViewWithFrame:(CGRect)frame withTarget:(id)target action:(SEL)action;

/************************************
 *  转换数据源为UTF8类型
 ************************************/
+(NSData*)converDataToUtf8Data:(NSData*)currentData;

/************************************
 *  获取当前时间的字符串形式:xxxx-xx-xx hh:mm:ss
 ************************************/
+(NSString*)getCurrentTimeString;

/************************************
 *  根据格式化类型进行格式化字符串
 ************************************/
+(NSString*)getCurrentTimeStringWithFormat:(NSString*)nsstrFormat;

/************************************
 *  获取使用时长
 ************************************/
+(void)settingReadBookUseTime;

/************************************
 *  获取使用时长
 ************************************/
+(NSInteger)getReadBookUseTimeCount;

+(UIColor *)getCateColor:(NSString *)novelCategory;

@end

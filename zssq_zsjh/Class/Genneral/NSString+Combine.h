//
//  NSString+Combine.h
//  ZYNovel
//
//

#import <Foundation/Foundation.h>

@interface NSString (Combine)
/************************************
 *  过滤字符串中的HTML标签
 *
 *  @param html html字符串
 *
 *  @return 返回过滤完标签的HTML字符串
 ************************************/
+(NSString *)filterHTML:(NSString *)html;

/************************************
 函数描述:判断是否是空字符串
 作者:S
 时间:2016-11-12
 ************************************/
+(BOOL)isBlankString:(NSString *)string;

/************************************
 *  根据格式化类型进行格式化字符串
 ************************************/
+(NSString*)getCurrentTimeStringWithFormat:(NSString*)nsstrFormat;

/************************************
 *  限免到期时间
 ************************************/
+(NSString*)getLimitFreeTimeString;

/************************************
 *  比较限免到期时间与当前时间(到期返回YES)
 ************************************/
+(BOOL)getResultComparedRecord:(NSString *)recordTime withNow:(NSString *)now;

/************************************
 *  获取当前时间的字符串形式:xxxx-xx-xx hh:mm:ss
 ************************************/
+(NSString*)getCurrentTimeString;

/************************************
 函数描述:动态计算文字高度
 作者:S
 时间:2017-03-14
 ************************************/
+ (CGSize)autoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize AttrButeString:(NSDictionary*)dictAttrButes;

/************************************
 函数描述:手机通讯录复制空格删除
 作者:C
 时间:2018-04-12
 ************************************/
+ (NSString *)getSeparatedPhoneNumberWithString:(NSString *)phoneString;

@end

//
//  ZY_DBHelper+NovelInfo.h
//  ZYNovel
//
//

#import "ZY_DBHelper.h"
#import "LSYReadModel.h"

@interface ZY_DBHelper (NovelInfo)

-(void)initNovelInfo;


/************************************
 函数描述:查询缓存书籍信息
 作者:C
 时间:2018-05-04
 ************************************/
-(NSMutableArray*)queryLocalBookInfoListWithNovelId:(NSString*)novelId;

/************************************
 函数描述:更新缓存书籍信息
 作者:C
 时间:2018-05-04
 ************************************/
-(void)updateLocalNovelInfo:(LSYReadModel*)readModel withNovelID:(NSString *)novelID;


/************************************
 函数描述:删除缓存书籍
 作者:C
 时间:2018-05-04
 ************************************/
-(void)removeNovelCacheWithNovelId:(NSString*)novelId;

/************************************
 函数描述:添加一本书到书架
 作者:C
 时间:2018-05-04
 ************************************/
-(void)addCacheNovels:(LSYReadModel*)readModel withNovelID:(NSString *)novelID;

@end

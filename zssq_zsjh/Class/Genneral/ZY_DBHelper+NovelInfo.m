//
//  ZY_DBHelper+NovelInfo.m
//  ZYNovel
//
//

#import "ZY_DBHelper+NovelInfo.h"
#import "LSYRecordModel.h"

@implementation ZY_DBHelper (NovelInfo)

-(void)initNovelInfo{
    [self.db open];
    // 初始化数据表
    NSString *createL_ReadInfoSql = @"CREATE TABLE L_ReadInfo ('id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,'novelId' VARCHAR(255),'resource' VARCHAR(500),'content' VARCHAR(500),'price' VARCHAR(100),'isVip' VARCHAR(100),'bookID' VARCHAR(200),'chapterID' VARCHAR(300),'type' VARCHAR(100),'marks' BLOB,'chapters' BLOB,'marksRecord' BLOB,'record' BLOB,'novelInfo' BLOB)";
    
    
    if (![self isTableOK:@"L_ReadInfo"]) {
        [self.db executeUpdate:createL_ReadInfoSql];
    }
  
    [self.db close];
}

-(void)addCacheNovels:(LSYReadModel *)readModel withNovelID:(NSString *)novelID{
    [self.db open];
    
    NSData *marksData = [NSKeyedArchiver archivedDataWithRootObject:readModel.marks];
    NSData *chaptersData = [NSKeyedArchiver archivedDataWithRootObject:readModel.chapters];
    NSData *marksRecordData = [NSKeyedArchiver archivedDataWithRootObject:readModel.marksRecord];
   NSData *recordData = [NSKeyedArchiver archivedDataWithRootObject:readModel.record];
    NSData *novelInfoData = [NSKeyedArchiver archivedDataWithRootObject:readModel.novelInfo];

//    NSString *nsstrSql =[NSString stringWithFormat:@"insert into L_ReadInfo(novelId,resource,content,price,isVip,bookID,chapterID,type,marks,chapters,marksRecord,record,novelInfo)values('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",novelID,[readModel.resource absoluteString],readModel.content,readModel.price,readModel.isVip,readModel.bookID,readModel.chapterID,[NSString stringWithFormat:@"%zd",readModel.type],marksData,chaptersData,marksRecordData,recordData,novelInfoData ];
    
    if ([self isTableOK:@"L_ReadInfo"]) {
       BOOL result = [self.db executeUpdate:@"insert into L_ReadInfo(novelId,resource,content,chapterID,type,marks,chapters,marksRecord,record,novelInfo)values(?,?,?,?,?,?,?,?,?,?)",novelID,[readModel.resource absoluteString],readModel.content,readModel.chapterID,[NSString stringWithFormat:@"%zd",readModel.type],marksData,chaptersData,marksRecordData,recordData,novelInfoData];
        if (result) {
            
        }
    }
    
    [self.db close];
}

-(NSMutableArray*)queryLocalBookInfoListWithNovelId:(NSString*)novelId{
    
    NSString *nsstrSql = [NSString stringWithFormat:@"select * from L_ReadInfo where novelId = %@",novelId];
    //select * from L_ReadInfo where novelId = 214602
    [self.db open];
    FMResultSet *rs = [self.db executeQuery:nsstrSql];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next])
    {
        LSYReadModel *readModel = [[LSYReadModel alloc] init];
        
        NSData *marksData = [rs dataForColumn:@"marks"];
        NSMutableArray * marks  = [NSKeyedUnarchiver unarchiveObjectWithData:marksData];
        //NSString *chaptersD = [rs stringForColumn:@"chapters"];
        //NSData *chaptersData = [chaptersD dataUsingEncoding:NSUTF8StringEncoding];
        NSData *chaptersData = [rs dataForColumn:@"chapters"];
        
        NSMutableArray *chapters   = [NSKeyedUnarchiver unarchiveObjectWithData:chaptersData];
        NSData *marksRecordData = [rs dataForColumn:@"marksRecord"];
        NSMutableDictionary *marksRecord   = [NSKeyedUnarchiver unarchiveObjectWithData:marksRecordData];
        NSData *recordData = [rs dataForColumn:@"record"];
        LSYRecordModel *record = [NSKeyedUnarchiver unarchiveObjectWithData:recordData];
        NSData *novelInfoData = [rs dataForColumn:@"novelInfo"];
        ZY_NovelInfo *novelInfo = [NSKeyedUnarchiver unarchiveObjectWithData:novelInfoData];

       
        readModel.resource =  [[NSURL alloc]initWithString:[NSString stringWithFormat:@"%d",[rs intForColumn:@"resource"]] ];
        readModel.content = [rs stringForColumn:@"content"];
        
        readModel.chapterID = [rs stringForColumn:@"chapterID"];
        readModel.type = [[rs stringForColumn:@"type"] integerValue];
        readModel.marks = marks ;
        readModel.chapters = chapters;
        readModel.marksRecord = marksRecord;
        readModel.record = record;
        readModel.novelInfo = novelInfo;
        
        [array addObject:readModel];
    }
    
    [self.db close];
    
    return array;
}

-(void)updateLocalNovelInfo:(LSYReadModel*)readModel withNovelID:(NSString *)novelID{
    
        NSData *marksData = [NSKeyedArchiver archivedDataWithRootObject:readModel.marks];
        NSData *chaptersData = [NSKeyedArchiver archivedDataWithRootObject:readModel.chapters];
        NSData *marksRecordData = [NSKeyedArchiver archivedDataWithRootObject:readModel.marksRecord];
        NSData *recordData = [NSKeyedArchiver archivedDataWithRootObject:readModel.record];
        NSData *novelInfoData = [NSKeyedArchiver archivedDataWithRootObject:readModel.novelInfo];
    [self.queue inDatabase:^(FMDatabase *db) {
        BOOL result = [db executeUpdate:@"update L_ReadInfo set resource = ?,content = ?,chapterID = ?,type = ?,marks = ?,chapters = ?,marksRecord = ?,record = ?,novelInfo= ? Where novelId= ?",readModel.resource,readModel.content,readModel.chapterID,[NSString stringWithFormat:@"%zd",readModel.type],marksData,chaptersData,marksRecordData,recordData,novelInfoData,novelID];;
        if (result) {
            NSLog(@"插入数据成功");
        } else {
            NSLog(@"插入数据失败");
        }
    }];
    
//    [self.db open];
//    NSData *marksData = [NSKeyedArchiver archivedDataWithRootObject:readModel.marks];
//    NSData *chaptersData = [NSKeyedArchiver archivedDataWithRootObject:readModel.chapters];
//    NSData *marksRecordData = [NSKeyedArchiver archivedDataWithRootObject:readModel.marksRecord];
//    NSData *recordData = [NSKeyedArchiver archivedDataWithRootObject:readModel.record];
//    NSData *novelInfoData = [NSKeyedArchiver archivedDataWithRootObject:readModel.novelInfo];
//
//    if ([self isTableOK:@"L_ReadInfo"]) {
//
//                BOOL result = [self.db executeUpdate:@"update L_ReadInfo set resource = ?,content = ?,price = ?,isVip = ?,bookID = ?,chapterID = ?,type = ?,marks = ?,chapters = ?,marksRecord = ?,record = ?,novelInfo= ? Where novelId= ?",readModel.resource,readModel.content,readModel.price,readModel.isVip,readModel.bookID,readModel.chapterID,[NSString stringWithFormat:@"%zd",readModel.type],marksData,chaptersData,marksRecordData,recordData,novelInfoData,novelID];
//                if (result) {
//
//                }
//
//
//    }
//
//    [self.db close];
}

-(void)removeNovelCacheWithNovelId:(NSString*)novelId{
    [self.db open];
    
    NSString *nsstrSql =[NSString stringWithFormat:@"delete from  L_ReadInfo where novelId='%@'",novelId];
    
    if ([self isTableOK:@"L_ReadInfo"]) {
        [self.db executeUpdate:nsstrSql];
    }
    
    [self.db close];
}

@end

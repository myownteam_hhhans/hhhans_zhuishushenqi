//
//  ZY_BookHelp.m
//  ZYNovel
//
//  Created by S on 2017/3/17.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_BookHelp.h"
#import "ZY_Common.h"
#import "ZY_XPathParser.h"
#import "ZY_XPathParserConfig.h"
#import "ZY_DBHelper.h"
#import "ZY_NovelInfo.h"  
#import "RegexKitLite.h"

static BOOL isUpdata = NO;
@implementation ZY_BookHelp

+(ZY_BookHelp *)shareBookHelp{
    static ZY_BookHelp *zy_bookHelp = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        zy_bookHelp = [[ZY_BookHelp alloc] init];
        zy_bookHelp.dictNovelUpdate = [[NSMutableDictionary alloc] init];
        zy_bookHelp.novelDownLoadQueue = [[NSOperationQueue alloc] init];
        [zy_bookHelp.novelDownLoadQueue setMaxConcurrentOperationCount:1];
    });
    
    return zy_bookHelp;
}

- (void)detectConfigurationTemplateUpdate:(NSString *)nsstrJson {
     
    NSString *nsstrBaseNovelTemplateConfig = nsstrJson;
    
    if (![ZY_Common isBlankString:nsstrBaseNovelTemplateConfig]) {
        
        NSDictionary *newsNovelDictConfig = [NSJSONSerialization JSONObjectWithData:[nsstrBaseNovelTemplateConfig dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
        
        if (newsNovelDictConfig&&[newsNovelDictConfig allKeys].count>0) {
            //[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig=[ZY_Common convertNSDictionaryToNSMutableDictionary:newsNovelDictConfig];
            [[ZY_XPathParserConfig shareXPathParserConfig] writeXPathConfigurationToCacheWithJson:nsstrBaseNovelTemplateConfig];
        }
    }
}

-(void)checkBookUpdate{
    if (isUpdata) {
        return;
    }
     isUpdata = !isUpdata;
    NSMutableArray *arrayBookShelf=[[ZY_DBHelper shareDBHelper] queryBookShelfInfoList];
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t checkUpdateQueue = dispatch_queue_create("com.zybooks.checkbookupdate", DISPATCH_QUEUE_PRIORITY_DEFAULT);
    
    for (ZY_NovelInfo *novelInfo in arrayBookShelf) {
        dispatch_group_async(group, checkUpdateQueue, ^{
            
            NSData *htmlData = [NSData dataWithContentsOfURL:[NSURL URLWithString:novelInfo.novelUrl]];
            
            if (htmlData) {
                ZY_NovelInfo *currentNovelInfo =[[[ZY_XPathParser alloc] init] getNovelDetailByData:[ZY_Common converDataToUtf8Data:htmlData]];
                currentNovelInfo.novelUrl = novelInfo.novelUrl;
                NSString *nsintNovelId =[currentNovelInfo.novelUrl stringByMatching:[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"ID"]];
                currentNovelInfo.novelId=nsintNovelId;
                
                if (![currentNovelInfo.novelLatestUpdateTime isEqualToString:novelInfo.novelLatestUpdateTime]&&![[ZY_BookHelp shareBookHelp].dictNovelUpdate objectForKey:novelInfo.novelId]) {
                    [[ZY_BookHelp shareBookHelp].dictNovelUpdate setObject:currentNovelInfo forKey:novelInfo.novelId];
                }
            }
        });
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        if ([ZY_BookHelp shareBookHelp].dictNovelUpdate) { 
//            dispatch_async(dispatch_get_main_queue(), ^{
                DebugLog(@"检查更新完成.....");
            isUpdata = !isUpdata;
                [[NSNotificationCenter defaultCenter] postNotificationName:ZY_CheckBookUpdateDidFinishedNotification object:nil];
//            });
        }
    });
}
 
@end

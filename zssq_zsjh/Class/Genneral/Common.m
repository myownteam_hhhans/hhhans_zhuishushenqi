//
//  Common.m
//  Palm_Novel
//
//  Created by shiwei on 2018/3/20.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "Common.h"
#import "CFT_parameterOnline.h"

@interface Common()

@property(nonatomic,assign) NSInteger privateZs_Open;
@end

@implementation Common

-(BOOL)zs_Open{
    

    NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"parameterOnline"];
    NSString *nsstrZS_Open=[dict objectForKey:[NSString stringWithFormat:@"GreenData_%@",k_APP_VERSION]];
    if (![Common isBlankString:nsstrZS_Open]) {
        if ([nsstrZS_Open integerValue]==1) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)getADType{   // 根据在线参数随机广告类型

    NSInteger index = arc4random() % 100;
    
    if (index < 20) {
        return @"5";
    } else if (index >= 20 && index <50) {
        return @"4";
    } else {
        return @"3";
    }

}


- (NSString *)randomADTypeWithKey:(NSString *)key {
    //return @"3";
    
    NSString *ADString = [CFT_parameterOnline getValueByKey:[NSString stringWithFormat:@"广告调配%@",k_APP_VERSION]];
    if (!ADString) {
        return nil;
    }
    NSData *ADData = [ADString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *ADDict = [NSJSONSerialization JSONObjectWithData:ADData options:NSJSONReadingMutableContainers error:nil];
    if (![NSJSONSerialization isValidJSONObject:ADDict]) {
        return nil;
    }
    
    CGFloat GDT = ((NSNumber *)ADDict[key][0]).floatValue*100;
    CGFloat Toutiao = ((NSNumber *)ADDict[key][1]).floatValue*100;
    CGFloat LuoMi = ((NSNumber *)ADDict[key][2]).floatValue*100;
    CGFloat Google = ((NSNumber *)ADDict[key][3]).floatValue*100;
    
    NSInteger index = arc4random() % 100;
    
    if (index < GDT) {
        return @"3";
    } else if (index >= GDT && index < GDT + Toutiao) {
        return @"4";
    } else if (index >= GDT + Toutiao && index < GDT + Toutiao + LuoMi){
        return @"5";
    } else if (index >= GDT + Toutiao + LuoMi && index <= 100) {
        return @"1";   // 0为无广告
    } else {
        return nil;
    }
    
}

+(Common *)shareCommon{
    
    static Common *common = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        common = [[Common alloc] init]; 
    });
    return common;
    
}



+(UIColor *)colorWithHexString:(NSString *)strColor alpha:(CGFloat)value
{
    if ((strColor == nil) && (![strColor isEqualToString:@""])) return nil;
    NSString *strProcess = [[strColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    if ([strProcess length] < 6){
        return [UIColor whiteColor];
    }
    
    if ([strProcess hasPrefix:@"#"]){
        strProcess = [strProcess substringFromIndex:1];
    }
    
    if ([strProcess length] != 6){
        return [UIColor whiteColor];
    }
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *strRed = [strProcess substringWithRange:range];
    
    range.location = 2;
    NSString *strGreen = [strProcess substringWithRange:range];
    
    range.location = 4;
    NSString *strBlue = [strProcess substringWithRange:range];
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:strRed] scanHexInt:&r];
    [[NSScanner scannerWithString:strGreen] scanHexInt:&g];
    [[NSScanner scannerWithString:strBlue] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:value];
    
}

+(void)settingControllerBarItemWithTitle:(NSString *)title withController:(UIViewController *)controller withImage:(NSString *)normalImage withSelectedImage:(NSString *)selectedImage{
    
    UITabBarItem *barItem = [[UITabBarItem alloc] init];
    [barItem setTitle:title];
    barItem.image = [[UIImage imageNamed:normalImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    barItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [controller setTabBarItem:barItem];
}
+(CGFloat)getRadiusByMap:(CGFloat)radius{
    const CGFloat aDesignChartImgWidth=375;//设置图是按照750*1334 得出来的
    CGFloat scale =k_ScreenWidth/aDesignChartImgWidth;
    return radius*scale;
}
+(CGFloat)get414RadiusByMap:(CGFloat)radius{
    const CGFloat aDesignChartImgWidth=414;//设置图是按照750*1334 得出来的
    CGFloat scale =k_ScreenWidth/aDesignChartImgWidth;
    
    return radius*scale;
}
+(CGFloat)getFontOfSizeByMap:(CGFloat)fontSize{
    const CGFloat aDesignChartImgWidth=375;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    
    return fontSize*scale;
}
+(CGFloat)get414FontOfSizeByMap:(CGFloat)fontSize{
    const CGFloat aDesignChartImgWidth=414;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    
    return fontSize*scale;
}
+(CGRect)getFrameByMap:(CGRect)tmpFrame{
    
    const CGFloat aDesignChartImgWidth=375;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    CGRect frame = CGRectMake(tmpFrame.origin.x*scale, tmpFrame.origin.y*scale, tmpFrame.size.width*scale,tmpFrame.size.height*scale);
    
    return frame;
}
+(CGRect)get414FrameByMap:(CGRect)tmpFrame{
    
    const CGFloat aDesignChartImgWidth=414;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    CGRect frame = CGRectMake(tmpFrame.origin.x*scale, tmpFrame.origin.y*scale, tmpFrame.size.width*scale,tmpFrame.size.height*scale);
    return frame;
}

+(CGSize)getSizeByMap:(CGFloat)width withHeight:(CGFloat)height{
    const CGFloat aDesignChartImgWidth=375;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    CGSize size = CGSizeMake(width*scale, height*scale);
    
    return size;
}
+(CGSize)get414SizeByMap:(CGFloat)width withHeight:(CGFloat)height{
    const CGFloat aDesignChartImgWidth=414;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    CGSize size = CGSizeMake(width*scale, height*scale);
    
    return size;
}

+(CGPoint)getPointByMap:(CGFloat)x withY:(CGFloat)y{
    
    const CGFloat aDesignChartImgWidth=375;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    CGPoint point = CGPointMake(x*scale, y*scale);
    
    return point;
}
+(CGPoint)get414PointByMap:(CGFloat)x withY:(CGFloat)y{
    
    const CGFloat aDesignChartImgWidth=414;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    CGPoint point = CGPointMake(x*scale, y*scale);
    
    return point;
}

+(UIEdgeInsets)getEdgeInsetsByMap:(UIEdgeInsets)edgeInsets{
    const CGFloat aDesignChartImgWidth=375;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    
    return UIEdgeInsetsMake(edgeInsets.top*scale, edgeInsets.left*scale, edgeInsets.bottom*scale, edgeInsets.right*scale);
}
+(UIEdgeInsets)get414EdgeInsetsByMap:(UIEdgeInsets)edgeInsets{
    const CGFloat aDesignChartImgWidth=414;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    
    return UIEdgeInsetsMake(edgeInsets.top*scale, edgeInsets.left*scale, edgeInsets.bottom*scale, edgeInsets.right*scale);
}

+(CGFloat)getLineSpacing:(CGFloat)lineSpace{
    const CGFloat aDesignChartImgWidth=375;//设置图是按照750*1334 得出来的
    CGFloat scale =(k_ScreenWidth/aDesignChartImgWidth);
    
    return lineSpace*scale;
}

+(UIImage*)imageWithColor:(UIColor*)color WithFrame:(CGRect)rect
{ 
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(BOOL)isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
     
    
    return NO;
}

+(NSString*)getCurrentTimeString{
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}

+(NSString*)getCurrentDayString{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}

+(NSString *)getUnixTime{
    NSDate *date = [NSDate date];
    NSTimeInterval timeInterval = [date timeIntervalSince1970];
    long long dTime = [[NSNumber numberWithDouble:timeInterval] longLongValue]; // 将double转为long long型
    NSString *tempTime = [NSString stringWithFormat:@"%llu",dTime];
   
    return tempTime;
}

+(CGFloat)heightForString:(NSString *)text andWidth:(CGFloat)width andFontSize:(CGFloat)size{
    
    CGSize sizeToFit = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:size]} context:nil].size;
    
    return sizeToFit.height;
}



+(NSString *)encryptReplacingString:(NSString *)nsstrOriginal {
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"=" withString:@"."];
    
    return nsstrOriginal;
}

+(NSMutableArray *)randomAccessData:(NSMutableArray *)dataArray WithSpecifiedNumber:(NSInteger)numberCount{
    
    if (dataArray.count < numberCount) {
        return dataArray;
    }
    
    NSMutableArray *indexRandomArray = [[NSMutableArray alloc] init];
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    
    NSInteger totalCount =dataArray.count-numberCount;
    
    while (YES) {
        
        if (indexRandomArray.count==totalCount) {
            break;
        }
        
        NSString *nsstrRandom = [NSString stringWithFormat:@"%ld",(long)(arc4random() % dataArray.count)];
        
        if (![indexRandomArray containsObject:nsstrRandom]) {
            [indexRandomArray addObject:nsstrRandom];
            [indexSet addIndex:[nsstrRandom integerValue]];
        }
    }
    
    [dataArray removeObjectsAtIndexes:indexSet];
    
    return dataArray;
}



+(NSString*)getCurrentTimeStringWithFormat:(NSString*)nsstrFormat{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:nsstrFormat];
    NSString *nsstrDateTime = [formatter stringFromDate:date];
    
    return nsstrDateTime;
}

+(NSString *)getAppIconName{
    NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
    NSString *iconName = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
    
    return iconName;
}

+(void)setLabelSpace:(UILabel*)label withValue:(NSString*)content withFont:(UIFont*)font withlineSpace:(CGFloat)lineSpace{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing =[Common getLineSpacing:lineSpace]; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@1.5f};
    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:content attributes:dic];
    label.attributedText = attributeStr;
}

+ (void)showDebugHUD:(NSString *)messege {
    
#ifdef DEBUG
    //[SVProgressHUD showWithStatus:messege];
#else

#endif
    
}

@end

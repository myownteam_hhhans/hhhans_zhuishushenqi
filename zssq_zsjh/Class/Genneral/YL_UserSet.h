//
//  YL_UserSet.h
//  ZYNovel
//
//  Created by niegang on 2017/11/23.
//  Copyright © 2017年 shiwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YL_UserSet : NSObject
/************************************
 *  获取UNIX时间戳
 ************************************/
+(NSInteger)getCurrentUnixTime;

/************************************
 *  获取使用时长
 ************************************/
+(void)settingReadBookUseTime;

/************************************
 *  获取使用时长
 ************************************/
+(NSInteger)getReadBookUseTimeCount;
@end

//
//  ZY_BookCategoryModel.m
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ZY_BookCategoryModel.h"

@implementation ZY_BookCategoryModel

+ (ZY_BookCategoryModel *)modelWithDic:(NSDictionary *)dic {
    ZY_BookCategoryModel *model = [[ZY_BookCategoryModel alloc] init];
    model.cateName = dic[@"name"];
    model.bookCount = dic[@"bookCount"];
    
    NSArray *urlArray = dic[@"bookCover"];
    NSString *halfURL = urlArray[0];
    if (halfURL && halfURL.length > 8) {
        halfURL = [halfURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        halfURL = [halfURL substringFromIndex:7];
        model.coverURL = halfURL;
    }
    
    return model;
}

@end

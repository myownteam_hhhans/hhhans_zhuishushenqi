//
//  ZY_HeadEntity.h
//  ZYNovel
//
//  Created by apple on 2018/6/14.
//

#import <Foundation/Foundation.h>

@interface ZY_HeadEntity : NSObject

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *urlKey;
@end

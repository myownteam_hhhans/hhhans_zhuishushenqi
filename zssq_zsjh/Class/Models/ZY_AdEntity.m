//
//  ZY_AdEntity.m
//  ZYNovel
//
//  Created by S on 2017/3/17.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_AdEntity.h"
#import "SHW_ADInterstitial.h"
#import "ZY_Common.h"
#import "CFT_parameterOnline.h"

#define kAdLabelHeight [UIFont systemFontOfSize:19.0f].lineHeight *2
#define kAdDownloadHeight 30
#define DOWNLOADSPACE 10
#define kCommonMargin 10.0f

@implementation ZY_AdEntity

+(ZY_AdEntity* )initWithSHW_AdsModel:(id)model{
    
    ZY_AdEntity* ad = [[ZY_AdEntity alloc]init];
    
    NSString *greenData = [CFT_parameterOnline getValueByKey:[NSString stringWithFormat:@"GreenData_%@",kAPP_VERSION]];
    
    if (greenData == nil && ![greenData isEqualToString:@"1"]) {
        model=nil;
    }
    
    if (model) {
        ad.isAdType=YES;
        SEL selGetName = NSSelectorFromString(@"name");
        SEL selGetadImgPath = NSSelectorFromString(@"adImgPath");
        SEL selGetWidth = NSSelectorFromString(@"width");
        SEL selGetHeight = NSSelectorFromString(@"height");
        SEL selGetAppId = NSSelectorFromString(@"appId");
        SEL selGetAppSourceUrlString = NSSelectorFromString(@"appSourceUrlString");
        SEL selGetAppSummaryString = NSSelectorFromString(@"appSummary");
        SEL selGetAdType = NSSelectorFromString(@"adType");
        SEL selGetAdId = NSSelectorFromString(@"adId");
        
        SuppressPerformSelectorLeakWarning(
                                           ad.name = [model performSelector:selGetName];
                                           );
        
        SuppressPerformSelectorLeakWarning(
                                           ad.adImgPath = [model performSelector:selGetadImgPath];
                                           );
        
        SuppressPerformSelectorLeakWarning(
                                           ad.appSummary = [model performSelector:selGetAppSummaryString];
                                           );
        
        SuppressPerformSelectorLeakWarning(
                                           ad.adType = [model performSelector:selGetAdType];
                                           );
        
        SuppressPerformSelectorLeakWarning(
                                           ad.adId = [model performSelector:selGetAdId];
                                           );
        
        id imageWidth;
        
        SuppressPerformSelectorLeakWarning(
                                           imageWidth = [model performSelector:selGetWidth];
                                           );
        
        id imageHeight;
        
        SuppressPerformSelectorLeakWarning(
                                           imageHeight = [model performSelector:selGetHeight];
                                           );
        
        ad.width = [imageWidth floatValue];
        ad.height = [imageHeight floatValue];
        if ([imageWidth integerValue]==0) {
            ad.width = 350.0f;
        }
        if([imageHeight integerValue]==0){
            ad.height=134.0f;
        }
        
        float imageScale = ad.height/ad.width;
        ad.CellHeight = kCommonMargin+(kCScreenWidth-20)*imageScale+kAdLabelHeight+5+ DOWNLOADSPACE + kAdDownloadHeight +10 +kCommonMargin;
        
        SuppressPerformSelectorLeakWarning(
                                           ad.appId = [model performSelector:selGetAppId];
                                           );
        
        SuppressPerformSelectorLeakWarning(
                                           ad.appSourceUrlString = [model performSelector:selGetAppSourceUrlString];
                                           );
        
    }else{
        ad.isAdType=NO;
        ad.CellHeight = 250.0+30.0f;
    }
    
    return ad;
    
}

@end

//
//  ZY_AdEntity.h
//  ZYNovel
//
//  Created by S on 2017/3/17.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZY_AdEntity : NSObject

@property(nonatomic,strong) NSString *name;

@property(nonatomic,strong) NSString *appId;

@property(nonatomic,strong) NSString *adType;

@property(nonatomic,assign) NSString *adId;

@property(nonatomic,strong) NSString *adImgPath;

@property(nonatomic,strong) NSString *weight;

@property(nonatomic,assign) CGFloat width;

@property(nonatomic,assign) CGFloat height;

@property(nonatomic,strong) NSString *appSourceUrlString;

@property(nonatomic,strong) NSString *appArtwork;

@property(nonatomic,strong) NSString *appSummary;

@property(nonatomic,strong) NSString *appleAppName;

@property(nonatomic,assign) CGFloat CellHeight;

//yes自营，NO谷歌
@property(nonatomic,assign) BOOL isAdType;

+(ZY_AdEntity*)initWithSHW_AdsModel:(id)model;
 
@end

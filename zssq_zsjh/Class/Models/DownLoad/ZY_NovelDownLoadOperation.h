//
//  ZY_NovelDownLoadOperation.h
//  ZYNovel
//
//  Created by S on 2017/3/30.
//  Copyright © 2017年 S. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LSYReadModel.h"

typedef NS_ENUM(NSInteger, novelDownloadType) {
    downloadTypeAll = 0,
    downloadTypeShengYuChapter,
    downloadTypeThe50Chapter,
};


@interface ZY_NovelContentDownLoadInfo : NSObject

@property(nonatomic,strong) LSYReadModel *readModel;
@property(nonatomic, assign) NSInteger StartDownloadIndex;
@property(nonatomic,assign) novelDownloadType downloadType;
@property(nonatomic, assign) NSInteger freeChapNum;

@property (nonatomic, strong) NSArray *boughtArray;

@end


@interface ZY_NovelDownLoadOperation : NSOperation

@property(nonatomic, assign) BOOL isLoading;
@property(nonatomic,strong) ZY_NovelContentDownLoadInfo *downloadInfo;

@property (nonatomic, assign) BOOL isPaused;

@end

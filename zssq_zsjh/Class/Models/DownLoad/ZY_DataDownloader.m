//
//  ZY_DataDownloader.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_DataDownloader.h"
#import "ZY_Common.h"

@interface ZY_DataDownloader()

@property(nonatomic,assign) NSInteger nsintPageIndex;

@property(nonatomic,strong) NSMutableData* mutableData;

@property(nonatomic,strong) NSURLConnection* connection;

@property(nonatomic,assign) NSUInteger type;

@end


@implementation ZY_DataDownloader

-(void)getNovelHtmlDataByUrl:(NSString*)nsstrUrl{
     self.type = 0;
    
    NSString *encodingString = [nsstrUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self startDownload:encodingString];
}
-(void)getNovelContentDataByUrl:(NSString*)nsstrUrl{
    self.type = 1;
    [self startDownload:nsstrUrl];
}

#pragma mark - Parser

-(void)parseHtmlString:(NSData*)data {
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(htmlDataFinish:)] &&data) {
            [self.delegate htmlDataFinish:[ZY_Common converDataToUtf8Data:data]];
        }
    }
}
-(void)parseConetntData:(NSData*)data {
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(ContentDataFinish:)]) { 
            [self.delegate ContentDataFinish:[ZY_Common converDataToUtf8Data:data]];
        }
    }
}

#pragma mark - RequestMethod

-(void)startDownload:(NSString*)urlStr
{
    DebugLog(@"uslStr.....%@",urlStr);
    NSURL* conUrl = [NSURL URLWithString:urlStr];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:conUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:6];
    
    NSString *userAgent = [NSString stringWithFormat:@"zhuishushenqi/%@/%@ (%@; iOS %@; Scale/%0.2f)", [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleExecutableKey] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleIdentifierKey], [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleVersionKey], [[UIDevice currentDevice] model], [[UIDevice currentDevice] systemVersion], [[UIScreen mainScreen] scale]];
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    self.mutableData = [NSMutableData data];
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data
{
    [self.mutableData appendData:data];
}

- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    if (self.delegate != nil) {
        if ([self.delegate respondsToSelector:@selector(downloadNavError:downloadErrorType:)])
        {
            [self.delegate downloadNavError:error downloadErrorType:[self errorType]];
        }
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection*)connection
{
    
    
    
    switch (self.type) {
        case 0:
            [self parseHtmlString:self.mutableData];
            break;
            
        default:
            [self parseConetntData:self.mutableData];
            break;
    }
}

-(downloadErrorType)errorType {
    if (self.type == 0) {
        return downloadErrorType_htmlData;
    }
    
    return downloadErrorType_noKnow;
}

@end

//
//  ZY_DataDownloader.h
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, downloadErrorType) {
    downloadErrorType_noKnow = 0,
    downloadErrorType_htmlData
};

@protocol  ZY_DataDownloaderDelegate <NSObject>

@optional

/************************************
 函数描述:书城页面数据下载成功
 作者:S
 时间:2017-03-13
 ************************************/
-(void)htmlDataFinish:(NSData*)htmlData;

-(void)ContentDataFinish:(NSData*)htmlData;

/************************************
 函数描述:下载失败
 作者:S
 时间:2017-03-13
 ************************************/
-(void)downloadNavError:(NSError*)error downloadErrorType:(downloadErrorType)type;

@end

@interface ZY_DataDownloader : NSObject

@property (nonatomic, weak) id<ZY_DataDownloaderDelegate> delegate;

-(void)getNovelHtmlDataByUrl:(NSString*)nsstrUrl;

-(void)getNovelContentDataByUrl:(NSString*)nsstrUrl;

@end

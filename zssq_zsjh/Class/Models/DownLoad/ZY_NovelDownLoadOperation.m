//
//  ZY_NovelDownLoadOperation.m
//  ZYNovel
//
//  Created by S on 2017/3/30.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_NovelDownLoadOperation.h"
#import "ZY_Common.h"
#import "ZY_XPathParser.h"
#import "NSString+Combine.h"
#import "YL_UserSet.h"
#import "LS_DownloadInfoManager.h"
#import "ZY_JsonParser.h"

@implementation ZY_NovelContentDownLoadInfo


@end

@interface ZY_NovelDownLoadOperation()

@property(nonatomic, strong) NSMutableData *mutableData;

@property(nonatomic, strong) NSURLConnection *urlConnection;


@property(nonatomic, assign) NSInteger downloadTaskQueueIndex;

@property(nonatomic, assign, getter=isExecuting) BOOL executing;

@property(nonatomic, assign, getter=isFinished) BOOL finished;

@property(nonatomic, assign) NSInteger downloadSussessCount;

@property (nonatomic, assign) NSInteger chapterCount;

@end

@implementation ZY_NovelDownLoadOperation

#pragma mark - 重写NSOperation 父类属性----------Begin

// 因为父类的属性是Readonly的，重载时如果需要setter的话则需要手动合成。
@synthesize finished = _finished, executing = _executing;

// 这里需要实现KVO相关的方法，NSOperationQueue是通过KVO来判断任务状态的
- (void)setFinished:(BOOL)finished {
    [self willChangeValueForKey:@"isFinished"];
    _finished = finished;
    [self didChangeValueForKey:@"isFinished"];
}

- (void)setExecuting:(BOOL)executing {
    [self willChangeValueForKey:@"isExecuting"];
    _executing = executing;
    [self didChangeValueForKey:@"isExecuting"];
}

#pragma mark - 重写NSOperation 父类属性----------End

-(NSMutableArray <LSYChapterModel *> *)getsTheListOfCurrentlyDownloadedTasks{
    
    
    
    LSYReadModel *readModel = self.downloadInfo.readModel;
    return readModel.chapters;
}

-(LSYChapterModel*)getCurrentLSYChapterModel{
    LSYChapterModel *chapterModel = nil;
    if ([self getsTheListOfCurrentlyDownloadedTasks].count > self.downloadTaskQueueIndex) {
        chapterModel = [[self getsTheListOfCurrentlyDownloadedTasks] objectAtIndex:self.downloadTaskQueueIndex];
    }
    return chapterModel;
}

-(void)start{
    self.isLoading=YES;
    self.downloadSussessCount = 0;
    self.chapterCount = [self getsTheListOfCurrentlyDownloadedTasks].count;
    
    if (self.downloadInfo.downloadType!=downloadTypeAll) {
        self.downloadTaskQueueIndex=self.downloadInfo.StartDownloadIndex;
    }
    
    
    
    [self beginConnection];
    
    
    
    while (self.isLoading) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    }
}

-(void)cancel{
    [super cancel];
    [self.urlConnection cancel];
    
    [self closeOperation];
    [[LS_DownloadInfoManager shareDownloadInfoManager] cancelItemWithModel:self.downloadInfo.readModel];
    
    // 如果正在执行中则表示已经start过，可以将isFinished设为yes
    if (self.isExecuting) {
        self.finished = YES;
        self.executing = NO;
    }
    
}


-(void)closeOperation{
    
    NSInteger downloadPercent;
    if (self.downloadInfo.downloadType==downloadTypeThe50Chapter) {
        downloadPercent = (float)self.downloadSussessCount/50*100;
    } else {
        downloadPercent = (float)self.downloadTaskQueueIndex/self.chapterCount*100;
    }
    [LS_DownloadInfoManager shareDownloadInfoManager].nowPercent = downloadPercent;
    _downloadTaskQueueIndex++;
    
    
    while (self.isPaused) {
        sleep(1);
    }
    
   // NSLog(@"%ld",self.downloadSussessCount);
    //if ((self.downloadTaskQueueIndex == 1500)
    if ((_downloadTaskQueueIndex >= _chapterCount)
        || (self.downloadInfo.downloadType==downloadTypeThe50Chapter && self.downloadSussessCount == 50)) {//下载完50章，或者全本

        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *nsstrMsgTitle =@"";
            
            if (self.downloadInfo.downloadType == downloadTypeAll) {
                nsstrMsgTitle=@"全本";
            }else if (self.downloadInfo.downloadType ==downloadTypeThe50Chapter){
                nsstrMsgTitle=@"50章";
            }else if (self.downloadInfo.downloadType ==downloadTypeShengYuChapter){
                nsstrMsgTitle=@"剩余";
            }
            
            // 加入下载完成
            [[LS_DownloadInfoManager shareDownloadInfoManager] addItemToDownLoadedArray:self.downloadInfo.readModel];
            

            NSString *strInfo = [NSString stringWithFormat:@"《%@》缓存%@成功",self.downloadInfo.readModel.novelInfo.novelName,nsstrMsgTitle];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"downLoadNovelSuccess" object:strInfo];

            
            
        });
        self.isLoading = NO;
        self.finished = YES;
        self.executing = NO;
    
    }else{
        if (self.finished) {
            return;
        } else {
            self.mutableData = nil;
            [self beginConnection];
        }
        
    }
}

-(void)beginConnection{
    
    LSYChapterModel *chapter = [self getCurrentLSYChapterModel];
    
    while (![NSString isBlankString:[chapter content]] && chapter) {
        _downloadSussessCount++;
        _downloadTaskQueueIndex++;
        chapter = [self getCurrentLSYChapterModel];
        
        if ((_downloadTaskQueueIndex >= _chapterCount)
            || (self.downloadInfo.downloadType==downloadTypeThe50Chapter && self.downloadSussessCount == 50)) {
            [self closeOperation];
            break;
            return;
        }
        
    }
    
    if (chapter) {
        
//        if (![NSString isBlankString:[chapter content]]) {
//            _downloadSussessCount++;
//            [self closeOperation];
//        }else{
        self.executing = YES;
        self.mutableData = [NSMutableData data];
        NSInteger ts = [YL_UserSet getCurrentUnixTime];
        

        NSString *chapterUrl = [NSString stringWithFormat:@"%@",chapter.ChapterUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:chapterUrl]cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:6];
        self.urlConnection = [NSURLConnection connectionWithRequest:request delegate:self];
//        }
    }
    
    
}

-(NSString *)encryptReplacingString:(NSString *)nsstrOriginal {
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"=" withString:@"."];
    
    return nsstrOriginal;
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.mutableData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    LSYChapterModel *chapterModel =[self getCurrentLSYChapterModel];
    
    if (self.mutableData.length>0) {
        self.downloadSussessCount++;
        
        self.downloadInfo.readModel.downloadedNumber++ ;
        
        
        
        NSString *nsstrNovelContent = [[[ZY_JsonParser alloc] init] getNovelContent:[ZY_Common converDataToUtf8Data:self.mutableData]];
        chapterModel.content = nsstrNovelContent;
        
        self.downloadInfo.readModel.content = chapterModel.content;
        self.downloadInfo.readModel.chapterID = chapterModel.chapterID;
        [LSYReadModel updateLocalModel:self.downloadInfo.readModel];
        DebugLog(@"任务编号:%@----开始下载第%ld个任务成功_%@",self.downloadInfo.readModel.novelInfo.novelId,(long)self.downloadTaskQueueIndex,chapterModel.ChapterUrl);
    }
    
    [self closeOperation];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    DebugLog(@"任务编号:%@----开始下载第%ld个任务失败_%@",self.downloadInfo.readModel.novelInfo.novelId,(long)self.downloadTaskQueueIndex, [error description]);
    [self closeOperation];
}

@end

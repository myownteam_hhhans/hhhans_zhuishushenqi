//
//  ZY_XPathParserConfig.h
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import <Foundation/Foundation.h>

/***************
 *HTMLXPath 配置解析器
 *
 ***************/
@interface ZY_XPathParserConfig : NSObject

@property(nonatomic,strong) NSMutableDictionary *novelParserConfig;

+(ZY_XPathParserConfig *)shareXPathParserConfig;

-(void)writeXPathConfigurationToCacheWithJson:(NSString*)nsstrJson;

@end

//
//  ZY_JsonParser.h
//  ZYNovel
//
//  Created by apple on 2018/6/12.

//

#import <Foundation/Foundation.h>

@interface ZY_JsonParser : NSObject

-(NSMutableArray*)getNovelStoreListByData:(NSData*)nsdata;
-(ZY_NovelInfo*)getNovelDetailByData:(NSData*)nsdata;
-(NSMutableArray *)getChapterListByData:(NSData *)nsdata;
-(NSString *)getNovelContent:(NSData *)nsdata;
-(NSMutableArray*)getNovelSearchListByData:(NSData*)nsdata;
-(NSMutableArray*)getShuDanListByData:(NSData*)nsdata;
-(ZY_NovelInfo *)getNovelByDic:(NSDictionary *)dic;
@end

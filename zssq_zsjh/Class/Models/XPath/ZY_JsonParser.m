//
//  ZY_JsonParser.m
//  ZYNovel
//
//  Created by apple on 2018/6/12.

//

#import "ZY_JsonParser.h"
#import "ZY_Common.h"
#import "ZY_ShuDan.h"

@implementation ZY_JsonParser

-(NSMutableArray*)getNovelStoreListByData:(NSData*)nsdata{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if (nsdata) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:nsdata options:kNilOptions error:nil];
        if (dict) {
            if ([[dict allKeys] containsObject:@"books"]) {
                    NSArray *arr_list = [dict objectForKey:@"books"];
                    for (NSDictionary *dic in arr_list) {
                        [array addObject:[self getNovelByDic:dic]];
                    }
            }
        }
    }
    
    return array;
}
-(NSString *)getNovelContent:(NSData *)nsdata{
    
    __block NSString *content = @"";
    if (nsdata) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:nsdata options:NSJSONReadingAllowFragments error:nil];
        
        NSLog(@"contentDict%@",dict);
        
        if (dict) {
            if ([[dict allKeys] containsObject:@"chapter"]) {
                NSDictionary *dic_data = [dict objectForKey:@"chapter"];
                
                if ([[dic_data allKeys] containsObject:@"body"]) {
                    NSString *tmp_content = [dic_data objectForKey:@"body"];
                    if (![ZY_Common isBlankString:tmp_content]) {
                        content = tmp_content;
                    }
                }
                if ([[dic_data allKeys] containsObject:@"cpContent"]) {
                    NSString *tmp_content = [dic_data objectForKey:@"cpContent"];
                    if (![ZY_Common isBlankString:tmp_content]) {
                        content = tmp_content;
                    }
                }
                
                
            }
        }
    }
    return content;
}

-(NSMutableArray *)getChapterListByData:(NSData *)nsdata{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if (nsdata) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:nsdata options:kNilOptions error:nil];
        if (dict) {
            if ([[dict allKeys] containsObject:@"mixToc"]) {
                NSDictionary *dic_data = [dict objectForKey:@"mixToc"];
                
                if ([[dic_data allKeys] containsObject:@"chapters"] && [[dic_data allKeys] containsObject:@"_id"]) {
                    NSArray *arr_AllList = [dic_data objectForKey:@"chapters"];
                    
                    for (NSDictionary *dic in arr_AllList) {
                        LSYChapterModel *m_chapter = [[LSYChapterModel alloc]init];
                        m_chapter.title = [dic objectForKey:@"title"];
                        
                        NSString *url = [self encodeParameter:[dic objectForKey:@"link"]];
                        m_chapter.ChapterUrl = [NSString stringWithFormat:@"https://chapter2.zhuishushenqi.com/chapter/%@",url];
                        
                        [array addObject:m_chapter];
                    }
                    
                }
            } else if ([[dict allKeys] containsObject:@"chapters"]) {
                NSArray *arr_AllList = [dict objectForKey:@"chapters"];
                
                for (NSDictionary *dic in arr_AllList) {
                    LSYChapterModel *m_chapter = [[LSYChapterModel alloc]init];
                    m_chapter.title = [dic objectForKey:@"title"];
                    
                    NSString *url = [self encodeParameter:[dic objectForKey:@"link"]];
                    m_chapter.ChapterUrl = [NSString stringWithFormat:@"https://chapter2.zhuishushenqi.com/chapter/%@",url];
                    
                    [array addObject:m_chapter];
                }
            }
        }
    }
    return array;
}

// url编码
- (NSString *)encodeParameter:(NSString *)originalPara {
    CFStringRef encodeParaCf = CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)originalPara, NULL, CFSTR("!*'();:@&=+$,/?%#[]"), kCFStringEncodingUTF8);
    NSString *encodePara = (__bridge NSString *)(encodeParaCf);
    CFRelease(encodeParaCf);
    return encodePara;
}

-(ZY_NovelInfo*)getNovelDetailByData:(NSData*)nsdata{
    ZY_NovelInfo *zyNovelInfo= [[ZY_NovelInfo alloc] init];

    if (nsdata) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:nsdata options:kNilOptions error:nil];
        if (dict) {
            
            zyNovelInfo = [self getNovelByDic:dict];
            
            zyNovelInfo.isForbitToRead = [[dict objectForKey:@"isForbidForFreeApp"] boolValue];
            zyNovelInfo.novelLatestUpdateTime = [dict objectForKey:@"updated"];
            zyNovelInfo.novelStatus = [dict objectForKey:@"BookStatus"];
            zyNovelInfo.novelLatestChapter = [dict objectForKey:@"lastChapter"];
            zyNovelInfo.novelAuthor = [dict objectForKey:@"author"];
            zyNovelInfo.novelName = [dict objectForKey:@"title"];
            zyNovelInfo.novelDes = [dict objectForKey:@"longIntro"];
            
            if ([dict objectForKey:@"isSerial"]) {
                NSNumber *status = dict[@"isSerial"];
                if (status.boolValue) {
                    zyNovelInfo.novelStatus = @"连载";
                } else {
                    zyNovelInfo.novelStatus = @"完结";
                }
            }
            
            id score = [dict objectForKey:@"rating"];
            if ([score isKindOfClass:[NSDictionary class]]) {
                zyNovelInfo.novelScore = [NSString stringWithFormat:@"%@",[score objectForKey:@"score"]];
            }
            
            NSArray * arr = [dict objectForKey:@"SameCategoryBooks"];
            if (arr) {
                zyNovelInfo.sameNovelList = [[NSMutableArray alloc]initWithArray:arr];
            }
            NSArray * arr_user = [dict objectForKey:@"SameUserBooks"];
            if (arr_user) {
                zyNovelInfo.sameUserNovelList = [[NSMutableArray alloc]initWithArray:arr_user];
            }
            
        }
    }
    return zyNovelInfo;
}
//
-(NSMutableArray*)getNovelSearchListByData:(NSData*)nsdata{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if (nsdata) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:nsdata options:kNilOptions error:nil];
        if (dict) {
            if ([[dict allKeys] containsObject:@"books"]) {
                NSArray *arr_list = [dict objectForKey:@"books"];
                for (NSDictionary *dic in arr_list) {
                    [array addObject:[self getNovelByDic:dic]];
                }
            }
            
        }
    }
    return array;
}

-(NSMutableArray*)getShuDanListByData:(NSData*)nsdata{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if (nsdata) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:nsdata options:kNilOptions error:nil];
        if (dict) {
            if ([[dict allKeys] containsObject:@"data"]) {
                NSArray *arr_list = [dict objectForKey:@"data"];
                for (NSDictionary *dic in arr_list) {
                    ZY_ShuDan *shudan = [[ZY_ShuDan alloc]init];
                    shudan.ShuDanId = [dic objectForKey:@"ListId"];
                    shudan.ShuDanName = [dic objectForKey:@"Title"];
                    shudan.ShuDanInfo = [dic objectForKey:@"Description"];
                    shudan.ShuDanImg = [dic objectForKey:@"CommendImage"];
                    [array addObject:shudan];
                }
            }
            
        }
    }
    return array;
}


-(ZY_NovelInfo *)getNovelByDic:(NSDictionary *)dic{
    ZY_NovelInfo *zyNovelInfo = [[ZY_NovelInfo alloc] init];
    if ([dic.allKeys containsObject:@"BookId"]) {
        zyNovelInfo.novelId = [dic objectForKey:@"BookId"];
    } else if ([dic.allKeys containsObject:@"_id"]) {
        zyNovelInfo.novelId = [dic objectForKey:@"_id"];
    }
    else
    {
        zyNovelInfo.novelId = [dic objectForKey:@"Id"];
    }
    
    if ([dic.allKeys containsObject:@"Name"]) {
       zyNovelInfo.novelName = [dic objectForKey:@"Name"];
    } else if ([dic.allKeys containsObject:@"title"]) {
        zyNovelInfo.novelName = [dic objectForKey:@"title"];
    }
    else
    {
         zyNovelInfo.novelName = [dic objectForKey:@"BookName"];
    }
    
    if ([dic.allKeys containsObject:@"Author"]) {
        zyNovelInfo.novelAuthor = [dic objectForKey:@"Author"];
    } else if ([dic.allKeys containsObject:@"author"]) {
        zyNovelInfo.novelAuthor = [dic objectForKey:@"author"];
    }
    
    if ([dic.allKeys containsObject:@"CName"]) {
        zyNovelInfo.novelCategory = [dic objectForKey:@"CName"];
    } else if ([dic.allKeys containsObject:@"cat"]) {
        zyNovelInfo.novelCategory = [dic objectForKey:@"cat"];
    }else if ([dic.allKeys containsObject:@"majorCate"]) {
        zyNovelInfo.novelCategory = [dic objectForKey:@"majorCate"];
    }
    else{
        zyNovelInfo.novelCategory = [dic objectForKey:@"CategoryName"];
    }
    
    
    
    
    zyNovelInfo.novelScore = [dic objectForKey:@"Score"];
    if ([dic.allKeys containsObject:@"Img"]) {
        
        zyNovelInfo.novelCoverUrl = [NSString stringWithFormat:@"https://shuapi.jiaston.com/BookFiles/BookImages/%@",[dic objectForKey:@"Img"]];
    } else if ([dic.allKeys containsObject:@"cover"]) {
        NSString *string  = dic[@"cover"];
        if (string && string.length>8) {
            string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            string = [string substringFromIndex:7];
            zyNovelInfo.novelCoverUrl = string;
        }
    }
    else
    {
        zyNovelInfo.novelCoverUrl = [NSString stringWithFormat:@"https://shuapi.jiaston.com/BookFiles/BookImages/%@",[dic objectForKey:@"BookImage"]];
    }
    
    zyNovelInfo.novelSource = @"oldzhuishu";
    
    if ([dic.allKeys containsObject:@"Desc"]) {
        zyNovelInfo.novelDes = [dic objectForKey:@"Desc"];
    } else if ([dic.allKeys containsObject:@"shortIntro"]) {
        zyNovelInfo.novelDes = [dic objectForKey:@"shortIntro"];
    }else{
        zyNovelInfo.novelDes = [dic objectForKey:@"Description"];
    }
    
    if ([dic.allKeys containsObject:@"BookVote"]) {
        NSDictionary *votedic = [dic objectForKey:@"BookVote"];
        if ([votedic.allKeys containsObject:@"Score"]) {
            if ([[votedic objectForKey:@"Score"] floatValue] > 0) {
                zyNovelInfo.novelScore = [NSString stringWithFormat:@"%@",[votedic objectForKey:@"Score"]];
            }
        }
    }
    
    zyNovelInfo.novelListUrl = [NSString stringWithFormat:@"https://api.zhuishushenqi.com/mix-toc/%@/",zyNovelInfo.novelId];
    zyNovelInfo.retentionRatio = dic[@"retentionRatio"];
    zyNovelInfo.ratedNumber = dic[@"latelyFollower"];
    zyNovelInfo.WordCountFormat = ((NSNumber *)dic[@"wordCount"]).stringValue;
    if (!dic[@"latelyFollower"]) {
        zyNovelInfo.ratedNumber = @0;
    }
    if (!dic[@"retentionRatio"] || [dic[@"retentionRatio"] isKindOfClass:[NSNull class]]) {
        zyNovelInfo.retentionRatio = @0;
    }
    return zyNovelInfo;
}


@end

//
//  ZY_XPathParserConfig.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_XPathParserConfig.h"
#import "ZY_Common.h" 

@implementation ZY_XPathParserConfig

+(ZY_XPathParserConfig *)shareXPathParserConfig{
    static ZY_XPathParserConfig *parserConfig = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        parserConfig = [[ZY_XPathParserConfig alloc] init];
        if ([parserConfig checkCacheConfiguratioFileExists]){
            [parserConfig loadCacheConfigurationFile];
        }else{
            [parserConfig loadLocalConfigurationFile];
        }
    });
    
    return parserConfig;
}

/************************************
 方法描述:检查是否有缓存
 作者:S
 时间:2017-03-28
 ************************************/
-(NSString*)getCacheConfiguratioFilePath{
    NSString *nsstrCacheDirPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) [0];
    NSString *nsstrCacheConfiguratioFilePath = [nsstrCacheDirPath stringByAppendingPathComponent:@"XPath.json"];
    
    return nsstrCacheConfiguratioFilePath;
}

/************************************
 方法描述:检查是否有缓存
 作者:S
 时间:2017-03-28
 ************************************/
-(BOOL)checkCacheConfiguratioFileExists{
    NSString *nsstrCacheConfiguratioFilePath =[self getCacheConfiguratioFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:nsstrCacheConfiguratioFilePath]) {
        NSData *configData = [NSData dataWithContentsOfFile:nsstrCacheConfiguratioFilePath];
        NSString *nsstrJson= [[NSString alloc] initWithData:configData encoding:NSUTF8StringEncoding];
        NSDictionary *novelConfigDict =[NSJSONSerialization JSONObjectWithData:[nsstrJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
        if (novelConfigDict&&[novelConfigDict allKeys].count>0) {
            return YES;
        }
    }

    return NO;
}


/************************************
 方法描述:加载本地配置文件
 作者:S
 时间:2017-03-28
 ************************************/
-(void)loadLocalConfigurationFile{
    NSString *nsstrXPathFilePath = [[NSBundle mainBundle] pathForResource:@"XPath" ofType:@"json"];
    NSString *json = [[NSString alloc] initWithContentsOfFile:nsstrXPathFilePath encoding:NSUTF8StringEncoding error:nil];
    [self writeXPathConfigurationToCacheWithJson:json];
}


/************************************
 方法描述:加载缓存配置文件
 作者:S
 时间:2017-03-28
 ************************************/
-(void)loadCacheConfigurationFile{
    NSString *nsstrCacheConfiguratioFilePath =[self getCacheConfiguratioFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:nsstrCacheConfiguratioFilePath]) {
        NSData *configData = [NSData dataWithContentsOfFile:nsstrCacheConfiguratioFilePath];
        NSString *nsstrJson=[[NSString alloc] initWithData:configData encoding:NSUTF8StringEncoding];
        
        if (![ZY_Common isBlankString:nsstrJson]) {
            [self parseJsonToConfigDictWithJson:nsstrJson];
        }
    }
}

/************************************
 方法描述:写入XPath到缓存
 作者:S
 时间:2017-03-28
 ************************************/
-(void)writeXPathConfigurationToCacheWithJson:(NSString*)nsstrJson{
    NSString *nsstrCacheConfiguratioFilePath =[self getCacheConfiguratioFilePath];
    NSData *jsonData = [nsstrJson dataUsingEncoding:NSUTF8StringEncoding];
    
    if ([jsonData length]) {
        [[NSFileManager defaultManager] removeItemAtPath:nsstrCacheConfiguratioFilePath error:NULL];
        
        
        BOOL isWriteSuccess =[[NSFileManager defaultManager] createFileAtPath:nsstrCacheConfiguratioFilePath
                                                contents:jsonData
                                              attributes:nil];
        
        if (isWriteSuccess) {
            DebugLog(@"写入缓存成功");
        }
    }
    
    [self parseJsonToConfigDictWithJson:nsstrJson];
}

-(void)parseJsonToConfigDictWithJson:(NSString *)nsstrJson{
    self.novelParserConfig = [[NSMutableDictionary alloc] init];
    
    if (nsstrJson != nil && ![nsstrJson isEqualToString:@""]) {
        self.novelParserConfig = [[NSMutableDictionary alloc] init];
        NSDictionary *tempDict = [NSJSONSerialization JSONObjectWithData:[nsstrJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
        self.novelParserConfig = [ZY_Common convertNSDictionaryToNSMutableDictionary:tempDict];
    }
}

@end

//
//  ZY_XPathParser.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_XPathParser.h"
#import "ZY_XPathParserConfig.h"
#import "NSString+HTML.h"
#import <Ono.h>
#import "RegexKitLite.h"

@implementation ZY_XPathParser

-(NSMutableArray*)getNovelStoreListByData:(NSData*)nsdata{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSError *error;
    ONOXMLDocument *doc=[ONOXMLDocument HTMLDocumentWithData:nsdata error:&error];
    
    if (!error) {
        
        NSString *nsstrNovelDoMain =[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"DoMain"];
        
        NSString *nsstrNovelListPath = [[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"Store"] objectForKey:@"ListPath"];
        
        NSString *nsstrNovelNameXPath= [[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"Store"] objectForKey:@"Name"];
        
        NSString *nsstrNovelCoverUrlXPath= [[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"Store"] objectForKey:@"CoverUrl"];
        
        NSString *nsstrNovelDesXPath= [[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"Store"] objectForKey:@"Des"];
        
        NSString *nsstrNovelAuthorXPath= [[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"Store"] objectForKey:@"Author"];
        
        
        [doc enumerateElementsWithXPath:nsstrNovelListPath usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
            ZY_NovelInfo *zyNovelInfo = [[ZY_NovelInfo alloc] init];
            
            [element enumerateElementsWithXPath:nsstrNovelNameXPath usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelName=elemenA.stringValue;
                
                NSString *nsstrHrefUrl =[elemenA.attributes objectForKey:@"href"];
                
                if ([nsstrHrefUrl hasPrefix:@"http"]) {
                    zyNovelInfo.novelUrl=nsstrHrefUrl;
                }else{
                    zyNovelInfo.novelUrl=[NSString stringWithFormat:@"%@%@",nsstrNovelDoMain,[elemenA.attributes objectForKey:@"href"]];
                }
                
            }];
            
            [element enumerateElementsWithXPath:nsstrNovelCoverUrlXPath usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelCoverUrl=[elemenA.attributes objectForKey:@"src"];
            }];
            
            [element enumerateElementsWithXPath:nsstrNovelDesXPath usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelDes=[elemenA.stringValue stringByReplacingOccurrencesOfString:@"简介：    " withString:@""];
            }];
            
            [element enumerateElementsWithXPath:nsstrNovelAuthorXPath usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelAuthor=[elemenA.stringValue stringByReplacingOccurrencesOfString:@"作者：" withString:@""];
            }];
            
            NSString *nsintNovelId =[zyNovelInfo.novelUrl stringByMatching:[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"ID"]];
            zyNovelInfo.novelId=nsintNovelId;
            
            
            [array addObject:zyNovelInfo];
        }];
    }
    
    return array;
}
-(NSString *)getNovelContent:(NSData *)nsdata{
    NSString *nsstrNovelContentPath = [[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"ReaderContent"] objectForKey:@"Content"];
    return [self getNovelContent:nsdata Path:nsstrNovelContentPath];
}
-(NSString *)getNovelContent:(NSData *)nsdata Path:(NSString *)nsstrNovelContentPath{
    NSError *error;
    __block NSString *content = @"";
    
    ONOXMLDocument *doc = [ONOXMLDocument HTMLDocumentWithData:nsdata error:&error];
    
    
    
    [doc enumerateElementsWithXPath:nsstrNovelContentPath usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
        
        content = [[NSString stringWithFormat:@"%@",element] stringByReplacingOccurrencesOfString:@"<br /><br />" withString:@"{zhanweifu}"];
        
        content = [content stringByReplacingOccurrencesOfString:@"<br />" withString:@"{zhanweifu}"];
        
        content = [content stringByReplacingOccurrencesOfString:@"<br/>" withString:@"{zhanweifu}"];
        
        content = [content stringByReplacingOccurrencesOfString:@"<br>" withString:@"{zhanweifu}"];
        
        content = [content stringByConvertingHTMLToPlainText];
        
        content = [content stringByReplacingOccurrencesOfString:@"{zhanweifu}" withString:@"\n"];
        
        content = [content stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (![content hasPrefix:@" "]) {
            content = [NSString stringWithFormat:@"  %@",content];
        }
    }];
    return content;
}
-(NSMutableArray *)getChapterListByData:(NSData *)nsdata{
    NSError *error;
    NSString *nsstrNovelDoMain =[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"DoMain"];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSString *nsstrNovelChapterListPath = [[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"ReaderContent"] objectForKey:@"ChapterList"];
    BOOL isSortAsc = [[[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"ReaderContent"] objectForKey:@"ChapterSortAsc"] boolValue];
    
    ONOXMLDocument *doc=[ONOXMLDocument HTMLDocumentWithData:nsdata error:&error];
    ONOXMLElement *postsParentElement= [doc firstChildWithXPath:nsstrNovelChapterListPath];
    [postsParentElement.children enumerateObjectsUsingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL * _Nonnull stop) {
        [element enumerateElementsWithXPath:@"a" usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
            LSYChapterModel *m_chapter = [[LSYChapterModel alloc]init];
            m_chapter.title = elemenA.stringValue;
            
            NSString *nsstrHrefUrl =[elemenA.attributes objectForKey:@"href"];
            
            if ([nsstrHrefUrl hasPrefix:@"http"]) {
                m_chapter.ChapterUrl=nsstrHrefUrl;
            }else{
                m_chapter.ChapterUrl = [NSString stringWithFormat:@"%@%@",nsstrNovelDoMain,[elemenA.attributes objectForKey:@"href"]];
            }
            if (isSortAsc) {
                [array addObject:m_chapter];
            }else{
                [array insertObject:m_chapter atIndex:0];
            }
        }];
        
    }];
    return array;
}
-(ZY_NovelInfo*)getNovelDetailByData:(NSData*)nsdata{
    ZY_NovelInfo *novelInfo= [[ZY_NovelInfo alloc] init];
    
    NSError *error;
    ONOXMLDocument *doc=[ONOXMLDocument HTMLDocumentWithData:nsdata error:&error];
    
    if (!error) {
        
        NSString *nsstrNovelDoMain =[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"DoMain"];
        
        NSDictionary *dictNovelDetail = [[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"Detail"];
        
        if (dictNovelDetail) {
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"CoverUrl"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelCoverUrl=[element.attributes objectForKey:@"src"];
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"Name"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelName=element.stringValue;
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"Author"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelAuthor=element.stringValue;
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"Cate"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelCategory=element.stringValue;
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"Status"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelStatus=element.stringValue;
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"LatestUpdateTime"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelLatestUpdateTime=element.stringValue;
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"LatestChapter"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelLatestChapter=element.stringValue;
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"Des"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                novelInfo.novelDes=element.stringValue;
            }];
            
            [doc enumerateElementsWithXPath:[dictNovelDetail objectForKey:@"ListUrl"] usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
                NSString *nsstrNovelListUrl =[element.attributes objectForKey:@"href"];
                if ([nsstrNovelListUrl hasPrefix:@"http"]) {
                    novelInfo.novelListUrl=nsstrNovelListUrl;
                }else{
                    novelInfo.novelListUrl= [NSString stringWithFormat:@"%@%@",nsstrNovelDoMain,nsstrNovelListUrl];
                }
                
            }];
            
            novelInfo.novelSource=nsstrNovelDoMain;
        }
    }
    
    return novelInfo;
}

-(NSMutableArray*)getNovelSearchListByData:(NSData*)nsdata{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    NSError *error;
    ONOXMLDocument *doc=[ONOXMLDocument HTMLDocumentWithData:nsdata error:&error];
    
    if (!error) { 
        
        NSDictionary *dictNovelSearch = [[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"Search"];
        
        ONOXMLElement *postsParentElement2= [doc firstChildWithXPath:[dictNovelSearch objectForKey:@"ListPath"]];
        [postsParentElement2.children enumerateObjectsUsingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL * _Nonnull stop) {
             ZY_NovelInfo *zyNovelInfo = [[ZY_NovelInfo alloc] init];
            
            [element enumerateElementsWithXPath:[dictNovelSearch objectForKey:@"Name"] usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelName=elemenA.stringValue;
                zyNovelInfo.novelUrl=[[elemenA.attributes objectForKey:@"href"] stringByReplacingOccurrencesOfString:@"www." withString:@"m."];
            }];
            
            [element enumerateElementsWithXPath:[dictNovelSearch objectForKey:@"CoverUrl"] usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelCoverUrl=[elemenA.attributes objectForKey:@"src"];
            }];
            
            [element enumerateElementsWithXPath:[dictNovelSearch objectForKey:@"Des"] usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelDes=[elemenA.stringValue stringByReplacingOccurrencesOfString:@"简介：    " withString:@""];
            }];
            
            [element enumerateElementsWithXPath:[dictNovelSearch objectForKey:@"Author"] usingBlock:^(ONOXMLElement *elemenA, NSUInteger idx, BOOL *stop) {
                zyNovelInfo.novelAuthor=[elemenA.stringValue stringByReplacingOccurrencesOfString:@"作者：" withString:@""];
            }];
            
            NSString *nsintNovelId =[zyNovelInfo.novelUrl stringByMatching:[[ZY_XPathParserConfig shareXPathParserConfig].novelParserConfig objectForKey:@"ID"]];
            zyNovelInfo.novelId=nsintNovelId;
            
            [array addObject:zyNovelInfo];
        }];
        
    }
    
    return array;
}

@end

//
//  ZY_XPathParser.h
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZY_NovelInfo.h"
#import "LSYChapterModel.h"

@interface ZY_XPathParser : NSObject

/************************************
 方法描述:获取书城列表数据
 作者:S
 时间:2017-03-13
 ************************************/
-(NSMutableArray*)getNovelStoreListByData:(NSData*)nsdata;

/************************************
 方法描述:获取小说详情页数据
 作者:S
 时间:2017-03-13
 ************************************/
-(ZY_NovelInfo*)getNovelDetailByData:(NSData*)nsdata;

/************************************
 方法描述:获取搜索结果
 作者:S
 时间:2017-03-13
 ************************************/
-(NSMutableArray*)getNovelSearchListByData:(NSData*)nsdata;

/************************************
方法描述:获取小说内容
作者:S
时间:2017-03-27
************************************/
-(NSString *)getNovelContent:(NSData *)nsdata;

    
/************************************
方法描述:获取章节列表数组
作者:S
时间:2017-03-27
************************************/

-(NSMutableArray *)getChapterListByData:(NSData *)nsdata;

-(NSString *)getNovelContent:(NSData *)nsdata Path:(NSString *)nsstrNovelContentPath;
@end

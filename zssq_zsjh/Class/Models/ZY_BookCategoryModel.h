//
//  ZY_BookCategoryModel.h
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZY_BookCategoryModel : NSObject

@property (nonatomic, strong) NSString *cateName;
@property (nonatomic, strong) NSNumber *bookCount;
@property (nonatomic, strong) NSString *coverURL;

+ (ZY_BookCategoryModel *)modelWithDic:(NSDictionary *)dic;

@end





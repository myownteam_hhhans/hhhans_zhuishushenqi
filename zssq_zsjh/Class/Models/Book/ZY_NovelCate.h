//
//  ZY_NovelCate.h
//  ZYNovel
//
//  Created by apple on 2018/6/13.
//

#import <Foundation/Foundation.h>

@interface ZY_NovelCate : NSObject
//分类ID
@property(nonatomic,strong) NSString *cateId;

//分类描述
@property(nonatomic,strong) NSString *cateInfo;

//分类名称
@property(nonatomic,strong) NSString *cateName;

@end

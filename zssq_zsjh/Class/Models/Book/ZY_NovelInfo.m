//
//  ZYNovelInfo.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_NovelInfo.h"
#import "RegexKitLite.h"
#import "ZY_XPathParserConfig.h"
#import "ZY_Common.h"

@implementation ZY_NovelInfo
-(NSString *)novelScore{
    return [NSString stringWithFormat:@"%@",_novelScore];
}

-(NSString *)novelId{
    return [NSString stringWithFormat:@"%@",_novelId];
}

-(NSString *)novelName{
    return [[[ZY_Common shareCommon] filterHTML:_novelName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString *)novelDes{
    return [[ZY_Common shareCommon] filterHTML:_novelDes];
}

-(NSString *)novelAuthor{
    return [[[[ZY_Common shareCommon] filterHTML:_novelAuthor] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

-(NSString *)novelLatestChapter{
    
    return [_novelLatestChapter stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString *)novelCategory{
    return [_novelCategory stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

-(NSString *)novelCoverUrl{
    return [_novelCoverUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

-(CGFloat)novelContentDescHeight{
    if (!self.novelDes) {
        return 0;
    }
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.novelDes];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:kLineSpacing];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.novelDes length])];
    
    CGFloat height =[ZY_Common autoCalculateRectWith:self.novelDes FontSize:15 MaxSize:CGSizeMake(kCScreenWidth-20, CGFLOAT_MAX) AttrButeString:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSParagraphStyleAttributeName:paragraphStyle.copy}].height;
    
    return height;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.novelId forKey:@"novelId"];
    [aCoder encodeObject:self.novelUrl forKey:@"novelUrl"];
    [aCoder encodeObject:self.novelCoverUrl forKey:@"novelCoverUrl"];
    [aCoder encodeObject:self.novelName forKey:@"novelName"];
    [aCoder encodeObject:self.novelAuthor forKey:@"novelAuthor"];
    [aCoder encodeObject:self.novelCategory forKey:@"novelCategory"];
    [aCoder encodeObject:self.novelStatus forKey:@"novelStatus"];
    [aCoder encodeObject:self.novelLatestUpdateTime forKey:@"novelLatestUpdateTime"];
    [aCoder encodeObject:self.novelLatestChapter forKey:@"novelLatestChapter"];
    [aCoder encodeObject:self.novelDes forKey:@"novelDes"];
    [aCoder encodeObject:self.novelListUrl forKey:@"novelListUrl"];
    [aCoder encodeObject:self.novelSource forKey:@"novelSource"];
    [aCoder encodeObject:self.novelScore forKey:@"novelScore"];
    [aCoder encodeObject:self.sameNovelList forKey:@"sameNovelList"];
    [aCoder encodeObject:self.sameUserNovelList forKey:@"sameUserNovelList"];
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.novelId = [aDecoder decodeObjectForKey:@"novelId"];
        self.novelUrl = [aDecoder decodeObjectForKey:@"novelUrl"];
        self.novelCoverUrl = [aDecoder decodeObjectForKey:@"novelCoverUrl"];
        self.novelName = [aDecoder decodeObjectForKey:@"novelName"];
        self.novelAuthor = [aDecoder decodeObjectForKey:@"novelAuthor"];
        self.novelCategory = [aDecoder decodeObjectForKey:@"novelCategory"];
        self.novelStatus = [aDecoder decodeObjectForKey:@"novelStatus"];
        self.novelLatestUpdateTime = [aDecoder decodeObjectForKey:@"novelLatestUpdateTime"];
        self.novelLatestChapter = [aDecoder decodeObjectForKey:@"novelLatestChapter"];
        self.novelDes = [aDecoder decodeObjectForKey:@"novelDes"];
        self.novelListUrl = [aDecoder decodeObjectForKey:@"novelListUrl"];
        self.novelSource = [aDecoder decodeObjectForKey:@"novelSource"];
        self.novelScore = [aDecoder decodeObjectForKey:@"novelScore"];
        self.sameNovelList = [aDecoder decodeObjectForKey:@"sameNovelList"];
        self.sameUserNovelList = [aDecoder decodeObjectForKey:@"sameUserNovelList"];
        
    }
    return self;
}
@end

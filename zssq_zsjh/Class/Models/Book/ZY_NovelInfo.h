//
//  ZYNovelInfo.h
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZY_NovelInfo : NSObject

//小说ID
@property(nonatomic,strong) NSString *novelId;

//详情页URL
@property(nonatomic,strong) NSString *novelUrl;

//封面图（头图）URL
@property(nonatomic,strong) NSString *novelCoverUrl;

//小说名称
@property(nonatomic,strong) NSString *novelName;

//作者
@property(nonatomic,strong) NSString *novelAuthor;

//小说分类
@property(nonatomic,strong) NSString *novelCategory;

//小说状态
@property(nonatomic,strong) NSString *novelStatus;
// 是否完结
@property(nonatomic,assign) BOOL Finished;

//最后更新时间
@property(nonatomic,strong) NSString *novelLatestUpdateTime;

//最新章节
@property(nonatomic,strong) NSString *novelLatestChapter;

//小说简介
@property(nonatomic,strong) NSString *novelDes;

//目录URL
@property(nonatomic,strong) NSString *novelListUrl;

//小说来源
@property(nonatomic,strong) NSString *novelSource;

//小说评分
@property(nonatomic,strong) NSString *novelScore;

//小说相似推荐
@property(nonatomic,strong) NSMutableArray *sameNovelList;

//同一作者小说
@property(nonatomic,strong) NSMutableArray *sameUserNovelList;

//小说描述高度
@property(nonatomic,assign) CGFloat novelContentDescHeight;

@property(nonatomic,assign) BOOL isForbitToRead;

@property (nonatomic, strong) NSString *WordCountFormat;

@property (nonatomic, strong) NSNumber *ratedNumber;

@property (nonatomic, strong) NSNumber *retentionRatio; // 留存

@end

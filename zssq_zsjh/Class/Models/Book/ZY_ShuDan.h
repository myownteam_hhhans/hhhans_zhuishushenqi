//
//  ZY_ShuDan.h
//  ZYNovel
//
//  Created by apple on 2018/7/11.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZY_ShuDan : NSObject

//书单ID
@property(nonatomic,strong) NSString *ShuDanId;

//书单描述
@property(nonatomic,strong) NSString *ShuDanInfo;

//书单名称
@property(nonatomic,strong) NSString *ShuDanName;

//书单图片
@property(nonatomic,strong) NSString *ShuDanImg;

@end

//
//  ZY_NovelDesTableViewCell.h
//  ZYNovel
//
//  Created by S on 2017/3/14.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZY_NovelRecommendTableViewCellDelegate <NSObject>

@optional

-(void)clickRecommendNovel:(ZY_NovelInfo*)currentNovel;

@end

@interface ZY_NovelRecommendTableViewCell : UITableViewCell

@property (nonatomic, weak) id<ZY_NovelRecommendTableViewCellDelegate> delegate;

@property(nonatomic,strong) UILabel *labelTitle;
-(void)bindRecommendNovelWithCurrentNovel;
-(void)createControl;
@property(nonatomic,strong) NSMutableArray *recommendNovelDataArray;
@end

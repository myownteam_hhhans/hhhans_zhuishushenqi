//
//  ZY_NovelInfoTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/14.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_NovelInfoTableViewCell.h"
#import "ZY_Common.h"
#import "UIImageView+WebCache.h"
#import "ZY_DBHelper.h"
#import "SVProgressHUD.h"
#import "BookScoreView.h"

#define kPaddingLeft 18
#define kButtonWidth (kCScreenWidth-15*4)/2
#define kImageViewRightMargin 20


@interface ZY_NovelInfoTableViewCell()

@property(nonatomic,strong) UIImageView *novelimageView;

@property(nonatomic,strong) UILabel *labelNovelName;

@property(nonatomic,strong) UILabel *labelNovelAuthor;

@property(nonatomic,strong) UILabel *labelCategory;

@property(nonatomic,strong) UILabel *labelStatus;

@property(nonatomic,strong) UILabel *labelLatestChapter;

@property(nonatomic,strong) UIButton *btnAddBookShelf;

@property(nonatomic,strong) UIButton *btnReadNow;

@property(nonatomic,strong) ZY_NovelInfo *currentNovelInfo;

@property(nonatomic,strong) UIView *bgShadowView;

@property(nonatomic,strong) UILabel *labelNovelDes;

@property(nonatomic,strong) UIView *bgView;

@property(nonatomic,strong) UIImageView *clickImageView;

@property(nonatomic,strong) BookScoreView *scoreView;

@end

@implementation ZY_NovelInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setBackgroundColor:kRGBCOLOR(242, 242, 242)];
        
        self.bgView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, 200)];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readerAddBookOnTheBookShelf) name:@"addToBookShelf" object:nil];
        [self.bgView setBackgroundColor:[UIColor whiteColor]];
        [self.bgView addSubview:self.bgShadowView];
        [self.bgShadowView addSubview:self.novelimageView];
        [self.bgView addSubview:self.labelNovelName];
        [self.bgView addSubview:self.scoreView];
        [self.bgView addSubview:self.labelNovelAuthor];
        [self.bgView addSubview:self.labelCategory];
        //[self.bgView addSubview:self.labelStatus];
        
        [self.bgView addSubview:self.labelNovelDes];
        
        [self.bgView addSubview:self.clickImageView];
        [self.btnReadNow addSubview:self.labelLatestChapter];
        [self.bgView addSubview:self.btnReadNow];
        [self.contentView addSubview:self.bgView];
        
    }
    
    return self;
}

#pragma mark - PrivateFunction

-(void)setAddButtonBookShelfStateWithShowHud:(BOOL)show{
    [self.btnAddBookShelf setTitle:@"已加入书架" forState:UIControlStateNormal];
    [self.btnAddBookShelf setImage:nil forState:UIControlStateNormal];
    [self.btnAddBookShelf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnAddBookShelf setBackgroundColor:kRGBCOLOR(199,199,199)];
    self.btnAddBookShelf.layer.borderColor = [kRGBCOLOR(199, 199, 199) CGColor];
    [self.btnAddBookShelf setEnabled:NO];
    
    if (show) {
        [SVProgressHUD setMinimumDismissTimeInterval:2];
        [SVProgressHUD showSuccessWithStatus:@"添加成功"];
    }
}

-(void)readerAddBookOnTheBookShelf{
    [self setAddButtonBookShelfStateWithShowHud:YES];
}

-(void)clickAddBookOnTheBookShelf:(id)sender{
    [self setAddButtonBookShelfStateWithShowHud:YES];
 
    if (![[ZY_DBHelper shareDBHelper] queryBookIsOnTheBookShelf:self.currentNovelInfo.novelId]) {
        [[ZY_DBHelper shareDBHelper] addBookToShelf:self.currentNovelInfo];
    }
    
}

-(void)handleSingleFingerEvent:(id)sender{
    if ([self.delegate respondsToSelector:@selector(didClickShowAllBookDesc)]) {
        [self.delegate didClickShowAllBookDesc];
    }
}

#pragma mark - Getter or Setter

-(UIView *)bgShadowView{
    if (!_bgShadowView) {
        _bgShadowView = [[UIView alloc] initWithFrame:CGRectMake(kPaddingLeft, 24, kNovelCoverImgWidth, kNovelCoverImgHeight)];
        _bgShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
        _bgShadowView.layer.shadowOffset = CGSizeMake(0, 0);
        _bgShadowView.layer.shadowOpacity = 0.5;
        _bgShadowView.layer.shadowRadius = 2.0;
    }
    
    return _bgShadowView;
}

-(UIImageView *)novelimageView{
    if (!_novelimageView) {
        _novelimageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kNovelCoverImgWidth, kNovelCoverImgHeight)];
        [_novelimageView setBackgroundColor:kRGBCOLOR(239, 239, 239)];
        [_novelimageView setImage:[UIImage imageNamed:@"暂无封面"]];
        _novelimageView.contentMode = UIViewContentModeCenter;
    }
    
    return _novelimageView;
}

-(UILabel *)labelNovelName{
    if (!_labelNovelName) {
        _labelNovelName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bgShadowView.frame)+kImageViewRightMargin, CGRectGetMinY(self.bgShadowView.frame)+3,(kCScreenWidth-CGRectGetMaxX(self.bgShadowView.frame)+20), 16)];
        [_labelNovelName setText:@"王牌进化"];
        [_labelNovelName setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelNovelName setFont:[UIFont boldSystemFontOfSize:16]];
    }
    
    return _labelNovelName;
}

-(UILabel *)labelNovelAuthor{
    if (!_labelNovelAuthor) {
        _labelNovelAuthor = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bgShadowView.frame)+kImageViewRightMargin, CGRectGetMaxY(self.labelNovelName.frame)+22, (kCScreenWidth-CGRectGetMaxX(self.bgShadowView.frame)+20), 12)];
        [_labelNovelAuthor setText:@"作者 :方向"];
        [_labelNovelAuthor setTextColor:kRGBCOLOR(153,153,153)];
        [_labelNovelAuthor setFont:[UIFont boldSystemFontOfSize:12]];
    }
    
    return _labelNovelAuthor;
}
-(BookScoreView *)scoreView{
    if (!_scoreView) {
        _scoreView = [[BookScoreView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.bgShadowView.frame)+kImageViewRightMargin, CGRectGetMaxY(self.labelNovelAuthor.frame)+12, 100, 20)];
    }
    return _scoreView;
}
-(UILabel *)labelCategory{
    if (!_labelCategory) {
        _labelCategory = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bgShadowView.frame)+kImageViewRightMargin, CGRectGetMaxY(self.scoreView.frame)+12, 70, 20)];
        [_labelCategory setText:@"分类 :玄幻"];
        _labelCategory.layer.cornerRadius = 4;
        _labelCategory.layer.masksToBounds = YES;
        [_labelCategory setTextAlignment:NSTextAlignmentCenter];
        [_labelCategory setTextColor:[UIColor whiteColor]];
        [_labelCategory setFont:[UIFont systemFontOfSize:12]];
        
    }
    
    return _labelCategory;
}

-(UILabel *)labelStatus{
    if (!_labelStatus) {
        _labelStatus = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.labelCategory.frame)+10, CGRectGetMaxY(self.scoreView.frame)+12, 44, 20)];
        [_labelStatus setText:@"状态 :完结"];
        _labelStatus.layer.cornerRadius = 4;
        _labelStatus.layer.masksToBounds = YES;
        [_labelStatus setTextAlignment:NSTextAlignmentCenter];
        [_labelStatus setTextColor:[UIColor whiteColor]];
        [_labelStatus setFont:[UIFont boldSystemFontOfSize:12]];
        [_labelStatus setBackgroundColor:RGB(250, 137, 128)];
    }
    
    return _labelStatus;
}

-(UILabel *)labelLatestChapter{
    if (!_labelLatestChapter) {
        _labelLatestChapter = [[UILabel alloc] initWithFrame:CGRectMake(60, 0,kCScreenWidth-90, 50)];
        [_labelLatestChapter setTextAlignment:NSTextAlignmentRight];
        _labelLatestChapter.textColor = RGB(102, 102, 102);
        [_labelLatestChapter setText:@"最新 :第550章 奔放粗俗的诗!"];
        [_labelLatestChapter setFont:[UIFont systemFontOfSize:15]];
        //        _labelLatestChapter.backgroundColor = [UIColor redColor];
    }
    
    return _labelLatestChapter;
}

-(UILabel *)labelNovelDes{
    if (!_labelNovelDes) {
        _labelNovelDes =[[UILabel alloc] initWithFrame:CGRectZero];
        [_labelNovelDes setFont:[UIFont systemFontOfSize:14]];
        [_labelNovelDes setTextColor:kRGBCOLOR(102, 102, 102)];
        [_labelNovelDes setNumberOfLines:0];
        [_labelNovelDes setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleFingerEvent:)];
        [_labelNovelDes addGestureRecognizer:ges];
        
    }
    
    return _labelNovelDes;
}

-(UIButton *)btnAddBookShelf{
    if (!_btnAddBookShelf) {
        _btnAddBookShelf = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(self.bgShadowView.frame)+13,kButtonWidth, 40)];
        _btnAddBookShelf.backgroundColor = [UIColor whiteColor];
        _btnAddBookShelf.layer.borderColor = [kRGBCOLOR(229, 20, 13) CGColor];
        _btnAddBookShelf.layer.borderWidth=0.8;
        _btnAddBookShelf.layer.shadowRadius = 4.0;
        [_btnAddBookShelf.layer setCornerRadius:4];
        [_btnAddBookShelf setTitle:@"加入书架" forState:UIControlStateNormal];
        [_btnAddBookShelf.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_btnAddBookShelf setTitleColor:kRGBCOLOR(229, 20, 13) forState:UIControlStateNormal];
        [_btnAddBookShelf addTarget:self action:@selector(clickAddBookOnTheBookShelf:) forControlEvents:UIControlEventTouchUpInside];
        [_btnAddBookShelf setImage:[UIImage imageNamed:@"添加"] forState:UIControlStateNormal];
        [_btnAddBookShelf setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 5)];
        
    }
    
    return _btnAddBookShelf;
}

-(UIButton *)btnReadNow{
    if (!_btnReadNow) {
        _btnReadNow = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.bgShadowView.frame)+13,kCScreenWidth, 50)];
        [_btnReadNow.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_btnReadNow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btnReadNow addTarget:self action:@selector(gotoCatelog) forControlEvents:UIControlEventTouchUpInside];
        _btnReadNow.backgroundColor = [UIColor whiteColor];
        [_btnReadNow setTitle:@"目录" forState:UIControlStateNormal];
        _btnReadNow.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _btnReadNow.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
        
        UIImageView *img_jiantou = [[UIImageView alloc]initWithFrame:CGRectMake(kCScreenWidth-15-7, 19, 7, 12)];
        img_jiantou.image = [UIImage imageNamed:@"目录箭头"];
        [_btnReadNow addSubview:img_jiantou];

        UIView *v_xian1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, 1)];
        v_xian1.backgroundColor = RGB(242, 242, 242);
        [_btnReadNow addSubview:v_xian1];
    }
    
    return _btnReadNow;
}

-(void)gotoCatelog{
    [MobClick event:@"Details" label:@"目录"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoCatelog" object:nov];
}


-(UIImageView *)clickImageView{
    if (!_clickImageView) {
        _clickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 28, 28)];
        [_clickImageView setImage:[UIImage imageNamed:@"更多"]];
        [_clickImageView setHidden:YES];
        UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleFingerEvent:)];
        //[_clickImageView addGestureRecognizer:singleFingerOne];
        [_clickImageView setUserInteractionEnabled:YES];
    }
    
    return _clickImageView;
}

-(void)gotoRead{

    [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoReadByNovel" object:self.currentNovelInfo];
}

-(void)bindItemWithNovelInfo:(ZY_NovelInfo*)novelInfo withLineNumberCount:(NSInteger)lineNumberCount withSingleDesHeight:(CGFloat)singleDesHeight{
    nov = novelInfo;
    CGFloat desContentHeight = lineNumberCount==3?singleDesHeight*lineNumberCount+lineNumberCount*kLineSpacing:novelInfo.novelContentDescHeight;
    [self.labelNovelDes setNumberOfLines:lineNumberCount];
    [self.bgView setFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)-10)];
    [self.labelNovelDes setFrame:CGRectMake(kPaddingLeft, CGRectGetMaxY(self.bgShadowView.frame)+22, kCScreenWidth-kPaddingLeft*2, desContentHeight)];
    [self.clickImageView setHidden:YES];
    if ((novelInfo.novelContentDescHeight>singleDesHeight*3.5)&&lineNumberCount>0) {
        [self.clickImageView setFrame:CGRectMake(CGRectGetMaxX(self.labelNovelDes.frame)-25, CGRectGetMaxY(self.labelNovelDes.frame)-23, 28,28)];
        [self.clickImageView setHidden:NO];
    }
    _scoreView.score = [novelInfo.novelScore floatValue]/2;
    [self.labelCategory setBackgroundColor:[ZY_Common getCateColor:novelInfo.novelCategory]];
    if (novelInfo.novelDes) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:novelInfo.novelDes];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:kLineSpacing];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [novelInfo.novelDes length])];
        [self.labelNovelDes setAttributedText:attributedString];
    }
    
    [self.btnReadNow setFrame:CGRectMake(self.btnReadNow.frame.origin.x, CGRectGetMaxY(self.labelNovelDes.frame)+20, self.btnReadNow.frame.size.width, self.btnReadNow.frame.size.height)];
    [self.btnAddBookShelf setFrame:CGRectMake(self.btnAddBookShelf.frame.origin.x, CGRectGetMaxY(self.labelNovelDes.frame)+20, self.btnAddBookShelf.frame.size.width, self.btnAddBookShelf.frame.size.height)];
    
    [self.labelNovelName setText:novelInfo.novelName];
    [self.labelNovelAuthor setText:[NSString stringWithFormat:@"作者：%@",novelInfo.novelAuthor]];
    [self.labelCategory setText:novelInfo.novelCategory];
    [self.labelStatus setText:novelInfo.novelStatus];
    [self.labelLatestChapter setText:[NSString stringWithFormat:@"最新章节: %@",novelInfo.novelLatestChapter]];
    self.currentNovelInfo=novelInfo;
    
    if ([[ZY_DBHelper shareDBHelper] queryBookIsOnTheBookShelf:novelInfo.novelId]) {
        [self setAddButtonBookShelfStateWithShowHud:NO];
    }
    
    [self.novelimageView sd_setImageWithURL:[NSURL URLWithString:novelInfo.novelCoverUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            self.novelimageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.novelimageView setClipsToBounds:YES];
        }else{
            [self.novelimageView setImage:[UIImage imageNamed:@"暂无封面"]];
        }
    }];
}

@end

//
//  ZY_SameUserTableViewCell.h
//  ZYNovel
//
//

#import <UIKit/UIKit.h>
#import "ZY_NovelRecommendTableViewCell.h"
@protocol ZY_SameUserTableViewCellDelegate <NSObject>

@optional

-(void)clickSameUserNovel:(ZY_NovelInfo*)currentNovel;
-(void)pushToMoreSameAuthurList;

@end

@interface ZY_SameUserTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource,ZY_NovelRecommendTableViewCellDelegate>

@property(nonatomic,strong) UIView *lineView;

@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) NSMutableArray *dataArray;

@property (nonatomic, strong) UIButton *moreButton;

-(void)reloadBookData;

@property (nonatomic, weak) id<ZY_SameUserTableViewCellDelegate> delegate;
@end

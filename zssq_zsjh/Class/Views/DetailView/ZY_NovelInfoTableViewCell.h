//
//  ZY_NovelInfoTableViewCell.h
//  ZYNovel
//
//  Created by S on 2017/3/14.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@protocol ZY_NovelInfoTableViewCellDelegate <NSObject>

@optional

-(void)didClickShowAllBookDesc;

@end

@interface ZY_NovelInfoTableViewCell : UITableViewCell{
    ZY_NovelInfo *nov;
}

@property (nonatomic, weak) id<ZY_NovelInfoTableViewCellDelegate> delegate;

-(void)bindItemWithNovelInfo:(ZY_NovelInfo*)novelInfo withLineNumberCount:(NSInteger)lineNumberCount withSingleDesHeight:(CGFloat)singleDesHeight;

@end

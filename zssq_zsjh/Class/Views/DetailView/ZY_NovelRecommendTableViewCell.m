//
//  ZY_NovelDesTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/14.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_NovelRecommendTableViewCell.h"
#import "ZY_Common.h"
#import "UIImageView+WebCache.h"
#import "ZY_JsonParser.h"

@interface ZY_NovelRecommendTableViewCell()



@property(nonatomic,strong) NSMutableArray *recommendNovelImageView;

@property(nonatomic,strong) NSMutableArray *recommendNovelTitle;




@end

@implementation ZY_NovelRecommendTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.contentView addSubview:self.labelTitle];
//        [self createControl];
    }
    
    return self;
}

#pragma mark - Private Function


-(void)handleSingleFingerEvent:(UITapGestureRecognizer *)recognizer{
    

    
    if (recognizer.view.tag < self.recommendNovelDataArray.count) {
         if ([self.delegate respondsToSelector:@selector(clickRecommendNovel:)]){
             ZY_NovelInfo *zyNovelInfo = [[ZY_NovelInfo alloc]init];
             zyNovelInfo.novelId = [[self.recommendNovelDataArray objectAtIndex:recognizer.view.tag] objectForKey:@"_id"];
             
            [self.delegate clickRecommendNovel:zyNovelInfo];
        }
    }
    
}

#pragma mark - Getter or Setter

-(UILabel *)labelTitle{
    if (!_labelTitle) {
        _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(18, 13, 150, 17)];
        
        [_labelTitle setTextColor:kRGBCOLOR(51,51,51)];
        [_labelTitle setFont:[UIFont systemFontOfSize:16]];
        
        UILabel *lbl_xian = [[UILabel alloc]initWithFrame:CGRectMake(-8, 1, 3, 15)];
        lbl_xian.backgroundColor = kNavigationBgColor;
        [_labelTitle addSubview:lbl_xian];
    }
    
    return _labelTitle;
}

-(void)createControl{
    self.recommendNovelImageView = [[NSMutableArray alloc] init];
    self.recommendNovelTitle = [[NSMutableArray alloc] init];
    CGFloat y=CGRectGetMaxY(self.labelTitle.frame)+17;
    CGFloat marginLeftAndRight = 18;
    CGFloat space = 32;
    CGFloat width = (kCScreenWidth-(marginLeftAndRight*2)-space*2)/3;
    CGFloat height = kNovelCoverImgHeight+32;
    
    for (NSInteger i = 0; i < self.recommendNovelDataArray.count; i++) {
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake((marginLeftAndRight+(i%3)*(width+space)), y+(height+20)*(i/3), width, height)];
        [backgroundView setBackgroundColor:[UIColor whiteColor]];
        UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleFingerEvent:)];
        [backgroundView addGestureRecognizer:singleFingerOne];
        [backgroundView setUserInteractionEnabled:YES];
        [backgroundView setTag:i];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((width-kNovelCoverImgWidth)/2, 0, kNovelCoverImgWidth, kNovelCoverImgHeight)];
        [imageView setBackgroundColor:kRGBCOLOR(239, 239, 239)];
        [imageView setImage:[UIImage imageNamed:@"暂无封面"]];
        imageView.contentMode = UIViewContentModeCenter;
        [backgroundView addSubview:imageView];
        [self.recommendNovelImageView addObject:imageView];
        
        UILabel *labelNovelName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(imageView.frame), CGRectGetMaxY(imageView.frame)+12, CGRectGetWidth(imageView.frame), 12)];
        [labelNovelName setFont:[UIFont systemFontOfSize:12]];
        [labelNovelName setTextAlignment:NSTextAlignmentCenter];
        [labelNovelName setBackgroundColor:[UIColor whiteColor]];
        [labelNovelName setTextColor:kRGBCOLOR(102, 102, 102)];
        [labelNovelName setText:@" "];
        [backgroundView addSubview:labelNovelName];
        [self.recommendNovelTitle addObject:labelNovelName];
        
        [self.contentView addSubview:backgroundView];
    }
}

-(void)bindRecommendNovelWithCurrentNovel{
    
    for (NSInteger i = 0; i < self.recommendNovelDataArray.count; i++) {
        
        NSDictionary *bookDic = [self.recommendNovelDataArray objectAtIndex:i];
        
        
            ZY_NovelInfo *zyNovelInfo = [[ZY_NovelInfo alloc]init];
            zyNovelInfo.novelId = [[self.recommendNovelDataArray objectAtIndex:i] objectForKey:@"_id"];
            zyNovelInfo.novelName = [[self.recommendNovelDataArray objectAtIndex:i] objectForKey:@"title"];
        
        
            NSString *string  = [[self.recommendNovelDataArray objectAtIndex:i] objectForKey:@"cover"];
            if (string && string.length>7) {
                string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                string = [string substringFromIndex:7];
                zyNovelInfo.novelCoverUrl = string;
            }

            UIImageView *imgView = [self.recommendNovelImageView objectAtIndex:i];
            [imgView sd_setImageWithURL:[NSURL URLWithString:zyNovelInfo.novelCoverUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image) {
                    imgView.contentMode = UIViewContentModeScaleAspectFit;
                    [imgView setClipsToBounds:YES];
                }else{
                    [imgView setImage:[UIImage imageNamed:@"暂无封面"]];
                }
            }];
            
            UILabel *label = [self.recommendNovelTitle objectAtIndex:i];
            [label setText:zyNovelInfo.novelName];
        
    }
}

@end

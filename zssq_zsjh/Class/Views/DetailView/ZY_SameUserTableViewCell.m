//
//  ZY_SameUserTableViewCell.m
//  ZYNovel
//

//

#import "ZY_SameUserTableViewCell.h"
#import "ZY_Common.h"

@implementation ZY_SameUserTableViewCell


-(void)reloadBookData{
    self.tableView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-6);
    [self.lineView setFrame:CGRectMake(0,CGRectGetHeight(self.frame)-12, self.frame.size.width, 6)];
    [self.tableView reloadData];
}
#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.frame.size.height-6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:nsstrBookIdentifier forIndexPath:indexPath];
    tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [((ZY_NovelRecommendTableViewCell*)tableViewCell) setDelegate:self];
    ((ZY_NovelRecommendTableViewCell*)tableViewCell).recommendNovelDataArray = self.dataArray;
    [(ZY_NovelRecommendTableViewCell*)tableViewCell createControl];
    [(ZY_NovelRecommendTableViewCell*)tableViewCell bindRecommendNovelWithCurrentNovel];
    [((ZY_NovelRecommendTableViewCell*)tableViewCell).labelTitle setText:@"作者还写过"];
    
    return tableViewCell;
}


#pragma mark - Getter or Setter

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        if(@available(iOS 11.0, *)) {
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        _tableView.scrollEnabled = NO;
        [_tableView setBackgroundColor:[UIColor whiteColor]];
        [_tableView registerClass:[ZY_NovelRecommendTableViewCell class] forCellReuseIdentifier:nsstrBookIdentifier];
    }
    
    return _tableView;
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectZero];
        [_lineView setBackgroundColor:kRGBCOLOR(242,242,242)];
    }
    
    return _lineView;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self addSubview:self.tableView];
        [self addSubview:self.lineView];
        [self addSubview:self.moreButton];
    }
    
    return self;
}

#pragma mark - ZY_NovelRecommendTableViewCellDelegate

-(void)clickRecommendNovel:(ZY_NovelInfo *)currentNovel{
 
    if ([self.delegate respondsToSelector:@selector(clickSameUserNovel:)]){
        [self.delegate clickSameUserNovel:currentNovel];
    }
}

-(void)clickMoreSameAuthurButton {
    if ([self.delegate respondsToSelector:@selector(pushToMoreSameAuthurList)]) {
        [self.delegate pushToMoreSameAuthurList];
    }
}

- (UIButton *)moreButton {
    if (_moreButton == nil) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton setTitle:@"查看更多" forState:UIControlStateNormal];
        [_moreButton addTarget:self action:@selector(clickMoreSameAuthurButton) forControlEvents:UIControlEventTouchUpInside];
        [_moreButton setTitleColor:kRGBCOLOR(51,51,51) forState:UIControlStateNormal];
        [_moreButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_moreButton sizeToFit];
        [_moreButton setCenter:CGPointMake(kCScreenWidth - 45, 22)];
        
    }
    return _moreButton;
}

@end

//
//  BookScoreView.h
//  ReplicaNovel
//
//

#import <UIKit/UIKit.h>

@interface BookScoreView : UIView

@property (nonatomic,assign) CGFloat score;
@end

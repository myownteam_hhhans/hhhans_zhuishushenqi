//
//  ZY_CommonTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/15.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_RecommentBookCell.h"
#import "UIImageView+WebCache.h"
#import "ZY_Common.h"
#import "Common.h"
#import "ZY_RecommentBookCell.h"
#import "BookScoreView.h"

@interface ZY_RecommentBookCell()

@property(nonatomic,strong) UILabel *firstNamelabel;

@property(nonatomic,strong) UILabel *introductionNamelabel;

@property(nonatomic,strong) UIImageView *imgView;

@property(nonatomic,strong) UILabel *scorelabel;

@property(nonatomic,strong) UILabel *authorNamelabel;

@property(nonatomic,strong) UILabel *categroylabel;

@property(nonatomic,strong) UIView *bgView;

@property(nonatomic,strong) BookScoreView *scoreView;

@end

@implementation ZY_RecommentBookCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        [self addSubview:self.bgView];
        [self addSubview:self.imgView];
        [self addSubview:self.introductionNamelabel];
        [self addSubview:self.scorelabel];
        [self addSubview:self.authorNamelabel];
        [self addSubview:self.selectedButton];
        [self addSubview:self.scoreView];

        [self addSubview:self.firstNamelabel];

    }
    return self;
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self.imgView setImage:[UIImage imageNamed:@"暂无封面"]];
}

#pragma mark - Getter or Setter

-(UIImageView *)imgView
{
    if (!_imgView) {
         _imgView=[[UIImageView alloc]initWithFrame:[Common get414FrameByMap:CGRectMake(31, 17.5, 79, 110)]];
        [_imgView setBackgroundColor:kRGBCOLOR(239, 239, 239)];
        [_imgView setImage:[UIImage imageNamed:@"暂无封面"]];
         _imgView.contentMode = UIViewContentModeCenter;

    }
    return _imgView;
}

-(UILabel *)firstNamelabel
{
    if (!_firstNamelabel) {
         _firstNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imgView.frame)+10, 23, kCScreenWidth-(CGRectGetMaxX(self.imageView.frame)+10), 16)];
        [_firstNamelabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
        [_firstNamelabel setTextColor:kRGBCOLOR(51, 51, 51)];
    }
    return _firstNamelabel;
}

-(UILabel *)introductionNamelabel
{
    if (!_introductionNamelabel) {
        CGSize size=[UIScreen mainScreen].bounds.size;
        CGFloat rightMargin=42;
        CGFloat leftMargin=15;
        CGFloat imageRightMargin=15;
        _introductionNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imgView.frame)+10, CGRectGetMaxY(self.authorNamelabel.frame), (size.width)-rightMargin-leftMargin-CGRectGetWidth(self.imgView.frame)-imageRightMargin, 60)];
        _introductionNamelabel.numberOfLines = 2;
        _introductionNamelabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_introductionNamelabel setTextColor:kRGBCOLOR(155, 155, 155)];
        [_introductionNamelabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:12]]];
    }
    return _introductionNamelabel;
}

-(UILabel *)scorelabel
{
    if (!_scorelabel) {
         _scorelabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imgView.frame)+10, CGRectGetMaxY(self.firstNamelabel.frame)+[Common get414RadiusByMap:5], 24, 20)];
        [_scorelabel setFont:[UIFont systemFontOfSize:14]];
        _scorelabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_scorelabel setTextColor:kRGBCOLOR(255, 206, 68)];
    }
    return _scorelabel;
}

-(UILabel *)authorNamelabel
{
    if (!_authorNamelabel) {
        
         _authorNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.imgView.frame)+10, CGRectGetMaxY(self.firstNamelabel.frame)+[Common get414RadiusByMap:29] ,68, 13)];
        [_authorNamelabel setTextColor:kRGBCOLOR(153, 153, 153)];
        _authorNamelabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_authorNamelabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
    }
    
    return _authorNamelabel;
}


-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(17, 5, 381, 135)]];
        _bgView.layer.cornerRadius = 10;
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.shadowColor = [UIColor blackColor].CGColor;
        _bgView.layer.shadowOffset = CGSizeMake(0, 0);
        _bgView.layer.shadowOpacity = 0.1;
        _bgView.layer.shadowRadius = 2.0;
        _bgView.layer.borderWidth = 0.000001;
    }
    
    return _bgView;
}



-(void)bindNovelItem:(ZY_NovelInfo *)novelInfo
{
    if (novelInfo) {
        
        self.authorNamelabel.frame = CGRectMake(self.authorNamelabel.frame.origin.x, self.authorNamelabel.frame.origin.y,[self calculateRowWidth:novelInfo.novelAuthor], self.authorNamelabel.frame.size.height);
        self.categroylabel.frame = CGRectMake(CGRectGetMaxX(self.authorNamelabel.frame)+10, self.categroylabel.frame.origin.y,self.categroylabel.frame.size.width, self.categroylabel.frame.size.height);
        if([novelInfo.novelScore intValue] > 0)
        {
            [self.scorelabel setText:[NSString stringWithFormat:@"%.1f",[novelInfo.novelScore floatValue]]];
            [self.scoreView setScore:[novelInfo.novelScore floatValue]/2];
            
            // 隐藏数字
            for (UIView *view in _scoreView.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view setHidden:YES];
                }
            }
        }
        [self.categroylabel setText:novelInfo.novelCategory];
        [self.categroylabel setBackgroundColor:[ZY_Common getCateColor:novelInfo.novelCategory]];
        [self.authorNamelabel setText:novelInfo.novelAuthor];
        [self.firstNamelabel setText:novelInfo.novelName];
        [self.introductionNamelabel setText:novelInfo.novelDes];
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:novelInfo.novelCoverUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                self.imgView.contentMode = UIViewContentModeScaleAspectFill;
               [self.imgView setClipsToBounds:YES];
            }else{
                self.imgView.contentMode = UIViewContentModeCenter;
               [self.imgView setClipsToBounds:NO];
               [self.imgView setImage:[UIImage imageNamed:@"暂无封面"]];
            }
        }];
    }
}

- (CGFloat)calculateRowWidth:(NSString *)string {
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:13]};  //指定字号
    CGRect rect = [string boundingRectWithSize:CGSizeMake(0, 13)/*计算宽度时要确定高度*/ options:NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:dic context:nil];
    return rect.size.width;
}

- (UIButton *)selectedButton {
    if (_selectedButton == nil) {
        _selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectedButton setFrame:[Common get414FrameByMap:CGRectMake(368, 25 + 9, 17, 16)]];
        [_selectedButton setImage:[UIImage imageNamed:@"guide_unselected"] forState:UIControlStateNormal];
        [_selectedButton setImage:[UIImage imageNamed:@"guide_selected"] forState:UIControlStateSelected];
        [_selectedButton setSelected:YES];
    }
    return _selectedButton;
}

-(BookScoreView *)scoreView{
    if (!_scoreView) {
        _scoreView = [[BookScoreView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.scorelabel.frame), CGRectGetMaxY(self.firstNamelabel.frame)+[Common get414RadiusByMap:8], 80, 20)];
        
        _scoreView.transform = CGAffineTransformMakeScale(0.7, 0.7);
        
    }
    return _scoreView;
}

@end

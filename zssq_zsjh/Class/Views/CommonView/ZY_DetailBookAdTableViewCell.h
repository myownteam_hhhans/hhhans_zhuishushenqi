//
//  ZY_DetailBookAdTableViewCell.h
//  ZYNovel
//
//  Created by S on 2017/3/16.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ZY_AdEntity.h"
#import "ZY_ADHelper.h"

@interface ZY_DetailBookAdTableViewCell : UITableViewCell

-(void)bindData:(ADModel*)data;

@end

//
//  ZY_NeedFiveStarView.m
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/11.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ZY_NeedFiveStarView.h"
#import "Common.h"
#import "CFT_parameterOnline.h"
#import "ZY_Common.h"
#import <MTGSDK/MTGSDK.h>
#import <MTGSDKReward/MTGRewardAdManager.h>

@interface ZY_NeedFiveStarView()

@property (nonatomic, strong) UIImageView *mainImageView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *goFiveStarButton;

@end

@implementation ZY_NeedFiveStarView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.mainImageView];
        [self addSubview:self.closeButton];
        [self.mainImageView addSubview:self.goFiveStarButton];
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    }
    return self;
}

- (void)removeSelf {
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)jumpToStore {
   
    // 看视频
    if (self.fiveStarOrVideo) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"postRewardVideo" object:nil];
    }
    
    // 要好评
    else {
        NSString *storeURL = [NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@?mt=8",kAppId];
        
        if ([[[UIDevice currentDevice] systemVersion]intValue]>=11) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:storeURL]];
        }else{
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:storeURL]];
        }
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"jiesuo"];
    }
    
    
    

    
    [self removeSelf];
}

// 修改样式
- (void)setFiveStarOrVideo:(BOOL)fiveStarOrVideo {
    
    _fiveStarOrVideo = fiveStarOrVideo;
    
    if (fiveStarOrVideo == 1) {
        [self.mainImageView setImage:[UIImage imageNamed:@"readBook_Video"]];
        [self.goFiveStarButton setTitle:@"朕准了" forState:UIControlStateNormal];
    } else {
        [self.mainImageView setImage:[UIImage imageNamed:@"readBook_FiveStar"]];
        [self.goFiveStarButton setTitle:@"去打五星好评" forState:UIControlStateNormal];
    }
}

#pragma mark ==== 懒加载 ====
- (UIImageView *)mainImageView {
    if (_mainImageView == nil) {
        _mainImageView = [[UIImageView alloc] init];
        [_mainImageView setFrame:[Common get414FrameByMap:CGRectMake(45, 171, 323, 393)]];
        [_mainImageView setImage:[UIImage imageNamed:@"readBook_FiveStar"]];
        [_mainImageView setUserInteractionEnabled:YES];
    }
    return _mainImageView;
}
- (UIButton *)goFiveStarButton {
    if (_goFiveStarButton == nil) {
        _goFiveStarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_goFiveStarButton setFrame:[Common get414FrameByMap:CGRectMake(37, 319, 250, 45)]];
        //[_goFiveStarButton setBackgroundColor:[Common colorWithHexString:@"#719EF1" alpha:1.0]];
        [_goFiveStarButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:24]]];
        [_goFiveStarButton setTitle:@"去打五星好评" forState:UIControlStateNormal];
        [_goFiveStarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_goFiveStarButton addTarget:self action:@selector(jumpToStore) forControlEvents:UIControlEventTouchUpInside];
        [_goFiveStarButton setBackgroundImage:[UIImage imageNamed:@"detailGETFiveButton"] forState:UIControlStateNormal];
    }
    return _goFiveStarButton;
}
- (UIButton *)closeButton {
    if (_closeButton == nil) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setFrame:[Common get414FrameByMap:CGRectMake(189, 607, 40, 40)] ];
        [_closeButton setBackgroundImage:[UIImage imageNamed:@"fiveStarClose"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(removeSelf) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

@end

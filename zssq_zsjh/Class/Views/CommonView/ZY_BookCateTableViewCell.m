//
//  ZY_BookCateTableViewCell.m
//  ZYNovel
//
//  Created by apple on 2018/6/13.
//

#import "ZY_BookCateTableViewCell.h"
#import "ZY_Common.h"

@interface ZY_BookCateTableViewCell()

@property(nonatomic,strong) UILabel *cateNamelabel;
@property(nonatomic,strong) UILabel *cateInfolabel;
@property(nonatomic,strong) UILabel *linelabel;
@property(nonatomic,strong) UIImageView *cateImg;
@property(nonatomic,strong) UIImageView *jiantouImg;

@end

@implementation ZY_BookCateTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.cateNamelabel];
        [self addSubview:self.jiantouImg];
        [self addSubview:self.cateInfolabel];
        [self addSubview:self.linelabel];
        [self addSubview:self.cateImg];
    }
    
    return self;
}

-(UIImageView *)cateImg{
    if (!_cateImg) {
        _cateImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 118, 99)];
    }
    return _cateImg;
}

-(UIImageView *)jiantouImg{
    if (!_jiantouImg) {
        _jiantouImg = [[UIImageView alloc]initWithFrame:CGRectMake(kCScreenWidth-34, 51, 24, 24)];
        _jiantouImg.image = [UIImage imageNamed:@"右翻"];
    }
    return _jiantouImg;
}

-(UILabel *)cateNamelabel
{
    if (!_cateNamelabel) {
        _cateNamelabel = [[UILabel alloc]initWithFrame:CGRectMake(138, 33, kCScreenWidth-34-138, 16)];
        [_cateNamelabel setFont:[UIFont systemFontOfSize:20]];
        [_cateNamelabel setTextColor:kRGBCOLOR(74, 74, 74)];
        [_cateNamelabel setBackgroundColor:[UIColor clearColor]];
        _cateNamelabel.textAlignment = NSTextAlignmentCenter;
    }
    return _cateNamelabel;
}

-(UILabel *)cateInfolabel
{
    if (!_cateInfolabel) {
        _cateInfolabel = [[UILabel alloc]initWithFrame:CGRectMake(138, 64, kCScreenWidth-34-138, 16)];
        [_cateInfolabel setFont:[UIFont systemFontOfSize:12]];
        _cateInfolabel.textAlignment = NSTextAlignmentCenter;
        [_cateInfolabel setTextColor:kRGBCOLOR(74, 74, 74)];
        [_cateInfolabel setBackgroundColor:[UIColor clearColor]];
        
    }
    return _cateInfolabel;
}

-(UILabel *)linelabel
{
    if (!_linelabel) {
        _linelabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 126, kCScreenWidth, 1)];
        [_linelabel setBackgroundColor:kRGBCOLOR(242, 242, 242)];
        
    }
    return _linelabel;
}
-(void)bindCateItem:(ZY_NovelCate *)cate{
    
    self.cateNamelabel.text = cate.cateName;
    self.cateInfolabel.text = cate.cateInfo;
    self.cateImg.image = [UIImage imageNamed:cate.cateName];
}
@end

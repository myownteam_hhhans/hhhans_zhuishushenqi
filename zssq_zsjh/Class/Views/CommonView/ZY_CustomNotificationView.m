//
//  ZY_CustomNotificationView.m
//  ZYNovel
//
//

#import "ZY_CustomNotificationView.h"
#import "ZY_Common.h"

@interface ZY_CustomNotificationView()

@property(nonatomic,strong) UIImageView *iconImageView;

@property(nonatomic,strong) UIView *containerView;

@property(nonatomic,strong) UILabel *labelMessageOne;

@property(nonatomic,strong) UILabel *labelMessageTwo;

@property(nonatomic,strong) UIView *lineView;

@property(nonatomic,strong) NSTimer *timer;

@property(nonatomic,assign) CGFloat speed;

@property(nonatomic,assign) CGSize titleSize;

@end

@implementation ZY_CustomNotificationView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:CGRectMake(frame.origin.x,frame.origin.y, frame.size.width, 44)];
    
    if(self){
        [self setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:self.containerView];
         self.speed=20;
        [self.containerView addSubview:self.labelMessageOne];
        [self.containerView addSubview:self.labelMessageTwo];
        UIView *leftBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(self.iconImageView.frame), CGRectGetMaxX(self.iconImageView.frame)+10, CGRectGetHeight(self.iconImageView.frame))];
        [leftBackgroundView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:leftBackgroundView];
        [self addSubview:self.iconImageView];
        
        UIView *rightBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(kCScreenWidth-18, CGRectGetMinY(self.iconImageView.frame), 18, CGRectGetHeight(self.iconImageView.frame))];
        [rightBackgroundView setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:rightBackgroundView];
        
        [self addSubview:self.lineView];
    }
    
    return self;
}

#pragma mark - Getter or Setter

-(UIImageView *)iconImageView{
    if (!_iconImageView) {
         _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(18, 12, 17, 17)];
         _iconImageView.contentMode = UIViewContentModeScaleToFill;
        [_iconImageView setBackgroundColor:[UIColor clearColor]];
        [_iconImageView setImage:[UIImage imageNamed:@"通知"]];
    }
    
    return _iconImageView;
}

-(UIView *)containerView{
    if (!_containerView) {
         _containerView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame)+9,12,kCScreenWidth-CGRectGetMaxX(self.iconImageView.frame)-18,17)];
        [_containerView setBackgroundColor:[UIColor whiteColor]];
    }
    
    return _containerView;
}

-(UILabel *)labelMessageOne{
    if (!_labelMessageOne) {
        _labelMessageOne = [[UILabel alloc]initWithFrame:CGRectZero];
        _labelMessageOne.backgroundColor = [UIColor whiteColor];
        _labelMessageOne.textColor = kRGBCOLOR(51, 51, 51);
        _labelMessageOne.font = [UIFont systemFontOfSize:12.0f];
        
    }
    
    return _labelMessageOne;
}

-(UILabel *)labelMessageTwo{
    if (!_labelMessageTwo) {
        _labelMessageTwo = [[UILabel alloc]initWithFrame:CGRectZero];
        _labelMessageTwo.backgroundColor = [UIColor whiteColor];
        _labelMessageTwo.textColor = kRGBCOLOR(51, 51, 51);
        _labelMessageTwo.font = [UIFont systemFontOfSize:12.0f];
    }
    
    return _labelMessageTwo;
}

- (UIView *)lineView{
    if (!_lineView) {
         _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, kCScreenWidth, 0.5)];
        [_lineView setBackgroundColor:kRGBCOLOR(234, 234, 234)];
    }
    
    return _lineView;
}

-(void)bindNotificationMessage:(NSString*)nsstrMessage{
    if (!self.timer) {
        [self.labelMessageOne setText:nsstrMessage];
        [self.labelMessageTwo setText:nsstrMessage];
         self.titleSize = [nsstrMessage sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(MAXFLOAT,17)];
        [self.labelMessageOne setFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame)+9,0,self.titleSize.width,17)];
        [self.labelMessageTwo setFrame:CGRectMake(CGRectGetMaxX(self.labelMessageOne.frame),0,self.titleSize.width,17)];
         self.timer = [NSTimer scheduledTimerWithTimeInterval:1.01 target:self selector:@selector(textMove) userInfo:nil repeats:YES];
        [self.timer fire];
    }
}

-(void)textMove{
    
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.labelMessageOne.frame = CGRectMake(self.labelMessageOne.frame.origin.x-self.speed, self.labelMessageOne.frame.origin.y, self.titleSize.width, CGRectGetHeight(self.containerView.frame));
        self.labelMessageTwo.frame = CGRectMake(self.labelMessageTwo.frame.origin.x-self.speed,self.labelMessageTwo.frame.origin.y, self.titleSize.width, CGRectGetHeight(self.containerView.frame));
    } completion:^(BOOL finished) {
        if (finished && self.labelMessageOne.frame.origin.x < -self.titleSize.width) {
            self.labelMessageOne.frame = CGRectMake(self.labelMessageTwo.frame.origin.x+self.labelMessageTwo.frame.size.width,self.labelMessageTwo.frame.origin.y, self.titleSize.width, CGRectGetHeight(self.labelMessageTwo.frame));
        }
        if(finished && self.labelMessageTwo.frame.origin.x < -self.titleSize.width){
            self.labelMessageTwo.frame = CGRectMake(self.labelMessageOne.frame.origin.x+self.labelMessageOne.frame.size.width,self.labelMessageOne.frame.origin.y, self.titleSize.width, CGRectGetHeight(self.labelMessageTwo.frame));
        }
    }];
}

@end

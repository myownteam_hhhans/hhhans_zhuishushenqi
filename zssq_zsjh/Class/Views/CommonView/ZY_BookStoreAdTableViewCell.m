//
//  ZY_BookStoreAdTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/16.
//  Copyright © 2017年 S. All rights reserved.
//
#import "ZY_Common.h"
#import "ZY_BookStoreAdTableViewCell.h"
#import "UIImageView+WebCache.h"


@interface ZY_BookStoreAdTableViewCell()

@property(nonatomic,strong) UILabel *adFirstNamelabel;

@property(nonatomic,strong) UILabel *adIntroductionNamelabel;

@property(nonatomic,strong) UIImageView *adImgView;

@property(nonatomic,strong) UIImageView *adAuthorimgView;

@property(nonatomic,strong) UILabel *adAuthorNamelabel;

@property(nonatomic,strong) UILabel *adLabel;

@property(nonatomic,strong) UILabel *adNameLabel;

@property(nonatomic,strong) UIView *bgShadowView;

@property(nonatomic,strong) UIView *lineView;



@end

@implementation ZY_BookStoreAdTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.bgShadowView];
        [self.bgShadowView addSubview:self.adImgView];
        [self addSubview:self.adLabel];
        [self addSubview:self.adIntroductionNamelabel];
        [self addSubview:self.adAuthorimgView];
        [self addSubview:self.adFirstNamelabel];
        [self addSubview:self.adauthorNamelabel];
        //[self addSubview:self.lineView];
    }
    
    return self;
}

#pragma mark - Getter or Setter

-(UIView *)bgShadowView{
    if (!_bgShadowView) {
        _bgShadowView = [[UIView alloc] initWithFrame:CGRectMake(15, 15, 95, 112)];
        _bgShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
        _bgShadowView.layer.shadowOffset = CGSizeMake(0, 0);
        _bgShadowView.layer.shadowOpacity = 0.5;
        _bgShadowView.layer.shadowRadius = 2.0;
    }
    
    return _bgShadowView;
}

-(UIImageView *)adImgView
{
    if (!_adImgView) {
        _adImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 95, 112)];
        [_adImgView setBackgroundColor:kRGBCOLOR(239, 239, 239)];
        _adImgView.contentMode = UIViewContentModeCenter;
        _adImgView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _adImgView;
}

-(UILabel *)adFirstNamelabel
{
    if (!_adFirstNamelabel) {
        _adFirstNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.bgShadowView.frame)+10, 23, kCScreenWidth-CGRectGetMaxX(self.bgShadowView.frame)-70, 16)];
        [_adFirstNamelabel setFont:[UIFont systemFontOfSize:15]];
        [_adFirstNamelabel setTextColor:kRGBCOLOR(51, 51, 51)];
        [_adFirstNamelabel setBackgroundColor:[UIColor clearColor]];
        
    }
    return _adFirstNamelabel;
}

-(UILabel *)adIntroductionNamelabel
{
    if (!_adIntroductionNamelabel) {
        _adIntroductionNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.bgShadowView.frame)+10, 49, kCScreenWidth-CGRectGetMaxX(self.bgShadowView.frame)-20, 45)];
        _adIntroductionNamelabel.numberOfLines=2;
        _adIntroductionNamelabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_adIntroductionNamelabel setTextColor:kRGBCOLOR(153, 153, 153)];
        [_adIntroductionNamelabel setFont:[UIFont systemFontOfSize:13]];
        [_adIntroductionNamelabel setBackgroundColor:[UIColor clearColor]];
    }
    
    return _adIntroductionNamelabel;
}

-(UIImageView *)adAuthorimgView
{
    if (!_adAuthorimgView) {
        _adAuthorimgView=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.bgShadowView.frame)+10, CGRectGetMaxY(self.bgShadowView.frame)-17, 13, 13)];
        [_adAuthorimgView setImage:[UIImage imageNamed:@"作者"]];
    }
    return _adAuthorimgView;
}


-(UILabel *)adLabel
{
    if (!_adLabel) {
        _adLabel = [[UILabel alloc]initWithFrame:CGRectMake(kCScreenWidth-40, 16, 24, 20)];
        [_adLabel setText:@"9.9"];
        [_adLabel setBackgroundColor:[UIColor whiteColor]];
        [_adLabel setFont:[UIFont systemFontOfSize:16]];
        [_adLabel setTextColor:kRGBCOLOR(255, 206, 68)];
        [_adLabel setTextAlignment:NSTextAlignmentCenter];
    }
    
    return _adLabel;
}




-(UILabel *)adauthorNamelabel
{
    if (!_adAuthorNamelabel) {
        _adAuthorNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.adAuthorimgView.frame)+5, CGRectGetMaxY(self.bgShadowView.frame)-17,150, 15)];
        [_adAuthorNamelabel setTextColor:kRGBCOLOR(153, 153, 153)];
        [_adAuthorNamelabel setFont:[UIFont systemFontOfSize:13]];
        [_adAuthorNamelabel setText:@"推荐"];
    }
    
    return _adAuthorNamelabel;
}

-(void)bindAdItem:(ADModel *)AdInfo
{

    [self.lineView setFrame:CGRectMake(0,CGRectGetHeight(self.frame)-6, kCScreenWidth, 6)];
    
    if (AdInfo) {
        
        [self.adFirstNamelabel setText:AdInfo.name];
        [self.adIntroductionNamelabel setText:AdInfo.appSummary];
//        NSString *imageUrl = [[ZY_ADHelper shareADHelper] ADImageUrl:AdInfo type:adType_Feed];
        [self.adImgView sd_setImageWithURL:[NSURL URLWithString:AdInfo.adImgPath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            if (image) {
                self.adImgView.contentMode = UIViewContentModeScaleAspectFit;
                [self.adImgView setClipsToBounds:YES];
            }else{
                [self.adImgView setImage:[UIImage imageNamed:@"暂无封面"]];
                self.adImgView.contentMode = UIViewContentModeCenter;
            }
        }];
        if ([AdInfo.data isKindOfClass:[ZY_AdEntity class]]) {
            [[ZY_Common shareCommon] advertisingDisplayReportWithModel:AdInfo.data];
        }
    }
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectZero];
        [_lineView setBackgroundColor:kRGBCOLOR(242,242,242)];
    }
    
    return _lineView;
}

@end

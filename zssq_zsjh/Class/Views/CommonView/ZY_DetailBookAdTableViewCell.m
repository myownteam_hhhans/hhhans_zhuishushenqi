//
//  ZY_DetailBookAdTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/16.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_DetailBookAdTableViewCell.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <GoogleMobileAds/GADInterstitial.h>
#import "ZY_Common.h"
#import "SHW_ADInterstitial.h"
#import "UIImageView+WebCache.h"
#import "CFT_parameterOnline.h"
#import "SHW_AdMobHelp.h"

#define kCommonMargin 10.0f
#define kAdDownloadHeight 30
#define DOWNLOADSPACE 10

@interface ZY_DetailBookAdTableViewCell()

@property(nonatomic,strong)UIView* googleBgView;

@property(nonatomic,strong)UILabel *adTitleLabel;

@property(nonatomic,strong)UIImageView *adImageView;

@property(nonatomic,strong)UIImageView *tuiguangImageView;

@property(nonatomic,strong)UIView *lineView;

@property(nonatomic,strong)UILabel *downloadLabel;

@end

@implementation ZY_DetailBookAdTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.googleBgView];
        [self addSubview:self.adTitleLabel];
        [self addSubview:self.adImageView];
        [self addSubview:self.tuiguangImageView];
        [self addSubview:self.downloadLabel];
        [self addSubview:self.lineView];
    }
    
    return self;
}

- (void)prepareForReuse{
    [super prepareForReuse];
    [self.googleBgView setHidden:YES];
    [self.adTitleLabel setHidden:YES];
    [self.adImageView setHidden:YES];
    [self.downloadLabel setHidden:YES];
    
}

- (void)bindData:(ADModel*)data{
    if (data.platform == adplatform_Google) {
        [self.googleBgView setHidden:NO];
        //加载广告
        int intHeight = 50;
        int intWidth = 320;
        GADBannerView * ADMob_bannerView;
        int intBannerLocation = 3;
        switch (intBannerLocation) {
            case 1:
                ADMob_bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
                break;
            case  2:
                ADMob_bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
                intHeight = 100;
                break;
            case 3:
                ADMob_bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeMediumRectangle];
                intHeight = 250;
                break;
            case 4:
                ADMob_bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeMediumRectangle];
                intWidth = 468;
                intHeight = 60;
                break;
            case 5:
                ADMob_bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeFullBanner];
                intWidth = 588;
                intHeight = 90;
                break;
        }
        
        NSString * strAdUnitID = [[SHW_AdMobHelp sharedInstance] getAdMobBannerUnitID];
        //测试id，上线前注释掉
        strAdUnitID = @"ca-app-pub-9664827263749384/8388894556";
        ADMob_bannerView.adUnitID = strAdUnitID;
        ADMob_bannerView.rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        //[ADMob_bannerView loadRequest:[GADRequest request]];
        self.googleBgView.frame = CGRectMake(10, 10,kCScreenWidth-20, intHeight);
        ADMob_bannerView.frame = CGRectMake(0, 0, kCScreenWidth-20, intHeight);
        [self.googleBgView addSubview:ADMob_bannerView];
        [ADMob_bannerView loadRequest:[GADRequest request]];
        //[SHW_ADInterstitial loadFeedAdData:kAdId];
        
        [self.lineView setFrame:CGRectMake(0,270, kCScreenWidth, 10)];
    }else{
        [self.adTitleLabel setHidden:NO];
        [self.adImageView setHidden:NO];
        [self.tuiguangImageView setHidden:NO];
        [self.downloadLabel setHidden:NO];
        
        CGFloat width = kCScreenWidth-kCommonMargin*2;
        if (data) {
            self.adImageView.frame = CGRectMake(kCommonMargin,CGRectGetMaxY(self.adTitleLabel.frame) +5, width,(data.height/data.width)*width);
            [self.adImageView sd_setImageWithPreviousCachedImageWithURL:[NSURL URLWithString:data.adImgPath] placeholderImage:nil options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                
            } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
            }];
            if ([data.data isKindOfClass:[ZY_AdEntity class]]) {
                [[ZY_Common shareCommon] advertisingDisplayReportWithModel:data.data];
            }
            
            [self.adTitleLabel setText:data.name];
        }
        
        
        [self.downloadLabel setFrame:CGRectMake(kCScreenWidth-kCommonMargin-90, CGRectGetMaxY(self.adImageView.frame)+DOWNLOADSPACE, 90, kAdDownloadHeight)];
        
        self.tuiguangImageView.frame = CGRectMake(kCommonMargin,self.downloadLabel.center.y - 7, 25, 13);
        [self.lineView setFrame:CGRectMake(0,CGRectGetMaxY(self.downloadLabel.frame)+10 , kCScreenWidth, 10)];
    }
}


#pragma mark - Getter or Setter

- (UIView* )googleBgView{
    if (!_googleBgView) {
         _googleBgView = [[UIView alloc] initWithFrame:CGRectMake(10, 10,kCScreenWidth-20, 100)];
        [_googleBgView setHidden:YES];
    }
    
    return _googleBgView;
}


- (UILabel* )adTitleLabel{
    
    if (!_adTitleLabel) {
        _adTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kCommonMargin,kCommonMargin,kCScreenWidth-20, [UIFont systemFontOfSize:19.0f].lineHeight *2)];
        _adTitleLabel.backgroundColor = [UIColor clearColor];
        _adTitleLabel.textColor = [UIColor blackColor];
        _adTitleLabel.font = [UIFont systemFontOfSize:19.0f];
        _adTitleLabel.numberOfLines = 2;
        [_adTitleLabel setHidden:YES];
    }
    
    return _adTitleLabel;
}

- (UIImageView* )adImageView{
    if (!_adImageView) {
        _adImageView = [[UIImageView alloc] init];
        _adImageView.contentMode = UIViewContentModeScaleToFill;
        [_adImageView setBackgroundColor:[UIColor clearColor]];
        _adImageView.clipsToBounds = YES;
        [_adImageView setHidden:YES];
    }
    
    return _adImageView;
}

- (UIImageView* )tuiguangImageView{
    if (!_tuiguangImageView) {
        _tuiguangImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"feed广告"]];
        _tuiguangImageView.layer.cornerRadius = 1; 
        [_tuiguangImageView setHidden:YES];
    }
    
    return _tuiguangImageView;
}

-(UILabel *)downloadLabel{
    if (!_downloadLabel) {
        _downloadLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [_downloadLabel setBackgroundColor:kRGBCOLOR(235, 102, 105)];
        [_downloadLabel setText:@"立即下载"];
        [_downloadLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_downloadLabel setTextAlignment:NSTextAlignmentCenter];
        [_downloadLabel setTextColor:kRGBCOLOR(255, 218, 225)];
        [_downloadLabel setHidden:YES];
    }
    
    return _downloadLabel;
}

- (UIView *)lineView{
    if (!_lineView) {
         _lineView = [[UIView alloc] initWithFrame:CGRectZero];
        [_lineView setBackgroundColor:kRGBCOLOR(240.0, 240.0, 240.0)];
    }
    
    return _lineView;
}

@end

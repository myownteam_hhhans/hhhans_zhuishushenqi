//
//  RNChosenTableViewCell.h
//  ReplicaNovel
//
//

#import <UIKit/UIKit.h>
#import "ZY_ShuDan.h"

@interface RNChosenTableViewCell : UITableViewCell

@property (nonatomic,strong) ZY_ShuDan *chosenMode;

@end

//
//  BookScoreView.m
//  ReplicaNovel
//
//

#import "BookScoreView.h"

#define ViewHeight 14
#define starHeight 14
#define interval 4
@implementation BookScoreView


-(instancetype)initWithFrame:(CGRect)frame{
    frame.size.height = ViewHeight;
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

-(void)setScore:(CGFloat)score{
    _score = score;
    int count = (int)score;
    for (int i = 0; i < 5; i++) {
        UIImageView *starView = [[UIImageView alloc] initWithFrame:CGRectMake(i*(starHeight + 4), (ViewHeight-starHeight)/2, starHeight, starHeight)];
        if (i < count) {
            starView.image = [UIImage imageNamed:@"bookDetail_star_h"];
        }
        else if (count == i){
            starView.image = [UIImage imageNamed:(int)(score + 0.5) > count?@"bookDetail_star_h":@"bookDetail_star_b"];
        }
        else {
            starView.image = [UIImage imageNamed:@"bookDetail_star_n"];
        }
        
        [self addSubview:starView];
    }
    UILabel *lbl_score = [[UILabel alloc]initWithFrame:CGRectMake(94, 0, 40, self.frame.size.height)];
    lbl_score.text = [NSString stringWithFormat:@"%.1f",score*2];
    [self addSubview:lbl_score];
}
@end

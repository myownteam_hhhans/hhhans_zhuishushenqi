//
//  ZY_BookCateTableViewCell.h
//  ZYNovel
//
//  Created by apple on 2018/6/13.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelCate.h"

@interface ZY_BookCateTableViewCell : UITableViewCell

-(void)bindCateItem:(ZY_NovelCate *)cate;
@end

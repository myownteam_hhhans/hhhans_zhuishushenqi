//
//  ZY_BookLIst.m
//  ZYNovel
//
//  Created by apple on 2018/6/13.
//

#import "ZY_BookLIst.h"
#import "ZY_DataDownloader.h"
#import "ZY_Common.h"
#import "ZY_JsonParser.h"
#import "ZY_CommonTableViewCell.h"
#import "MJRefresh.h"
#import "SHW_ADInterstitial.h"
#import "UITableView+JRTableViewPlaceHolder.h"
#import "ZY_BookStoreAdTableViewCell.h"
#import "CFT_parameterOnline.h"
#import "ZY_HeadEntity.h"
#import "ZY_BookDetailsViewController.h"
#import <MTGSDK/MTGCampaign.h>
#import "MTGNativeAdsViewCell.h"
#import "Common.h"
#import <IIViewDeckController.h>
#import "GDTNativeAd.h"

@interface ZY_BookLIst()<ZY_DataDownloaderDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,GDTNativeAdDelegate>

@property(nonatomic,strong) ZY_DataDownloader *downLoader;

@property(nonatomic,strong) NSMutableDictionary *paraArrDic;

@property(nonatomic,assign) int currentIndex;

@property(nonatomic,strong) ADModel *currentAd;

@property(nonatomic,strong) NSMutableArray *bookStoreDataArray;

@property(nonatomic,strong) UIScrollView *scrollView;

@property(nonatomic,strong) NSString *sort;

@property(nonatomic, strong) MTGCampaign *campaign;

@property (nonatomic, strong) NSMutableArray *campaignArray;

@property (nonatomic, strong) NSString *adType;

@property (nonatomic, strong) GDTNativeAd *GDTAd;
@property (nonatomic, strong) NSArray *GDTDataArray;

@end

static NSString *nsstrBookStoreAdIdentifier=@"BookStoreAdIdentifier";

@implementation ZY_BookLIst

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.bookStoreDataArray = [NSMutableArray array];
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 50, frame.size.width, frame.size.height-50)];
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        //_scrollView.bounces = NO;
        [self addSubview:_scrollView];
        self.paraArrDic = [[NSMutableDictionary alloc]init];
        
        self.adType = [[Common shareCommon] randomADTypeWithKey:@"bookflow"];
        
        if ([self.adType isEqualToString:@"3"] && [Common shareCommon].zs_Open) {
            NSDictionary *gdtKeyAndID = [[ZY_ADHelper shareADHelper] getGDTKeyWithADPlace:@"bookflow"];
            self.GDTAd = [[GDTNativeAd alloc] initWithAppkey:gdtKeyAndID[@"ID"] placementId:gdtKeyAndID[@"KEY"]];
            self.GDTAd.delegate = self;
            self.GDTAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
            [self.GDTAd loadAd:3];
        }
        
    }
    return self;
}

-(void)loadData{
    
    [[ZY_ADHelper shareADHelper] requestLomiPic];
    
    [(NSMutableArray *)[self.paraArrDic objectForKey:@"nsintPage"] replaceObjectAtIndex:self.currentIndex withObject:@"1"];
    [(NSMutableArray *)[self.paraArrDic objectForKey:@"isPulldownRefresh"] replaceObjectAtIndex:self.currentIndex withObject:@"YES"];
    
    //dispatch_sync(dispatch_get_main_queue(), ^{
        [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).jr_placeHolderView setHidden:YES];
    //});
    
    
    [self.downLoader getNovelHtmlDataByUrl:[self resetBaseUrl:1]];
}

-(void)loadNextData{
    [[ZY_ADHelper shareADHelper] requestLomiPic];
    
    int nextpage = [[(NSMutableArray *)[self.paraArrDic objectForKey:@"nsintPage"] objectAtIndex:self.currentIndex] intValue];
    [self.downLoader getNovelHtmlDataByUrl:[self resetBaseUrl:nextpage]];
    [(NSMutableArray *)[self.paraArrDic objectForKey:@"isRefresh"] replaceObjectAtIndex:self.currentIndex withObject:@"YES"];
}

#pragma mark - Getter or Setter

-(ZY_DataDownloader *)downLoader{
    if (!_downLoader) {
        _downLoader = [[ZY_DataDownloader alloc] init];
        [_downLoader setDelegate:self];
    }
    
    return _downLoader;
}


#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [(NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[[(NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex] objectAtIndex:indexPath.row] objectAtIndex:1] floatValue];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentIndex >= self.bookStoreDataArray.count) {
        self.currentIndex = 0;
    }
    
    NSArray *dataArray = [(NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex] objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier = [dataArray firstObject];
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:nsstrIdentifier forIndexPath:indexPath];
    
    
    if ([nsstrIdentifier isEqualToString:nsstrBookIdentifier]){
        tableViewCell.selectionStyle = UITableViewCellSelectionStyleGray;
        ZY_NovelInfo *novelInfo = [dataArray lastObject];
        if (novelInfo) {
            [(ZY_CommonTableViewCell*)tableViewCell bindNovelItem:novelInfo];
        }
    }else if ([nsstrIdentifier isEqualToString:nsstrBookStoreAdIdentifier]){
        tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.currentAd) {
            [Common showDebugHUD:self.adType];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            [(ZY_BookStoreAdTableViewCell*)tableViewCell bindAdItem:self.currentAd];
            if ([self.adType isEqualToString:@"5"]) {
                // 展示汇报
                NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiPic;
                [[ZY_ADHelper shareADHelper] upLoadLomiShowWithURL:kaipingDic[@"count_url"]];
               
            }
            // 头条注册点击
            if ([self.adType isEqualToString:@"4"]) {
                BUNativeAd *ad = self.currentAd.data;
                [ad registerContainer:tableViewCell withClickableViews:tableViewCell.subviews];
                
            }
            if ([self.adType isEqualToString:@"3"]) {
                [self.GDTAd attachAd:self.currentAd.data toView:self];
                
            }
        }
    }
    
    return tableViewCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *dataArray = [(NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex] objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier = [dataArray firstObject];
    
    if ([nsstrIdentifier isEqualToString:nsstrBookIdentifier]){
        ZY_NovelInfo *novelInfo = [dataArray lastObject];
        
        if (novelInfo) {
            
            IIViewDeckController *iivc = (IIViewDeckController *)self.window.rootViewController;
            
            UINavigationController *nav = ((UITabBarController *)iivc.centerViewController).selectedViewController;
            ZY_BookDetailsViewController *zyDetailViewController = [[ZY_BookDetailsViewController alloc] init];
            zyDetailViewController.w_novel = novelInfo;
            zyDetailViewController.hidesBottomBarWhenPushed=YES;
            [nav pushViewController:zyDetailViewController animated:YES];
            
        }
        
    }else if ([nsstrIdentifier isEqualToString:nsstrBookStoreAdIdentifier]){
        if (self.currentAd) {
  
            [[ZY_ADHelper shareADHelper] tapAD:self.currentAd CurrentVC:[UIApplication sharedApplication].keyWindow.rootViewController];
            if ([self.adType isEqualToString:@"5"]) {
               
            }
            if ([self.adType isEqualToString:@"4"]) {
              
            }
            if ([self.adType isEqualToString:@"3"]) {
               
            }
            
        }
    }
}

#pragma mark - 广告代理
- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray {
    self.GDTDataArray = nativeAdDataArray;
    if (self.GDTDataArray && self.GDTDataArray.count > 0) {
        self.currentAd = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_BookShelf];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self loadData];
        });
        
        
    }
    
}

#pragma mark - ZY_DataDownloaderDelegate

-(void)downloadNavError:(NSError*)error downloadErrorType:(downloadErrorType)type{
    
    [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).jr_placeHolderView setHidden:NO];
    DebugLog(@"错误___%@",error);
    [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).mj_header endRefreshing];
    
    if ([[(NSMutableArray *)[self.paraArrDic objectForKey:@"isRefresh"] objectAtIndex:self.currentIndex] boolValue]) {
        [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).mj_footer endRefreshing];
        [(NSMutableArray *)[self.paraArrDic objectForKey:@"isRefresh"] replaceObjectAtIndex:self.currentIndex withObject:@"NO"];
    }
}

-(void)htmlDataFinish:(NSData *)htmlData{
    
    NSMutableArray *array = [[[ZY_JsonParser alloc] init] getNovelStoreListByData:htmlData];
    
    if (array&&array.count>0) {
        
        if ([[(NSMutableArray *)[self.paraArrDic objectForKey:@"isPulldownRefresh"] objectAtIndex:self.currentIndex] boolValue]) {
            [(NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex] removeAllObjects];
            [(NSMutableArray *)[self.paraArrDic objectForKey:@"isPulldownRefresh"] replaceObjectAtIndex:self.currentIndex withObject:@"NO"];
        }
        int randx = arc4random() % 4;
        
        for (NSInteger i = 0; i < array.count; i++) {
            
            if (((NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex]).count == randx) {
                
                NSString *adtype = self.adType;
                AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:adtype];
                
                if (platform != adplatform_Google && [[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookflow"] && ![self.adType isEqualToString:@"3"]) {
                    self.currentAd = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_BookShelf adPlatform:platform];
                }
                
                if (self.currentAd && [Common shareCommon].zs_Open) {
                    [(NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex] insertObject:@[nsstrBookStoreAdIdentifier,@(150),self.currentAd] atIndex:randx];
                }
                

                
                
            }
            
            [(NSMutableArray *)[self.bookStoreDataArray objectAtIndex:self.currentIndex] addObject:@[nsstrBookIdentifier,@(150),[array objectAtIndex:i]]];
        }
        
        
        int nextpage = [[(NSMutableArray *)[self.paraArrDic objectForKey:@"nsintPage"] objectAtIndex:self.currentIndex]intValue] + 1;
        [(NSMutableArray *)[self.paraArrDic objectForKey:@"nsintPage"] replaceObjectAtIndex:self.currentIndex withObject:[NSString stringWithFormat:@"%d",nextpage]];
        
    }
    
    if ([[(NSMutableArray *)[self.paraArrDic objectForKey:@"isRefresh"] objectAtIndex:self.currentIndex] boolValue]) {
        [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).mj_footer endRefreshing];
        [(NSMutableArray *)[self.paraArrDic objectForKey:@"isRefresh"] replaceObjectAtIndex:self.currentIndex withObject:@"NO"];
    }
    
    [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).mj_header endRefreshing];
    
    [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]) reloadData];
}

-(void)createHeadView{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, 50)];
    float padding2 = (kCScreenWidth-70*self.head_arr.count)/2;
    int i = 0;
    NSMutableArray *arr_page = [[NSMutableArray alloc]init];
    NSMutableArray *arr_isRefresh = [[NSMutableArray alloc]init];
    NSMutableArray *arr_isPulldownRefresh = [[NSMutableArray alloc]init];
    if(self.head_arr)
    {
        for (ZY_HeadEntity *head in self.head_arr) {
            UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(padding2+70*i, 2, 70, 46)];
            [btn setTitle:head.title forState:UIControlStateNormal];
            [btn setTitleColor:RGB(128, 128, 128) forState:UIControlStateNormal];
            btn.tag = 2000+i;
            btn.titleLabel.font = [UIFont systemFontOfSize:16];
            [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
            [headView addSubview:btn];
            
            UIView *v_xian1 = [[UIView alloc]initWithFrame:CGRectMake(20, 44, 30, 2)];
            v_xian1.backgroundColor = RGB(238, 62, 62);
            v_xian1.hidden = YES;
            v_xian1.tag = 1001;
            [btn addSubview:v_xian1];
            
            if (i == 0) {
                [btn setTitleColor:RGB(238, 62, 62) forState:UIControlStateNormal];
                v_xian1.hidden = NO;
                self.sort = head.urlKey;
            }
            [arr_page addObject:@"0"];
            [arr_isRefresh addObject:@"0"];
            [arr_isPulldownRefresh addObject:@"0"];
            [self.bookStoreDataArray addObject:[[NSMutableArray array]init]];
            i++;
        }
        UIView *v_xian4 = [[UIView alloc]initWithFrame:CGRectMake(0, 49, kCScreenWidth, 1)];
        v_xian4.backgroundColor = RGB(242, 242, 242);
        [headView addSubview:v_xian4];
        [self addSubview: headView];
    }else{
        [arr_page addObject:@"0"];
        [arr_isRefresh addObject:@"0"];
        [arr_isPulldownRefresh addObject:@"0"];
        [self.bookStoreDataArray addObject:[[NSMutableArray array]init]];
        _scrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }
    
    
    [self.paraArrDic setObject:arr_page forKey:@"nsintPage"];
    [self.paraArrDic setObject:arr_isRefresh forKey:@"isRefresh"];
    [self.paraArrDic setObject:arr_isPulldownRefresh forKey:@"isPulldownRefresh"];
    
}

-(void)createTableView{
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width*self.head_arr.count, 1);
    int totalCount = (int)self.head_arr.count;
    if (totalCount == 0) {
        totalCount = 1;
    }
    for (int i = 0;i < totalCount ;i++) {
        UITableView *tab = [[UITableView alloc]initWithFrame:CGRectMake(_scrollView.frame.size.width*i, 0,_scrollView.frame.size.width, _scrollView.frame.size.height) style:UITableViewStylePlain];
        if(@available(iOS 11.0, *)) {
            tab.estimatedRowHeight = 0;
            tab.estimatedSectionFooterHeight = 0;
            tab.estimatedSectionHeaderHeight = 0;
            tab.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        tab.tag = 10 + i;
        [tab setDelegate:self];
        [tab setDataSource:self];
        [tab registerClass:[ZY_CommonTableViewCell class] forCellReuseIdentifier:nsstrBookIdentifier];
        [tab registerClass:[ZY_BookStoreAdTableViewCell class] forCellReuseIdentifier:nsstrBookStoreAdIdentifier];
        // M广告
        [tab registerNib:[UINib nibWithNibName:@"MTGNativeAdsViewCell" bundle:nil] forCellReuseIdentifier:@"MTGNativeAdsViewCell"];
        tab.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        tab.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadNextData)];
        tab.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [tab jr_configureWithPlaceHolderBlock:^UIView * _Nonnull(UITableView * _Nonnull sender) {
        UIView *errorView = [ZY_Common createLoadingErrorViewWithFrame:tab.bounds withTarget:tab.mj_header action:@selector(beginRefreshing)];
            [errorView setHidden:YES];
            return errorView;
        } normalBlock:^(UITableView * _Nonnull sender) {
            [tab setScrollEnabled:YES];
        }];
        if (i == 0) {
            [tab.mj_header beginRefreshing];
        }
        [_scrollView addSubview:tab];
    }
}

-(void)clickBtn:(id)sender{
    UIButton *btn_curr = sender;
    for (int i = 0;i < self.head_arr.count;i++) {
        UIButton *btn = [self viewWithTag:2000+i];
        if (i == (btn_curr.tag - 2000)) {
            [btn setTitleColor:RGB(238, 62, 62) forState:UIControlStateNormal];
            [btn viewWithTag:1001].hidden = NO;
            self.sort = ((ZY_HeadEntity *)[self.head_arr objectAtIndex:i]).urlKey;
        }
        else{
            [btn setTitleColor:RGB(128, 128, 128) forState:UIControlStateNormal];
            [btn viewWithTag:1001].hidden = YES;
        }
    }
    
    self.currentIndex = (int)btn_curr.tag - 2000;
    if ([[(NSMutableArray *)[self.paraArrDic objectForKey:@"nsintPage"] objectAtIndex:self.currentIndex]intValue] == 0) {
        //第一次加载
        [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).mj_header beginRefreshing];
    }
    _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width*self.currentIndex, 0);
}
-(NSString *)resetBaseUrl:(int)page{
    return [NSString stringWithFormat:self.BaseMethodUrl,self.sort,(page - 1)*20, 20];
}

#pragma mark - scrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat pageWidth = _scrollView.frame.size.width;
    int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.currentIndex = page;
    
    for (int i = 0;i < self.head_arr.count;i++) {
        UIButton *btn = [self viewWithTag:2000+i];
        if (i == page) {
            [btn setTitleColor:RGB(238, 62, 62) forState:UIControlStateNormal];
            [btn viewWithTag:1001].hidden = NO;
            self.sort = ((ZY_HeadEntity *)[self.head_arr objectAtIndex:i]).urlKey;
        }
        else{
            [btn setTitleColor:RGB(128, 128, 128) forState:UIControlStateNormal];
            [btn viewWithTag:1001].hidden = YES;
        }
    }
    
    if ([[(NSMutableArray *)[self.paraArrDic objectForKey:@"nsintPage"] objectAtIndex:self.currentIndex]intValue] == 0) {
        //第一次加载
        [((UITableView *)[_scrollView viewWithTag:(self.currentIndex+10)]).mj_header beginRefreshing];
    }
}

- (NSMutableArray *)campaignArray {
    if (_campaignArray == nil) {
        _campaignArray = [NSMutableArray array];
    }
    return _campaignArray;
}


@end

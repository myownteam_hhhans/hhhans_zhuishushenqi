//
//  ZY_BookLIst.h
//  ZYNovel
//
//  Created by apple on 2018/6/13.
//

#import <UIKit/UIKit.h>

@interface ZY_BookLIst : UIView

@property(nonatomic,strong) NSString *BaseMethodUrl;
@property(nonatomic,assign) BOOL isPalmStore;
@property(nonatomic,strong) NSMutableArray *head_arr;

-(void)createHeadView;
-(void)createTableView;
-(void)loadData;

@end

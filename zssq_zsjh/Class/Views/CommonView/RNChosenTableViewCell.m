//
//  RNChosenTableViewCell.m
//  ReplicaNovel
//
//

#import "RNChosenTableViewCell.h"
#import <UIImageView+WebCache.h>
#import "SDWebImageManager.h"

@interface RNChosenTableViewCell ()
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIImageView *iconImageView;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) UILabel *descLabel;
@property (nonatomic) int shuDanCellHeight;
@end

@implementation RNChosenTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _shuDanCellHeight = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)?385:285;
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.iconImageView];
        [self.bgView addSubview:self.titleLabel];
        [self.bgView addSubview:self.descLabel];
        
    }
    return self;
}

-(UIView *)bgView{
    if (_bgView == nil) {
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(11, 10, CGRectGetWidth([UIScreen mainScreen].bounds) - 22, _shuDanCellHeight-20)];
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.cornerRadius = 8;
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.userInteractionEnabled = YES;
        
        CALayer *subLayer=[CALayer layer];
        CGRect subFrame = _bgView.frame;
        subFrame.origin.x = subFrame.origin.x +0.5;
        subFrame.origin.y = subFrame.origin.y +0.5;
        subFrame.size.width = subFrame.size.width-1;
        subFrame.size.height =  subFrame.size.height-1;
        subLayer.frame= subFrame;
        subLayer.cornerRadius = 8;;
        subLayer.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8].CGColor;
        subLayer.masksToBounds=NO;
        subLayer.shadowColor = [[UIColor grayColor] colorWithAlphaComponent:0.8].CGColor;//shadowColor阴影颜色
        subLayer.shadowOffset = CGSizeMake(0,0);//shadowOffset阴影偏移,x向右偏移3，y向下偏移2，默认(0, -3),这个跟shadowRadius配合使用
        subLayer.shadowOpacity = 0.8;//阴影透明度，默认0
        subLayer.shadowRadius = 4;//阴影半径，默认3
        [self.contentView.layer insertSublayer:subLayer below:_bgView.layer];
    }
    return _bgView;
}

-(UIImageView *)iconImageView{
    if (_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bgView.frame), _shuDanCellHeight-130)];
        _iconImageView.layer.masksToBounds = YES;
    }
    return _iconImageView;
}



-(UILabel *)titleLabel{
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((17),CGRectGetMaxY(self.iconImageView.frame) +(19),CGRectGetWidth(self.bgView.frame)-34, (22))];
        _titleLabel.font = [UIFont systemFontOfSize:16 weight:1];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.textColor = [UIColor blackColor];
    }
    return _titleLabel;
}

-(UILabel *)descLabel{
    if (_descLabel == nil) {
        _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.titleLabel.frame), (55) + CGRectGetMaxY(self.iconImageView.frame), CGRectGetWidth(self.bgView.frame) - CGRectGetMinX(self.titleLabel.frame) *2, (34))];
        _descLabel.numberOfLines = 2;
        _descLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _descLabel.font = [UIFont systemFontOfSize:12];
        _descLabel.textColor = RGB(69, 69, 69);
        _descLabel.textAlignment = NSTextAlignmentLeft;
    }
    return _descLabel;
}


-(void)setChosenMode:(ZY_ShuDan *)chosenMode{
    _chosenMode = chosenMode;
    if (chosenMode.ShuDanImg) {
        self.iconImageView.contentMode = UIViewContentModeScaleToFill;
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:chosenMode.ShuDanImg] placeholderImage:[[UIImage imageNamed:@"bookDefalue"]resizableImageWithCapInsets:UIEdgeInsetsMake(50, 50, 50, 50) resizingMode:UIImageResizingModeStretch] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (image) {
                _iconImageView.contentMode = UIViewContentModeScaleAspectFill;
            }
        }];
    }
    
    self.titleLabel.text = chosenMode.ShuDanName;
    self.descLabel.text = chosenMode.ShuDanInfo;
}


@end

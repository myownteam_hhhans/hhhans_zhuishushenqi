//
//  ZY_CustomNotificationView.h
//  ZYNovel
//
//

#import <UIKit/UIKit.h>

@interface ZY_CustomNotificationView : UIView

-(void)bindNotificationMessage:(NSString*)nsstrMessage;

@end

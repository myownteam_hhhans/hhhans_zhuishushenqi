//
//  ZY_NeedFiveStarView.h
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/11.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZY_NeedFiveStarView : UIView

// 0要好评 1看视频
@property (nonatomic, assign) BOOL fiveStarOrVideo;

- (void)removeSelf;

@end

//
//  ZY_CommonTableViewCell.h
//  ZYNovel
//
//  Created by S on 2017/3/15.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@interface ZY_CommonTableViewCell : UITableViewCell

-(void)bindNovelItem:(ZY_NovelInfo *)novelInfo;

@end

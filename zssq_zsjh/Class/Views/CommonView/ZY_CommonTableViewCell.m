//
//  ZY_CommonTableViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/15.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_CommonTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "ZY_Common.h"

@interface ZY_CommonTableViewCell()

@property(nonatomic,strong) UILabel *firstNamelabel;

@property(nonatomic,strong) UILabel *introductionNamelabel;

@property(nonatomic,strong) UIImageView *imgView;

@property(nonatomic,strong) UILabel *scorelabel;

@property(nonatomic,strong) UILabel *authorNamelabel;

@property(nonatomic,strong) UILabel *categroylabel;

@property(nonatomic,strong) UIView *bgView;

//@property(nonatomic,strong) UIView *lineView;

@end

@implementation ZY_CommonTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.imgView];
        [self addSubview:self.introductionNamelabel];
        [self addSubview:self.scorelabel];
        [self addSubview:self.authorNamelabel];
        [self addSubview:self.categroylabel];
        [self addSubview:self.firstNamelabel];

    }
    return self;
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self.imgView setImage:[UIImage imageNamed:@"暂无封面"]];
}

#pragma mark - Getter or Setter

-(UIImageView *)imgView
{
    if (!_imgView) {
         _imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 92,115)];
        [_imgView setBackgroundColor:kRGBCOLOR(239, 239, 239)];
        [_imgView setImage:[UIImage imageNamed:@"暂无封面"]];
         _imgView.contentMode = UIViewContentModeCenter;

    }
    return _imgView;
}

-(UILabel *)firstNamelabel
{
    if (!_firstNamelabel) {
         _firstNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.bgView.frame)+10, 23, kCScreenWidth-(CGRectGetMaxX(self.bgView.frame)+10), 16)];
        [_firstNamelabel setFont:[UIFont systemFontOfSize:16]];
        [_firstNamelabel setTextColor:kRGBCOLOR(51, 51, 51)];
    }
    return _firstNamelabel;
}

-(UILabel *)introductionNamelabel
{
    if (!_introductionNamelabel) {
        CGSize size=[UIScreen mainScreen].bounds.size;
        CGFloat rightMargin=15;
        CGFloat leftMargin=8;
        CGFloat imageRightMargin=15;
        _introductionNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.bgView.frame)+10, 45, (size.width)-rightMargin-leftMargin-CGRectGetWidth(self.imgView.frame)-imageRightMargin, 60)];
        _introductionNamelabel.numberOfLines = 2;
        _introductionNamelabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_introductionNamelabel setTextColor:kRGBCOLOR(155, 155, 155)];
        [_introductionNamelabel setFont:[UIFont systemFontOfSize:14]];
    }
    return _introductionNamelabel;
}

-(UILabel *)scorelabel
{
    if (!_scorelabel) {
         _scorelabel = [[UILabel alloc]initWithFrame:CGRectMake(kCScreenWidth-40, 20, 24, 20)];
        [_scorelabel setFont:[UIFont systemFontOfSize:16]];
        _scorelabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_scorelabel setTextColor:kRGBCOLOR(255, 206, 68)];
    }
    return _scorelabel;
}

-(UILabel *)authorNamelabel
{
    if (!_authorNamelabel) {
        
         _authorNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.bgView.frame)+10, CGRectGetMaxY(self.bgView.frame)-17,68, 13)];
        [_authorNamelabel setTextColor:kRGBCOLOR(153, 153, 153)];
        _authorNamelabel.lineBreakMode = NSLineBreakByWordWrapping;
        [_authorNamelabel setFont:[UIFont systemFontOfSize:13]];
    }
    
    return _authorNamelabel;
}
-(UILabel *)categroylabel
{
    if (!_categroylabel) {
        _categroylabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.authorNamelabel.frame)+10, CGRectGetMaxY(self.bgView.frame)-18,50, 16)];
        _categroylabel.layer.cornerRadius = 4;
        _categroylabel.layer.masksToBounds = YES;
        [_categroylabel setTextAlignment:NSTextAlignmentCenter];
        [_categroylabel setTextColor:[UIColor whiteColor]];
        [_categroylabel setFont:[UIFont systemFontOfSize:10]];
    }
    
    return _categroylabel;
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectMake(15, 15, 92,115)];
        _bgView.layer.shadowColor = [UIColor blackColor].CGColor;
        _bgView.layer.shadowOffset = CGSizeMake(0, 0);
        _bgView.layer.shadowOpacity = 0.5;
        _bgView.layer.shadowRadius = 2.0;
    }
    
    return _bgView;
}



-(void)bindNovelItem:(ZY_NovelInfo *)novelInfo
{
    if (novelInfo) {
        
        self.authorNamelabel.frame = CGRectMake(self.authorNamelabel.frame.origin.x, self.authorNamelabel.frame.origin.y,[self calculateRowWidth:novelInfo.novelAuthor], self.authorNamelabel.frame.size.height);
        self.categroylabel.frame = CGRectMake(CGRectGetMaxX(self.authorNamelabel.frame)+10, self.categroylabel.frame.origin.y,self.categroylabel.frame.size.width, self.categroylabel.frame.size.height);
        if([novelInfo.novelScore intValue] > 0)
        {
            [self.scorelabel setText:[NSString stringWithFormat:@"%.1f",[novelInfo.novelScore floatValue]]];
        }
        [self.categroylabel setText:novelInfo.novelCategory];
        [self.categroylabel setBackgroundColor:[ZY_Common getCateColor:novelInfo.novelCategory]];
        [self.authorNamelabel setText:novelInfo.novelAuthor];
        [self.firstNamelabel setText:novelInfo.novelName];
        [self.introductionNamelabel setText:novelInfo.novelDes];
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:novelInfo.novelCoverUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                self.imgView.contentMode = UIViewContentModeScaleAspectFill;
               [self.imgView setClipsToBounds:YES];
            }else{
                self.imgView.contentMode = UIViewContentModeCenter;
               [self.imgView setClipsToBounds:NO];
               [self.imgView setImage:[UIImage imageNamed:@"暂无封面"]];
            }
        }];
    }
}

- (CGFloat)calculateRowWidth:(NSString *)string {
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:13]};  //指定字号
    CGRect rect = [string boundingRectWithSize:CGSizeMake(0, 13)/*计算宽度时要确定高度*/ options:NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:dic context:nil];
    return rect.size.width;
}
@end

//
//  F_TitleView.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/24.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "F_TitleView.h"
#import "ZY_Common.h"

@interface F_TitleView()

@property (nonatomic, strong) UIView *sliderView;

@end

@implementation F_TitleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.sliderView];
        [self addSubview:self.maleButton];
        [self addSubview:self.femaleButton];
        [self addSubview:self.collectionButton];
    }
    return self;
}

#pragma mark ==== method ====

// 外部调用
- (void)clickSexButton:(NSInteger)sex {
    if (sex == 1) {
        [self titleButtonClick:self.maleButton];
    } else if (sex == 0){
        [self titleButtonClick:self.femaleButton];
    } else {
        [self titleButtonClick:self.collectionButton];
    }
}

// 内部点击
- (void)titleButtonClick:(UIButton *)sender {
    
    // 滑动slider
    [UIView animateWithDuration:0.3 animations:^{
        CGPoint sliderPoint = self.sliderView.center;
        sliderPoint.x = sender.center.x;
        self.sliderView.center = sliderPoint;
    }];

    
    // 通知外部
    if (![self.delegate respondsToSelector:@selector(buttonClickWithSex:)]) {
        return;
    }
    [self.delegate buttonClickWithSex:sender.tag];
    
}


#pragma mark ==== lazyLoad ====

- (UIView *)sliderView {
    if (_sliderView == nil) {
        _sliderView = [[UIView alloc] initWithFrame:CGRectMake(165, 44 - 8, 15, 3)];
        _sliderView.layer.cornerRadius = 1.5;
        _sliderView.backgroundColor = [ZY_Common colorWithHexString:@"#ED6574" alpha:1.0];
    }
    return _sliderView;
}
- (UIButton *)femaleButton {
    if (_femaleButton == nil) {
        _femaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_femaleButton setTitle:@"男生" forState:UIControlStateNormal];
        [_femaleButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [_femaleButton setTitleColor:[ZY_Common colorWithHexString:@"#333333" alpha:1.0] forState:UIControlStateNormal];
        [_femaleButton setTitleColor:[ZY_Common colorWithHexString:@"#ED6574" alpha:1.0] forState:UIControlStateSelected];
        [_femaleButton setTag:0];
        [_femaleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_femaleButton sizeToFit];
        [_femaleButton setCenter:CGPointMake(self.frame.size.width/2 - 28 - 17, 14 + 17.0/2)];
    }
    return _femaleButton;
}
- (UIButton *)maleButton {
    if (_maleButton == nil) {
        _maleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_maleButton setTitle:@"女生" forState:UIControlStateNormal];
        [_maleButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [_maleButton setTitleColor:[ZY_Common colorWithHexString:@"#333333" alpha:1.0] forState:UIControlStateNormal];
        [_maleButton setTitleColor:[ZY_Common colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateSelected];
        [_maleButton setTag:1];
        [_maleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_maleButton sizeToFit];
        [_maleButton setCenter:CGPointMake(self.frame.size.width/2, 14 + 17.0/2)];
    }
    return _maleButton;
}
- (UIButton *)collectionButton {
    if (_collectionButton == nil) {
        _collectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_collectionButton setTitle:@"精品" forState:UIControlStateNormal];
        [_collectionButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [_collectionButton setTitleColor:[ZY_Common colorWithHexString:@"#333333" alpha:1.0] forState:UIControlStateNormal];
        [_collectionButton setTitleColor:[ZY_Common colorWithHexString:@"#FFFFFF" alpha:1.0] forState:UIControlStateSelected];
        [_collectionButton setTag:2];
        [_collectionButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_collectionButton sizeToFit];
        [_collectionButton setCenter:CGPointMake(self.frame.size.width/2 + 28 + 17, 14 + 17.0/2)];
    }
    return _collectionButton;
}

@end

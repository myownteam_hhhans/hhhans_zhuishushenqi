//
//  BookStoreCell.h
//  ZYNovel
//
//  Created by 包涵 on 2018/9/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookStoreCell : UITableViewCell

@property (nonatomic ,strong) NSDictionary *dataDic;
@property (nonatomic, strong) NSString *urlString;

- (void)bindDataWithName:(NSString *)name urlString:(NSString *)url;

@end

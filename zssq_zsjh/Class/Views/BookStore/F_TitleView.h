//
//  F_TitleView.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/24.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol titleClickDelegate <NSObject>

- (void)buttonClickWithSex:(NSInteger)sex; // 0 男 1 女  2 精品 点击代理

@end

@interface F_TitleView : UIView

@property (nonatomic, strong) UIButton *maleButton;
@property (nonatomic, strong) UIButton *femaleButton;
@property (nonatomic, strong) UIButton *collectionButton;

@property (nonatomic, weak) id<titleClickDelegate> delegate;
- (void)clickSexButton:(NSInteger)sex;   // 0 男 1 女   滑动之后调用此点击方法

@end

//
//  BookStoreCell.m
//  ZYNovel
//
//  Created by 包涵 on 2018/9/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BookStoreCell.h"
#import "NWS_httpClient.h"
#import <UIImageView+WebCache.h>

@interface BookStoreCell()

@property (nonatomic, strong) UIImageView *coverImage;
@property (nonatomic, strong) UILabel *listNameLabel;
@property (nonatomic, strong) UILabel *bookNameLabel;
@property (nonatomic, strong) UIImageView *nextPageView;

@end

@implementation BookStoreCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.coverImage];
        [self addSubview:self.listNameLabel];
        [self addSubview:self.bookNameLabel];
        [self addSubview:self.nextPageView];
    }
    return self;
}

- (void)bindDataWithName:(NSString *)name urlString:(NSString *)url {
    [self.listNameLabel setText:name];
    self.urlString = url;
    [[NWS_httpClient sharedInstance] getWithURLString:url parameters:nil success:^(NSDictionary *responseObject) {
        
        NSNumber *suceese = responseObject[@"ok"];
        if (!suceese) {
            return;
        }

        self.dataDic = responseObject[@"ranking"];
        NSArray *bookArray = self.dataDic[@"books"];
        
        if (bookArray.count == 0 || !bookArray) {
            return;
        }
        
        NSDictionary *firstBook = bookArray[0];
        NSString *coverURLString = firstBook[@"cover"];
        if (coverURLString && coverURLString.length >8) {
            coverURLString = [coverURLString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            coverURLString = [coverURLString substringFromIndex:7];
            [self.coverImage sd_setImageWithURL:[NSURL URLWithString:coverURLString]];
        }
        NSMutableString *bookNameString = [NSMutableString stringWithFormat:@""];
        for (NSDictionary *bookDic in bookArray) {
            [bookNameString appendString:@" "];
            [bookNameString appendString:bookDic[@"title"]];
            if (bookNameString.length > 30) {
                break;
            }
        }
        [self.bookNameLabel setText:bookNameString];
        
    } failure:^(NSError *error) {
        
    }];
}


#pragma mark ==== 懒加载 ====
- (UIImageView *)coverImage {
    if (_coverImage == nil) {
        _coverImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 36, 50)];
    }
    return _coverImage;
}
- (UILabel *)listNameLabel {
    if (_listNameLabel == nil) {
        _listNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(57, 12, self.frame.size.width - 80, 17)];
        [_listNameLabel setTextAlignment:NSTextAlignmentLeft];
        [_listNameLabel setFont:[UIFont systemFontOfSize:16]];
    }
    return _listNameLabel;
}
- (UILabel *)bookNameLabel {
    if (_bookNameLabel == nil) {
        _bookNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(57, 35, self.frame.size.width - 60, 11)];
        [_bookNameLabel setFont:[UIFont systemFontOfSize:10]];
        [_bookNameLabel setTextColor:[UIColor colorWithRed:102/256.0 green:102/256.0 blue:102/256.0 alpha:1.0]];
    }
    return _bookNameLabel;
}
- (UIImageView *)nextPageView {
    if (_nextPageView == nil) {
        _nextPageView = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-22, 24, 8, 13)];
        [_nextPageView setImage:[UIImage imageNamed:@"bookStoreNext"]];
    }
    return _nextPageView;
}
@end

//
//  My_NoDataView.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/23.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "My_NoDataView.h"
#import "Common.h"

@implementation My_NoDataView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:({
            UIImageView *noMesView = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(142, 186, 133, 121)]];
            [noMesView setImage:[UIImage imageNamed:@"My_NoQueue"]];
            noMesView;
        })];
        [self addSubview:({
            UILabel *noMesView = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(95, 186, 219, 154)]];
            [noMesView setFont:[UIFont systemFontOfSize:16]];
            [noMesView setText:@"还没有下载任务哦(·_·？)"];
            [noMesView sizeToFit];
            [noMesView setCenter:[Common get414PointByMap:207 withY:349]];
            noMesView;
        })];
    }
    return self;
}

@end

//
//  M_DownloadInfoCell.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/17.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSYReadModel.h"

@interface M_DownloadInfoCell : UITableViewCell

@property (nonatomic, strong) UILabel *downloadInfoLabel;
@property (nonatomic, strong) UIButton *startButton;

- (void)bindDataWithModel:(LSYReadModel *)model;

@end

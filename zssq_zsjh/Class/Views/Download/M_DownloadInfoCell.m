//
//  M_DownloadInfoCell.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/17.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "M_DownloadInfoCell.h"
#import <UIImageView+WebCache.h>
#import "LSYReadModel.h"
#import "ZY_BookHelp.h"
#import "ZY_NovelDownLoadOperation.h"
#import "LS_DownloadInfoManager.h"
#import "Common.h"

@interface M_DownloadInfoCell()

@property (nonatomic, strong) UIImageView *novelCoverView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *authurLabel;
@property (nonatomic, strong) UIButton *deleteButton;

@property (nonatomic, weak) LSYReadModel *selfModel;

@end

@implementation M_DownloadInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.novelCoverView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.authurLabel];
        [self addSubview: self.downloadInfoLabel];
        [self addSubview:self.deleteButton];
    
        [self addSubview:({
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(20, 117, k_ScreenWidth-20, 1)];
            [lineView setBackgroundColor:[Common colorWithHexString:@"#E2E2E5" alpha:1.0]];
            lineView;
        })];
    }
    return self;
}

#pragma mark ==== 私有方法 ====
- (void)bindDataWithModel:(LSYReadModel *)model {
    [self.novelCoverView sd_setImageWithURL:[NSURL URLWithString:model.novelInfo.novelCoverUrl]];
    [self.titleLabel setText:model.novelInfo.novelName];
    [self.titleLabel sizeToFit];
    [self.authurLabel setText:model.novelInfo.novelAuthor];
    [model.novelInfo.novelAuthor containsString:@"null"];
    self.authurLabel.hidden = YES;
    [self.authurLabel sizeToFit];
    
    _selfModel = model;
}

- (void)deleteButtonClick {
    if ([self.reuseIdentifier isEqualToString:@"downloadingCell"]) {  // 正在下载删除
        
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确定删除下载任务？" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            // 取消正在进行的任务
            for (ZY_NovelDownLoadOperation *downOP in [ZY_BookHelp shareBookHelp].novelDownLoadQueue.operations) {
                if (downOP.downloadInfo.readModel == self.selfModel) {
                    downOP.isPaused = NO;
                    [downOP cancel];
                }
            }
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
        
    } else {  // 下载过的删除
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确定删除此书的所有下载内容？（书架、进度、书签、等不受影响）" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [[LS_DownloadInfoManager shareDownloadInfoManager] deletaDownloadedItem:self.selfModel];
        }]];

        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
        
        
    }
    
}

#pragma mark ==== 懒加载 ====
-(UIImageView *)novelCoverView {
    if (_novelCoverView == nil) {
        _novelCoverView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 55, 78)];
    }
    return _novelCoverView;
}
- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(89, 24, 1, 1)];
        [_titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_titleLabel setTextColor:[UIColor blackColor]];
    }
    return _titleLabel;
}
- (UILabel *)authurLabel {
    if (_authurLabel == nil) {
        _authurLabel = [[UILabel alloc] initWithFrame:CGRectMake(106, 57, 1, 1)];
        [_authurLabel setFont:[UIFont systemFontOfSize:12]];
        [_authurLabel setTextColor:[Common colorWithHexString:@"#AAAAAA" alpha:1.0]];
    }
    return _authurLabel;
}
- (UILabel *)downloadInfoLabel {
    if (_downloadInfoLabel == nil) {
        _downloadInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(89, 79, 80, 12)];
        [_downloadInfoLabel setFont:[UIFont systemFontOfSize:12]];
        [_downloadInfoLabel setTextColor:[Common colorWithHexString:@"#AAAAAA" alpha:1.0]];
        [_downloadInfoLabel setTextAlignment:NSTextAlignmentLeft];
    }
    return _downloadInfoLabel;
}

-(UIButton *)deleteButton {
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setFrame:CGRectMake(k_ScreenWidth - 17 - 20 - 6, 51 - 6, 30, 30)];
        [_deleteButton setImage:[UIImage imageNamed:@"My_Delete"] forState:UIControlStateNormal];
        [_deleteButton addTarget:self action:@selector(deleteButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}


@end

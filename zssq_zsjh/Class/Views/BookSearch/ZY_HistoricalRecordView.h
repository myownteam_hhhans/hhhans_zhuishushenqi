//
//  HistoricalRecordView.h
//  ZYNovel
//

//

#import <UIKit/UIKit.h>

@protocol ZY_HistoricalRecordViewDelegate <NSObject>

@optional

-(void)clickHistoricalWord:(NSString*)word;

@end


@interface ZY_HistoricalRecordView : UIView

@property (nonatomic, weak) id<ZY_HistoricalRecordViewDelegate> delegate;

-(void)reloadData;

@end

//
//  ZY_SearchSelectCell.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/14.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZY_SearchSelectCell : UICollectionViewCell

@property (nonatomic, strong) UIButton *button;

@end

NS_ASSUME_NONNULL_END

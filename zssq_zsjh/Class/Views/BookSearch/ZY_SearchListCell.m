//
//  ZY_SearchListCell.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/15.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "ZY_SearchListCell.h"
#import "Common.h"
#import <UIImageView+WebCache.h>

@interface ZY_SearchListCell ()

@property (nonatomic, strong) UILabel *listNameLabel;
@property (nonatomic, strong) UILabel *bookCountLabel;
@property (nonatomic, strong) UILabel *desLabel;

@property (nonatomic, strong) UIImageView *leftImage;
@property (nonatomic, strong) UIImageView *centerImage;
@property (nonatomic, strong) UIImageView *rightImage;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation ZY_SearchListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.lineView];
        
        [self addSubview:self.listNameLabel];
        [self addSubview:self.bookCountLabel];
        [self addSubview:self.desLabel];
        [self addSubview:self.leftImage];
        [self addSubview:self.rightImage];
        [self addSubview:self.centerImage];
    }
    return self;
}


- (void)bindDataWithModel:(ZY_SearchListModel *)model {
    [self.listNameLabel setText:model.listName];
    [self.bookCountLabel setText:[NSString stringWithFormat:@"%ld本 | %ld收藏",model.bookCount.integerValue,model.collectionCount.integerValue]];
    [self.desLabel setText:model.listDes];
    if (model.coverURLArray.count == 1) {
        
        NSString *urlString = model.coverURLArray[0];
        if ([urlString isKindOfClass:[NSString class]] && urlString.length > 8) {
            urlString = [urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            urlString = [urlString substringFromIndex:7];
            [self.centerImage sd_setImageWithURL:[NSURL URLWithString:urlString]];
        }
        
        
    } else if (model.coverURLArray.count == 3) {
        
        for (int i = 0; i < model.coverURLArray.count; i++) {
            NSString *str = model.coverURLArray[i];
            if ([str isKindOfClass:[NSString class]] && str.length > 8) {
                str = [str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                str = [str substringFromIndex:7];
                
            }
            switch (i) {
                case 0:
                    [self.centerImage sd_setImageWithURL:[NSURL URLWithString:str]];
                    break;
                case 1:
                    [self.leftImage sd_setImageWithURL:[NSURL URLWithString:str]];
                    break;
                case 2:
                    [self.rightImage sd_setImageWithURL:[NSURL URLWithString:str]];
                    break;
                default:
                    break;
            }
        }
    }
}

#pragma mark - lazy load
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 143.3, 414, 0.7)]];
        [_lineView setBackgroundColor:[Common colorWithHexString:@"#EBEBF0" alpha:1.0]];
    }
    return _lineView;
}
- (UILabel *)listNameLabel {
    if (!_listNameLabel) {
        _listNameLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(12, 35, 260, 16)]];
        [_listNameLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
        [_listNameLabel setTextAlignment:NSTextAlignmentLeft];
    }
    return _listNameLabel;
}
- (UILabel *)bookCountLabel {
    if (!_bookCountLabel) {
        _bookCountLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(12, 65, 260, 11)]];
        [_bookCountLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:11]]];
        [_bookCountLabel setTextAlignment:NSTextAlignmentLeft];
        [_bookCountLabel setTextColor:[Common colorWithHexString:@"#B7B7BD" alpha:1.0]];
    }
    return _bookCountLabel;
}
- (UILabel *)desLabel {
    if (!_desLabel) {
        _desLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(12, 95, 260, 13)]];
        [_desLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
        [_desLabel setTextAlignment:NSTextAlignmentLeft];
        [_desLabel setTextColor:[Common colorWithHexString:@"#8E8E93" alpha:1.0]];
    }
    return _desLabel;
}
- (UIImageView *)centerImage {
    if (_centerImage == nil) {
        _centerImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(315, 39, 58, 82)]];
        [_centerImage setContentMode:UIViewContentModeScaleAspectFill];
    }
    return _centerImage;
}
- (UIImageView *)leftImage {
    if (!_leftImage) {
        _leftImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(281, 47, 47, 68)]];
        [_leftImage setContentMode:UIViewContentModeScaleAspectFill];
    }
    return _leftImage;
}
- (UIImageView *)rightImage {
    if (!_rightImage) {
        _rightImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(352, 47, 47, 68)]];
        [_rightImage setContentMode:UIViewContentModeScaleAspectFill];
    }
    return _rightImage;
}

@end

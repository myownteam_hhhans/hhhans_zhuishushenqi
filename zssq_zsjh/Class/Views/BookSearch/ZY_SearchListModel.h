//
//  ZY_SearchListModel.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/15.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZY_SearchListModel : NSObject

@property (nonatomic, strong) NSString *listID;
@property (nonatomic, strong) NSString *listName;
@property (nonatomic, strong) NSString *listDes;
@property (nonatomic, strong) NSNumber *bookCount;
@property (nonatomic, strong) NSNumber *collectionCount; // 收藏数
@property (nonatomic, strong) NSArray *coverURLArray;

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

//
//  ZY_SearchSelectView.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/1/30.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SearchSelectSure <NSObject>

- (void)searchSelectSure;

@end

@interface ZY_SearchSelectView : UIView

@property (nonatomic, weak) id<SearchSelectSure> delegate;
@property (nonatomic, strong) NSMutableArray *categoryArray;
@property (nonatomic, strong) NSArray *selectedItemArray;

- (void)loadCollectionViewWithArray:(NSMutableArray *)arr;

@end

NS_ASSUME_NONNULL_END

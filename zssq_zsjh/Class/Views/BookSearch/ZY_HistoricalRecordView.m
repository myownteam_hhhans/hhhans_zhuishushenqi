//
//  HistoricalRecordView.m
//  ZYNovel
//
//

#import "ZY_HistoricalRecordView.h"
#import "ZY_Common.h"
#import "ZY_DBHelper.h"

static NSString *nsstrHistoricalRecordCellIdentifier=@"HistoricalRecordCell";

@interface ZY_HistoricalRecordCell : UITableViewCell

@property(nonatomic,strong) UIImageView *imageIconView;

@property(nonatomic,strong) UILabel *labelTitle;

@property(nonatomic,strong) UIView *lineView;

-(void)bindData:(NSString*)nsstrHotWod;

@end

@implementation ZY_HistoricalRecordCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
      [self addSubview:self.imageIconView];
      [self addSubview:self.labelTitle];
      [self addSubview:self.lineView];
    }
    
    return self;
}

#pragma mark - Getter or Setter 

-(UIImageView *)imageIconView{
    if (!_imageIconView) {
         _imageIconView = [[UIImageView alloc] initWithFrame:CGRectMake(17,(40-13)/2, 13, 13)];
        [_imageIconView setImage:[UIImage imageNamed:@"历史-2"]]; 
    }
    
    return _imageIconView;
}

-(UILabel *)labelTitle{
    if (!_labelTitle) {
         _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.imageIconView.frame)+7, CGRectGetMinY(self.imageIconView.frame), kCScreenWidth-20, 13)];
        [_labelTitle setFont:[UIFont systemFontOfSize:13]];
        [_labelTitle setTextColor:kRGBCOLOR(27, 27, 27)];
    }
    
    return _labelTitle;
}

-(UIView *)lineView{
    if (!_lineView) {
         _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, kCScreenWidth, 0.5)];
        [_lineView setBackgroundColor:kRGBCOLOR(225, 225, 225)];
    }
    
    return _lineView;
}

-(void)bindData:(NSString *)nsstrHotWod{
    [self.labelTitle setText:nsstrHotWod];
}

@end

@interface ZY_HistoricalRecordView()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *mainTableView;

@property(nonatomic,strong) UIView *headerView;

@property(nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation ZY_HistoricalRecordView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
      [self addSubview:self.mainTableView];
       _dataArray = [[ZY_DBHelper shareDBHelper] queryHotWordList];
    }
        
    return self;
}

#pragma mark - PrivateFuntion

-(void)deleteAllHotWords{
    [[ZY_DBHelper shareDBHelper] removeHotWords];
     self.dataArray = [[ZY_DBHelper shareDBHelper] queryHotWordList];
    [self.mainTableView reloadData];
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:nsstrHistoricalRecordCellIdentifier forIndexPath:indexPath];
    tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *data = [self.dataArray objectAtIndex:indexPath.row];
    [(ZY_HistoricalRecordCell*)tableViewCell bindData:data];
    return tableViewCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSString *data = [self.dataArray objectAtIndex:indexPath.row];
    if (![ZY_Common isBlankString:data]&&[self.delegate respondsToSelector:@selector(clickHistoricalWord:)]) {
        [self.delegate clickHistoricalWord:data];
    }
}

#pragma mark - Getter or Setter 

-(UIView *)headerView{
    if (!_headerView) {
         _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, 40)];
        
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 4, 80, 18)];
        [labelTitle setText:@"搜索历史"];
        [labelTitle setFont:[UIFont systemFontOfSize:15]];
        [labelTitle setTextColor:kRGBCOLOR(27, 27, 27)];
        [_headerView addSubview:labelTitle];
        
        UIButton *btnClearHotWrod = [[UIButton alloc] initWithFrame:CGRectMake(kCScreenWidth-65,4, 60, 18)];
        [btnClearHotWrod setTitle:@"清空" forState:UIControlStateNormal];
        [btnClearHotWrod.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [btnClearHotWrod setTitleColor:kRGBCOLOR(147, 147, 147) forState:UIControlStateNormal];
        [btnClearHotWrod setImage:[UIImage imageNamed:@"清理缓存"] forState:UIControlStateNormal];
        [btnClearHotWrod setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 10)];
        [btnClearHotWrod addTarget:self action:@selector(deleteAllHotWords) forControlEvents:UIControlEventTouchUpInside];
        [_headerView addSubview:btnClearHotWrod];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, kCScreenWidth, 0.5)];
        [lineView setBackgroundColor:kRGBCOLOR(225, 225, 225)];
        [_headerView addSubview:lineView];
    }
    
    return _headerView;
}

-(UITableView *)mainTableView{
    if (!_mainTableView) {
         _mainTableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView registerClass:[ZY_HistoricalRecordCell class] forCellReuseIdentifier:nsstrHistoricalRecordCellIdentifier];
         _mainTableView.separatorStyle = NO;
        [_mainTableView setTableHeaderView:self.headerView];
    }
    return _mainTableView;
}

-(void)reloadData{
    _dataArray = [[ZY_DBHelper shareDBHelper] queryHotWordList];
    [self.mainTableView reloadData];
}

@end

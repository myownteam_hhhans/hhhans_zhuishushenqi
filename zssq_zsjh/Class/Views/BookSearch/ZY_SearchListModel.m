//
//  ZY_SearchListModel.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/15.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "ZY_SearchListModel.h"

@implementation ZY_SearchListModel

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    ZY_SearchListModel *model = [[ZY_SearchListModel alloc] init];
    
    model.listID = dict[@"_id"];
    model.listName = dict[@"title"];
    model.listDes = dict[@"desc"];
    model.bookCount = dict[@"bookCount"];
    model.collectionCount = dict[@"collectorCount"];
    model.coverURLArray = dict[@"covers"];
    
    return model;
}

@end

//
//  ZY_SearchSelectView.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/1/30.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "ZY_SearchSelectView.h"
#import "Common.h"
#import "ZY_Common.h"
#import "ZY_SearchSelectCell.h"
#import "ZY_SearchSelectFloyLayout.h"

static NSString *const headerId = @"headerId";
static NSString *const cellId = @"cellId";

@interface ZY_SearchSelectView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *mainCollection;

@property (nonatomic, strong) NSArray *collectionTitleNameArray;
@property (nonatomic, strong) NSMutableArray *collectionTitleButtonArray;

@property (nonatomic, strong) UIView *sureBackView;
@property (nonatomic, strong) UIButton *sureButton;



@end

@implementation ZY_SearchSelectView



- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.mainCollection];
        [self addSubview:self.sureBackView];
    }
    return self;
}

#pragma mark - privateFunc
- (void)loadCollectionViewWithArray:(NSMutableArray *)arr {
    
    NSMutableArray *array1 = self.selectedItemArray[0];
    NSMutableArray *array2 = self.selectedItemArray[1];
    [array1 removeAllObjects];
    [array2 removeAllObjects];
    
    self.categoryArray = arr;
    [self.mainCollection reloadData];
}
- (void)sureButtonClick {
    if ([self.delegate respondsToSelector:@selector(searchSelectSure)]) {
        [self.delegate searchSelectSure];
    }
}

#pragma mark - Collection DataSource & Delegate & Layout
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.categoryArray && self.categoryArray.count >= 0) {
        return self.categoryArray.count;
    }
    return 0;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([self.categoryArray[section] isKindOfClass: [NSArray class]] || [self.categoryArray[section] isKindOfClass: [NSMutableArray class]]) {
        return ((NSArray *)self.categoryArray[section]).count;
    }
    return 0;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *headerView = [_mainCollection dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:headerId forIndexPath:indexPath];
        if(headerView == nil)
        {
            headerView = [[UICollectionReusableView alloc] init];
        }
        for (UIView *view in headerView.subviews) {
            [view removeFromSuperview];
        }
        headerView.backgroundColor = [UIColor whiteColor];
        
        if (indexPath.section != 0) {
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, [Common get414RadiusByMap:10], k_ScreenWidth, 0.7)];
            lineView.backgroundColor = [Common colorWithHexString:@"#EBEBF0" alpha:1.0];
            [headerView addSubview:lineView];
        }

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(13, 35, 50, 16)]];
        [titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setTextColor:[Common colorWithHexString:@"#616166" alpha:1.0]];
        [titleLabel setText:(NSString *)self.collectionTitleNameArray[indexPath.section]];
        [headerView addSubview:titleLabel];

        return headerView;
    }
    
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZY_SearchSelectCell *cell = [_mainCollection dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    [cell.button setTitle:self.categoryArray[indexPath.section][indexPath.row] forState:UIControlStateNormal];
    cell.button.tag = indexPath.section;
    cell.button.selected = NO;
    [cell.button addTarget:self action:@selector(cellButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)cellButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    if (sender.selected) {
        [((NSMutableArray *)self.selectedItemArray[sender.tag]) addObject:sender];
    } else {
        [((NSMutableArray *)self.selectedItemArray[sender.tag]) removeObject:sender];
    }
    
    NSInteger maxSeletedItem = 3;
    
    for (ZY_SearchSelectCell *cell in self.mainCollection.visibleCells) {
        if (cell.button.tag != sender.tag) {
            continue;
        }
        if (cell.button.selected) {
            continue;
        }
        cell.button.enabled = ((NSMutableArray *)self.selectedItemArray[sender.tag]).count != maxSeletedItem;
    }


   
    
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return [Common get414SizeByMap:76 withHeight:28];
//    } else if (indexPath.section == 1) {
//        return [Common get414SizeByMap:45 withHeight:28];
//    } else {
//        return [Common get414SizeByMap:106 withHeight:28];
//    }
//}

#pragma mark - lazyload
- (UICollectionView *)mainCollection {
    if (!_mainCollection) {
        ZY_SearchSelectFloyLayout *layout = [[ZY_SearchSelectFloyLayout alloc] init];
        //layout.itemSize = [Common get414SizeByMap:76 withHeight:28];
        layout.estimatedItemSize = [Common get414SizeByMap:76 withHeight:28];
        layout.minimumInteritemSpacing = 10;
        
        layout.minimumLineSpacing = 10;
        layout.headerReferenceSize = CGSizeMake(k_ScreenWidth, [Common get414RadiusByMap:74]);
        layout.sectionInset = UIEdgeInsetsMake(0, 20, 0, 20);
        _mainCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, self.frame.size.height - self.sureBackView.frame.size.height) collectionViewLayout:layout];
        _mainCollection.dataSource = self;
        _mainCollection.delegate = self;
        _mainCollection.backgroundColor = [UIColor whiteColor];
        [_mainCollection registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerId];
        [_mainCollection registerClass:[ZY_SearchSelectCell class] forCellWithReuseIdentifier:cellId];
    }
    return _mainCollection;
}
- (UIView *)sureBackView {
    if (!_sureBackView) {
        
        CGFloat sureBackHeight = 60;
        if (isIPhoneX) {
            sureBackHeight += 34;
        }
        _sureBackView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - sureBackHeight, k_ScreenWidth, sureBackHeight)];
        [_sureBackView addSubview:self.sureButton];
        [_sureBackView addSubview:({
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, 0.7)];
            [lineView setBackgroundColor:[Common colorWithHexString:@"#EBEBF0" alpha:1.0]];
            lineView;
        })];
    }
    return _sureBackView;
}
- (UIButton *)sureButton {
    if (!_sureButton) {
        _sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureButton setFrame:CGRectMake(22, 10, k_ScreenWidth - 44, 40)];
        [_sureButton setBackgroundColor:[Common colorWithHexString:@"#1B88EE" alpha:1.0]];
        [_sureButton setTitle:@"确认" forState:UIControlStateNormal];
        [_sureButton.titleLabel setFont:[UIFont systemFontOfSize:18]];
        [_sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_sureButton.layer setCornerRadius:20];
        [_sureButton addTarget:self action:@selector(sureButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sureButton;
}

- (NSArray *)collectionTitleNameArray {
    if (!_collectionTitleNameArray) {
        _collectionTitleNameArray = @[@"分类",@"字数"];
    }
    return _collectionTitleNameArray;
}

- (NSArray *)selectedItemArray {
    if (!_selectedItemArray) {
        _selectedItemArray = @[[NSMutableArray array],[NSMutableArray array]];
    }
    return _selectedItemArray;
}

@end

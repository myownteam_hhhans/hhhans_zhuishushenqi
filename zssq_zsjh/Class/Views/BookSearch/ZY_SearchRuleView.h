//
//  ZY_SearchRuleView.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/1/29.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SearchRuleDelegate <NSObject>

- (void)selectedSearchRule:(UIButton *)sender;

@end

@interface ZY_SearchRuleView : UIView

@property (nonatomic, weak) id<SearchRuleDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

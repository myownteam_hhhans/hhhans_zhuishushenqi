//
//  ZY_SearchSelectCell.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/14.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "ZY_SearchSelectCell.h"
#import "Common.h"

@implementation ZY_SearchSelectCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.button setFrame:self.bounds];
        [self.button setTitleColor:[Common colorWithHexString:@"#858E98" alpha:1.0] forState:UIControlStateNormal];
        [self.button setTitleColor:[Common colorWithHexString:@"#1B88EE" alpha:1.0] forState:UIControlStateSelected];
        [self.button setTitleColor:[Common colorWithHexString:@"#C1CDDB" alpha:1.0] forState:UIControlStateDisabled];
        [self.button setBackgroundImage:[Common imageWithColor:[Common colorWithHexString:@"#F0F2F5" alpha:1.0] WithFrame:CGRectMake(0, 0, 1, 1)] forState:UIControlStateNormal];
        [self.button setBackgroundImage:[Common imageWithColor:[Common colorWithHexString:@"#B6D9FA" alpha:1.0] WithFrame:CGRectMake(0, 0, 1, 1)] forState:UIControlStateSelected];
        [self.button setBackgroundImage:[Common imageWithColor:[Common colorWithHexString:@"#F0F2F5" alpha:1.0] WithFrame:CGRectMake(0, 0, 1, 1)] forState:UIControlStateDisabled];
        [self.button.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:15]]];
        [self addSubview:self.button];
        self.button.layer.cornerRadius = self.button.frame.size.height/2;
        self.button.layer.masksToBounds = YES;
    }
    return self;
}

// 自适应宽度
- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    // 获得每个cell的属性集
    UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
    // 计算cell里面textfield的宽度
    CGRect frame = [self.button.titleLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, [Common get414RadiusByMap:28]) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:[NSDictionary dictionaryWithObjectsAndKeys:self.button.titleLabel.font,NSFontAttributeName, nil] context:nil];
    
    // 这里在本身宽度的基础上 又增加了10
    frame.size.width += 15;
    frame.size.height = [Common get414RadiusByMap:28];
    
    self.button.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    
    // 重新赋值给属性集
    attributes.frame = frame;
    
    return attributes;
}

@end

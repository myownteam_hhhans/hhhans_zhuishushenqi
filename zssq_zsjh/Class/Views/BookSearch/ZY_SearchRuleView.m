//
//  ZY_SearchRuleView.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/1/29.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "ZY_SearchRuleView.h"
#import "Common.h"

@interface ZY_SearchRuleView ()

@property (nonatomic, strong) NSMutableArray *buttonArray;

@property (nonatomic, strong) UIImageView *rightImage;

@end

@implementation ZY_SearchRuleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        NSArray *buttonNameArray = @[@"按综合",@"按人气",@"按留存",@"按评分",@"按字数"];
        for (int i=0; i<buttonNameArray.count; i++) {
            // 加一个按钮
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
            [button setTitleColor:[Common colorWithHexString:@"#1B88EE" alpha:1.0] forState:UIControlStateSelected];
            [button setTitleColor:[Common colorWithHexString:@"#B7B7BD" alpha:1.0] forState:UIControlStateNormal];
            [button setTitle:buttonNameArray[i] forState:UIControlStateNormal];
            [button setFrame:[Common get414FrameByMap:CGRectMake(0, 55*i, 414, 55)]];
            [button addTarget:self action:@selector(ruleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
            button.tag = i;
            if (i == 0) {
                [button setSelected:YES];
            }
            [self addSubview:button];
            [self.buttonArray addObject:button];
            // 加一条线，第一条格外长
            CGFloat x = 15, y = 55 * (i+1), width = 380, height = 1;
            if (i == 5) {
                x = 0;
                width = 414;
                y -= 2;
            }
            UIView *view = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(x, y, width, height)]];
            [view setBackgroundColor:[Common colorWithHexString:@"#DDDDDD" alpha:1.0]];
            [self addSubview:view];
        }
        [self addSubview:self.rightImage];
    }
    return self;
}

- (void)ruleButtonClick:(UIButton *)sender {
    [self.rightImage setCenter:CGPointMake(self.rightImage.center.x, sender.center.y)];
    for (UIButton *button in self.buttonArray) {
        [button setSelected:NO];
    }
    sender.selected = YES;
    if ([self.delegate respondsToSelector:@selector(selectedSearchRule:)]) {
        [self.delegate selectedSearchRule:sender];
    }
}

- (UIImageView *)rightImage {
    if (!_rightImage) {
        _rightImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(379, 20, 15, 12)]];
        [_rightImage setImage:[UIImage imageNamed:@"searchRight"]];
    }
    return _rightImage;
}
- (NSMutableArray *)buttonArray {
    if (!_buttonArray) {
        _buttonArray = [NSMutableArray array];
    }
    return _buttonArray;
}

@end

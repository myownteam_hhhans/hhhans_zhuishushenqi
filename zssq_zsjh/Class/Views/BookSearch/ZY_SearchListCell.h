//
//  ZY_SearchListCell.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/15.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_SearchListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZY_SearchListCell : UITableViewCell

- (void)bindDataWithModel:(ZY_SearchListModel *)model;

@end

NS_ASSUME_NONNULL_END

//
//  M_UserFavourateButton.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/13.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface M_UserFavourateButton : UIButton

@property (nonatomic, assign) NSInteger style; // 0男 1女 2出版

@end

//
//  ZY_FounctionTableViewCell.h
//  ZYNovel
//
//

#import <UIKit/UIKit.h>

@interface ZY_FounctionTableViewCell : UITableViewCell


-(void)bindData:(NSString*)imageName withfunctionName:(NSString*)functionName;

@end

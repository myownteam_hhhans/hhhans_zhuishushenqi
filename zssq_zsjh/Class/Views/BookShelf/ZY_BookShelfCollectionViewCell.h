//
//  BookShelfCollectionViewCell.h
//  ZYNovel
//
//  Created by S on 2017/3/15.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@protocol ZY_BookShelfCollectionViewCellDelegate <NSObject>

@optional

-(void)deleteCollectionViewCell:(NSIndexPath*)indexPath;

@end

@interface ZY_BookShelfCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<ZY_BookShelfCollectionViewCellDelegate> delegate;
@property(nonatomic,strong) UIImageView *bookShelfImageView;

/************************************
 函数描述:绑定数据
 作者:S
 时间:2017-01-05
 ************************************/
- (void)bindData:(ZY_NovelInfo*)novelInfo withIsEditor:(BOOL)isEditor withIndexPath:(NSIndexPath*)indexPath withIsUpdate:(BOOL)isUpdate;
 
@end

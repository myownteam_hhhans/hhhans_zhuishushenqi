//
//  ZY_BookShelfListAdCollectionViewCell.m
//  ZYNovel
//
//

#import "ZY_BookShelfListAdCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "ZY_Common.h"

@interface ZY_BookShelfListAdCollectionViewCell()

@property(nonatomic,strong) UIImageView *bookAdImageView;

@property(nonatomic,strong) UILabel *labelAdName;

@property(nonatomic,strong) UILabel *labelAdSummaryInfo;

@property(nonatomic,strong) UILabel *labelTuiGuangIcon;

@property(nonatomic,strong) UIView *lineView;

@property(nonatomic,strong) UILabel *labelRecommend;

@end


@implementation ZY_BookShelfListAdCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self.contentView addSubview:self.bookAdImageView];
        [self.contentView addSubview:self.labelAdName];
        [self.contentView addSubview:self.labelTuiGuangIcon];
        [self.contentView addSubview:self.labelAdSummaryInfo];
        [self.contentView addSubview:self.labelRecommend];
        [self.contentView addSubview:self.lineView];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.bookAdImageView setImage:[UIImage imageNamed:@"精选小说-List"]];
}

#pragma mark - Getter or Setter

-(UIImageView *)bookAdImageView{
    if (!_bookAdImageView) {
         _bookAdImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 13, 45, 56)];
        [_bookAdImageView setBackgroundColor:kRGBCOLOR(236, 236, 236)];
         _bookAdImageView.contentMode = UIViewContentModeCenter;
    }
    
    return _bookAdImageView;
}

-(UILabel *)labelAdName{
    if (!_labelAdName) {
         _labelAdName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bookAdImageView.frame)+7, 13, 120, 16)];
        [_labelAdName setText:@"黑暗王者"];
        [_labelAdName setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelAdName setFont:[UIFont systemFontOfSize:16]];
         _labelAdName.lineBreakMode = NSLineBreakByClipping;
        [_labelAdName setTextAlignment:NSTextAlignmentLeft];
    }
    
    return _labelAdName;
}

- (UILabel *)labelAdSummaryInfo{
    if (!_labelAdSummaryInfo) {
        _labelAdSummaryInfo = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.labelAdName.frame), CGRectGetMaxY(self.labelAdName.frame)+8, kCScreenWidth-80, 13)];
        [_labelAdSummaryInfo setText:@"更新至:第2500章 来了一堆胖子"];
        [_labelAdSummaryInfo setTextColor:kRGBCOLOR(102, 102, 102)];
        [_labelAdSummaryInfo setFont:[UIFont systemFontOfSize:13]];
        _labelAdSummaryInfo.lineBreakMode = NSLineBreakByClipping;
        [_labelAdSummaryInfo setTextAlignment:NSTextAlignmentLeft];
    }
    
    return _labelAdSummaryInfo;
}

-(UILabel *)labelTuiGuangIcon{
    if (!_labelTuiGuangIcon) {
        _labelTuiGuangIcon = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.labelAdName.frame)+7, CGRectGetMinY(self.labelAdName.frame)+1, 27, 14)];
        [_labelTuiGuangIcon setText:@"推广"];
        [_labelTuiGuangIcon setTextColor:kRGBCOLOR(210,210,210)];
        [_labelTuiGuangIcon setFont:[UIFont systemFontOfSize:10]];
        _labelTuiGuangIcon.lineBreakMode = NSLineBreakByClipping;
        [_labelTuiGuangIcon setTextAlignment:NSTextAlignmentCenter];
        [_labelTuiGuangIcon setBackgroundColor:[UIColor clearColor]];
        _labelTuiGuangIcon.clipsToBounds = YES;
        _labelTuiGuangIcon.layer.borderWidth = 1;
        _labelTuiGuangIcon.layer.borderColor = kRGBCOLOR(212,212,212).CGColor;
    }
    
    return _labelTuiGuangIcon;
}

-(UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.bookAdImageView.frame),kBookShelfListHeight-0.5,kCScreenWidth-CGRectGetMinX(self.bookAdImageView.frame), 0.5)];
        [_lineView setBackgroundColor:kRGBCOLOR(209, 209, 209)];
    }
    
    return _lineView;
}

-(UILabel *)labelRecommend{
    if (!_labelRecommend) {
         _labelRecommend = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.labelAdName.frame), CGRectGetMaxY(self.labelAdSummaryInfo.frame)+6, 100, 13)];
        [_labelRecommend setText:@"小编推荐"];
        [_labelRecommend setNumberOfLines:1];
        [_labelRecommend setTextColor:kRGBCOLOR(102,102,102)];
        [_labelRecommend setFont:[UIFont systemFontOfSize:13]];
        [_labelRecommend setTextAlignment:NSTextAlignmentLeft];
    }
    
    return _labelRecommend;
}

- (void)bindData:(ADModel*)adEntityInfo{
//    NSString *imageUrl = [[ZY_ADHelper shareADHelper] ADImageUrl:adEntityInfo type:adType_BookShelf];
    [self.bookAdImageView sd_setImageWithURL:[NSURL URLWithString:adEntityInfo.adImgPath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            self.bookAdImageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.bookAdImageView setClipsToBounds:YES];
        }else{
            [self.bookAdImageView setImage:[UIImage imageNamed:@"精选小说-List"]];
            self.bookAdImageView.contentMode = UIViewContentModeCenter;
        }
    }];
    
    if ([adEntityInfo.data isKindOfClass:[ZY_AdEntity class]]) {
        [[ZY_Common shareCommon] advertisingDisplayReportWithModel:adEntityInfo.data];
    }
    
    CGFloat labelBookNameWidth =kCScreenWidth-100;
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bookAdImageView.frame)+7, 13, 120, 16)];
    [tempLabel setTextColor:kRGBCOLOR(51, 51, 51)];
    [tempLabel setFont:[UIFont systemFontOfSize:16]];
    
    [tempLabel setText:adEntityInfo.name];
    [tempLabel sizeToFit];
    
    if (tempLabel.frame.size.width<labelBookNameWidth) {
        labelBookNameWidth=tempLabel.frame.size.width;
    }
    
    [self.labelAdName setFrame:CGRectMake(self.labelAdName.frame.origin.x, self.labelAdName.frame.origin.y, labelBookNameWidth, self.labelAdName.frame.size.height)];
    [self.labelTuiGuangIcon setFrame:CGRectMake(CGRectGetMaxX(self.labelAdName.frame)+7, CGRectGetMinY(self.labelAdName.frame)+1, 27, 14)];
    [self.labelAdName setText:adEntityInfo.name];
    [self.labelAdSummaryInfo setText:adEntityInfo.appSummary];
}

@end

//
//  ZY_BookShelfCollectionListCell.m
//  ZYNovel
//
//

#import "ZY_BookShelfCollectionListCell.h"
#import "UIImageView+WebCache.h"
#import "ZY_Common.h"
#import "LSYReadModel.h"

@interface ZY_BookShelfCollectionListCell()

@property(nonatomic,strong) UIImageView *bookShelfImageView;

@property(nonatomic,strong) UILabel *labelBookName;

@property(nonatomic,strong) UILabel *labelUpdateInfo;

@property(nonatomic,strong) UILabel *labelUpdateIcon;

@property(nonatomic,strong) UILabel *labelRecord;

@property(nonatomic,strong) UILabel *labelDelete;

@property(nonatomic,strong) UIImageView *imageViewDelete;

@property(nonatomic,strong) UIView *verticalLineView;

@property(nonatomic,strong) UIView *lineView;

@property(nonatomic,strong) ZY_NovelInfo *currentNovelInfo;

@property(nonatomic,strong) NSIndexPath *indexPath;

@end

@implementation ZY_BookShelfCollectionListCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self.contentView addSubview:self.bookShelfImageView];
        [self.contentView addSubview:self.labelBookName];
        [self.contentView addSubview:self.labelUpdateIcon];
        [self.contentView addSubview:self.labelUpdateInfo];
        [self.contentView addSubview:self.labelRecord];
        [self.contentView addSubview:self.imageViewDelete];
        [self.contentView addSubview:self.labelDelete];
        [self.contentView addSubview:self.verticalLineView];
        [self.contentView addSubview:self.lineView];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.bookShelfImageView setImage:[UIImage imageNamed:@"精选小说-List"]];
    [self.labelUpdateIcon setHidden:YES];
}

#pragma mark - Getter or Setter

-(UIImageView *)bookShelfImageView{
    if (!_bookShelfImageView) {
         _bookShelfImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 13, 45, 56)];
        [_bookShelfImageView setBackgroundColor:kRGBCOLOR(236, 236, 236)];
         _bookShelfImageView.contentMode = UIViewContentModeCenter;
    }
    
    return _bookShelfImageView;
}

-(UILabel *)labelBookName{
    if (!_labelBookName) {
         _labelBookName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bookShelfImageView.frame)+7, 13, 120, 16)];
        [_labelBookName setText:@"黑暗王者"];
        [_labelBookName setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelBookName setFont:[UIFont systemFontOfSize:16]];
         _labelBookName.lineBreakMode = NSLineBreakByClipping;
        [_labelBookName setTextAlignment:NSTextAlignmentLeft]; 
    }
    
    return _labelBookName;
}

- (UILabel *)labelUpdateInfo{
    if (!_labelUpdateInfo) {
         _labelUpdateInfo = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.labelBookName.frame), CGRectGetMaxY(self.labelBookName.frame)+8, kCScreenWidth-80, 13)];
        [_labelUpdateInfo setText:@"更新至:第2500章 来了一堆胖子"];
        [_labelUpdateInfo setTextColor:kRGBCOLOR(102, 102, 102)];
        [_labelUpdateInfo setFont:[UIFont systemFontOfSize:13]];
         _labelUpdateInfo.lineBreakMode = NSLineBreakByClipping;
        [_labelUpdateInfo setTextAlignment:NSTextAlignmentLeft];
    }
    
    return _labelUpdateInfo;
}

-(UILabel *)labelUpdateIcon{
    if (!_labelUpdateIcon) {
         _labelUpdateIcon = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.labelBookName.frame)+7, CGRectGetMinY(self.labelBookName.frame)+1, 27, 14)];
        [_labelUpdateIcon setText:@"更新"];
        [_labelUpdateIcon setTextColor:[UIColor whiteColor]];
        [_labelUpdateIcon setFont:[UIFont systemFontOfSize:10]];
         _labelUpdateIcon.lineBreakMode = NSLineBreakByClipping;
        [_labelUpdateIcon setTextAlignment:NSTextAlignmentCenter];
        [_labelUpdateIcon setBackgroundColor:kRGBCOLOR(190, 28, 28)];
        [_labelUpdateIcon setHidden:YES];
    }
    
    return _labelUpdateIcon;
}

-(UILabel *)labelRecord{
    if (!_labelRecord) {
         _labelRecord = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.labelBookName.frame), CGRectGetMaxY(self.labelUpdateInfo.frame)+6, 100, 13)];
        [_labelRecord setText:@"阅读至第25章"];
        [_labelRecord setNumberOfLines:1];
        [_labelRecord setTextColor:kRGBCOLOR(102,102,102)];
        [_labelRecord setFont:[UIFont systemFontOfSize:13]];
        [_labelRecord setTextAlignment:NSTextAlignmentLeft];
    }
    
    return _labelRecord;
}

-(UIImageView *)imageViewDelete{
    if (!_imageViewDelete) {
        //82= Cell 高度
         _imageViewDelete = [[UIImageView alloc] initWithFrame:CGRectMake(kCScreenWidth-19*2, (kBookShelfListHeight-19)/2-6, 21, 19)];
        [_imageViewDelete setBackgroundColor:[UIColor clearColor]];
        [_imageViewDelete setImage:[UIImage imageNamed:@"删除"]];
        [_imageViewDelete setHidden:YES];
    }
    
    return _imageViewDelete;
}

-(UILabel *)labelDelete{
    if (!_labelDelete) {
         _labelDelete = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.imageViewDelete.frame), CGRectGetMaxY(self.imageViewDelete.frame)+4, 30, 12)];
        [_labelDelete setText:@"删除"];
        [_labelDelete setTextColor:kRGBCOLOR(195, 73, 64)];
        [_labelDelete setFont:[UIFont systemFontOfSize:10]];
        [_labelDelete setTextAlignment:NSTextAlignmentLeft];
        [_labelDelete setHidden:YES];
    }
    
    return _labelDelete;
}

-(UIView *)verticalLineView{
    if (!_verticalLineView) {
         _verticalLineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.imageViewDelete.frame)-19, (kBookShelfListHeight-20)/2, 1, 20)];
        [_verticalLineView setBackgroundColor:kRGBCOLOR(209, 209, 209)];
        [_verticalLineView setHidden:YES];
    }
    
    return _verticalLineView;
}

-(UIView *)lineView{
    if (!_lineView) {
         _lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.bookShelfImageView.frame),kBookShelfListHeight-0.5,kCScreenWidth-CGRectGetMinX(self.bookShelfImageView.frame), 0.5)];
        [_lineView setBackgroundColor:kRGBCOLOR(209, 209, 209)];
    }
    
    return _lineView;
}

-(void)settingDeleteViewIsHidden:(BOOL)isHidden{
    [self.imageViewDelete setHidden:isHidden];
    [self.labelDelete setHidden:isHidden];
    [self.verticalLineView setHidden:isHidden];
}

- (void)bindData:(ZY_NovelInfo*)novelInfo withIsEditor:(BOOL)isEditor withIndexPath:(NSIndexPath*)indexPath withIsUpdate:(BOOL)isUpdate{
    self.currentNovelInfo=novelInfo;
    self.indexPath=indexPath;
    [self settingDeleteViewIsHidden:!isEditor];
    [self.bookShelfImageView sd_setImageWithURL:[NSURL URLWithString:novelInfo.novelCoverUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
             self.bookShelfImageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.bookShelfImageView setClipsToBounds:YES];
        }else{
            [self.bookShelfImageView setImage:[UIImage imageNamed:@"精选小说-List"]];
            self.bookShelfImageView.contentMode = UIViewContentModeCenter;
        }
    }];
     
    
    CGFloat labelBookNameWidth =200;
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.bookShelfImageView.frame)+7, 13, 120, 16)];
    [tempLabel setTextColor:kRGBCOLOR(51, 51, 51)];
    [tempLabel setFont:[UIFont systemFontOfSize:16]];
    
    [tempLabel setText:novelInfo.novelName];
    [tempLabel sizeToFit];
    
    if (tempLabel.frame.size.width<labelBookNameWidth) {
        labelBookNameWidth=tempLabel.frame.size.width;
    }
    
    [self.labelBookName setFrame:CGRectMake(self.labelBookName.frame.origin.x, self.labelBookName.frame.origin.y, labelBookNameWidth, self.labelBookName.frame.size.height)];
    [self.labelUpdateIcon setFrame:CGRectMake(CGRectGetMaxX(self.labelBookName.frame)+7, CGRectGetMinY(self.labelBookName.frame)+1, 27, 14)];
    [self.labelBookName setText:novelInfo.novelName];
    [self.labelUpdateInfo setText:novelInfo.novelLatestChapter];
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString *nsstrRecordText = [LSYReadModel getReadInfo:novelInfo];
       // dispatch_async(dispatch_get_main_queue(), ^{
    if (nsstrRecordText) {
        [self.labelRecord setText:nsstrRecordText];

    }
    
//        });
//    });
    
    if (isUpdate) {
        [self.labelUpdateIcon setHidden:NO];
    }
}

@end

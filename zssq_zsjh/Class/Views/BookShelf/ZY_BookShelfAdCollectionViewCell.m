//
//  ZY_BookShelfAdCollectionViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/24.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_BookShelfAdCollectionViewCell.h"

#import "UIImageView+WebCache.h"
#import "ZY_Common.h"

@interface ZY_BookShelfAdCollectionViewCell()<UIAlertViewDelegate>

@property(nonatomic,strong) UIImageView *bookAdImageView;

@property(nonatomic,strong) UILabel *labelAdBookName;

@property(nonatomic,strong) UIView *bgView;

@property(nonatomic,strong) UILabel *labelRecord;

@property (nonatomic,strong) UIImageView *adFlag;

@end

@implementation ZY_BookShelfAdCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.bookAdImageView];
        [self.contentView addSubview:self.labelAdBookName];
        [self.contentView addSubview:self.labelRecord];
        [self.contentView addSubview:self.adFlag];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.bookAdImageView setImage:[UIImage imageNamed:@"暂无封面"]]; 
}


#pragma mark - Getter or Setter 

-(UILabel *)labelRecord{
    if (!_labelRecord) {
        _labelRecord = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelRecord setText:@"小编推荐"];
        [_labelRecord setNumberOfLines:1];
        [_labelRecord setTextColor:kRGBCOLOR(153,153,153)];
        [_labelRecord setFont:[UIFont systemFontOfSize:11]];
        [_labelRecord setTextAlignment:NSTextAlignmentCenter];
    }
    return _labelRecord;
}

-(UIImageView *)adFlag{
    if (!_adFlag) {
        _adFlag = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.contentView.frame)-17, 0, 17, 10)];
        _adFlag.image = [UIImage imageNamed:@"SF广告"];
        _adFlag.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    }
    return _adFlag;
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectZero];
        _bgView.layer.shadowColor = [UIColor blackColor].CGColor;
        _bgView.layer.shadowOffset = CGSizeMake(0, 0);
        _bgView.layer.shadowOpacity = 0.5;
        _bgView.layer.shadowRadius = 2.0;
    }
    return _bgView;
}

-(UIImageView *)bookAdImageView{
    if (!_bookAdImageView) {
        _bookAdImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_bookAdImageView setBackgroundColor:kRGBCOLOR(239, 239, 239)];
        [_bookAdImageView setImage:[UIImage imageNamed:@"暂无封面"]];
         _bookAdImageView.contentMode = UIViewContentModeCenter;
         _bookAdImageView.layer.shadowColor = [UIColor blackColor].CGColor;
         _bookAdImageView.layer.shadowOffset = CGSizeMake(0, 0);
         _bookAdImageView.layer.shadowOpacity = 0.5;
         _bookAdImageView.layer.shadowRadius = 4.0;
    }
    return _bookAdImageView;
}

-(UILabel *)labelAdBookName{
    if (!_labelAdBookName) {
         _labelAdBookName = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelAdBookName setText:@" "];
        [_labelAdBookName setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelAdBookName setFont:[UIFont systemFontOfSize:14]];
         _labelAdBookName.lineBreakMode = NSLineBreakByClipping;
        [_labelAdBookName setTextAlignment:NSTextAlignmentCenter];
    }
    return _labelAdBookName;
}

- (void)bindData:(ADModel*)adEntityInfo{
//    NSString *imageUrl = [[ZY_ADHelper shareADHelper] ADImageUrl:adEntityInfo type:adType_BookShelf];
    [self.bookAdImageView sd_setImageWithURL:[NSURL URLWithString:adEntityInfo.adImgPath] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            self.bookAdImageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.bookAdImageView setClipsToBounds:YES];
        }else{
            [self.bookAdImageView setImage:[UIImage imageNamed:@"暂无封面"]];
            self.bookAdImageView.contentMode = UIViewContentModeCenter;
        }
    }];
    
    if ([adEntityInfo.data isKindOfClass:[ZY_AdEntity class]]) {
        [[ZY_Common shareCommon] advertisingDisplayReportWithModel:adEntityInfo.data];
    }
    
    CGFloat height =CGRectGetHeight(self.frame)>125?125:CGRectGetHeight(self.frame)*(125/100);
    
    CGFloat x=(CGRectGetWidth(self.frame)-100)/2;
    [self.bgView setFrame:CGRectMake(x,2, 100, height)];
    [self.bookAdImageView setFrame:CGRectMake(0,0, 100, height)];
    [self.labelAdBookName setFrame:CGRectMake(x, CGRectGetMaxY(self.bookAdImageView.frame)+9, CGRectGetWidth(self.frame)-x*2, 20)];
    [self.labelAdBookName setText:adEntityInfo.name];
    [self.labelRecord setFrame:CGRectMake(x,CGRectGetHeight(self.frame)-20, CGRectGetWidth(self.frame)-x*2, 20)];
    self.adFlag.frame = CGRectMake(CGRectGetMaxX(self.contentView.frame)-17 - x, 2, 17, 10);
}

@end

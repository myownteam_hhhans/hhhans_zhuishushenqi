//
//  ZY_AddNovelCollectionViewCell.m
//  ZYNovel
//
//

#import "ZY_AddNovelCollectionViewCell.h"
#import "ZY_Common.h"

@interface ZY_AddNovelCollectionViewCell()

@property(nonatomic,strong) UIImageView *bookShelfImageView;

@property(nonatomic,strong) UILabel *labelBookName;

@property(nonatomic,strong) UILabel *labelRecord;

@end

@implementation ZY_AddNovelCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.contentView addSubview:self.bookShelfImageView];
        [self.contentView addSubview:self.labelBookName];
        [self.contentView addSubview:self.labelRecord];
    }
    
    return self;
}

-(UIImageView *)bookShelfImageView{
    if (!_bookShelfImageView) {
         _bookShelfImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_bookShelfImageView setBackgroundColor:RGB(250, 250, 250)];
        [_bookShelfImageView setImage:[UIImage imageNamed:@"暂无封面"]];
         _bookShelfImageView.layer.borderWidth = 0.5;
         _bookShelfImageView.layer.borderColor = [kRGBCOLOR(214, 214, 214) CGColor];
    }
    
    return _bookShelfImageView;
}

-(UILabel *)labelBookName{
    if (!_labelBookName) {
        _labelBookName = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelBookName setText:@" "];
        [_labelBookName setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelBookName setFont:[UIFont systemFontOfSize:14]];
        _labelBookName.lineBreakMode = NSLineBreakByClipping;
        [_labelBookName setTextAlignment:NSTextAlignmentCenter];
    }
    
    return _labelBookName;
}

-(UILabel *)labelRecord{
    if (!_labelRecord) {
        _labelRecord = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelRecord setText:@" "];
        [_labelRecord setNumberOfLines:1];
        [_labelRecord setTextColor:kRGBCOLOR(153,153,153)];
        [_labelRecord setFont:[UIFont systemFontOfSize:11]];
        [_labelRecord setTextAlignment:NSTextAlignmentCenter];
    }
    return _labelRecord;
}

-(void)bindDataWithNoMoreData{
    [self.bookShelfImageView setImage:[UIImage imageNamed:@"_增加"]];
     self.bookShelfImageView.contentMode = UIViewContentModeCenter; 
    CGFloat height =CGRectGetHeight(self.frame)>125?125:CGRectGetHeight(self.frame)*(125/100);
    CGFloat x=(CGRectGetWidth(self.frame)-100)/2;
    [self.bookShelfImageView setFrame:CGRectMake(x,0, 100, height)];
    [self.labelBookName setFrame:CGRectMake(x, CGRectGetMaxY(self.bookShelfImageView.frame)+8, CGRectGetWidth(self.frame)-x*2, 20)];
    [self.labelBookName setText:@"添加小说"];
    [self.labelRecord setFrame:CGRectMake(x, CGRectGetMaxY(self.labelBookName.frame), CGRectGetWidth(self.frame)-x*2, 20)];
    [self.labelRecord setText:@"快速查找"];
}

@end

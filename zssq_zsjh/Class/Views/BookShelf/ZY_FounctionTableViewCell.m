//
//  ZY_FounctionTableViewCell.m
//  ZYNovel
//
//

#import "ZY_FounctionTableViewCell.h"
#import "ZY_Common.h"

@interface ZY_FounctionTableViewCell()

@property(nonatomic,strong) UIView *lineView;

@property(nonatomic,strong) UIImageView *imgView;

@property(nonatomic,strong) UILabel *functionNamelabel;

@end

@implementation ZY_FounctionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addSubview:self.imgView];
        [self addSubview:self.functionNamelabel];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark - Getter or Setter

-(UIImageView *)imgView
{
    if (!_imgView) {
         _imgView=[[UIImageView alloc]initWithFrame:CGRectMake(scaleValue(20),(CGRectGetHeight(self.frame)-scaleValue(15))/2, scaleValue(15),scaleValue(15))];
        _imgView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
         _imgView.contentMode = UIViewContentModeCenter; 
    }
    return _imgView;
}

-(UILabel *)functionNamelabel
{
    if (!_functionNamelabel) {
        CGFloat x= CGRectGetMaxX(self.imgView.frame);
         _functionNamelabel=[[UILabel alloc]initWithFrame:CGRectMake(x+scaleValue(18), (CGRectGetHeight(self.frame)-scaleValue(28))/2, CGRectGetWidth(self.frame)-x-scaleValue(18), scaleValue(28))];
        [_functionNamelabel setFont:[UIFont systemFontOfSize:scaleValue(28)]];
        _functionNamelabel.textColor = [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
        _functionNamelabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleWidth;
    }
    return _functionNamelabel;
}

- (UIView *)lineView{
    if (!_lineView) {
         _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame)-0.5, CGRectGetWidth(self.frame), 0.5)];
        [_lineView setBackgroundColor:kRGBCOLOR(232,232,232)];
        _lineView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    }
    
    return _lineView;
}

-(void)bindData:(NSString*)imageName withfunctionName:(NSString*)functionName{
    [self.imgView setImage:[UIImage imageNamed:imageName]];
    [self.functionNamelabel setText:functionName];
}

@end

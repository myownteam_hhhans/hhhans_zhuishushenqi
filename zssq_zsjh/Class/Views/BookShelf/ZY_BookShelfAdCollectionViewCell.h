//
//  ZY_BookShelfAdCollectionViewCell.h
//  ZYNovel
//
//  Created by S on 2017/3/24.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_ADHelper.h"

@interface ZY_BookShelfAdCollectionViewCell : UICollectionViewCell

/************************************
 函数描述:绑定广告数据
 作者:S
 时间:2017-03-23
 ************************************/
- (void)bindData:(ADModel*)adEntityInfo;

@end

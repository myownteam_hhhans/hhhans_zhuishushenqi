//
//  ZY_BookShelfCollectionListCell.h
//  ZYNovel
//
//

#import <UIKit/UIKit.h>

@interface ZY_BookShelfCollectionListCell : UICollectionViewCell

- (void)bindData:(ZY_NovelInfo*)novelInfo withIsEditor:(BOOL)isEditor withIndexPath:(NSIndexPath*)indexPath withIsUpdate:(BOOL)isUpdate;

@end

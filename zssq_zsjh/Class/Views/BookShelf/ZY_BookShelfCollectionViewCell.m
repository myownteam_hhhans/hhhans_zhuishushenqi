//
//  BookShelfCollectionViewCell.m
//  ZYNovel
//
//  Created by S on 2017/3/15.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_BookShelfCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "ZY_Common.h"
#import "LSYReadModel.h"

@interface ZY_BookShelfCollectionViewCell()<UIAlertViewDelegate>



@property(nonatomic,strong) UILabel *labelBookName;

@property(nonatomic,strong) UIButton *deleteButton;

@property(nonatomic,strong) ZY_NovelInfo *currentNovelInfo;

@property(nonatomic,strong) NSIndexPath *indexPath;

@property(nonatomic,strong) UIImageView *updateImageView;

@property(nonatomic,strong) UILabel *labelRecord;

@property(nonatomic,strong) UIView *bgView;

@end

@implementation ZY_BookShelfCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
       [self setBackgroundColor:[UIColor whiteColor]];
       [self.contentView addSubview:self.bgView];
       [self.bgView addSubview:self.bookShelfImageView];
       [self.bookShelfImageView addSubview:self.updateImageView];
       [self.contentView addSubview:self.deleteButton];
       [self.contentView addSubview:self.labelBookName];
       [self.contentView addSubview:self.labelRecord];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.bookShelfImageView setImage:[UIImage imageNamed:@"暂无封面"]];
    [self.updateImageView setHidden:YES];
}

#pragma mark - PrivateFunction

-(void)deleteBookShelf:(id)sender{
    
    UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"确定删除?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alerView show];
}

#pragma mark - Getter or Setter

-(UIImageView *)bookShelfImageView{
    if (!_bookShelfImageView) {
        _bookShelfImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_bookShelfImageView setBackgroundColor:kRGBCOLOR(239, 239, 239)];
        [_bookShelfImageView setImage:[UIImage imageNamed:@"暂无封面"]];
         _bookShelfImageView.contentMode = UIViewContentModeCenter;
         _bookShelfImageView.layer.shadowColor = [UIColor blackColor].CGColor;
         _bookShelfImageView.layer.shadowOffset = CGSizeMake(0, 0);
         _bookShelfImageView.layer.shadowOpacity = 0.5;
         _bookShelfImageView.layer.shadowRadius = 4.0;
    }
    return _bookShelfImageView;
}

-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:CGRectZero];
        _bgView.layer.shadowColor = [UIColor blackColor].CGColor;
        _bgView.layer.shadowOffset = CGSizeMake(0, 0);
        _bgView.layer.shadowOpacity = 0.5;
        _bgView.layer.shadowRadius = 2.0;
    }
    return _bgView;
}

-(UILabel *)labelBookName{
    if (!_labelBookName) {
         _labelBookName = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelBookName setText:@" "];
        [_labelBookName setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelBookName setFont:[UIFont systemFontOfSize:14]];
         _labelBookName.lineBreakMode = NSLineBreakByClipping;
        [_labelBookName setTextAlignment:NSTextAlignmentCenter];
    }
    return _labelBookName;
}

-(UILabel *)labelRecord{
    if (!_labelRecord) {
         _labelRecord = [[UILabel alloc] initWithFrame:CGRectZero];
        [_labelRecord setText:@" "];
        [_labelRecord setNumberOfLines:1];
        [_labelRecord setTextColor:kRGBCOLOR(153,153,153)];
        [_labelRecord setFont:[UIFont systemFontOfSize:11]];
        [_labelRecord setTextAlignment:NSTextAlignmentCenter];
    }
    return _labelRecord;
}

-(UIButton *)deleteButton{
    if (!_deleteButton) {
         _deleteButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [_deleteButton setImage:[UIImage imageNamed:@"删除书籍"] forState:UIControlStateNormal];
        [_deleteButton addTarget:self action:@selector(deleteBookShelf:) forControlEvents:UIControlEventTouchUpInside];
        [_deleteButton setBackgroundColor:[UIColor clearColor]];
        [_deleteButton setHidden:YES];
    }
    return _deleteButton;
}

-(UIImageView *)updateImageView{
    if (!_updateImageView) {
         _updateImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_updateImageView setImage:[UIImage imageNamed:@"更新"]];
        [_updateImageView setHidden:YES];
    }
    return _updateImageView;
}

- (void)bindData:(ZY_NovelInfo*)novelInfo withIsEditor:(BOOL)isEditor withIndexPath:(NSIndexPath*)indexPath withIsUpdate:(BOOL)isUpdate{
    self.currentNovelInfo=novelInfo;
    self.indexPath=indexPath;
    [self.deleteButton setHidden:!isEditor];
    [self.bookShelfImageView sd_setImageWithURL:[NSURL URLWithString:novelInfo.novelCoverUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            self.bookShelfImageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.bookShelfImageView setClipsToBounds:YES];
        }else{
            [self.bookShelfImageView setImage:[UIImage imageNamed:@"暂无封面"]];
            self.bookShelfImageView.contentMode = UIViewContentModeCenter;
        }
    }];
    
    CGFloat height =CGRectGetHeight(self.frame)>125?125:CGRectGetHeight(self.frame)*(125/100);
    
    CGFloat x=(CGRectGetWidth(self.frame)-100)/2;
    [self.bgView setFrame:CGRectMake(x,2, 100, height)];
    [self.bookShelfImageView setFrame:CGRectMake(0,0, 100, height)];
    [self.labelBookName setFrame:CGRectMake(x, CGRectGetMaxY(self.bookShelfImageView.frame)+8, CGRectGetWidth(self.frame)-x*2, 20)];
    [self.labelBookName setText:novelInfo.novelName];
    [self.labelRecord setFrame:CGRectMake(x, CGRectGetMaxY(self.labelBookName.frame), CGRectGetWidth(self.frame)-x*2, 20)];
    
    //  dispatch_async(dispatch_get_global_queue(0, 0), ^{
    NSString *nsstrRecordText = [LSYReadModel getReadInfo:novelInfo];
    if (nsstrRecordText) {
        [self.labelRecord setText:nsstrRecordText];
    }
    
    [self.deleteButton setFrame:self.bounds];
    [self.deleteButton setImageEdgeInsets:UIEdgeInsetsMake((CGRectGetHeight(self.frame)-5)*-1,(CGRectGetWidth(self.frame)-20),0,0)];
    
    if (isUpdate) {
        [self.updateImageView setHidden:NO];
        [self.updateImageView setFrame:CGRectMake(0, 0, 30, 30)];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        if ([self.delegate respondsToSelector:@selector(deleteCollectionViewCell:)]) {
            [self.delegate deleteCollectionViewCell:self.indexPath];
        }
    }
}

@end

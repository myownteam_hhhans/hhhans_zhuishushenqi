//
//  ZY_AddNovelCollectionViewListCell.m
//  ZYNovel
//
//

#import "ZY_AddNovelCollectionViewListCell.h"
#import "ZY_Common.h"

@interface ZY_AddNovelCollectionViewListCell()

@property(nonatomic,strong) UIImageView *iconImageView;

@property(nonatomic,strong) UILabel *labelTitle;

@end

@implementation ZY_AddNovelCollectionViewListCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setBackgroundColor:kRGBCOLOR(248, 248, 248)];
        [self.contentView addSubview:self.iconImageView];
        [self.contentView addSubview:self.labelTitle];
    }
    
    return self;
}

#pragma mark - Getter or Setter

-(UIImageView *)iconImageView{
    if (!_iconImageView) {
         _iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, (kBookShelfListAdHeight-38)/2, 38, 38)];
        [_iconImageView setImage:[UIImage imageNamed:@"添加-List"]];
    }
    
    return _iconImageView;
}

-(UILabel *)labelTitle{
    if (!_labelTitle) {
         _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame)+10,(kBookShelfListAdHeight-16)/2, 140, 16)];
        [_labelTitle setText:@"添加你喜欢的小说"];
        [_labelTitle setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelTitle setFont:[UIFont systemFontOfSize:16]];
         _labelTitle.lineBreakMode = NSLineBreakByClipping;
        [_labelTitle setTextAlignment:NSTextAlignmentLeft]; 
    }
    
    return _labelTitle;
}

@end

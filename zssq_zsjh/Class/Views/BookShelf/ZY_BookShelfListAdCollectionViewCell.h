//
//  ZY_BookShelfListAdCollectionViewCell.h
//  ZYNovel
//
//

#import <UIKit/UIKit.h> 
#import "ZY_ADHelper.h"

@interface ZY_BookShelfListAdCollectionViewCell : UICollectionViewCell

- (void)bindData:(ADModel*)adEntityInfo;

@end

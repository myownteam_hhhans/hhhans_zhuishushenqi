//
//  ZY_AddNovelCollectionViewCell.h
//  ZYNovel
//
//

#import <UIKit/UIKit.h>

@interface ZY_AddNovelCollectionViewCell : UICollectionViewCell

/************************************
 函数描述:绑定添加内容
 作者:S
 时间:2017-05-10
 ************************************/
- (void)bindDataWithNoMoreData;

@end

//
//  ZY_CategoryCell.h
//  ZYNovel
//
//  Created by 包涵 on 2018/9/17.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZY_CategoryCell : UICollectionViewCell

- (void)bindDataWithTitle:(NSString *)title count:(NSNumber *)count;

@end

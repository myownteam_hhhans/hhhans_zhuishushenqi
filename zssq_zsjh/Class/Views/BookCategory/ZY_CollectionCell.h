//
//  ZY_CollectionCell.h
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_BookCategoryModel.h"

@interface ZY_CollectionCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *hotImage;

- (void)bindDataWithModel:(ZY_BookCategoryModel *)model;

@end

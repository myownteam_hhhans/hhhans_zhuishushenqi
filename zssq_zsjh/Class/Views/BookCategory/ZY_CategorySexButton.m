//
//  ZY_CategorySexButton.m
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/9.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ZY_CategorySexButton.h"
#import "Common.h"

@interface ZY_CategorySexButton()



@end

@implementation ZY_CategorySexButton

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
        [self setTitleColor:[Common colorWithHexString:@"#202020" alpha:1.0] forState:UIControlStateNormal];
        [self setTitleColor:[Common colorWithHexString:@"#EC3636" alpha:1.0] forState:UIControlStateSelected];
        [self setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1]] forState:UIControlStateNormal];
        [self setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateSelected];
        [self addSubview:self.lineView];
    }
    return self;
}



// 根据颜色生成图片
- (UIImage *)imageWithColor:(UIColor *)color{
    // 描述矩形
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    // 开启位图上下文
    UIGraphicsBeginImageContext(rect.size);
    // 获取位图上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    // 使用color演示填充上下文
    CGContextSetFillColorWithColor(context, [color CGColor]);
    // 渲染上下文
    CGContextFillRect(context, rect);
    // 从上下文中获取图片
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    // 结束上下文
    UIGraphicsEndImageContext();
    return theImage;
}

#pragma mark === 懒加载 ===
- (UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 4, 55)]];
        [_lineView setBackgroundColor:[Common colorWithHexString:@"#EC3636" alpha:1.0]];
        [_lineView setHidden:YES];
    }
    return _lineView;
}

@end

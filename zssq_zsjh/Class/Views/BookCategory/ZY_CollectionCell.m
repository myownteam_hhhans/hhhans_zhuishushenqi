//
//  ZY_CollectionCell.m
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ZY_CollectionCell.h"
#import "Common.h"
#import <UIImageView+WebCache.h>

@interface ZY_CollectionCell()

@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *countLabel;



@end

@implementation ZY_CollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.coverImageView];
        [self addSubview:self.nameLabel];
        [self addSubview:self.countLabel];
        [self addSubview:self.hotImage];
    }
    return self;
}

- (void)bindDataWithModel:(ZY_BookCategoryModel *)model {
    
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:model.coverURL]];
    [self.countLabel setText:[NSString stringWithFormat:@"%@本",model.bookCount.stringValue]];
    [self.nameLabel setText:model.cateName];
    
}

#pragma mark ==== 懒加载 ====
- (UIImageView *)coverImageView {
    if (_coverImageView == nil) {
        _coverImageView = [[UIImageView alloc] init];
        [_coverImageView setFrame:[Common get414FrameByMap:CGRectMake(0, 0, 50, 64)]];
        
        _coverImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        _coverImageView.layer.shadowOffset = CGSizeMake(0, 0);
        _coverImageView.layer.shadowRadius = 2;
        _coverImageView.layer.shadowOpacity = 0.2;
    }
    return _coverImageView;
}
- (UILabel *)nameLabel {
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        [_nameLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:15]]];
        [_nameLabel setFrame:[Common get414FrameByMap:CGRectMake(58, 17, 58+16, 14)]];
        [_nameLabel setTextColor:[Common colorWithHexString:@"#141414" alpha:1.0]];
        [_nameLabel setTextAlignment:NSTextAlignmentLeft];
    }
    return _nameLabel;
}
- (UILabel *)countLabel {
    if (_countLabel == nil) {
        _countLabel = [[UILabel alloc] init];
        [_countLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:12]]];
        [_countLabel setFrame:[Common get414FrameByMap:CGRectMake(56, 36, 57+16, 14)]];
        [_countLabel setTextColor:[Common colorWithHexString:@"#999999" alpha:1.0]];
        [_countLabel setTextAlignment:NSTextAlignmentLeft];
    }
    return _countLabel;
}
- (UIImageView *)hotImage {
    if (_hotImage == nil) {
        _hotImage = [[UIImageView alloc] init];
        [_hotImage setFrame:[Common get414FrameByMap:CGRectMake(115, 0, 14, 14)]];
        [_hotImage setImage:[UIImage imageNamed:@"HotBookCategory"]];
        [_hotImage setHidden:YES];
    }
    return _hotImage;
}

@end

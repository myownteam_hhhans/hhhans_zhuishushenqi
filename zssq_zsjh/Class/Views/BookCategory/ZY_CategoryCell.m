//
//  ZY_CategoryCell.m
//  ZYNovel
//
//  Created by 包涵 on 2018/9/17.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ZY_CategoryCell.h"

@interface ZY_CategoryCell()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *countLabel;

@end

@implementation ZY_CategoryCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.countLabel];
    }
    return self;
}

- (void)bindDataWithTitle:(NSString *)title count:(NSNumber *)count {
    [self.titleLabel setText:title];
    [self.countLabel setText:[NSString stringWithFormat:@"%ld本",count.integerValue]];
}

#pragma mark ==== 懒加载 ====
- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setTextColor:RGB(69, 69, 69)];
        [_titleLabel setFont:[UIFont systemFontOfSize:17]];
        [_titleLabel setText:@"玄幻玄幻"];
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel sizeToFit];;
        [_titleLabel setCenter:CGPointMake(self.frame.size.width/2, 15+8)];
    }
    return _titleLabel;
}
- (UILabel *)countLabel {
    if (_countLabel == nil) {
        _countLabel = [[UILabel alloc] init];
        [_countLabel setTextColor:RGB(153, 153, 153)];
        [_countLabel setFont:[UIFont systemFontOfSize:13]];
        [_countLabel setText:@"10000玄幻玄幻"];
        [_countLabel setTextAlignment:NSTextAlignmentCenter];
        [_countLabel sizeToFit];;
        [_countLabel setCenter:CGPointMake(self.frame.size.width/2, 49)];
    }
    return _countLabel;
}

@end

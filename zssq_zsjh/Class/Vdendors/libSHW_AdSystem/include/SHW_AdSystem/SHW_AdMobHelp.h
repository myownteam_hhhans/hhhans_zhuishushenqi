//
//  AdMobHelp.h
//  SHWAdSystem
//
//

#import <Foundation/Foundation.h>

@interface SHW_AdMobHelp : NSObject

+ (SHW_AdMobHelp *)sharedInstance;

/**
 *  获取AdMob 原生广告
 *
 *  @return 原生广告AdUnitID
 */
-(NSString*)getAdMobNativeUnitID;

/**
 *  获取AdMob 插页广告
 *
 *  @return 插页广告AdUnitID
 */
-(NSString *)getAdMobInterstitialUnitID;

/**
 *  获取AdMob Banner广告
 *
 *  @return Banner广告AdUnitID
 */
-(NSString *)getAdMobBannerUnitID;

@end

//
//  SHWSecurity.h
//  SHWSecurity
//

//

#import <Foundation/Foundation.h>

@interface SHWSecurity : NSObject

/**
 *  函数描述:把字符串加密成(AES-CBC128模式)
 *
 *  作者:石伟
 *
 *  @param string 待加密字符串
 *
 *  @return 返回加密成功字符串
 */
+(NSString*)encryptAESData:(NSString*)string;

/**
 *  函数描述:把加密字符解密成明文字符串(AES-CBC128模式)
 *
 *  作者:石伟
 *
 *  @param string 加密字符串
 *
 *  @return 返回解密成功字符串
 */
+(NSString*)decryptAESData:(NSString *)string;

@end

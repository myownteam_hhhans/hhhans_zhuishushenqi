//
//  SHWADBanner.h
//  SHWAdSystem
//
//

#import <UIKit/UIKit.h>

extern NSString* const SHWADBannerDidReceiveTouchEventNotification;

extern NSString* const SHWADBannerDidAppearNotification;

extern NSString* const SHWADBannerDidDisappearNotification;

extern NSString* const SHWADBannerErrorNotification;

extern NSString* const SHWADBannerStatusUserInfoKey;

extern NSString* const SHWADBannerPoolIsEmpty;

extern CGSize kSHWADSize;

@interface SHW_ADBanner : UIView

// 广告条是否可以关闭
@property (assign, nonatomic) BOOL unclosable;

// 广告条友盟统计设置来源
@property (strong, nonatomic) NSString* umengAdLabel;

// AdMob rootViewController
@property (weak, nonatomic) UIViewController* currentViewController;

// AdMob刷新时间
@property (assign, nonatomic) NSTimeInterval interval;

// AdMob 广告ID
@property (strong, nonatomic) NSString *adBannerMobUnitID;

- (instancetype)initWithFrame:(CGRect)frame;

- (BOOL)isAdPoolEmpty;

- (void)loadAd;

@end

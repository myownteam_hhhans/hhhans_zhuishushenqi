//
//  SHWADInterstitial.h
//  SHWAdSystem
//
//

#import <UIKit/UIKit.h>

extern NSString* const SHWADInterstitialDidReceiveTouchEventNotification;
extern NSString* const SHWADInterstitialWillDisappearNotification;
extern NSString* const SHWADInterstitialDidDisappearNotification;
extern NSString* const SHWADInterstitialWillAppearNotification;
extern NSString* const SHWADInterstitialDidAppearNotification;

extern NSString* const SHWADBannerPoolIsEmpty;

extern NSString* const SHWADInterstitialStatusUserInfoKey;
extern NSString* const kSHWADIntersitialKey;


@class SHW_AdsModel;

@interface SHW_ADInterstitial : UIButton

+ (void)settingDomain:(NSString*)nsstrDomain;

+ (void)showAdsWithAppId:(NSString*)appid withIsAsdMobInterstitial:(BOOL)isAsdMobInterstitial withWindow:(UIWindow*)window;

+ (void)showAdsWithAppId:(NSString*)appid withWindow:(UIWindow*)window;

+ (void)dismiss;

+ (BOOL)isVisible;

+ (void)reloadBannerData;

+ (BOOL)interstitialPoolIsEmpty;

+ (BOOL)bannerPoolIsEmpty;

+ (BOOL)feedPoolIsEmpty;

+ (void)loadFeedAdData:(NSString*)appId;

+ (void)loadBookAdData:(NSString*)appId;

+ (SHW_AdsModel *)pickUpFeedAd;

+ (SHW_AdsModel *)pickUpBookAd;

@end

//
//  SDCollectionViewFlowLayout.m
//  AFNetworking
//
//  Created by 包涵 on 2018/8/3.
//

#import "SDCollectionViewFlowLayout.h"

@implementation SDCollectionViewFlowLayout

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    // 系统父类写的方法， 系统子类必须调用父类，进行执行(只是对部分属性进行修改,所以不必一个个进行设置布局属性)
    NSArray *layoutAtts =  [super layoutAttributesForElementsInRect:rect];
    CGFloat collectionViewCenterX = self.collectionView.bounds.size.width * 0.5;
    CGFloat contentOffsetX = self.collectionView.contentOffset.x;
    for (UICollectionViewLayoutAttributes *layoutAtt in layoutAtts) {
        CGFloat centerX = layoutAtt.center.x;
        // 形变值，根据当前cell 距离中心位置，的远近  进行反比例缩放。 （不要忘记算其偏移量的值。）
        CGFloat scaleX = 1 - ABS((centerX - collectionViewCenterX - contentOffsetX)/self.collectionView.bounds.size.width)/20;
        CGFloat scaleY = 1 - ABS((centerX - collectionViewCenterX - contentOffsetX)/self.collectionView.bounds.size.width)/3;
        // 给 布局属性  添加形变
        layoutAtt.transform = CGAffineTransformMakeScale(scaleX, scaleY);
    }
    return layoutAtts;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

@end

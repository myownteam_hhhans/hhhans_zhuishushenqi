//
//  MyBattery.m
//  BatteryManager
//
//  Created by kangzh on 13-6-26.
//  Copyright (c) 2013年 kangzh. All rights reserved.
//

#import "MyBattery.h"


@interface MyBattery()
//@property (nonatomic, assign) CGFloat prevBatteryLev;
//@property (nonatomic, strong) NSDate * startDate;
@end

@implementation MyBattery
-(void)dealloc{
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryLevelDidChangeNotification object:nil];
//     [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryStateDidChangeNotification object:nil];
}

-(instancetype)init{
    self = [super init];
    if (self) {
//        self.prevBatteryLev = [UIDevice currentDevice].batteryLevel;
        
        [UIDevice currentDevice].batteryMonitoringEnabled = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryLevelCharged:) name:UIDeviceBatteryLevelDidChangeNotification object:nil
         ];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryStateCharged:) name:UIDeviceBatteryStateDidChangeNotification object:nil];
    }
    return self;
}

- (void)batteryLevelCharged:(NSNotification *)note
{

//    float currBatteryLev = [UIDevice currentDevice].batteryLevel;
//    
//    float avgChgSpeed = (self.prevBatteryLev - currBatteryLev) / [self.startDate timeIntervalSinceNow];
//    
//    float remBatteryLev = 1.0 - currBatteryLev;
//    
//    NSTimeInterval remSeconds = remBatteryLev / avgChgSpeed;
//    NSLog(@"%f", remSeconds);
    
    
    if (self.ChargeLevelChange) {
        self.ChargeLevelChange(note.object);
    }
}

-(void)batteryStateCharged:(NSNotification *)note {

    
    if (self.ChargeStateChange) {
        self.ChargeStateChange(note.object);
    }

}

+(MyBattery*)sharedIntanceStatic{
    static MyBattery *m = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        m = [[MyBattery alloc] init];
    });
    return m;
}
+ (MyBattery*)sharedIntance
{
    MyBattery* m = [MyBattery sharedIntanceStatic];
    
    return m;
}

-(BOOL)isChargState{
    return [UIDevice currentDevice].batteryState == UIDeviceBatteryStateCharging;
}

-(NSInteger)currentBatteryDL{
    CGFloat dl= [UIDevice currentDevice].batteryLevel * 100;
    NSInteger dlInt = [[NSString stringWithFormat:@"%.0f", dl] integerValue];
    
    if (dlInt - dl >0.5 ) {
        dlInt --;
    }
    else if (dlInt - dl < -0.5) {
        dlInt ++;
    }
    
    if (dlInt > 100) {
        dlInt = 100;
    }
    else if(dlInt < 0) {
        dlInt = 0;
    }
    return dlInt;
}

-(UIDeviceBatteryState)chargeState{
    return [UIDevice currentDevice].batteryState;
}
-(NSInteger)level{
    return self.currentBatteryDL;
}
@end

//
//  MyBattery.h
//  BatteryManager
//
//  Created by kangzh on 13-6-26.
//  Copyright (c) 2013年 kangzh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MyBattery : NSObject {
}

@property (nonatomic,assign) NSInteger currentBatteryDL;
@property (nonatomic,assign) BOOL isChargState;
@property (nonatomic,assign) NSInteger level;

@property (nonatomic,assign,readonly) UIDeviceBatteryState chargeState;

@property (nonatomic,copy) void (^ChargeLevelChange)(UIDevice *device);

@property (nonatomic,copy) void (^ChargeStateChange)(UIDevice *device);
+ (MyBattery*)sharedIntance;


@end

//
//  ZY_BookSexCategoryController.m
//  zssq_zsjh
//
//  Created by 包涵 on 2018/9/19.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ZY_BookSexCategoryController.h"
#import "ZY_Common.h"
#import "ZY_CategoryCell.h"
#import "ZY_BookCateListViewController.h"

@interface ZY_BookSexCategoryController () <UICollectionViewDelegate, UICollectionViewDataSource>


@end

@implementation ZY_BookSexCategoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.mainCollectionView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ==== collectionViewDelegate ====
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.dataArray.count == 0) {
        return 0;
    }
    NSArray *arr = self.dataArray;
    return arr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ZY_CategoryCell *cell;

    cell = [self.mainCollectionView dequeueReusableCellWithReuseIdentifier:@"categoryCollectionCell" forIndexPath:indexPath];
    
    
    
    [cell setBackgroundColor:[UIColor whiteColor]];
    NSDictionary *dic = self.dataArray[indexPath.row];
    [cell bindDataWithTitle:dic[@"name"] count:dic[@"bookCount"]];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = self.dataArray[indexPath.row];
    
    ZY_BookCateListViewController *cateList = [[ZY_BookCateListViewController alloc]init];
    
  
    
    cateList.navigationItem.title = dic[@"name"];
    cateList.gender = self.gender ? @"male" : @"female";
    cateList.categoryName = dic[@"name"];
    cateList.hidesBottomBarWhenPushed = YES;
    
    [self.parentViewController.navigationController pushViewController:cateList animated:YES];
}

#pragma mark ==== 懒加载 ====
- (UICollectionView *)mainCollectionView {
    if (_mainCollectionView == nil) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake((kCScreenWidth - 23)/3, 70);
        layout.minimumLineSpacing = 5;
        layout.minimumInteritemSpacing = 5;
        layout.sectionInset = UIEdgeInsetsMake(6, 6, 6, 6);
        
        _mainCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        [_mainCollectionView setBackgroundColor:RGB(238, 238, 238)];
        [_mainCollectionView setDataSource:self];
        [_mainCollectionView setDelegate:self];
        _mainCollectionView.tag = 0;
        
        [_mainCollectionView registerClass:[ZY_CategoryCell class] forCellWithReuseIdentifier:@"categoryCollectionCell"];
    }
    return _mainCollectionView;
}

@end

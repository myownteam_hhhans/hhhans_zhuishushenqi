//
//  ZY_SecondCategoryController.m
//  zssq_zsjh
//
//  Created by 包涵 on 2018/10/9.
//  Copyright © 2018年 shiwei. All rights reserved.
//
//  第二种风格的分类页面

#import "ZY_SecondCategoryController.h"
#import "Common.h"
#import "ZY_Common.h"
#import "ZY_CategorySexButton.h"
#import "NWS_httpClient.h"
#import "ZY_BookCategoryModel.h"
#import "ZY_CollectionCell.h"
#import "ZY_BookCateListViewController.h"
#import <SVProgressHUD.h>
#import <MJRefresh.h>

@interface ZY_SecondCategoryController () <UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *mainCollection;

// 选择性别按钮
@property (nonatomic, strong) ZY_CategorySexButton *maleButton;
@property (nonatomic, strong) ZY_CategorySexButton *femaleButton;
@property (nonatomic, strong) ZY_CategorySexButton *publicationButton;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSArray *nowDataArray;

@property (nonatomic, strong) UIImageView *bookCountImage;
@property (nonatomic, strong) UILabel *totalCountLabel;

@end

@implementation ZY_SecondCategoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 加一个灰色条
    [self.view addSubview:({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [Common get414RadiusByMap:88], k_ScreenHeight - k_TabBarHeight - k_StatusBarHeight - k_NavigatiBarHeight)];
        [view setBackgroundColor:[UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1]];
        view;
    })];
    
    [self.view addSubview:self.bookCountImage];
    [self.view addSubview:self.totalCountLabel];
    
    [self.view addSubview:self.maleButton];
    [self.view addSubview:self.femaleButton];
    [self.view addSubview:self.publicationButton];
    [self.view addSubview:self.mainCollection];
    
    [self.mainCollection.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ==== 私有方法 ====
- (void)sexButtonClick:(ZY_CategorySexButton *)sender {
    
    if (sender.selected == YES) {
        return;
    }
    
    sender.selected = !sender.selected;
    sender.lineView.hidden = !sender.selected;
    
    if (sender.tag == 0) {
        [MobClick event:@"sort" label:@"男生"];
        self.femaleButton.selected = NO;
        self.femaleButton.lineView.hidden = YES;
        self.publicationButton.selected = NO;
        self.publicationButton.lineView.hidden = YES;
    } else if (sender.tag == 1) {
        [MobClick event:@"sort" label:@"女生"];
        self.maleButton.selected = NO;
        self.maleButton.lineView.hidden = YES;
        self.publicationButton.selected = NO;
        self.publicationButton.lineView.hidden = YES;
    } else {
        [MobClick event:@"sort" label:@"出版"];
        self.maleButton.selected = NO;
        self.maleButton.lineView.hidden = YES;
        self.femaleButton.selected = NO;
        self.femaleButton.lineView.hidden = YES;
    }
    
    if (self.dataArray && self.dataArray.count > 0) {
        self.nowDataArray = self.dataArray[sender.tag];
        [self.mainCollection reloadData];
        [self setTotalCount];
    } 
    
    
}

- (void)loadData {
    
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://api.zhuishushenqi.com/cats/lv2/statistics" parameters:nil success:^(NSDictionary *responseObject) {
        
        [self.dataArray removeAllObjects];
        
        NSArray *maleArray = responseObject[@"male"];
        NSArray *femaleArray = responseObject[@"female"];
        NSArray *publicationArray = responseObject[@"press"];
        
        NSMutableArray *maleModelArray = [NSMutableArray array];
        NSMutableArray *femaleModelArray = [NSMutableArray array];
        NSMutableArray *publicationModelArray = [NSMutableArray array];
        
        for (NSDictionary *dic in maleArray) {
            ZY_BookCategoryModel *model = [ZY_BookCategoryModel modelWithDic:dic];
            [maleModelArray addObject:model];
        }
        for (NSDictionary *dic in femaleArray) {
            ZY_BookCategoryModel *model = [ZY_BookCategoryModel modelWithDic:dic];
            [femaleModelArray addObject:model];
        }
        for (NSDictionary *dic in publicationArray) {
            ZY_BookCategoryModel *model = [ZY_BookCategoryModel modelWithDic:dic];
            [publicationModelArray addObject:model];
        }
        
        [self.dataArray addObject:maleModelArray];
        [self.dataArray addObject:femaleModelArray];
        [self.dataArray addObject:publicationModelArray];
        self.nowDataArray = maleModelArray;
        
        if (self.femaleButton.selected) {
            self.femaleButton.selected = NO;
            [self sexButtonClick:self.femaleButton];
        } else if (self.publicationButton.selected) {
            self.publicationButton.selected = NO;
            [self sexButtonClick:self.publicationButton];
        } else {
            self.maleButton.selected = NO;
            [self sexButtonClick:self.maleButton];
        }
        
        
        [self.mainCollection reloadData];
        
        [self setTotalCount];
        
        [self.mainCollection.mj_header endRefreshing];
        
        
        
    } failure:^(NSError *error) {
         [self.mainCollection.mj_header endRefreshing];
    }];
}

- (void)setTotalCount {
    
    NSInteger totalCount = 0;
    for (ZY_BookCategoryModel *model in self.nowDataArray) {
        totalCount += model.bookCount.integerValue;
    }
    
    NSString *countString = [NSString stringWithFormat:@"共%ld本",totalCount];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:countString];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(1, string.length - 2)];
    
    self.totalCountLabel.attributedText = string;
}

#pragma mark ==== delegate && dataSource ====
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    NSArray *arr = self.nowDataArray;
    return arr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ZY_CollectionCell *cell;
    
    cell = [self.mainCollection dequeueReusableCellWithReuseIdentifier:@"cateCollectionCell" forIndexPath:indexPath];
    
    ZY_BookCategoryModel *model = self.nowDataArray[indexPath.row];
    
    [cell bindDataWithModel:model];
    
    
    if (self.nowDataArray == self.dataArray[0]) {
        
        if (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 6) {
            [cell.hotImage setHidden:NO];
        } else {
            [cell.hotImage setHidden:YES];
        }
    } else if (self.nowDataArray == self.dataArray[1]) {
        
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 5 || indexPath.row == 6) {
            [cell.hotImage setHidden:NO];
        } else {
            [cell.hotImage setHidden:YES];
        }
        
    } else {
        [cell.hotImage setHidden:YES];
    }
    
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    ZY_BookCategoryModel *dic = self.nowDataArray[indexPath.row];

    ZY_BookCateListViewController *cateList = [[ZY_BookCateListViewController alloc]init];
    
    
    cateList.navigationItem.title = dic.cateName;
    
    if (self.nowDataArray == self.dataArray[0]) {
        [MobClick event:@"sort" label:[NSString stringWithFormat:@"男生_%ld",indexPath.row]];
        cateList.gender = @"male";
    } else if (self.nowDataArray == self.dataArray[1]) {
        [MobClick event:@"sort" label:[NSString stringWithFormat:@"女生_%ld",indexPath.row]];
        cateList.gender = @"female";
    } else {
        [MobClick event:@"sort" label:[NSString stringWithFormat:@"出版_%ld",indexPath.row]];
         cateList.gender = @"press";
    }
    cateList.categoryName = dic.cateName;
    cateList.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:cateList animated:YES];
    
    
}


#pragma mark ==== 懒加载 ====
- (UICollectionView *)mainCollection {
    if (_mainCollection == nil) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake([Common get414RadiusByMap:150], [Common get414RadiusByMap:64]);
        layout.minimumLineSpacing = [Common get414RadiusByMap:20];
        //layout.minimumInteritemSpacing = 5;
        layout.sectionInset = UIEdgeInsetsMake(4, [Common get414RadiusByMap:12], 0, 0);
        
        _mainCollection = [[UICollectionView alloc] initWithFrame:CGRectMake([Common get414RadiusByMap:88], [Common get414RadiusByMap:49] - 4, k_ScreenWidth - [Common get414RadiusByMap:88], k_ScreenHeight - k_StatusBarHeight - k_NavigatiBarHeight - k_TabBarHeight - [Common get414RadiusByMap:49]) collectionViewLayout:layout];
        [_mainCollection setBackgroundColor:[UIColor whiteColor]];
        [_mainCollection registerClass:[ZY_CollectionCell class] forCellWithReuseIdentifier:@"cateCollectionCell"];
        [_mainCollection setDelegate:self];
        [_mainCollection setDataSource:self];
        
        _mainCollection.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    }
    return _mainCollection;
}
- (ZY_CategorySexButton *)maleButton {
    if (_maleButton == nil) {
        _maleButton = [ZY_CategorySexButton buttonWithType:UIButtonTypeCustom];
        [_maleButton setFrame:[Common get414FrameByMap:CGRectMake(0, 0, 88, 55)]];
        [_maleButton setTitle:@"男生" forState:UIControlStateNormal];
        [_maleButton setSelected:YES];
        [_maleButton setTag:0];
        [_maleButton addTarget:self action:@selector(sexButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        _maleButton.lineView.hidden = NO;
    }
    return _maleButton;
}
- (ZY_CategorySexButton *)femaleButton {
    if (_femaleButton == nil) {
        _femaleButton = [ZY_CategorySexButton buttonWithType:UIButtonTypeCustom];
        [_femaleButton setFrame:[Common get414FrameByMap:CGRectMake(0, 55, 88, 55)]];
        [_femaleButton setTitle:@"女生" forState:UIControlStateNormal];
        [_femaleButton setTag:1];
        [_femaleButton addTarget:self action:@selector(sexButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _femaleButton;
}
- (ZY_CategorySexButton *)publicationButton {
    if (_publicationButton == nil) {
        _publicationButton = [ZY_CategorySexButton buttonWithType:UIButtonTypeCustom];
        [_publicationButton setFrame:[Common get414FrameByMap:CGRectMake(0, 110, 88, 55)]];
        [_publicationButton setTitle:@"出版" forState:UIControlStateNormal];
        [_publicationButton setTag:2];
        [_publicationButton addTarget:self action:@selector(sexButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _publicationButton;
}
- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (UIImageView *)bookCountImage {
    if (_bookCountImage == nil) {
        _bookCountImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(105, 17, 19, 15)]];
        [_bookCountImage setImage:[UIImage imageNamed:@"BookCount"]];
    }
    return _bookCountImage;
}
- (UILabel *)totalCountLabel {
    if (_totalCountLabel == nil) {
        _totalCountLabel = [[UILabel alloc] init];
        [_totalCountLabel setFrame:[Common get414FrameByMap:CGRectMake(130, 18, 200, 14)]];
        [_totalCountLabel setTextAlignment:NSTextAlignmentLeft];
        [_totalCountLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
        [_totalCountLabel setTextColor:[Common colorWithHexString:@"#999999" alpha:1.0]];
    }
    return _totalCountLabel;
}

@end

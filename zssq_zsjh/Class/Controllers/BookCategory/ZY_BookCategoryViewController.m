//
//  ZY_BookCategoryViewController.m
//  ZYNovel
//
//  Created by apple on 2018/6/13.
//

#import "ZY_BookCategoryViewController.h"
#import "ZY_Common.h"
#import "ZY_BookCateTableViewCell.h"
#import "ZY_BookCateListViewController.h"
#import "ZY_SearchViewController.h"
#import "NWS_httpClient.h"
#import "F_TitleView.h"
#import "ZY_CategoryCell.h"
#import "ZY_BookSexCategoryController.h"

@interface ZY_BookCategoryViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,titleClickDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) ZY_BookSexCategoryController *femaleVC;
@property (nonatomic, strong) ZY_BookSexCategoryController *maleVC;

@property (nonatomic, strong) UIScrollView *mainScrollView;

// 导航
@property (nonatomic, strong) F_TitleView *myNavTitleView;

// 数据源
@property (nonatomic, assign) BOOL gender;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

@implementation ZY_BookCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
    // 导航title
    self.myNavTitleView = [[F_TitleView alloc] initWithFrame:CGRectMake(0, 0, ScreenSize.width + kCScreenWidth/11, 44)];
    self.myNavTitleView.delegate = self;
    self.myNavTitleView.collectionButton.hidden = YES;
    self.navigationItem.titleView= self.myNavTitleView;
    [self.myNavTitleView clickSexButton:0];
    
    [self.view addSubview:self.mainScrollView];
    
    
    UIButton *btnsearch = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 44)];
    [btnsearch setImage:[UIImage imageNamed:@"搜索-白色"] forState:UIControlStateNormal];
    [btnsearch addTarget:self action:@selector(gotoSearchController) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnsearch];
    
    // 添加子控制器
    self.femaleVC = [[ZY_BookSexCategoryController alloc] init];
    self.femaleVC.gender = 0;
    self.femaleVC.view.frame = self.view.bounds;
    
    self.maleVC = [[ZY_BookSexCategoryController alloc] init];
    self.maleVC .gender = 1;
    self.maleVC .view.frame = self.view.bounds;
    CGRect frame = self.maleVC .view.frame;
    frame.origin.x = kCScreenWidth;
    self.maleVC .view.frame = frame;
    
    [self addChildViewController:self.femaleVC];
    [self addChildViewController:self.maleVC ];
    [self.mainScrollView addSubview:self.femaleVC.view];
    [self.mainScrollView addSubview:self.maleVC.view];
    
    [self loadData];
    
}

-(void)gotoSearchController{
    ZY_SearchViewController *searchVC = [[ZY_SearchViewController alloc] init];
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searchVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
}


- (void)loadData {
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://api.zhuishushenqi.com/cats/lv2/statistics" parameters:nil success:^(id responseObject) {

        
        self.femaleVC.dataArray = responseObject[@"female"];
        self.maleVC.dataArray = responseObject[@"male"];
        
        [self.maleVC.mainCollectionView reloadData];
        [self.femaleVC.mainCollectionView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark ==== subViewDelegate ====
- (void)buttonClickWithSex:(NSInteger)sex {

    [UIView animateWithDuration:0.3 animations:^{
        if (sex) {
            self.mainScrollView.contentOffset = CGPointMake(kCScreenWidth, 0);
        } else {
            self.mainScrollView.contentOffset = CGPointMake(0, 0);
        }
    }];
    
}




- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x > kCScreenWidth-1) {
        [self.myNavTitleView clickSexButton:1];
    } else {
        [self.myNavTitleView clickSexButton:0];
    }
}

#pragma mark ==== 懒加载 ====


- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (UIScrollView *)mainScrollView {
    if (_mainScrollView == nil) {
        _mainScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        [_mainScrollView setContentSize:CGSizeMake(kCScreenWidth *2, 1)];
        [_mainScrollView setBackgroundColor:RGB(238, 238, 238)];
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.bounces = NO;
        _mainScrollView.delegate = self;
    }
    return _mainScrollView;
}

@end

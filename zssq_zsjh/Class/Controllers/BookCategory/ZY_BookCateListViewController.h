//
//  ZY_BookCateListViewController.h
//  ZYNovel
//
//  Created by apple on 2018/6/15.
//  Copyright © 2018年 zhaoyunei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelCate.h"

@interface ZY_BookCateListViewController : UIViewController

@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic,strong) ZY_NovelCate *cate;

@end

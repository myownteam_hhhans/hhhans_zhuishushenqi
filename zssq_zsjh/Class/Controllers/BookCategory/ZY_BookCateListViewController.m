//
//  ZY_BookCateListViewController.m
//  ZYNovel
//
//  Created by apple on 2018/6/15.

//

#import "ZY_BookCateListViewController.h"
#import "ZY_BookLIst.h"
#import "ZY_HeadEntity.h"
#import "ZY_Common.h"

@interface ZY_BookCateListViewController ()

@end

@implementation ZY_BookCateListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.cate.cateName;
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    ZY_BookLIst *_bookList = [[ZY_BookLIst alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight-JH_Navigation_Height)];
    _bookList.BaseMethodUrl = [NSString stringWithFormat: @"https://shuapi.jiaston.com/Categories/%i/%%@/%%i.html",[self.cate.cateId intValue]];
    _bookList.BaseMethodUrl = [NSString stringWithFormat:@"http://api.zhuishushenqi.com/book/by-categories?gender=%@&type=%%@&major=%@&minor=&start=%%ld&limit=%%ld",self.gender,self.categoryName];
    
    [self.view addSubview:_bookList];
    
    NSMutableArray *headArr = [[NSMutableArray alloc]init];
    ZY_HeadEntity *head1 = [[ZY_HeadEntity alloc]init];
    head1.title = @"热门";
    head1.urlKey = @"hot";
    [headArr addObject:head1];
    
    ZY_HeadEntity *head2 = [[ZY_HeadEntity alloc]init];
    head2.title = @"新书";
    head2.urlKey = @"new";
    [headArr addObject:head2];
    
    ZY_HeadEntity *head3 = [[ZY_HeadEntity alloc]init];
    head3.title = @"好评";
    head3.urlKey = @"reputation";
    [headArr addObject:head3];
    
    ZY_HeadEntity *head4 = [[ZY_HeadEntity alloc]init];
    head4.title = @"完结";
    head4.urlKey = @"over";
    [headArr addObject:head4];
    
    _bookList.head_arr = headArr;
    [_bookList createHeadView];
    [_bookList createTableView];
    [self.view addSubview:_bookList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

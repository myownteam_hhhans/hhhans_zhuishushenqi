//
//  ZY_BookSexCategoryController.h
//  zssq_zsjh
//
//  Created by 包涵 on 2018/9/19.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZY_BookSexCategoryController : UIViewController

@property(nonatomic,strong) NSArray *dataArray;
@property(nonatomic, assign) BOOL gender;
@property(nonatomic,strong) UICollectionView *mainCollectionView;

@end

//
//  ZY_SearchResultViewController.m
//  ZYNovel
//
//  Created by S on 2017/3/15.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_SearchResultViewController.h"
#import "ZY_BookStoreViewController.h"
#import "ZY_Common.h"
#import "ZY_DataDownloader.h"
#import "ZY_XPathParserConfig.h"
#import "ZY_XPathParser.h"
#import "ZY_CommonTableViewCell.h"
#import "MJRefresh.h"
#import "ZY_BookDetailsViewController.h"
#import "ZY_SearchViewController.h"
#import "SHW_ADInterstitial.h"
#import "UITableView+JRTableViewPlaceHolder.h"
#import "ZY_BookStoreAdTableViewCell.h"
#import "ZY_ADHelper.h"
#import "CFT_parameterOnline.h"
#import "ZY_JsonParser.h"
#import "Common.h"
#import "ZY_SearchRuleView.h"
#import "ZY_SearchSelectView.h"
#import "NWS_httpClient.h"
#import "ZY_SearchListModel.h"
#import "ZY_SearchListCell.h"
#import "ZY_ListDetailController.h"

static NSString *nsstrSearchResultBookIdentifier =@"BookCellIdentifier";

static NSString *nsstrSearchResultBookStoreAdIdentifier=@"BookStoreAdIdentifier";


@interface ZY_SearchResultViewController ()<UITableViewDataSource,UITableViewDelegate,ZY_DataDownloaderDelegate,UIScrollViewDelegate,SearchRuleDelegate,SearchSelectSure>

@property(nonatomic,strong) UITableView *tableView; // 书籍搜索列表
@property(nonatomic,strong) NSMutableArray *dataArray;

@property (nonatomic, strong) UITableView *listTabelView; // 书单搜索列表
@property (nonatomic, strong) NSMutableArray *listDataArray;

@property(nonatomic,strong) ZY_DataDownloader *downLoader;

//@property(nonatomic,strong) ZY_AdEntity *adEntity;
@property(nonatomic,strong) ADModel *adEntity;

@property(nonatomic,assign) NSInteger nsintPage;

@property(nonatomic,assign) BOOL isRefresh;

@property(nonatomic,assign) BOOL isPulldownRefresh;

@property (nonatomic,strong) NSString *adType;

// 滑动控制
@property (nonatomic, strong) UIButton *bookButton;
@property (nonatomic, strong) UIButton *bookListButton;
@property (nonatomic, strong) UIView *sliderView;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIScrollView *mainScroll; // 横向滑动

// 书籍列表筛选
@property (nonatomic, strong) UIButton *ruleButton;
@property (nonatomic, strong) UIButton *selectButton;
@property (nonatomic, strong) UIImageView *ruleButtonImageView;
@property (nonatomic, strong) UIImageView *selectButtonImageView;
@property (nonatomic, strong) UIView *downView_Button_Line;

// 筛选下拉控件
@property (nonatomic, strong) ZY_SearchRuleView *ruleView;
@property (nonatomic, strong) ZY_SearchSelectView *selectView;

// 缺省图片
@property (nonatomic, strong) UIImageView *noBookImageView;

@end

@implementation ZY_SearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"搜索结果";
    //     self.adEntity = [ZY_AdEntity initWithSHW_AdsModel:[SHW_ADInterstitial pickUpBookAd]];
    self.dataArray = [[NSMutableArray alloc] init];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    ZY_WS(weakSelf);
    [weakSelf.tableView jr_configureWithPlaceHolderBlock:^UIView * _Nonnull(UITableView * _Nonnull sender) {
        UIView *errorView =[ZY_Common createLoadingErrorViewWithFrame:weakSelf.tableView.bounds withTarget:weakSelf.tableView.mj_header action:@selector(beginRefreshing)];
        [errorView setHidden:YES];
        return errorView;
    } normalBlock:^(UITableView * _Nonnull sender) {
        
    }];
    
    
    
    [self.view addSubview:self.bookButton];
    [self.view addSubview:self.bookListButton];
    [self.view addSubview:self.lineView];
    [self.view addSubview:self.sliderView];
    
    [self.view addSubview:self.mainScroll];
    
    [self.view addSubview:self.noBookImageView];
    
    [self.mainScroll addSubview:self.ruleButton];
    [self.mainScroll addSubview:self.selectButton];
    [self.mainScroll addSubview:self.downView_Button_Line];
    [self.mainScroll addSubview:self.tableView];
    [self.mainScroll addSubview:self.listTabelView];
    
    [self.mainScroll addSubview:self.ruleView];
    [self.mainScroll addSubview:self.selectView];
    
    
    [self loadListData];
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - PrivateFunction

-(void)loadData{
    self.nsintPage=1;
    self.isPulldownRefresh=YES;
    ZY_WS(weakSelf);
    [weakSelf.tableView.jr_placeHolderView setHidden:YES];
    NSString *urlString = [NSString stringWithFormat:@"https://b.zhuishushenqi.com/books/fuzzy-search?model.start=0&model.limit=100&model.query=%@",[self.keyWords stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *isOpenReview = [[NSUserDefaults standardUserDefaults] objectForKey:@"urlString"];
    if ([isOpenReview isEqualToString:@"0"]) {
        urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&packageName=%@",[[NSBundle mainBundle]bundleIdentifier]]];
    }
    [self.downLoader getNovelHtmlDataByUrl:urlString];
}
-(void)loadNextData{
    [self.downLoader getNovelHtmlDataByUrl:[NSString stringWithFormat:@"https://api.zhuishushenqi.com/book/fuzzy-search?start=%d&limit=50&v=1&query=%@",(int)(self.nsintPage-1)*50,[self.keyWords stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    self.isRefresh=YES;
}

- (void)loadListData {
    [[NWS_httpClient sharedInstance] getWithURLString:[NSString stringWithFormat:@"https://api.zhuishushenqi.com/book-list/ugcbooklist-search?query=%@",[self.keyWords stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] parameters:nil success:^(NSDictionary *responseObject) {

        if (![responseObject[@"ok"] isKindOfClass:[NSNumber class]] || !((NSNumber *)responseObject[@"ok"]).boolValue == YES ) {
            [self.listTabelView.mj_header endRefreshing];
            return;
        }
        if (![responseObject[@"ugcbooklists"] isKindOfClass:[NSArray class]]) {
            [self.listTabelView.mj_header endRefreshing];
            return;
        }
        NSArray *listDictArray = responseObject[@"ugcbooklists"];
        for (NSDictionary *dict in listDictArray) {
            ZY_SearchListModel *model = [[ZY_SearchListModel alloc] initWithDictionary:dict];
            [self.listDataArray addObject:model];
        }
        [self.listTabelView reloadData];
        [self.listTabelView.mj_header endRefreshing];
        
    } failure:^(NSError *error) {
        [self.listTabelView.mj_header endRefreshing];
    }];
}
- (void)loadNextListData {
    
}

-(void)gotoSearchController{
    ZY_SearchViewController *searchVC = [[ZY_SearchViewController alloc] init];
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)showRuleView {
    self.ruleButton.selected = !self.ruleButton.selected;
    self.selectButton.selected = NO;
    self.selectView.hidden = YES;
    self.ruleView.hidden = !self.ruleButton.selected;
    [self.selectButtonImageView setImage:[UIImage imageNamed:@"searchRuleDownIcon"]];
    [self.ruleButtonImageView setImage:self.ruleButton.selected ? [UIImage imageNamed:@"searchRuleUpIcon"] : [UIImage imageNamed:@"searchRuleDownIcon"]];
    [MobClick event:@"search" label:@"按综合"];
}
- (void)showSelectView {
    self.selectButton.selected = !self.selectButton.selected;
    self.ruleButton.selected = NO;
    self.selectView.hidden = !self.selectButton.selected;
    self.ruleView.hidden = YES;
    [MobClick event:@"search" label:@"筛选"];
    [self.selectButtonImageView setImage:self.selectButton.selected ? [UIImage imageNamed:@"searchRuleUpIcon"] : [UIImage imageNamed:@"searchRuleDownIcon"]];
    [self.ruleButtonImageView setImage:[UIImage imageNamed:@"searchRuleDownIcon"]];
}

- (void)changePage:(NSInteger)page {
    [UIView animateWithDuration:0.3 animations:^{
        self.mainScroll.contentOffset = CGPointMake(page * k_ScreenWidth, 0);
    }];
}
- (void)sliderButtonClick:(UIButton *)sender {
    self.bookButton.selected = (sender.tag == 0);
    self.bookListButton.selected = (sender.tag == 1);
    [self changePage:sender.tag];
    UIButton *selectedButton = sender.tag == 0 ? self.bookButton : self.bookListButton;
    [UIView animateWithDuration:0.2 animations:^{
        self.sliderView.center = CGPointMake(selectedButton.center.x, self.sliderView.center.y);
    }];
    if (sender.tag == 1) {
        [MobClick event:@"search" label:@"书单"];
    }
}

#pragma mark - subViewDelegate
- (void)selectedSearchRule:(UIButton *)sender {
    
    self.ruleView.hidden = YES;
    self.ruleButton.selected = NO;
    [self.ruleButtonImageView setImage: [UIImage imageNamed:@"searchRuleDownIcon"]];
    
    [self.ruleButton setTitle:sender.titleLabel.text forState:UIControlStateNormal];
    
    if (sender.tag == 0) {
        [self.tableView.mj_header beginRefreshing];
        return;
    }
    
    [self.dataArray sortUsingComparator:^NSComparisonResult(NSArray*  _Nonnull obj1, NSArray*  _Nonnull obj2) {
        NSNumber *number1;
        NSNumber *number2;
        
        NSString *cellType1 = obj1[0];
        NSString *cellType2 = obj2[0];
        if (![cellType1 isEqualToString:@"BookCellIdentifier"] || ![cellType2 isEqualToString:@"BookCellIdentifier"]) {
            return NSOrderedAscending;
        }
        
        ZY_NovelInfo *info1 = obj1[2];
        ZY_NovelInfo *info2 = obj2[2];
        
        switch (sender.tag) {
            case 1:  // 人气(追书人数）
                number1 = info1.ratedNumber;
                number2 = info2.ratedNumber;
                break;
            case 2:  // 留存
                number1 = info1.retentionRatio;
                number2 = info2.retentionRatio;
                break;
            case 3:  // 评分
                number1 = [NSNumber numberWithInteger:info1.novelScore.integerValue];
                number2 = [NSNumber numberWithInteger:info2.novelScore.integerValue];
                break;
            case 4:  // 字数
                number1 = [NSNumber numberWithInteger:info1.WordCountFormat.integerValue];
                number2 = [NSNumber numberWithInteger:info2.WordCountFormat.integerValue];
                break;
                
            default:
                break;
        }
        NSComparisonResult result = [number1 compare:number2];
        return result == NSOrderedAscending;
        
    }];
    
    [self.tableView reloadData];
    
    
}

- (void)searchSelectSure {
    self.selectView.hidden = YES;
    self.selectButton.selected = NO;
    [self.selectButtonImageView setImage: [UIImage imageNamed:@"searchRuleDownIcon"]];
    
    NSMutableArray *readyToDeleteItem = [NSMutableArray array];
    
    for (NSInteger i = 0; i<self.dataArray.count; i++) {
        NSArray *array = self.dataArray[i];
        if (![((NSString *)array[0]) isEqualToString:@"BookCellIdentifier"]) {
            continue;
        }
        BOOL containsCategory = NO;
        
        if (((NSArray *)self.selectView.selectedItemArray[0]).count == 0) {
            break;
        }
        for (UIButton *btn in self.selectView.selectedItemArray[0]) {
            if ( [((ZY_NovelInfo *)array[2]).novelCategory isEqualToString:btn.titleLabel.text]) {
                containsCategory = YES;
            }
        }
        if (!containsCategory) {
            [readyToDeleteItem addObject:array];
        }
    }
    
    for (id object in readyToDeleteItem) {
        [self.dataArray removeObject:object];
    }
    [readyToDeleteItem removeAllObjects];
    
    for (NSInteger i = 0; i<self.dataArray.count; i++) {
        NSArray *array = self.dataArray[i];
        if (![((NSString *)array[0]) isEqualToString:@"BookCellIdentifier"]) {
            continue;
        }
        BOOL containsCount = NO;
        if (((NSArray *)self.selectView.selectedItemArray[1]).count == 0) {
            break;
        }
        for (UIButton *btn in self.selectView.selectedItemArray[1]) {
            
            NSInteger wordCount = ((ZY_NovelInfo *)array[2]).WordCountFormat.integerValue;
            NSString *workCountFormat;
            if (wordCount < 200000) {
                workCountFormat = @"20万字内";
            } else if (wordCount >= 200000 && wordCount < 500000) {
                workCountFormat = @"20-50万字";
            } else if (wordCount >= 500000 && wordCount < 1000000) {
                workCountFormat = @"50-100万字";
            } else if (wordCount >= 1000000 && wordCount < 2000000) {
                workCountFormat = @"100-200万字";
            } else {
                workCountFormat = @"200万字以上";
            }
            
            if ( [workCountFormat isEqualToString:btn.titleLabel.text]) {
                containsCount = YES;
            }
        }
        if (!containsCount) {
            [readyToDeleteItem addObject:array];
        }
    }
    
    for (id object in readyToDeleteItem) {
        [self.dataArray removeObject:object];
    }
    
    [self.tableView reloadData];
}

#pragma mark - ZY_DataDownloaderDelegate
-(void)downloadNavError:(NSError*)error downloadErrorType:(downloadErrorType)type
{
    NSLog(@"Error:%@",error);
    __weak __typeof(self)weakSelf = self;
    [weakSelf.tableView.jr_placeHolderView setHidden:NO];
    DebugLog(@"错误___%@",error);
    [self.tableView.mj_header endRefreshing];
    if (self.isRefresh) {
        [self.tableView.mj_footer endRefreshing];
        self.isRefresh=NO;
    }
}

-(void)htmlDataFinish:(NSData *)htmlData{
    
    NSMutableArray *array = [[[ZY_JsonParser alloc] init] getNovelSearchListByData:htmlData];
    
    if ((!array ||array.count == 0) && ( !self.dataArray || self.dataArray.count == 0)) {
        self.noBookImageView.hidden = NO;
        self.tableView.hidden = YES;
        self.listTabelView.hidden = YES;
        
        return;
    }
    
    self.noBookImageView.hidden = YES;
    self.tableView.hidden = NO;
    self.listTabelView.hidden = NO;
    
    if (array&&array.count>0) {
        if (self.isPulldownRefresh) {
            [self.dataArray removeAllObjects];
            self.isPulldownRefresh=NO;
        }
        int randx = arc4random() % 4;
        for (NSInteger i=0; i<array.count; i++) {
            if (self.dataArray.count==randx) {

                self.adType = [[Common shareCommon] randomADTypeWithKey:@"bookflow"];

                NSString *adtype = self.adType;
                AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:adtype];
                if (platform != adplatform_Google && [[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookflow"]) {
                    self.adEntity = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_BookShelf adPlatform:platform];
                }
                
                if (self.adEntity && self.dataArray.count > randx) {
                    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"_iapManager"]) {
                        [self.dataArray insertObject:@[nsstrSearchResultBookStoreAdIdentifier,@(150),self.adEntity] atIndex:randx];
                    }
                }
            }

            [self.dataArray addObject:@[nsstrSearchResultBookIdentifier,@(150),[array objectAtIndex:i]]];
        }
        if (self.nsintPage<=10) {
            self.nsintPage++;
        }
    }
    if (self.isRefresh) {
        [self.tableView.mj_footer endRefreshing];
        self.isRefresh=NO;
    }
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
    
    // 筛选项数据
    NSMutableArray *categoryArray = [NSMutableArray array];
    
    for (ZY_NovelInfo *info in array) {
        // 添加小说类型
        BOOL containCat = NO;
        for (NSString *str in categoryArray) {
            if ([info.novelCategory isEqualToString:str]) {
                containCat = YES;
            }
        }
        if (!containCat) {
            [categoryArray addObject:info.novelCategory];
        }
        // 添加标签
    }
    // 通知筛选页刷新数据
    [self.selectView loadCollectionViewWithArray:[NSMutableArray arrayWithObjects:categoryArray,@[@"20万字内",@"20-50万字",@"50-100万字",@"100-200万字",@"200万字以上"], nil]];
    
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 1) {
        return self.dataArray.count;
    } else {
        return self.listDataArray.count;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1) {
        return [[[self.dataArray objectAtIndex:indexPath.row] objectAtIndex:1] floatValue];
    } else {
        return [Common get414RadiusByMap:144];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag == 2) {
        ZY_SearchListCell *cell = [self.listTabelView dequeueReusableCellWithIdentifier:@"listCell"];
        [cell bindDataWithModel:self.listDataArray[indexPath.row]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    
    NSArray *dataArray = [self.dataArray objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier=[dataArray firstObject];
    
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:nsstrIdentifier forIndexPath:indexPath];
    tableViewCell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([nsstrIdentifier isEqualToString:nsstrSearchResultBookIdentifier]){
        ZY_NovelInfo *novelInfo = [dataArray lastObject];
        
        if (novelInfo) {
            [(ZY_CommonTableViewCell*)tableViewCell bindNovelItem:novelInfo];
        }
        
    }else if ([nsstrIdentifier isEqualToString:nsstrSearchResultBookStoreAdIdentifier]){
        if (self.adEntity&&self.adEntity.platform != adplatform_Google) {
            [(ZY_BookStoreAdTableViewCell*)tableViewCell bindAdItem:self.adEntity];
        }
        // 头条注册点击
        if ([self.adType isEqualToString:@"5"]) {
            
        }
        if ([self.adType isEqualToString:@"4"]) {
            BUNativeAd *ad = self.adEntity.data;
            [ad registerContainer:tableViewCell withClickableViews:tableViewCell.subviews];
            
        }
        if ([self.adType isEqualToString:@"3"]) {
           
        }
    }
    
    return tableViewCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 2) {
        ZY_ListDetailController *listVC =[[ZY_ListDetailController alloc] init];
        ZY_SearchListModel *model = self.listDataArray[indexPath.row];
        listVC.listID = model.listID;
        [self.navigationController pushViewController:listVC animated:YES];
        return;
    }
    
    NSArray *dataArray = [self.dataArray objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier=[dataArray firstObject];
    
    if ([nsstrIdentifier isEqualToString:nsstrSearchResultBookIdentifier]){
        ZY_NovelInfo *novelInfo = [dataArray lastObject];
        
        if (novelInfo) {
            ZY_BookDetailsViewController *zyDetailViewController = [[ZY_BookDetailsViewController alloc] init];
            zyDetailViewController.w_novel = novelInfo;
            self.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:zyDetailViewController animated:YES];
            self.hidesBottomBarWhenPushed=NO;
        }
        
    }else if ([nsstrIdentifier isEqualToString:nsstrSearchResultBookStoreAdIdentifier]){
        if (self.adEntity&&self.adEntity.platform != adplatform_Google) {
            if ([self.adType isEqualToString:@"5"]) {
               
            }
            if ([self.adType isEqualToString:@"4"]) {
               
            }
            if ([self.adType isEqualToString:@"3"]) {
             
            }
            [[ZY_ADHelper shareADHelper] tapAD:self.adEntity CurrentVC:self];
        }
    }
}

#pragma mark - scrollView delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    if (scrollView.contentOffset.x < k_ScreenWidth) {
        [self sliderButtonClick:self.bookButton];
    } else {
        [self sliderButtonClick:self.bookListButton];
    }
}

#pragma mark - Getter or Setter

-(ZY_DataDownloader *)downLoader{
    if (!_downLoader) {
        _downLoader = [[ZY_DataDownloader alloc] init];
        [_downLoader setDelegate:self];
    }
    return _downLoader;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [Common get414RadiusByMap:60],kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height) style:UITableViewStylePlain];
        if(@available(iOS 11.0, *)) {
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [_tableView setTag:1];
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_tableView setBackgroundColor:[UIColor whiteColor]];
        [_tableView registerClass:[ZY_CommonTableViewCell class] forCellReuseIdentifier:nsstrSearchResultBookIdentifier];
        [_tableView registerClass:[ZY_BookStoreAdTableViewCell class] forCellReuseIdentifier:nsstrSearchResultBookStoreAdIdentifier];
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadNextData)];
    }
    return _tableView;
}

- (UITableView *)listTabelView {
    if (!_listTabelView) {

        _listTabelView = [[UITableView alloc] initWithFrame:CGRectMake(k_ScreenWidth, 0,kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height) style:UITableViewStylePlain];
        [_listTabelView registerClass:[ZY_SearchListCell class] forCellReuseIdentifier:@"listCell"];
        [_listTabelView setDelegate:self];
        [_listTabelView setDataSource:self];
        [_listTabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_listTabelView setBackgroundColor:[UIColor whiteColor]];
        [_listTabelView setTag:2];
        _listTabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadListData)];
    }
    return _listTabelView;
}

- (UIButton *)bookButton {
    if (!_bookButton) {
        _bookButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bookButton setTitle:@"书籍" forState:UIControlStateNormal];
        [_bookButton setTitleColor:[Common colorWithHexString:@"#666666" alpha:1.0] forState:UIControlStateNormal];
        [_bookButton setTitleColor:[Common colorWithHexString:@"#1B88EE" alpha:1.0] forState:UIControlStateSelected];
        [_bookButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_bookButton sizeToFit];
        [_bookButton setCenter:[Common get414PointByMap:74+13.5 withY:39.5]];
        [_bookButton setTag:0];
        [_bookButton setSelected:YES];
        [_bookButton addTarget:self action:@selector(sliderButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bookButton;
}
- (UIButton *)bookListButton {
    if (!_bookListButton) {
        _bookListButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bookListButton setTitle:@"书单" forState:UIControlStateNormal];
        [_bookListButton setTitleColor:[Common colorWithHexString:@"#666666" alpha:1.0] forState:UIControlStateNormal];
        [_bookListButton setTitleColor:[Common colorWithHexString:@"#1B88EE" alpha:1.0] forState:UIControlStateSelected];
        [_bookListButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_bookListButton sizeToFit];
        [_bookListButton setCenter:[Common get414PointByMap:304+13.5 withY:39.5]];
        _bookListButton.tag = 1;
        [_bookListButton addTarget:self action:@selector(sliderButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bookListButton;
}
- (UIView *)sliderView {
    if (!_sliderView) {
        _sliderView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(61, 60, 52, 1)]];
        [_sliderView setBackgroundColor:[Common colorWithHexString:@"#1B88EE" alpha:1.0]];
    }
    return _sliderView;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 61, 414, 1)]];
        [_lineView setBackgroundColor:[Common colorWithHexString:@"#DDDDDD" alpha:1.0]];
    }
    return _lineView;
}
- (UIButton *)ruleButton {
    if (!_ruleButton) {
        _ruleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_ruleButton setFrame:[Common get414FrameByMap:CGRectMake(20, 14, 177, 32)]];
        [_ruleButton setBackgroundImage:[Common imageWithColor:[Common colorWithHexString:@"#B6D9FA" alpha:1.0] WithFrame:_ruleButton.bounds ]forState:UIControlStateSelected];
        [_ruleButton setBackgroundImage:[Common imageWithColor:[Common colorWithHexString:@"#F7F7F9" alpha:1.0] WithFrame:_ruleButton.bounds ]forState:UIControlStateNormal];
        [_ruleButton setTitleColor:[Common colorWithHexString:@"#69AFF0" alpha:1.0] forState:UIControlStateSelected];
        [_ruleButton setTitleColor:[Common colorWithHexString:@"#858E98" alpha:1.0] forState:UIControlStateNormal];
        [_ruleButton setTitle:@"按综合      " forState:UIControlStateNormal];
        [_ruleButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_ruleButton.layer setCornerRadius:7];
        [_ruleButton.layer setMasksToBounds:YES];
        [_ruleButton addTarget:self action:@selector(showRuleView) forControlEvents:UIControlEventTouchUpInside];
        
        [_ruleButton addSubview:({
            UIImageView *image = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(142, 13, 10, 6)]];
            [image setImage:[UIImage imageNamed:@"searchRuleDownIcon"]];
            self.ruleButtonImageView = image;
            image;
        })];
        
    }
    return _ruleButton;
}
- (UIButton *)selectButton {
    if (!_selectButton) {
        _selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectButton setFrame:[Common get414FrameByMap:CGRectMake(217, 14, 177, 32)]];
        [_selectButton setBackgroundImage:[Common imageWithColor:[Common colorWithHexString:@"#B6D9FA" alpha:1.0] WithFrame:_ruleButton.bounds ]forState:UIControlStateSelected];
        [_selectButton setBackgroundImage:[Common imageWithColor:[Common colorWithHexString:@"#F7F7F9" alpha:1.0] WithFrame:_ruleButton.bounds ]forState:UIControlStateNormal];
        [_selectButton setTitleColor:[Common colorWithHexString:@"#69AFF0" alpha:1.0] forState:UIControlStateSelected];
        [_selectButton setTitleColor:[Common colorWithHexString:@"#858E98" alpha:1.0] forState:UIControlStateNormal];
        [_selectButton setTitle:@"筛选     " forState:UIControlStateNormal];
        [_selectButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_selectButton.layer setCornerRadius:7];
        [_selectButton.layer setMasksToBounds:YES];
        [_selectButton addTarget:self action:@selector(showSelectView) forControlEvents:UIControlEventTouchUpInside];
        
        [_selectButton addSubview:({
            UIImageView *image = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(135, 13, 10, 6)]];
            [image setImage:[UIImage imageNamed:@"searchRuleDownIcon"]];
            self.selectButtonImageView = image;
            image;
        })];
    }
    return _selectButton;
}
- (UIView *)downView_Button_Line {
    if (!_downView_Button_Line) {
        _downView_Button_Line = [[UIView alloc] initWithFrame:CGRectMake(0, self.ruleView.frame.origin.y - 1, k_ScreenWidth, 0.7)];
        [_downView_Button_Line setBackgroundColor:[Common colorWithHexString:@"#DDDDDD" alpha:1.0]];
    }
    return _downView_Button_Line;
}
- (ZY_SearchRuleView *)ruleView {
    if (!_ruleView) {
        CGRect frame = [Common get414FrameByMap:CGRectMake(0, 60, 414, 55*5)];
        frame.origin.y = self.tableView.frame.origin.y;
        _ruleView = [[ZY_SearchRuleView alloc] initWithFrame:frame];
        [_ruleView setBackgroundColor:[UIColor whiteColor]];
        _ruleView.layer.masksToBounds = YES;
        _ruleView.hidden = YES;
        _ruleView.delegate = self;
    }
    return _ruleView;
}
- (ZY_SearchSelectView *)selectView {
    if (!_selectView) {
        _selectView = [[ZY_SearchSelectView alloc] initWithFrame:CGRectMake(0, self.tableView.frame.origin.y, k_ScreenWidth, self.view.frame.size.height - [Common get414RadiusByMap:122] - k_StatusBarHeight - k_NavigatiBarHeight)];
        [_selectView setBackgroundColor:[UIColor whiteColor]];
         _selectView.layer.masksToBounds = YES;
        _selectView.hidden = YES;
        _selectView.delegate = self;
    }
    return _selectView;
}

- (UIScrollView *)mainScroll {
    if (_mainScroll == nil) {
        _mainScroll = [[UIScrollView alloc] init];
        [_mainScroll setFrame:CGRectMake(0, [Common get414RadiusByMap:64], k_ScreenWidth, self.view.frame.size.height - [Common get414RadiusByMap:64])];
        [_mainScroll setContentSize:CGSizeMake(k_ScreenWidth*2, _mainScroll.frame.size.height)];
        [_mainScroll setPagingEnabled:YES];
        [_mainScroll setDelegate:self];
    }
    return _mainScroll;
}

- (NSMutableArray *)listDataArray {
    if (!_listDataArray) {
        _listDataArray = [NSMutableArray array];
    }
    return _listDataArray;
}
- (UIImageView *)noBookImageView {
    if (!_noBookImageView) {
        _noBookImageView = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(71, 256, 273, 201)]];
        [_noBookImageView setImage:[UIImage imageNamed:@"searchNoBook"]];
        [_noBookImageView setHidden:YES];
    }
    return _noBookImageView;
}

@end

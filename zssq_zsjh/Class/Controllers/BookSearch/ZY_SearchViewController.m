//
//  SearchViewController.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//
#import "ZY_SearchViewController.h"
#import "ZY_Common.h"
#import "ZY_SearchResultViewController.h"
#import "ZY_XPathParserConfig.h"
#import "ZY_HistoricalRecordView.h"
#import "ZY_DBHelper.h"
#import "NWS_httpClient.h"
#import "Common.h"
#import "ZY_BookDetailsViewController.h"

@interface ZY_SearchViewController ()<UITextFieldDelegate,ZY_HistoricalRecordViewDelegate>

@property(nonatomic,strong) UITextField *searchTxtField;

@property(nonatomic,strong) UILabel *labelTitle;



@property(nonatomic,strong) NSMutableArray *controlArray;

@property(nonatomic,strong) NSMutableArray *dataArray;

@property(nonatomic,strong) ZY_HistoricalRecordView *historicalView;

@property(nonatomic,strong) UIView *lineView; // 热门推荐上的lineView
@property (nonatomic, strong) UIButton *randomRecommendButton;
@property (nonatomic, strong) UILabel *hotRecommendLabel;
@property (nonatomic, strong) NSMutableArray *hotRecommendButtonArray;   // 储存button
@property (nonatomic, strong) NSMutableArray *hotRecommendDataArray;    // 储存全部热门推荐
@property (nonatomic, strong) NSMutableArray *hotRecommendDataSubArray;  // 储存显示出的热门推荐，最多六个

@end

@implementation ZY_SearchViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
     self.navigationItem.hidesBackButton = YES;
     self.navigationItem.leftBarButtonItem = nil;
    [self addLeftBar];
     self.controlArray = [NSMutableArray array];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    [self.view addSubview:self.labelTitle];
    //[self.view addSubview:self.lineView];
     self.dataArray = [@"圣墟,一念永恒,斗破苍穹,玄界之门,辰东,大主宰,天蚕土豆,我欲封天" componentsSeparatedByString:@","];
    [self createHotButton];
    
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    [self loadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PrivateFunction

-(void)loadData {
    // 搜索热词
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://api.zhuishushenqi.com/book/search-hotwords" parameters:nil success:^(id responseObject) {
        
        if (![responseObject isKindOfClass:[NSDictionary class]]) {
            return;
        }
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        NSNumber *okNumber = responseObject[@"ok"];
        if (!responseObject[@"ok"] || okNumber.boolValue == false) {
            return;
        }
        
        NSArray *hotWordArray = responseDict[@"searchHotWords"];
        if (hotWordArray.count > 8) {
            hotWordArray = [Common randomAccessData:hotWordArray WithSpecifiedNumber:8];
        }
        
        self.dataArray = nil;
        
        for (NSDictionary *dict in hotWordArray) {
            NSString *hotWordString = dict[@"word"];
            if (hotWordString && hotWordString.length > 0) {
                [self.dataArray addObject:hotWordString];
            }
        }
        
        for (UIButton *btn in self.controlArray) {
            [btn removeFromSuperview];
        }
        [self.controlArray removeAllObjects];
        [self createHotButton];
        
        // 加载热门推荐
        [self loadHotRecommend];
        
    } failure:^(NSError *error) {
        
        [self.view addSubview:self.historicalView];
        [self.historicalView reloadData];
    }];
    
}

- (void)recommendButtonClick:(UIButton *)sender {
    ZY_BookDetailsViewController *detailVC = [[ZY_BookDetailsViewController alloc] init];
    ZY_NovelInfo *info = [[ZY_NovelInfo alloc] init];
    info.novelId = self.hotRecommendDataSubArray[sender.tag][@"book"];
    info.novelName = self.hotRecommendDataSubArray[sender.tag][@"word"];
    info.isForbitToRead = NO;
    detailVC.w_novel = info;
    detailVC.spetialBackImageName = @"darkBack";
    detailVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)resetRecommendData {
    [MobClick event:@"search" label:@"换一批"];
    if (self.hotRecommendDataArray.count > 6) {
        self.hotRecommendDataSubArray = [Common randomAccessData:[self.hotRecommendDataArray mutableCopy] WithSpecifiedNumber:6];
    }
    [self createRecommendButtonWithArray:self.hotRecommendDataSubArray];
    
}

- (void)loadHotRecommend {
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://api.zhuishushenqi.com/book/hot-word" parameters:nil success:^(id responseObject) {
        
        if (![responseObject isKindOfClass:[NSDictionary class]]) {
            return;
        }
        NSDictionary *responseDict = (NSDictionary *)responseObject;
        NSNumber *okNumber = responseObject[@"ok"];
        if (!responseObject[@"ok"] || okNumber.boolValue == false) {
            return;
        }
        
        self.hotRecommendDataArray = responseDict[@"newHotWords"];
        
        NSMutableArray *hotWordArray = [responseDict[@"newHotWords"] mutableCopy];
        if (hotWordArray.count > 6) {
            hotWordArray = [Common randomAccessData:hotWordArray WithSpecifiedNumber:6];
        }
        
        if (hotWordArray.count > 0) {
            [self.view addSubview:self.lineView];
            [self.hotRecommendLabel sizeToFit];
            [self.hotRecommendLabel setCenter:CGPointMake([Common get414RadiusByMap:20 + 22.5], self.lineView.frame.origin.y + [Common get414RadiusByMap:20])];
            [self.view addSubview:self.hotRecommendLabel];
            [self.randomRecommendButton setCenter:CGPointMake(k_ScreenWidth - [Common get414RadiusByMap:18] - 20, self.hotRecommendLabel.center.y)];
            [self.view addSubview:self.randomRecommendButton];
            
            self.hotRecommendDataSubArray = hotWordArray;
            [self createRecommendButtonWithArray:hotWordArray];
            [self.historicalView reloadData];
        }
        [self.view addSubview:self.historicalView];
        
        
    } failure:^(NSError *error) {
        [self.view addSubview:self.historicalView];
        [self.historicalView reloadData];
    }];
}

- (void)createRecommendButtonWithArray:(NSArray *)arr {
    
    for (UIButton *btn in self.hotRecommendButtonArray) {
        [btn removeFromSuperview];
    }
    [self.hotRecommendButtonArray removeAllObjects];
    
    for (int i=0; i<arr.count; i++) {
        NSDictionary *dict = arr[i];
        if (![dict isKindOfClass:[NSDictionary class]] || !dict[@"word"] || !dict[@"book"]) {
            continue;
        }
        UIButton *recommendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [recommendButton setImage:[UIImage imageNamed:@"searchRecommend"] forState:UIControlStateNormal];
        NSString *str = [NSString stringWithFormat:@" %@",dict[@"word"]];
        [recommendButton setTitle:str forState:UIControlStateNormal];
        [recommendButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [recommendButton setTitleColor:[Common colorWithHexString:@"#636466" alpha:1.0] forState:UIControlStateNormal];
        [recommendButton setFrame:[Common get414FrameByMap:CGRectMake(22 + (i%2)*188, 233 + 35*(i/2), 140, 14)]];
        recommendButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [recommendButton setTag:i];
        [recommendButton addTarget:self action:@selector(recommendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:recommendButton];
        
        [self.hotRecommendButtonArray addObject:recommendButton];
    }
    
    
}

-(void)goPopBack{
    [self.searchTxtField endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

//点击空白处的手势要实现的方法
-(void)viewTapped:(UITapGestureRecognizer*)tap1
{
    [self.searchTxtField endEditing:YES];
}

-(void)clickButtonSearch:(id)sender{
    UIButton *btn = (UIButton*)sender;
    [MobClick event:@"search" label:@"搜索热词"];
    if (btn) {
        NSString *nsstrWord =[self.dataArray objectAtIndex:(NSInteger)btn.tag];

        [self.searchTxtField setText:nsstrWord];
        [self gotoSearchResult:nsstrWord];
    }
    [self.historicalView reloadData];
}

-(CGSize)getContentSize:(NSString*)content{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
    [button setTitle:content forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [button sizeToFit];
    
    return button.frame.size;
}

-(void)gotoSearchResult:(NSString*)nsstrKeyWord{
    
    [[ZY_DBHelper shareDBHelper] addHotWord:nsstrKeyWord];
    ZY_SearchResultViewController *searchResult = [[ZY_SearchResultViewController alloc] init];
    searchResult.keyWords=nsstrKeyWord;
    self.hidesBottomBarWhenPushed=YES;
    
    [self addChildViewController:searchResult];
    [self.view addSubview:searchResult.view];
    
   // [self.navigationController pushViewController:searchResult animated:YES]; 
}

-(void)createHotButton{
    CGFloat padding =15;
    CGFloat juanJu=8;
    
    CGFloat x=padding;
    CGFloat y=CGRectGetMaxY(self.labelTitle.frame)+[Common get414RadiusByMap:10];
    
    for (NSInteger i=0; i<self.dataArray.count; i++) {
        
        NSString *nsstrContent = [self.dataArray objectAtIndex:i];
        CGSize size = [self getContentSize:nsstrContent];
        CGFloat width = size.width;
        CGFloat height = size.height;
        
        UIButton *btn =nil;
        if (i>0&&(i-1)<self.controlArray.count) {
            btn = [self.controlArray objectAtIndex:i-1];
            x=CGRectGetMaxX(btn.frame)+juanJu;
            
            if ((x+width)>kCScreenWidth-padding) {
                y=CGRectGetMaxY(btn.frame)+12;
            }
            
            x=(x+width)>kCScreenWidth-padding?padding:x;
        }
        
        UIButton *labelTitle = [[UIButton alloc] initWithFrame:CGRectMake(x, y,width, height)];
        [labelTitle setTag:i];
        [labelTitle setTitle:nsstrContent forState:UIControlStateNormal];
        [labelTitle.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [labelTitle setBackgroundColor:[UIColor whiteColor]];
        [labelTitle setTitleColor:kRGBCOLOR(102, 102, 102) forState:UIControlStateNormal];
        labelTitle.layer.borderColor = [Common colorWithHexString:@"#EBEDF0" alpha:1.0].CGColor;
        labelTitle.layer.borderWidth=1;
        [labelTitle.layer setCornerRadius:height/2];
        [labelTitle addTarget:self action:@selector(clickButtonSearch:) forControlEvents:UIControlEventTouchUpInside];
        [self.controlArray addObject:labelTitle];
        [self.view addSubview:labelTitle];
    }
}

-(void)addLeftBar{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(-15, 0, 45, 40)];
    [button addTarget:self action:@selector(goPopBack) forControlEvents:UIControlEventTouchUpInside];
    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, 40)];
    [barView addSubview:self.searchTxtField];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (40-16)/2, 10,16)];
    [imageView setImage:[UIImage imageNamed:@"readerBack"]];
    [button addSubview:imageView];
    [barView addSubview:button];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:barView];
    self.navigationItem.leftBarButtonItem = backButton;
}

#pragma mark - ZY_HistoricalRecordViewDelegate

-(void)clickHistoricalWord:(NSString *)word{
    [self.searchTxtField setText:word];
    [self gotoSearchResult:word];
    [MobClick event:@"search" label:@"热门推荐"];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    [self gotoSearchResult:textField.text];
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    
    if (textField.text.length == 1 && string.length == 0) {
        for (UIViewController *resultVC in self.childViewControllers) {
            if ([resultVC isKindOfClass:[ZY_SearchResultViewController class]]) {
                [resultVC.view removeFromSuperview];
                [resultVC removeFromParentViewController];
            }
        }
    }
    
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField{
    
    for (UIViewController *resultVC in self.childViewControllers) {
        if ([resultVC isKindOfClass:[ZY_SearchResultViewController class]]) {
            [resultVC.view removeFromSuperview];
            [resultVC removeFromParentViewController];
        }
    }
    
    return YES;
    
}

#pragma mark - Getter or Setter 

-(UIColor *)randomColor{
    
    NSInteger randdomIndex = (arc4random() % self.dataArray.count);
    
    return randdomIndex==1?kRGBCOLOR(229, 20, 13):kRGBCOLOR(0, 126, 255);
}

-(UITextField *)searchTxtField{
    if (!_searchTxtField) {
         _searchTxtField = [[UITextField alloc] initWithFrame:CGRectMake(17, 6, kCScreenWidth-47, 28)];
        [_searchTxtField setTextAlignment:NSTextAlignmentLeft];
        [_searchTxtField setTextColor:kRGBCOLOR(51, 51, 51)];
        [_searchTxtField setBackgroundColor:[Common colorWithHexString:@"#F7F7F9" alpha:1.0]];
        [_searchTxtField setReturnKeyType:UIReturnKeyGo];
        [_searchTxtField setPlaceholder:@" 请输入书名、作者"];
        [_searchTxtField setTintColor:kRGBCOLOR(0, 122, 255)];
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 33, 15)];
        UIImageView *passwordImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchSearch"]];
        passwordImage.frame = CGRectMake(13, 0, 15, 15);
        [leftView addSubview:passwordImage];
         _searchTxtField.leftView = leftView;
         _searchTxtField.leftView.userInteractionEnabled = NO;
         _searchTxtField.leftViewMode = UITextFieldViewModeAlways;
    
         _searchTxtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        //[_searchTxtField setClearsOnBeginEditing:YES];
        [_searchTxtField setFont:[UIFont systemFontOfSize:13]];
        [_searchTxtField setDelegate:self];
        _searchTxtField.clearButtonMode = UITextFieldViewModeWhileEditing;
        // _searchTxtField.layer.borderColor = [kRGBCOLOR(210, 210, 210) CGColor];
        //_searchTxtField.layer.borderWidth=1;
        _searchTxtField.layer.cornerRadius = _searchTxtField.frame.size.height/2;
        //[_searchTxtField.layer setCornerRadius:4];
        [_searchTxtField becomeFirstResponder];
    }
    return _searchTxtField;
}

-(UILabel *)labelTitle{
    if (!_labelTitle) {
         _labelTitle=[[UILabel alloc]initWithFrame:CGRectMake(15,8,100, 45)];
        [_labelTitle setTextColor:kRGBCOLOR(51, 51, 51)];
        [_labelTitle setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_labelTitle setTextAlignment:NSTextAlignmentLeft];
        [_labelTitle setText:@"搜索热词"];
    }
    return _labelTitle;
}





-(ZY_HistoricalRecordView *)historicalView{
    if (!_historicalView) {
         UIButton *lastButton =((UIButton*)[self.hotRecommendButtonArray lastObject]);
        if (!lastButton) {
            lastButton = (UIButton *)[self.controlArray lastObject];
        }
         _historicalView = [[ZY_HistoricalRecordView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lastButton.frame)+36, kCScreenWidth, kCScreenHeight-CGRectGetMaxY(lastButton.frame))];
        [_historicalView setDelegate:self];
    }
    return _historicalView;
}

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (UIView *)lineView {
    if (_lineView == nil) {
        
        CGFloat lineViewY = 0;
        
        if (self.controlArray.count > 0) {
            UIView *view = [self.controlArray lastObject];
            lineViewY = CGRectGetMaxY(view.frame) + 17 +17;
        }
        
        _lineView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(20, lineViewY, 373, 1)]];
        [_lineView setBackgroundColor:[Common colorWithHexString:@"#EBEDF0" alpha:1.0]];
    }
    return _lineView;
}

- (UIButton *)randomRecommendButton {
    if (_randomRecommendButton == nil) {
        _randomRecommendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_randomRecommendButton setTitle:@"换一批" forState:UIControlStateNormal];
        [_randomRecommendButton setTitleColor:[Common colorWithHexString:@"#1B88EE" alpha:1.0] forState:UIControlStateNormal];
        [_randomRecommendButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_randomRecommendButton sizeToFit];
        [_randomRecommendButton addTarget:self action:@selector(resetRecommendData) forControlEvents:UIControlEventTouchUpInside];
    }
    return _randomRecommendButton;
}

- (UILabel *)hotRecommendLabel {
    if (_hotRecommendLabel == nil) {
        _hotRecommendLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 0, 0)]];
        [_hotRecommendLabel setText:@"热门推荐"];
        [_hotRecommendLabel setTextColor:[Common colorWithHexString:@"#353C46" alpha:1.0]];
        [_hotRecommendLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_hotRecommendLabel sizeToFit];
    }
    return _hotRecommendLabel;
}

- (NSMutableArray *)hotRecommendButtonArray {
    if (_hotRecommendButtonArray == nil) {
        _hotRecommendButtonArray = [NSMutableArray array];
    }
    return _hotRecommendButtonArray;
}

@end

//
//  ZY_ListDetailController.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/18.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZY_ListDetailController : UIViewController

@property (nonatomic, strong) NSString *listID;

@end

NS_ASSUME_NONNULL_END

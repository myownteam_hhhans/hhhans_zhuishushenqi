//
//  ZY_SearchResultViewController.h
//  ZYNovel
//
//  Created by S on 2017/3/15.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZY_SearchResultViewController : UIViewController

@property(nonatomic,strong) NSString *keyWords;

@end

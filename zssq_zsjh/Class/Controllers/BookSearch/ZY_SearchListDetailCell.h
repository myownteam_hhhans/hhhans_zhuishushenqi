//
//  ZY_SearchListDetailCell.h
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/18.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZY_NovelInfoTableViewCellDelegate <NSObject>

@optional

-(void)didClickShowAllBookDesc;

@end

@interface ZY_SearchListDetailCell : UITableViewCell


@property (nonatomic, weak) id<ZY_NovelInfoTableViewCellDelegate> delegate;

- (void)bindData:(ZY_NovelInfo *)info withIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END

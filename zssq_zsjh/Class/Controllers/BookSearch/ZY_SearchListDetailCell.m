//
//  ZY_SearchListDetailCell.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/18.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "ZY_SearchListDetailCell.h"
#import "Common.h"
#import "ZY_Common.h"
#import <UIImageView+WebCache.h>

@interface ZY_SearchListDetailCell ()

@property (nonatomic, strong) UILabel *indexLabel;
@property (nonatomic, strong) UILabel *bookNameLabel;
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UILabel *descLabel;

@property (nonatomic, strong) UILabel *labelNovelDes;

@property (nonatomic, strong) UIImageView *coverImage;
@property(nonatomic,strong) UIImageView *clickImageView;


@end

@implementation ZY_SearchListDetailCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.bookNameLabel];
        [self addSubview:self.countLabel];
        [self addSubview:self.descLabel];
        [self addSubview:self.coverImage];
        [self addSubview:self.clickImageView];
        [self addSubview:self.descLabel];
        
        UIView *greyView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 414, 8)]];
        [greyView setBackgroundColor:[Common colorWithHexString:@"#F0F0F3" alpha:1.0]];
        [self addSubview:greyView];
    }
    return self;
}

- (void)bindData:(ZY_NovelInfo *)info withIndex:(NSInteger)index {
    [self.coverImage sd_setImageWithURL:[NSURL URLWithString:info.novelCoverUrl]];
    [self.bookNameLabel setText:info.novelName];
    [self.countLabel setText:[NSString stringWithFormat:@"%ld人在追 | %ld万字",info.ratedNumber.integerValue,info.WordCountFormat.integerValue/10000]];
    
    CGFloat lineNumberCount = 3;
    CGFloat singleDesHeight = 14;
    CGFloat desContentHeight = singleDesHeight*lineNumberCount+lineNumberCount*kLineSpacing;
    [self.labelNovelDes setText:info.novelDes];
    [self.labelNovelDes setNumberOfLines:lineNumberCount];
    [self.labelNovelDes setFrame:CGRectMake(18, [Common get414RadiusByMap:136], kCScreenWidth-18*2, desContentHeight)];
    if ((info.novelContentDescHeight>singleDesHeight*3.5)&&lineNumberCount>0) {
        [self.clickImageView setFrame:CGRectMake(CGRectGetMaxX(self.labelNovelDes.frame)-25, CGRectGetMaxY(self.labelNovelDes.frame)-23, 28,28)];
        [self.clickImageView setHidden:NO];
    }
}

-(void)handleSingleFingerEvent:(id)sender{
    if ([self.delegate respondsToSelector:@selector(didClickShowAllBookDesc)]) {
        [self.delegate didClickShowAllBookDesc];
    }
}

#pragma mark - lazyLoad

- (UIImageView *)coverImage {
    if (!_coverImage) {
        _coverImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(315, 31, 73, 88)]];
        [_coverImage setContentMode:UIViewContentModeScaleAspectFill];
        [_coverImage.layer setShadowRadius:2];
        [_coverImage.layer setShadowOpacity:0.7];
        [_coverImage.layer setShadowColor:[UIColor blackColor].CGColor];
        [_coverImage.layer setShadowOffset:CGSizeZero];
    }
    return _coverImage;
}

- (UILabel *)bookNameLabel {
    if (!_bookNameLabel) {
        _bookNameLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(43, 43, 260, 19)]];
        [_bookNameLabel setTextAlignment:NSTextAlignmentLeft];
        [_bookNameLabel setTextColor:[Common colorWithHexString:@"#333333" alpha:1.0]];
        [_bookNameLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:19] weight:UIFontWeightHeavy]];
    }
    return _bookNameLabel;
}

- (UILabel *)countLabel {
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(44, 83, 280, 12)]];
        [_countLabel setTextAlignment:NSTextAlignmentLeft];
        [_countLabel setTextColor:[Common colorWithHexString:@"#999999" alpha:1.0]];
        [_countLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:12]]];
    }
    return _countLabel;
}

-(UILabel *)labelNovelDes{
    if (!_labelNovelDes) {
        _labelNovelDes =[[UILabel alloc] initWithFrame:CGRectZero];
        [_labelNovelDes setFont:[UIFont systemFontOfSize:14]];
        [_labelNovelDes setTextColor:kRGBCOLOR(102, 102, 102)];
        [_labelNovelDes setNumberOfLines:0];
        [_labelNovelDes setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleFingerEvent:)];
        [_labelNovelDes addGestureRecognizer:ges];
        
    }
    
    return _labelNovelDes;
}

-(UIImageView *)clickImageView{
    if (!_clickImageView) {
        _clickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 28, 28)];
        [_clickImageView setImage:[UIImage imageNamed:@"更多"]];
        [_clickImageView setHidden:YES];
        UITapGestureRecognizer *singleFingerOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleFingerEvent:)];
        //[_clickImageView addGestureRecognizer:singleFingerOne];
        [_clickImageView setUserInteractionEnabled:YES];
    }
    
    return _clickImageView;
}


@end

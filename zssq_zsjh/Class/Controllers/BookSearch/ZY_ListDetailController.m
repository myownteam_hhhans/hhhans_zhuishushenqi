//
//  ZY_ListDetailController.m
//  zssq_zsjh
//
//  Created by 包涵 on 2019/2/18.
//  Copyright © 2019 shiwei. All rights reserved.
//

#import "ZY_ListDetailController.h"
#import <MJRefresh.h>
#import "NWS_httpClient.h"
#import "Common.h"
#import <UIImageView+WebCache.h>
#import "ZY_NovelInfo.h"
#import "ZY_SearchListDetailCell.h"
#import "ZY_BookDetailsViewController.h"

@interface ZY_ListDetailController ()<UITableViewDelegate, UITableViewDataSource, ZY_NovelInfoTableViewCellDelegate>

@property (nonatomic, strong) UITableView *mainTable;
@property (nonatomic, strong) NSMutableArray *bookModelsArray;

// header上的控件
@property (nonatomic, strong) UIImageView *headerImage;
@property (nonatomic, strong) UILabel *headerNickNameLabel;
@property (nonatomic, strong) UILabel *headerListNameLabel;
@property (nonatomic, strong) UILabel *headerListDescLabel;

@end

@implementation ZY_ListDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.navigationItem.title = @"书单详情";
    [self.view addSubview:self.mainTable];
    
    [self loadData];
}

- (void)loadData {
    [[NWS_httpClient sharedInstance] getWithURLString:[NSString stringWithFormat:@"http://api.zhuishushenqi.com/book-list/%@",self.listID] parameters:nil success:^(NSDictionary * responseObject) {
        
        if (!responseObject[@"ok"] || !((NSNumber *)responseObject[@"ok"]).boolValue == YES ) {
            return;
        }
        
        NSDictionary *authorDict = responseObject[@"bookList"][@"author"];
        [self.headerImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.zhuishushenqi.com%@",authorDict[@"avatar"]]]];
        [self.headerNickNameLabel setText:authorDict[@"nickname"]];
        [self.headerListNameLabel setText:responseObject[@"bookList"][@"title"]];
        [self.headerListDescLabel setText:responseObject[@"bookList"][@"desc"]];
        
        NSArray *bookDictsArray = responseObject[@"bookList"][@"books"];
        if (![bookDictsArray isKindOfClass:[NSArray class]] || bookDictsArray.count == 0) {
            return;
        }
        for (NSDictionary *dict in bookDictsArray) {
            ZY_NovelInfo *info = [[ZY_NovelInfo alloc] init];
            info.novelId = dict[@"book"][@"_id"];
            info.novelName = dict[@"book"][@"title"];
            info.WordCountFormat = ((NSNumber *)dict[@"book"][@"wordCount"]).stringValue;
            NSString *string  = dict[@"book"][@"cover"];
            if (string && string.length > 8) {
                string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                string = [string substringFromIndex:7];
                info.novelCoverUrl = string;
            }
            info.novelDes = dict[@"comment"];
            info.ratedNumber = dict[@"book"][@"latelyFollower"];
            [self.bookModelsArray addObject:info];
            
        }
        [self.mainTable reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - tableView delegate & dataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bookModelsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZY_SearchListDetailCell *cell = [self.mainTable dequeueReusableCellWithIdentifier:@"detailCell"];
    [cell bindData:self.bookModelsArray[indexPath.row] withIndex:indexPath.row];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.delegate = self;
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 128;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZY_BookDetailsViewController *detailVC = [[ZY_BookDetailsViewController alloc] init];
    detailVC.w_novel = self.bookModelsArray[indexPath.row];
    detailVC.spetialBackImageName = @"darkBack";
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - lazyLoad
- (UITableView *)mainTable {
    if (!_mainTable) {
        _mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight - k_StatusBarHeight - k_NavigatiBarHeight) style:UITableViewStylePlain];
        [_mainTable setDelegate:self];
        [_mainTable setDataSource:self];
        [_mainTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_mainTable setBackgroundColor:[UIColor whiteColor]];
        
        [_mainTable registerClass:[ZY_SearchListDetailCell class] forCellReuseIdentifier:@"detailCell"];
        
        UIView *headerView = [[UIView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 414, 144)]];
        [headerView addSubview:self.headerImage];
        [headerView addSubview:self.headerNickNameLabel];
        [headerView addSubview:self.headerListNameLabel];
        [headerView addSubview:self.headerListDescLabel];
        
        [_mainTable setTableHeaderView:headerView];
    }
    return _mainTable;
}

- (UIImageView *)headerImage {
    if (!_headerImage) {
        _headerImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(27, 7, 38, 38)]];
        _headerImage.layer.cornerRadius = _headerImage.frame.size.height/2;
        _headerImage.layer.masksToBounds = YES;
    }
    return _headerImage;
}
- (UILabel *)headerNickNameLabel {
    if (!_headerNickNameLabel) {
        _headerNickNameLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(78, 20, 72, 13)]];
        [_headerNickNameLabel setTextAlignment:NSTextAlignmentLeft];
        [_headerNickNameLabel setTextColor:[Common colorWithHexString:@"#999999" alpha:1.0]];
        [_headerNickNameLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:13]]];
    }
    return _headerNickNameLabel;
}
- (UILabel *)headerListNameLabel {
    if (!_headerListNameLabel) {
        _headerListNameLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(29, 67, 354, 20)]];
        [_headerListNameLabel setTextAlignment:NSTextAlignmentLeft];
        [_headerListNameLabel setTextColor:[Common colorWithHexString:@"#333333" alpha:1.0]];
        [_headerListNameLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:20]]];
    }
    return _headerListNameLabel;
}
- (UILabel *)headerListDescLabel {
    if (!_headerListDescLabel) {
        _headerListDescLabel = [[UILabel alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(29, 110, 354, 14)]];
        [_headerListDescLabel setTextAlignment:NSTextAlignmentLeft];
        [_headerListDescLabel setTextColor:[Common colorWithHexString:@"#999999" alpha:1.0]];
        [_headerListDescLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:14]]];
    }
    return _headerListDescLabel;
}
- (NSMutableArray *)bookModelsArray {
    if (!_bookModelsArray) {
        _bookModelsArray = [NSMutableArray array];
    }
    return _bookModelsArray;
}

@end

//
//  ZY_SubListController.h
//  ZYNovel
//
//  Created by 包涵 on 2018/9/9.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZY_SubListController : UIViewController

@property (nonatomic, strong) NSDictionary *booksDic;
@property (nonatomic, strong) NSString *urlString;

@property (nonatomic, strong) NSString *nodeID;

@property (nonatomic, strong) NSArray *booksArray;

@end

//
//  ZY_SubListController.m
//  ZYNovel
//
//  Created by 包涵 on 2018/9/9.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ZY_SubListController.h"
#import "ZY_NovelInfo.h"
#import "ZY_CommonTableViewCell.h"
#import "ZY_BookDetailsViewController.h"
#import "CFT_parameterOnline.h"
#import "ZY_ADHelper.h"
#import "ZY_BookStoreAdTableViewCell.h"
#import "ZY_Common.h"
#import "NWS_httpClient.h"
#import <MJRefresh.h>
#import "Common.h"
#import <MTGSDK/MTGSDK.h>
#import "MTGNativeAdsViewCell.h"
#import <BUAdSDK/BUAdSDK.h>
#import "GDTNativeAd.h"

@interface ZY_SubListController () <UITableViewDelegate, UITableViewDataSource, MTGNativeAdManagerDelegate, GDTNativeAdDelegate>

@property (nonatomic, strong) UITableView *mainTableView;

@property (nonatomic, strong) NSMutableArray *dataSource;

@property(nonatomic,strong) ADModel *currentAd;

@property (nonatomic, strong) NSString *adType;

// M广告 已弃用
@property (nonatomic, strong) MTGNativeAdManager *nativeVideoAdManager;
@property (nonatomic, strong) MTGCampaign *compaign;
// 广点通
@property (nonatomic, strong) GDTNativeAd *GDTAd;
@property (nonatomic, strong) NSArray *GDTDataArray;

@end

@implementation ZY_SubListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    if ([[self.booksDic allKeys] containsObject:@"shortTitle"]) {
        self.navigationItem.title = self.booksDic[@"shortTitle"];
    } else {
        self.navigationItem.title = self.booksDic[@"title"];
    }
    
    NSArray *bookDicArray = self.booksDic[@"books"];
    for (NSDictionary *bookDic in bookDicArray) {
        ZY_NovelInfo *info = [[ZY_NovelInfo alloc] init];
        info.novelCategory = bookDic[@"minorCate"];
        info.novelAuthor = bookDic[@"author"];
        info.novelId = bookDic[@"_id"];
        info.novelName = bookDic[@"title"];
        NSString *string  = bookDic[@"cover"];
        if (string && string.length > 8) {
            string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            string = [string substringFromIndex:7];
            info.novelCoverUrl = string;
        }
        
        info.novelDes = bookDic[@"shortIntro"];
        [self.dataSource addObject:@[@"commonCellIdentifier",info]];
        
    }
    
    self.adType = [[Common shareCommon] randomADTypeWithKey:@"bookflow"];
    
    if ([self.adType isEqualToString:@"3"] && [[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookflow"] && [Common shareCommon].zs_Open) {
        NSDictionary *gdtKeyAndID = [[ZY_ADHelper shareADHelper] getGDTKeyWithADPlace:@"bookflow"];
        self.GDTAd = [[GDTNativeAd alloc] initWithAppkey:gdtKeyAndID[@"ID"] placementId:gdtKeyAndID[@"KEY"]];
        self.GDTAd.delegate = self;
        self.GDTAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
        [self.GDTAd loadAd:1];
    }
    // 也得加入广告
    if ([[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookflow"] && [Common shareCommon].zs_Open && self.dataSource.count >= 3) {
        
        if (![self.adType isEqualToString:@"3"]) {
            AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:self.adType];
            self.currentAd = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_BookShelf adPlatform:platform];
        }
        
        if (self.currentAd && self.dataSource.count >= 3) {
            [self.dataSource insertObject:@[@"ad",self.currentAd] atIndex:2];
        }
    }

    
    [self.view addSubview:self.mainTableView];
    
    if (bookDicArray.count == 0) {
        [self.mainTableView.mj_header beginRefreshing];
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadLocolData {
    
    [self.dataSource removeAllObjects];
    
    
    for (NSDictionary *bookDic in self.booksArray) {
        ZY_NovelInfo *info = [[ZY_NovelInfo alloc] init];
        info.novelCategory = bookDic[@"cat"];
        info.novelAuthor = bookDic[@"author"];
        info.novelId = bookDic[@"_id"];
        info.novelName = bookDic[@"title"];
        NSString *string  = bookDic[@"cover"];
        string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if (string.length > 7) {
            string = [string substringFromIndex:7];
        } 
        
        info.novelCoverUrl = string;
        info.novelDes = bookDic[@"shortIntro"];
        [self.dataSource addObject:@[@"commonCellIdentifier",info]];
    }
    

    if ([[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookflow"] && [Common shareCommon].zs_Open) {
        
        if (![self.adType isEqualToString:@"3"]) {
            AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:self.adType];
            self.currentAd = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_BookShelf adPlatform:platform];
        }
        
        if (self.currentAd && self.dataSource.count >= 3) {
            [self.dataSource insertObject:@[@"ad",self.currentAd] atIndex:2];
        }
    }
    
    [self.mainTableView reloadData];
    [self.mainTableView.mj_header endRefreshing];
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

- (void)loadData {
    
    if (self.booksArray) {
        [self loadLocolData];
        return;
    }
    
    [[ZY_ADHelper shareADHelper] requestLomiPic];
    
    if (self.nodeID) {
        self.urlString = [NSString stringWithFormat:@"https://b.zhuishushenqi.com/category/booklist?ajax=ajax&node=%@&size=50&st=1",self.nodeID];
    }
    
    [[NWS_httpClient sharedInstance] getWithURLString:self.urlString parameters:nil success:^(NSDictionary *responseObject) {
        
        [self.dataSource removeAllObjects];
        
        NSArray *bookDicArray;
        
        if ([responseObject valueForKey:@"ranking"]) {
            self.booksDic = (NSDictionary *)responseObject[@"ranking"];
            bookDicArray = self.booksDic[@"books"];
        } else {
            bookDicArray = responseObject[@"book"];
        }

        // title
        if ([[self.booksDic allKeys] containsObject:@"shortTitle"]) {
            self.navigationItem.title = self.booksDic[@"shortTitle"];
        } else {
            self.navigationItem.title = self.booksDic[@"title"];
        }
        // 书
        
        for (NSDictionary *bookDic in bookDicArray) {
            ZY_NovelInfo *info = [[ZY_NovelInfo alloc] init];
            info.novelCategory = bookDic[@"minorCate"];
            info.novelAuthor = bookDic[@"author"];
            info.novelId = bookDic[@"_id"];
            info.novelName = bookDic[@"title"];
            NSString *string  = bookDic[@"cover"];
            string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if (self.nodeID && string.length > 35) {
                string = [string substringFromIndex:34];
            } else if(string && string.length > 8) {
                string = [string substringFromIndex:7];
            }
            
            info.novelCoverUrl = string;
            info.novelDes = bookDic[@"shortIntro"];
            [self.dataSource addObject:@[@"commonCellIdentifier",info]];
        }
        
        // 广告
        if ([[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookflow"] && [Common shareCommon].zs_Open) {
            
            if (![self.adType isEqualToString:@"3"]) {
                AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:self.adType];
                self.currentAd = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_BookShelf adPlatform:platform];
            }
            if (self.currentAd && self.dataSource.count >= 3) {
                [self.dataSource insertObject:@[@"ad",self.currentAd] atIndex:2];
            }
        }

        [self.mainTableView reloadData];
        [self.mainTableView.mj_header endRefreshing];
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
    } failure:^(NSError *error) {
        [self.mainTableView.mj_header endRefreshing];
    }];
}

#pragma mark AdManger delegate
- (void)nativeAdsLoaded:(NSArray *)nativeAds nativeManager:(nonnull MTGNativeAdManager *)nativeManager
{
    
    if (nativeAds.count > 0) {
        MTGCampaign *campain=nativeAds[0];
        self.compaign = campain;
        
        if (self.dataSource.count != 0) {
            [self loadData];
        }

    }
    
}

- (void)nativeAdsFailedToLoadWithError:(NSError *)error nativeManager:(nonnull MTGNativeAdManager *)nativeManager
{
    
}

- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray {
    self.GDTDataArray = nativeAdDataArray;
    self.currentAd = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_BookShelf];
    [self.mainTableView.mj_header beginRefreshing];
}

#pragma mark ==== delegate ====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *info = self.dataSource[indexPath.row];
    NSString *identifier = info[0];
    UITableViewCell *cell;
    if ([identifier isEqualToString:@"commonCellIdentifier"]) {
        cell = [self.mainTableView dequeueReusableCellWithIdentifier:@"commonCellIdentifier"];
        [(ZY_CommonTableViewCell *)cell bindNovelItem:self.dataSource[indexPath.row][1]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    } else if ([identifier isEqualToString:@"ad"]) {
        
        [Common showDebugHUD:self.adType];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        cell = [self.mainTableView dequeueReusableCellWithIdentifier:@"ad"];
        [(ZY_BookStoreAdTableViewCell*)cell bindAdItem:self.currentAd];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // luomi汇报
        if ([self.adType isEqualToString:@"5"]) {
            // 展示汇报
            NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiPic;
            [[ZY_ADHelper shareADHelper] upLoadLomiShowWithURL:kaipingDic[@"count_url"]];
        
        }
        // 头条注册点击
        if ([self.adType isEqualToString:@"4"]) {
            BUNativeAd *ad = self.currentAd.data;
            [ad registerContainer:cell withClickableViews:cell.subviews];
         
        }
        
        if ([self.adType isEqualToString:@"3"]) {
           
            [self.GDTAd attachAd:self.currentAd.data toView:cell];
        }
        
    }
    return cell;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 144;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *info = self.dataSource[indexPath.row];
    NSString *identifier = info[0];
    if ([identifier isEqualToString:@"commonCellIdentifier"]) {
        ZY_BookDetailsViewController *zyDetailViewController = [[ZY_BookDetailsViewController alloc] init];
        zyDetailViewController.w_novel = self.dataSource[indexPath.row][1];
        zyDetailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:zyDetailViewController animated:YES];
    } else {
        if (self.currentAd) {
  
            [[ZY_ADHelper shareADHelper] tapAD:self.currentAd CurrentVC:[UIApplication sharedApplication].keyWindow.rootViewController];
            
            if ([self.adType isEqualToString:@"5"]) {
               
            }
            if ([self.adType isEqualToString:@"4"]) {
               
            }
            if ([self.adType isEqualToString:@"3"]) {
               
            }
            
        }
    }
    
}

#pragma mark ==== lazyLoad ====
- (UITableView *)mainTableView {
    if (_mainTableView == nil) {
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight - k_StatusBarHeight - k_NavigatiBarHeight) style:UITableViewStylePlain];
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        [_mainTableView registerClass:[ZY_CommonTableViewCell class] forCellReuseIdentifier:@"commonCellIdentifier"];
        [_mainTableView registerClass:[ZY_BookStoreAdTableViewCell class] forCellReuseIdentifier:@"ad"];
        
        // M广告
        [_mainTableView registerNib:[UINib nibWithNibName:@"MTGNativeAdsViewCell" bundle:nil] forCellReuseIdentifier:@"MTGNativeAdsViewCell"];
        
        if(@available(iOS 11.0, *)) {
            _mainTableView.estimatedRowHeight = 0;
            _mainTableView.estimatedSectionFooterHeight = 0;
            _mainTableView.estimatedSectionHeaderHeight = 0;
            _mainTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        _mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    }
    return _mainTableView;
}
- (NSMutableArray *)dataSource {
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}
@end

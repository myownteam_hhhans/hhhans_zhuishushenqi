//
//  BookStoreViewController.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_BookStoreViewController.h"
#import "ZY_SearchViewController.h"
#import "ZY_Common.h"
#import "ZY_HeadEntity.h"
#import "ZY_BookRankViewController.h"
#import "ZY_ShuDanViewController.h"
#import "BookStoreCell.h"
#import "F_TitleView.h"
#import "ZY_SubListController.h"
#import <MJRefresh.h>

@interface ZY_BookStoreViewController () <UITableViewDelegate,UITableViewDataSource,titleClickDelegate>


@property(nonatomic,strong) UIButton *searchBtn;

@property(nonatomic,strong) NSMutableArray *rankHeadArr;

@property(nonatomic,strong) NSString *sex;

@property (nonatomic, strong) F_TitleView *myNavTitleView;

@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UITableView *femaleTableView;
@property (nonatomic, strong) UITableView *maleTableView;
@property (nonatomic, strong) UITableView *collectionTableView;

// 数据源数组
@property (nonatomic, strong) NSMutableArray *totalArray;

@property (nonatomic, strong) NSArray *maleNameArray;
@property (nonatomic, strong) NSArray *maleURLArray;

@property (nonatomic, strong) NSArray *femaleNameArray;
@property (nonatomic, strong) NSArray *femaleURLArray;

@property (nonatomic, strong) NSArray *collectionNameArray;
@property (nonatomic, strong) NSArray *collectionURLArray;

@end

@implementation ZY_BookStoreViewController

#pragma mark - Life cycle

- (void)viewDidLoad {

    
    [super viewDidLoad]; 
    
    [self addTitleView];
    
    CGFloat width =  ScreenSize.width - 100;
    if (kCScreenWidth > 376) {
        width -= 20;
    }
    
    // 导航title
    self.myNavTitleView = [[F_TitleView alloc] initWithFrame:CGRectMake(0, 0, width , 44)];
    self.myNavTitleView.delegate = self;
    self.navigationItem.titleView= self.myNavTitleView;
    
    [self.myNavTitleView clickSexButton:0];
    
    
    [self.view addSubview:self.mainScrollView];
    [self.mainScrollView addSubview:self.femaleTableView];
    [self.mainScrollView addSubview:self.maleTableView];
    [self.mainScrollView addSubview:self.collectionTableView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - PrivateFunction
-(void)gotoRank:(id)sender{
    int i = (int)((UIButton *)sender).tag - 500;
    NSString *info = [[_rankHeadArr objectAtIndex:i] title];
    
    if (i == 0) {//推荐书单
        ZY_ShuDanViewController *rank = [[ZY_ShuDanViewController alloc]init];
        rank.sex = self.sex;
        rank.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:rank animated:YES];
        return;
    }
    ZY_BookRankViewController *rank = [[ZY_BookRankViewController alloc]init];
    rank.sex = self.sex;
    rank.titleInfo = [NSString stringWithFormat:@"%@排行榜",info];
    rank.urlKey = [[_rankHeadArr objectAtIndex:i] urlKey];
    [self.navigationController pushViewController:rank animated:YES];
}
-(UIView *)addHeadView{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, 100)];
    float padding = (kCScreenWidth-144)/5;
    for (int i = 0; i < 4; i++) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(padding*(i+1)+36*i, 20, 36, 66)];
        btn.tag = 500+i;
        [btn addTarget:self action:@selector(gotoRank:) forControlEvents:UIControlEventTouchUpInside];
        UIImageView *img_tuijian = [[UIImageView alloc]initWithImage:[UIImage imageNamed:[[_rankHeadArr objectAtIndex:i] title]]];
        img_tuijian.frame = CGRectMake(0, 0, 36, 36);
        [btn addSubview:img_tuijian];
        
        UILabel *lbl_tuijian = [[UILabel alloc]initWithFrame:CGRectMake(0, 38, 36, 30)];
        lbl_tuijian.text = [[_rankHeadArr objectAtIndex:i] title];
        lbl_tuijian.font = [UIFont systemFontOfSize:12];
        lbl_tuijian.textAlignment = NSTextAlignmentCenter;
        [btn addSubview:lbl_tuijian];
        [headView addSubview:btn];
    }
    
    UIView *v_xian = [[UIView alloc]initWithFrame:CGRectMake(0, 99, kCScreenWidth, 1)];
    v_xian.backgroundColor = RGB(242, 242, 242);
    [headView addSubview:v_xian];
    
    return headView;
}

-(void)addTitleView{
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setText:@"排行"];
    [titleLabel setFont:[UIFont systemFontOfSize:10]];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    UIButton *btnsearch = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 44)];
    [btnsearch setImage:[UIImage imageNamed:@"storeSearch"] forState:UIControlStateNormal];
    [btnsearch addTarget:self action:@selector(gotoSearchController) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnsearch];
}
-(void)SegmentedChange:(id)sender{
    UISegmentedControl *seg = sender;
    if (seg.selectedSegmentIndex == 0) {
        self.sex = @"man";//男生
    }else{
        self.sex = @"lady";//女生
    }

//    _bookList.BaseMethodUrl = [NSString stringWithFormat: @"https://shuapi.jiaston.com/top/%@/top/commend/%%@/%%i.html",self.sex];
//    [_bookList loadData];
}
-(void)addNavBar{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(-10, 10, 71, 17)];
    [imageview setImage:[UIImage imageNamed:@"精选小说"]];
    
    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, 40)];
    [barView addSubview:imageview];
    [barView addSubview:self.searchBtn];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:barView];
    self.navigationItem.leftBarButtonItem = backButton;
}



-(void)gotoSearchController{
    [MobClick event:@"ranking" label:@"搜索"];
    ZY_SearchViewController *searchVC = [[ZY_SearchViewController alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
     self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searchVC animated:YES];
     self.hidesBottomBarWhenPushed=NO;
}

- (void)refresh:(MJRefreshHeader *)sender {
    
    
    for (UIView *subView in self.mainScrollView.subviews) {
        if (subView.tag == sender.tag && [subView isKindOfClass:[UITableView class]]) {
            [(UITableView *)subView reloadData];
        }
    }
    
    [sender endRefreshing];
}

-(UIButton *)searchBtn{
    if (!_searchBtn) {
         _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(71, 6, kCScreenWidth-101, 28)];
        [_searchBtn.titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_searchBtn setBackgroundColor:[UIColor whiteColor]];
        [_searchBtn setTitleColor:kRGBCOLOR(112, 112, 112) forState:UIControlStateNormal];
         UIImage *image = [UIImage imageNamed:@"search"]; 
         UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, (28-image.size.height)/2, image.size.width, image.size.height)];
        [iconImageView setImage:image];
        [_searchBtn addSubview:iconImageView];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(iconImageView.frame)+3, CGRectGetMinY(iconImageView.frame),kCScreenWidth-(CGRectGetMaxX(iconImageView.frame)+3+30), CGRectGetHeight(iconImageView.frame))];
        [label setFont:[UIFont systemFontOfSize:13]];
        [label setTextColor:kRGBCOLOR(112, 112, 112)];
        [label setTextAlignment:NSTextAlignmentLeft];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setText:@"请输入书名称或作者名"];
        [_searchBtn addSubview:label];
        
        [_searchBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
         _searchBtn.layer.borderColor = [kRGBCOLOR(244, 244, 244) CGColor];
         _searchBtn.layer.borderWidth=1;
        [_searchBtn.layer setCornerRadius:4];
        [_searchBtn addTarget:self action:@selector(gotoSearchController) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _searchBtn;
}
#pragma mark ==== delegate ====

- (void)buttonClickWithSex:(NSInteger)sex {
    
    [UIView animateWithDuration:0.3 animations:^{
        self.mainScrollView.contentOffset = CGPointMake(kCScreenWidth * sex, 0);
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *nameArr;
    
    if (tableView.tag == 0) {

        nameArr = self.femaleNameArray;

    } else if (tableView.tag == 1) {

        nameArr = self.maleNameArray;

    } else if (tableView.tag == 2) {

        nameArr = self.collectionNameArray;

    }
    
    return nameArr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BookStoreCell *cell;
    NSArray *nameArr;
    NSArray *urlArr;
    
    if (tableView.tag == 0) {
        cell = [self.femaleTableView dequeueReusableCellWithIdentifier:@"femaleBookStoreCellIdentifier"];
        nameArr = self.femaleNameArray;
        urlArr = self.femaleURLArray;
    } else if (tableView.tag == 1) {
        cell = [self.maleTableView dequeueReusableCellWithIdentifier:@"maleBookStoreCellIdentifier"];
        nameArr = self.maleNameArray;
        urlArr = self.maleURLArray;
    } else if (tableView.tag == 2) {
        cell = [self.collectionTableView dequeueReusableCellWithIdentifier:@"collectionBookStoreCellIdentifier"];
        nameArr = self.collectionNameArray;
        urlArr = self.collectionURLArray;
    }
    

    [cell bindDataWithName:nameArr[indexPath.row] urlString:urlArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView.tag == 0) {
        
        NSString *label = [NSString stringWithFormat:@"女生_%ld",indexPath.row];
        [MobClick event:@"ranking" label:label];
        
    } else if (tableView.tag == 1) {
        
        NSString *label = [NSString stringWithFormat:@"男生_%ld",indexPath.row];
        [MobClick event:@"ranking" label:label];
        
    } else if (tableView.tag == 2) {
        
        NSString *label = [NSString stringWithFormat:@"精品_%ld",indexPath.row];
        [MobClick event:@"ranking" label:label];
        
    }
    
    BookStoreCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    ZY_SubListController *subVC = [[ZY_SubListController alloc] init];
    subVC.booksDic = cell.dataDic;
    subVC.hidesBottomBarWhenPushed = YES;
    subVC.urlString = cell.urlString;
    
    [self.navigationController pushViewController:subVC animated:YES];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.mainScrollView.contentOffset.x < kCScreenWidth - 1) {
        [self.myNavTitleView clickSexButton:0];
    } else if (self.mainScrollView.contentOffset.x < kCScreenWidth*2 - 1 && self.mainScrollView.contentOffset.x > kCScreenWidth - 1) {
        [self.myNavTitleView clickSexButton:1];
    } else {
        [self.myNavTitleView clickSexButton:2];
    }
}

#pragma mark ==== 懒加载 ====

- (UITableView *)femaleTableView {
    if (_femaleTableView == nil) {
        _femaleTableView = [[UITableView alloc] initWithFrame:CGRectMake(kCScreenWidth, 0, kCScreenWidth, kCScreenHeight-k_NavigatiBarHeight-k_StatusBarHeight-k_TabBarHeight) style:UITableViewStylePlain];
        _femaleTableView.delegate = self;
        _femaleTableView.dataSource = self;
        [_femaleTableView registerClass:[BookStoreCell class] forCellReuseIdentifier:@"femaleBookStoreCellIdentifier"];
        _femaleTableView.tag = 0;
        _femaleTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh:)];
        _femaleTableView.mj_header.tag = 0;
    }
    return _femaleTableView;
}
- (UITableView *)maleTableView {
    if (_maleTableView == nil) {
        _maleTableView = [[UITableView alloc] initWithFrame:self.mainScrollView.bounds style:UITableViewStylePlain];
        CGRect frame = _maleTableView.frame;
        frame.origin.x = 0;
        _maleTableView.frame = frame;
        _maleTableView.delegate = self;
        _maleTableView.dataSource = self;
        [_maleTableView registerClass:[BookStoreCell class] forCellReuseIdentifier:@"maleBookStoreCellIdentifier"];
        _maleTableView.tag = 1;
        _maleTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh:)];
        _maleTableView.mj_header.tag = 1;
    }
    return _maleTableView;
}
- (UITableView *)collectionTableView {
    if (_collectionTableView == nil) {
        _collectionTableView = [[UITableView alloc] initWithFrame:self.mainScrollView.bounds style:UITableViewStylePlain];
        CGRect frame = _collectionTableView.frame;
        frame.origin.x = kCScreenWidth*2;
        _collectionTableView.frame = frame;
        _collectionTableView.delegate = self;
        _collectionTableView.dataSource = self;
        [_collectionTableView registerClass:[BookStoreCell class] forCellReuseIdentifier:@"collectionBookStoreCellIdentifier"];
        _collectionTableView.tag = 2;
        _collectionTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh:)];
        _collectionTableView.mj_header.tag = 2;
    }
    return _collectionTableView;
}

- (NSArray *)maleURLArray {
    if (_maleURLArray == nil) {
        _maleURLArray = @[
                               @"https://api.zhuishushenqi.com/ranking/54d42d92321052167dfb75e3",
                               @"https://api.zhuishushenqi.com/ranking/54d42e72d9de23382e6877fb",
                               @"https://api.zhuishushenqi.com/ranking/54d43709fd6ec9ae04184aa5",
                               @"https://api.zhuishushenqi.com/ranking/564547c694f1c6a144ec979b",
                               @"https://api.zhuishushenqi.com/ranking/564eb878efe5b8e745508fde",
                               @"https://api.zhuishushenqi.com/ranking/564d820bc319238a644fb408",
                               @"https://api.zhuishushenqi.com/ranking/564d8494fe996c25652644d2",
                               @"https://api.zhuishushenqi.com/ranking/564d898f59fd983667a5e3fa",
                               @"https://api.zhuishushenqi.com/ranking/564d8a004a15bb8369d9e28d",
                               @"https://api.zhuishushenqi.com/ranking/564eb12c3edb8b45511139ff",
                               @"https://api.zhuishushenqi.com/ranking/564eea0b731ade4d6c509493",
                               @"https://api.zhuishushenqi.com/ranking/564eee3ea82e3ada6f14b195",
                               @"https://api.zhuishushenqi.com/ranking/564eeeabed24953671f2a577"];

    }
    return _maleURLArray;
}
- (NSArray *)maleNameArray {
    if (_maleNameArray == nil) {
        _maleNameArray = @[
                                @"最热男频：top100",
                                @"本周潜力男榜",
                                @"本周潜力榜",
                                @"留存率男榜top100",
                                @"完结男榜top100",
                                @"最热榜月榜男",
                                @"最热榜总榜男",
                                @"留存榜月榜男",
                                @"留存榜总榜男",
                                @"完结榜月榜男",
                                @"完结榜总榜男",
                                @"本周潜力月榜男",
                                @"本周潜力总榜男",
                           ];
    }
    return _maleNameArray;
}
- (NSArray *)femaleURLArray {
    if (_femaleURLArray == nil) {
        _femaleURLArray = @[
                            
                                 @"https://api.zhuishushenqi.com/ranking/54d43437d47d13ff21cad58b",
                                 
                                 @"https://api.zhuishushenqi.com/ranking/54d43709fd6ec9ae04184aa5",

                                 @"https://api.zhuishushenqi.com/ranking/5645482405b052fe70aeb1b5",

                                 @"https://api.zhuishushenqi.com/ranking/564eb8a9cf77e9b25056162d",
                                 
                                 @"https://api.zhuishushenqi.com/ranking/54d43709fd6ec9ae04184aa5",

                                 @"https://api.zhuishushenqi.com/ranking/564d853484665f97662d0810",
                                 
                                 @"https://api.zhuishushenqi.com/ranking/564d85b6dd2bd1ec660ea8e2",

                                 @"https://api.zhuishushenqi.com/ranking/564d8b6b36d10ccd6951195d",
                                 
                                 @"https://api.zhuishushenqi.com/ranking/564d8c37752bcca16a976168",

                                 @"https://api.zhuishushenqi.com/ranking/564ee8ec146f8f1739777740",

                                 @"https://api.zhuishushenqi.com/ranking/564eeae6c3345baa6bf06e38",

                                 @"https://api.zhuishushenqi.com/ranking/564eee77e3a44c9f0e5fd7ae",

                                 @"https://api.zhuishushenqi.com/ranking/564eeeca5e6ba6ae074f10ec"];
    }
    return _femaleURLArray;
}
- (NSArray *)femaleNameArray {
    
    if (_femaleNameArray == nil) {
        _femaleNameArray = @[
                                @"最热女频：top100",
                                @"本周潜力榜",
                                @"留存率女榜top100",
                                @"完结女榜top100",
                                @"本周潜力女榜",
                                @"最热榜月榜女",
                                @"最热榜总榜女",
                                @"留存榜月榜女",
                                @"留存榜总榜女",
                                @"完结榜月榜女",
                                @"完结榜总榜女",
                                @"本周潜力月榜女",
                                @"本周潜力总榜女",
                             ];
    }
    return _femaleNameArray;
}
- (NSArray *)collectionURLArray {
    if (_collectionURLArray == nil) {
        _collectionURLArray = @[
                                
                                @"https://api.zhuishushenqi.com/ranking/564ef4f985ed965d0280c9c2",
                                
                                @"https://api.zhuishushenqi.com/ranking/564d8003aca44f4f61850fcd",
                                
                                @"https://api.zhuishushenqi.com/ranking/564d80457408cfcd63ae2dd0",
                                
                                @"https://api.zhuishushenqi.com/ranking/54d430e9a8cb149d07282496",
                                
                                @"https://api.zhuishushenqi.com/ranking/54d4306c321052167dfb75e4",
                                
                                @"https://api.zhuishushenqi.com/ranking/54d430962c12d3740e4a3ed2",
                                
                                @"https://api.zhuishushenqi.com/ranking/54d4312d5f3c22ae137255a1",
                                
                                @"https://api.zhuishushenqi.com/ranking/582fb5c412a401a20ea50275",
                                
                                @"https://api.zhuishushenqi.com/ranking/564d80d0e8c613016446c5aa",
                                
                                @"https://api.zhuishushenqi.com/ranking/564d81151109835664770ad7",
                                
                                @"https://api.zhuishushenqi.com/ranking/550b841715db45cd4b022107",
                                
                                @"https://api.zhuishushenqi.com/ranking/550b836229cd462830ff4d1d",
                                
                                @"https://api.zhuishushenqi.com/ranking/550b8397de12381038ad8c0b",
                                
                                @"https://api.zhuishushenqi.com/ranking/5732aac02dbb268b5f037fc4"
                                ];
    }
    return _collectionURLArray;
}
- (NSArray *)collectionNameArray {
    if (_collectionNameArray == nil) {
        _collectionNameArray = @[
                                 @"百度热搜榜",
                                 @"掌阅热销榜",
                                 @"书旗热搜榜",
                                 @"17k鲜花榜",
                                 @"起点月票榜",
                                 @"纵横月票榜",
                                 @"和阅读原创榜",
                                 @"圣诞热搜榜",
                                 @"掌阅热销榜",
                                 @"书旗热搜榜",
                                 @"17k订阅榜",
                                 @"起点粉红票榜",
                                 @"潇湘月票榜",
                                 @"逐浪点击榜",
                                 ];
    }
    return _collectionNameArray;
}
- (NSMutableArray *)totalArray {
    if (_totalArray == nil) {
        _totalArray = [NSMutableArray array];
    }
    return _totalArray;
}
- (UIScrollView *)mainScrollView {
    if (_mainScrollView == nil) {
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight - k_StatusBarHeight - k_NavigatiBarHeight - k_TabBarHeight)];
        [_mainScrollView setContentSize:CGSizeMake(kCScreenWidth * 3, 1)];
        [_mainScrollView setDelegate:self];
        [_mainScrollView setPagingEnabled:YES];
        [_mainScrollView setBounces:NO];
        [_mainScrollView setShowsHorizontalScrollIndicator:NO];
    }
    return _mainScrollView;
}

@end

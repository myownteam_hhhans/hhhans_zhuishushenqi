//
//  M_DownloaderController.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/16.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "M_DownloaderController.h"
#import "LS_DownloadInfoManager.h"
#import "M_DownloadInfoCell.h"
#import "ZY_NovelDownLoadOperation.h"
#import "ZY_BookHelp.h"
//#import "M_BookInfo.h"
#import "ZY_Common.h"
#import "My_NoDataView.h"
#import "Common.h"

@interface M_DownloaderController () <UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

// 顶部按钮
@property (nonatomic, strong) UIView *pageControl;
@property (nonatomic, strong) UIButton *downloadedButton;
@property (nonatomic, strong) UIButton *downloadingButton;
@property (nonatomic, strong) UIView *sliderView;

// 底部按钮
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) UIButton *pauseButton;

// 两个table
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UITableView *downloadedTable;
@property (nonatomic, strong) UITableView *downloadingTable;

// 当前下载进度
@property (nonatomic, assign) NSInteger downloadProgress;
// 正在下载第几本书
@property (nonatomic, assign) NSInteger downloaddingIndex;

@property (nonatomic, strong) My_NoDataView *noDataLeft;
@property (nonatomic, strong) My_NoDataView *noDataRight;

@end

@implementation M_DownloaderController

#pragma mark ==== Life Circle ====

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"下载管理";
    
    [self setupChildVC];
    // 点击第一个按钮
    if (self.downloadedFirst) {
        [self pageButtonClick:self.downloadedButton];
    } else {
        [self pageButtonClick:self.downloadingButton];
    }
    
    // 监听下载管理
    [[NSNotificationCenter defaultCenter] addObserverForName:@"chaptersStartDownload" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self reloadData];
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"chapterEndDownload" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self reloadData];
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"downloadProgressChange" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        
        NSNumber *number = note.object;
        
        self.downloadProgress = number.integerValue;
        NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
        if ([LS_DownloadInfoManager shareDownloadInfoManager].downloadingArray.count > path.row) {
            [self.downloadingTable reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    }];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"chapterEndDownload" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        [self reloadData];
    }];
    
    [self reloadData];
    
}


- (void)reloadData {

    [self.downloadedTable reloadData];
    [self.downloadingTable reloadData];
    
    NSInteger leftCount = [LS_DownloadInfoManager shareDownloadInfoManager].downloadedArray.count;
    self.noDataLeft.hidden = leftCount == 0 ? NO : YES;
    self.downloadedTable.hidden = leftCount == 0 ? YES : NO;
    
    NSInteger rightCount = [LS_DownloadInfoManager shareDownloadInfoManager].downloadingArray.count;
    self.noDataRight.hidden = rightCount == 0 ? NO : YES;
    self.downloadingTable.hidden = rightCount == 0 ? YES : NO;
    self.bottomView.hidden = rightCount == 0 ? YES : NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"chapterEndDownload" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadProgressChange" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"chaptersStartDownload" object:nil];
}

#pragma mark ==== 初始化 ====
- (void)setupChildVC {
    [self.view addSubview:self.pageControl];
    [self.view addSubview:self.mainScrollView];
    
    [self.mainScrollView addSubview:self.noDataLeft];
    [self.mainScrollView addSubview:self.noDataRight];
    
    [self.view addSubview:self.bottomView];
}

- (void)pageButtonClick:(UIButton *)sender {
    
    // 选中状态
    if (sender.tag == 0) {
        self.downloadedButton.selected = YES;
        self.downloadingButton.selected = NO;
    } else {
        self.downloadedButton.selected = NO;
        self.downloadingButton.selected = YES;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        // 移动scrollView
        self.mainScrollView.contentOffset = CGPointMake(k_ScreenWidth * sender.tag, 0);
        // 移动滑块
        CGPoint sliderCenter = self.sliderView.center;
        sliderCenter.x = sender.center.x;
        self.sliderView.center = sliderCenter;
    }];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 隐藏bottom
    CGFloat offsetX = scrollView.contentOffset.x;
    CGFloat bottomOffsetY = k_TabBarHeight * (offsetX/k_ScreenWidth);
    self.bottomView.transform = CGAffineTransformMakeTranslation(0, -bottomOffsetY);
}

#pragma mark ==== 私有方法 ====
- (void)deleteAllItem {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确定删除全部下载任务？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self pauseAllItem:self.pauseButton];
        ZY_NovelDownLoadOperation *downOP = [ZY_BookHelp shareBookHelp].novelDownLoadQueue.operations.firstObject;
        downOP.isPaused = NO;
        [[ZY_BookHelp shareBookHelp].novelDownLoadQueue cancelAllOperations];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];

}
- (void)pauseAllItem:(UIButton *)sender {
    
    if ([LS_DownloadInfoManager shareDownloadInfoManager].downloadingArray.count <= 0) {
        return;
    }
    
    ZY_NovelDownLoadOperation *downOP = [ZY_BookHelp shareBookHelp].novelDownLoadQueue.operations.firstObject;
    downOP.isPaused = !sender.selected;
    sender.selected = !sender.selected;
}




#pragma mark ==== scrollViewDelegate ====
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    if (scrollView.contentOffset.x == 0) {
        [self pageButtonClick:self.downloadedButton];
    } else {
        [self pageButtonClick:self.downloadingButton];
    }
}

#pragma mark ==== tableView ====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView.tag == 0) {
        return [LS_DownloadInfoManager shareDownloadInfoManager].downloadedArray.count;
    } else {
        return [LS_DownloadInfoManager shareDownloadInfoManager].downloadingArray.count;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 117;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    M_DownloadInfoCell *cell;
    
    if (tableView.tag == 0) {
        
        cell = [self.downloadedTable dequeueReusableCellWithIdentifier:@"downloadedCell"];
        if ([LS_DownloadInfoManager shareDownloadInfoManager].downloadedArray.count > indexPath.row) {
            LSYReadModel *model = [LS_DownloadInfoManager shareDownloadInfoManager].downloadedArray[indexPath.row];
            [cell bindDataWithModel:model];
            [cell.downloadInfoLabel setText:[NSString stringWithFormat:@"已下载%ld章",model.downloadedNumber]];
        }
        
    } else {
        
        cell = [self.downloadingTable dequeueReusableCellWithIdentifier:@"downloadingCell"];
        if ([LS_DownloadInfoManager shareDownloadInfoManager].downloadingArray.count > indexPath.row) {
            [cell bindDataWithModel:[LS_DownloadInfoManager shareDownloadInfoManager].downloadingArray[indexPath.row]];
        }
        if (indexPath.row == self.downloaddingIndex) {
            [cell.downloadInfoLabel setText:[NSString stringWithFormat:@"已下载%ld%%",self.downloadProgress]];
        } else {
            [cell.downloadInfoLabel setText:[NSString stringWithFormat:@"等待下载"]];
        }
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 1) {
        return;
    }
    
    
    
    LSYReadModel *readModel = [LS_DownloadInfoManager shareDownloadInfoManager].downloadedArray[indexPath.row];
//    M_BookInfo *bookInfo = [M_BookInfo converNovelInfoToBookInfo:readModel.novelInfo];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoReadByNovel" object:readModel.novelInfo];
    
}

#pragma mark ==== 懒加载 ====
- (UIView *)pageControl {
    if (_pageControl == nil) {
        _pageControl = [[UIView alloc] init];
        [_pageControl setFrame:CGRectMake(0, 0, k_ScreenWidth, 44)];
        [_pageControl addSubview:self.downloadedButton];
        [_pageControl addSubview:self.downloadingButton];
        [_pageControl addSubview:self.sliderView];
    }
    return _pageControl;
}
- (UIButton *)downloadedButton {
    if (_downloadedButton == nil) {
        _downloadedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_downloadedButton setTitle:@"下载完成" forState:UIControlStateNormal];
        [_downloadedButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_downloadedButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_downloadedButton setTitleColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0] forState:UIControlStateSelected];
        [_downloadedButton setTag:0];
        [_downloadedButton setFrame:CGRectMake(0, 0, k_ScreenWidth/2, 44)];
        [_downloadedButton addTarget:self action:@selector(pageButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _downloadedButton;
}
- (UIButton *)downloadingButton {
    if (_downloadingButton == nil) {
        _downloadingButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_downloadingButton setTitle:@"正在下载" forState:UIControlStateNormal];
        [_downloadingButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_downloadingButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_downloadingButton setTitleColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0] forState:UIControlStateSelected];
        [_downloadingButton setTag:1];
        [_downloadingButton setFrame:CGRectMake(k_ScreenWidth/2, 0, k_ScreenWidth/2, 44)];
        [_downloadingButton addTarget:self action:@selector(pageButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _downloadingButton;
}
- (UIView *)sliderView {
    if (_sliderView == nil) {
        _sliderView = [[UIView alloc] initWithFrame:CGRectMake(0, 44 - 3, 18, 3)];
        [_sliderView setBackgroundColor:[Common colorWithHexString:@"48BEFE" alpha:1.0]];
        [_sliderView.layer setCornerRadius:1.5];
    }
    return _sliderView;
}

- (UIScrollView *)mainScrollView {
    if (_mainScrollView == nil) {
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, k_ScreenWidth, k_ScreenHeight - k_StatusBarHeight - 88)];
        _mainScrollView.contentSize = CGSizeMake(k_ScreenWidth * 2, 1);
        _mainScrollView.pagingEnabled = YES;
        _mainScrollView.delegate = self;
        _mainScrollView.bounces = NO;
        
        [_mainScrollView addSubview:self.downloadedTable];
        [_mainScrollView addSubview:self.downloadingTable];
    }
    return _mainScrollView;
}

- (UIView *)bottomView {
    if (_bottomView == nil) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, k_ScreenHeight - k_NavigatiBarHeight - k_StatusBarHeight, k_ScreenWidth, k_TabBarHeight)];
        
        [_bottomView addSubview:self.deleteButton];
        [_bottomView addSubview:self.pauseButton];
        [_bottomView setBackgroundColor:[UIColor whiteColor]];
        [_bottomView addSubview:({
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(k_ScreenWidth/2 - 0.5, 11, 1, 24)];
            view.backgroundColor = [Common colorWithHexString:@"D3D3D3" alpha:1.0];
            view;
        })];
        [_bottomView addSubview:({
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, 1)];
            [view setBackgroundColor:[Common colorWithHexString:@"#D3D3D3" alpha:1.0]];
            view;
        })];
    }
    return _bottomView;
}
- (UIButton *)deleteButton {
    if (_deleteButton == nil) {
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setFrame:CGRectMake(0, 0, k_ScreenWidth/2, 49)];
        [_deleteButton setTitle:@"全部删除" forState:UIControlStateNormal];
        [_deleteButton setTitleColor:[Common colorWithHexString:@"00BF99" alpha:1.0] forState:UIControlStateNormal];
        [_deleteButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_deleteButton addTarget:self action:@selector(deleteAllItem) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteButton;
}
- (UIButton *)pauseButton {
    if (_pauseButton == nil) {
        _pauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pauseButton setFrame:CGRectMake(k_ScreenWidth/2, 0, k_ScreenWidth/2, 49)];
        [_pauseButton setTitle:@"全部暂停" forState:UIControlStateNormal];
        [_pauseButton setTitle:@"全部开始" forState:UIControlStateSelected];
        [_pauseButton setTitleColor:[Common colorWithHexString:@"00BF99" alpha:1.0] forState:UIControlStateNormal];
        [_pauseButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_pauseButton addTarget:self action:@selector(pauseAllItem:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pauseButton;
}

- (UITableView *)downloadedTable {
    if (_downloadedTable == nil) {
        _downloadedTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight - 88 - k_StatusBarHeight) style:UITableViewStylePlain];
        _downloadedTable.delegate = self;
        _downloadedTable.dataSource = self;
        [_downloadedTable registerClass:[M_DownloadInfoCell class] forCellReuseIdentifier:@"downloadedCell"];
        _downloadedTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _downloadedTable.tag = 0;
        _downloadedTable.bounces = NO;
        _downloadedTable.hidden = YES;
    }
    return _downloadedTable;
}

- (UITableView *)downloadingTable {
    if (_downloadingTable == nil) {
        _downloadingTable = [[UITableView alloc] initWithFrame:CGRectMake(k_ScreenWidth, 0, k_ScreenWidth, k_ScreenHeight - 88 - k_StatusBarHeight) style:UITableViewStylePlain];
        _downloadingTable.delegate = self;
        _downloadingTable.dataSource = self;
        [_downloadingTable registerClass:[M_DownloadInfoCell class] forCellReuseIdentifier:@"downloadingCell"];
        _downloadingTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _downloadingTable.tag = 1;
        _downloadingTable.bounces = NO;
        _downloadingTable.hidden = YES;
        _downloadingTable.contentInset = UIEdgeInsetsMake(0, 0, k_TabBarHeight, 0);
    }
    return _downloadingTable;
}

- (My_NoDataView *)noDataLeft {
    if (_noDataLeft == nil) {
        _noDataLeft = [[My_NoDataView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight - 88 - k_StatusBarHeight)];
    }
    return _noDataLeft;
}
- (My_NoDataView *)noDataRight {
    if (_noDataRight == nil) {
        _noDataRight = [[My_NoDataView alloc] initWithFrame:CGRectMake(k_ScreenWidth, 0, k_ScreenWidth, k_ScreenHeight - 88 - k_StatusBarHeight)];
    }
    return _noDataRight;
}
@end

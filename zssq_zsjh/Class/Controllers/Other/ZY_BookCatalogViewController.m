//
//  ZY_BookCatalogViewController.m
//  ZYNovel
//
//  Created by apple on 2018/6/26.
//

#import "ZY_BookCatalogViewController.h"
#import "LSYDottedLine.h"
#import "LSYChapterTableViewCell.h"
#import "ZY_Common.h"
#import "Common.h"


@interface ZY_BookCatalogViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
@property (nonatomic,strong) UITableView *tabView;
@property (nonatomic) NSUInteger readChapter;
@property (nonatomic,strong) UIButton *btnQuick;

@property (nonatomic,assign) BOOL isScrollToEnd;

@property (nonatomic,assign) float rowHeight;

@end

@implementation ZY_BookCatalogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"目录";
    self.rowHeight = 49;
    [self.view addSubview:self.tabView];
    [self.view addSubview:self.btnQuick];
    self.isScrollToEnd = YES;
}


-(UIButton *)btnQuick{
    if (!_btnQuick) {
        _btnQuick = [[UIButton alloc] initWithFrame:CGRectZero];
        [_btnQuick setImage:[UIImage imageNamed:@"向下"] forState:UIControlStateNormal];
        [_btnQuick addTarget:self action:@selector(clickEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnQuick;
}

-(UITableView *)tabView
{
    if (!_tabView) {
        _tabView = [[UITableView alloc] init];
        _tabView.delegate = self;
        _tabView.dataSource = self;
        _tabView.backgroundColor = RGB(217, 217, 217);
        _tabView.backgroundColor = [UIColor whiteColor];
        _tabView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [_tabView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"chapterCell"];
        
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth,    49)];
        UILabel *headLabel = [[UILabel alloc] init];
        [headLabel setFont:[UIFont systemFontOfSize:14]];
        [headLabel setTextColor:[Common colorWithHexString:@"#A1A1B3" alpha:1.0]];
        [headLabel setText:[NSString stringWithFormat:@"共%ld章",self.readModel.chapters.count]];
        if (self.readModel.novelInfo.novelStatus) {
            [headLabel setText:[NSString stringWithFormat:@"%@  共%ld章",self.readModel.novelInfo.novelStatus,self.readModel.chapters.count]];
        }
        [headLabel sizeToFit];
        [headView addSubview:headLabel];
        [headLabel setCenter:CGPointMake(headLabel.frame.size.width/2 + 15, headView.frame.size.height/2)];
        _tabView.tableHeaderView = headView;
        
    }
    return _tabView;
}


#pragma mark -PrivateFunction

-(void)clickEvent:(id)sender{
    if (self.isScrollToEnd)
    {
        [self.tabView setContentOffset:CGPointMake(0, self.tabView.contentSize.height - self.tabView.bounds.size.height) animated:YES];
    }else{
        [self.tabView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGPoint contentOffsetPoint = self.tabView.contentOffset;
    CGRect frame = self.tabView.frame;
    if (contentOffsetPoint.y == self.tabView.contentSize.height - frame.size.height || self.tabView.contentSize.height < frame.size.height)
    {
        [self.btnQuick setImage:[UIImage imageNamed:@"向上"] forState:UIControlStateNormal];
        self.isScrollToEnd=NO;
    }else{
        [self.btnQuick setImage:[UIImage imageNamed:@"向下"] forState:UIControlStateNormal];
        self.isScrollToEnd = YES;
    }
}

#pragma mark - UITableView Delagete DataSource

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (_firstDisplay) {
        _firstDisplay = NO;
        CGFloat offsetY = _readModel.record.chapter * self.rowHeight - tableView.frame.size.height/2;
        if (offsetY > _readModel.chapters.count * self.rowHeight - tableView.frame.size.height) {
            offsetY = _readModel.chapters.count * self.rowHeight - tableView.frame.size.height;
        } else if (offsetY < 0) {
            offsetY = 0;
        }
        tableView.contentOffset = CGPointMake(0,offsetY);
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _readModel.chapters.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"chapterCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    
    NSString *nsstrTitle =_readModel.chapters[indexPath.row].title;
    NSString *content = _readModel.chapters[indexPath.row].content;
    UIColor *bgColor =RGB(153, 153, 153);
    UIColor *titleColor =RGB(102, 102, 102);
    
    if (content && ![content isEqualToString:@""]) {
        bgColor = RGB(11, 202, 34);
    }
    
//    if (indexPath.row == _readModel.record.chapter) {
//        titleColor = RGB(229, 20, 13);
//        bgColor = RGB(229, 20, 13);
//    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    [cell.textLabel setText:[NSString stringWithFormat:@"      %@",nsstrTitle]];
    [cell.textLabel setTextColor:[Common colorWithHexString:@"#C6C6CC" alpha:1.0]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  self.rowHeight;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    self.readModel.record.chapter = indexPath.row;
    [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoReadFromCatelog" object:self.readModel];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _tabView.frame = CGRectMake(0, 0, ViewSize(self.view).width, ViewSize(self.view).height);
    self.btnQuick.frame = CGRectMake(ViewSize(self.view).width-46, ViewSize(self.view).height-36, 36,36);
    
}
@end

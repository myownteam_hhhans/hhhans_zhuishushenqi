//
//  BookDetailsViewController.m
//  ZYNovel
//
//

#import "ZY_BookDetailsViewController.h"
#import "ZY_Common.h"
#import "ZY_NovelInfo.h"
#import "ZY_DataDownloader.h"
#import "ZY_XPathParser.h"
#import "ZY_NovelInfoTableViewCell.h"
#import "ZY_NovelRecommendTableViewCell.h"
#import "MJRefresh.h"
#import "UITableView+JRTableViewPlaceHolder.h"
#import "ZY_DetailBookAdTableViewCell.h"
#import "ZY_AdEntity.h"
#import "SHW_ADInterstitial.h"
#import "RegexKitLite.h"
#import "ZY_XPathParserConfig.h"
#import "SDWebImageManager.h"
#import "CFT_parameterOnline.h"
#import "ZY_ADHelper.h"
#import "ZY_JsonParser.h"
#import "ZY_DBHelper.h"
#import "SVProgressHUD.h"
#import "ZY_NovelDownLoadOperation.h"
#import "ZY_BookHelp.h"
#import "ZY_SameUserTableViewCell.h"
#import "NWS_httpClient.h"
#import "ZY_SubListController.h"
#import "ZY_NeedFiveStarView.h"
#import "LS_DownloadInfoManager.h"
#import "Common.h"
#import <MTGSDK/MTGSDK.h>
#import "MTGNativeVideoCell.h"
#import <MTGSDKReward/MTGRewardAdManager.h>
#import <BUAdSDK/BUBannerAdView.h>
#import <BUAdSDK/BUAdSDK.h>
#import "GDTNativeAd.h"
#import "NSObject+DZM.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
//#import "BD_BookInfoCell.h"

static NSString *novelInfoCellIdentifier = @"novelInfoCellIdentifier";

static NSString *adCellIdentifier = @"adCellIdentifier";

static NSString *recommendCellIdentifier = @"recommendCellIdentifier";

static NSString *sameUserCellIdentifier = @"sameUserCellIdentifier";

@interface ZY_BookDetailsViewController ()<UITableViewDelegate,UITableViewDataSource,ZY_DataDownloaderDelegate,ZY_NovelInfoTableViewCellDelegate,ZY_NovelRecommendTableViewCellDelegate,ZY_SameUserTableViewCellDelegate,MTGNativeAdManagerDelegate,MTGRewardAdLoadDelegate,MTGRewardAdShowDelegate,BUBannerAdViewDelegate,GDTNativeAdDelegate,BURewardedVideoAdDelegate,UIScrollViewDelegate>

@property(nonatomic,strong) UITableView *mainTableView;

@property(nonatomic,strong) ZY_NovelInfo *novelInfo;

@property(nonatomic,strong) NSMutableArray *dataSource;
@property(nonatomic,strong) NSMutableArray *allSameAuthurBook;

@property(nonatomic,strong) ZY_DataDownloader *downLoader;

@property (nonatomic,strong) ADModel *adEntity;

@property (nonatomic,strong) UIButton *btnAddBookShelf;
@property (nonatomic,strong) UIButton *btnReadNow;
@property (nonatomic,strong) UIButton *btnDownLoad;

@property(nonatomic,assign) NSInteger lineNumberCount;

@property (nonatomic, assign) BOOL showMoreSameAurhurBook;
@property (nonatomic, assign) BOOL showMoreSimilerBook;

@property (nonatomic, strong) UIView *v_foot;

// 在线参数
@property (nonatomic, strong) NSDictionary *paramentOnline;

// 加强广告
@property (nonatomic, strong) MTGNativeAdManager *nativeVideoAdManager;
@property (nonatomic, strong) MTGCampaign *compaign;

@property (nonatomic, strong) NSString *adType;

@property (nonatomic, strong) GDTNativeAd *GDTAd;
@property (nonatomic, strong) NSArray *GDTDataArray;

// 激励视频实例
@property (nonatomic, strong) BURewardedVideoAd *rewardedVideoAd;

@end

@implementation ZY_BookDetailsViewController

#pragma mark - Life cycle

-(UIButton *)btnAddBookShelf{
    if (!_btnAddBookShelf) {
        _btnAddBookShelf = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,kCScreenWidth/3, 49)];
        _btnAddBookShelf.backgroundColor = RGB(240, 240, 240);
        
        if (![Common shareCommon].zs_Open) {
            [_btnAddBookShelf setFrame:CGRectMake(0, 0,kCScreenWidth/2, 49)];
        }
        
        [_btnAddBookShelf setTitle:@"加入书架" forState:UIControlStateNormal];
        [_btnAddBookShelf.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_btnAddBookShelf setTitleColor:kRGBCOLOR(229, 20, 13) forState:UIControlStateNormal];
        [_btnAddBookShelf addTarget:self action:@selector(clickAddBookOnTheBookShelf:) forControlEvents:UIControlEventTouchUpInside];
        [_btnAddBookShelf setImage:[UIImage imageNamed:@"加入书架"] forState:UIControlStateNormal];
        [_btnAddBookShelf setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 5)];
        
    }
    
    return _btnAddBookShelf;
}
-(UIButton *)btnDownLoad{
    if (!_btnDownLoad) {
        _btnDownLoad = [[UIButton alloc] initWithFrame:CGRectMake(kCScreenWidth-kCScreenWidth/3, 0,kCScreenWidth/3, 49)];
        _btnDownLoad.backgroundColor = RGB(240, 240, 240);
        
        [_btnDownLoad setTitle:@"缓存全本" forState:UIControlStateNormal];
        [_btnDownLoad.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_btnDownLoad setTitleColor:kRGBCOLOR(229, 20, 13) forState:UIControlStateNormal];
        [_btnDownLoad addTarget:self action:@selector(downLoadAll) forControlEvents:UIControlEventTouchUpInside];
        [_btnDownLoad setImage:[UIImage imageNamed:@"下载全本"] forState:UIControlStateNormal];
        [_btnDownLoad setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 5)];
        
        if (![Common shareCommon].zs_Open) {
            [_btnDownLoad setHidden:YES];
        }
    }
    return _btnDownLoad;
}

-(UIButton *)btnReadNow{
    if (!_btnReadNow) {
        _btnReadNow = [[UIButton alloc] initWithFrame:CGRectMake(kCScreenWidth/3, 0,kCScreenWidth/3, 49)];
        if (![Common shareCommon].zs_Open) {
            [_btnReadNow setFrame:CGRectMake(kCScreenWidth/2, 0,kCScreenWidth/2, 49)];
        }
        
        _btnReadNow.backgroundColor = kRGBCOLOR(211, 61, 60);
        [_btnReadNow setTitle:@"开始阅读" forState:UIControlStateNormal];
        [_btnReadNow setImage:[UIImage imageNamed:@"开始阅读"] forState:UIControlStateNormal];
        [_btnReadNow.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_btnReadNow addTarget:self action:@selector(gotoRead) forControlEvents:UIControlEventTouchUpInside];
        [_btnReadNow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnReadNow setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 5)];
    }
    return _btnReadNow;
}

-(void)addFooterView{
    self.v_foot = [[UIView alloc]initWithFrame:CGRectMake(0, self.mainTableView.frame.size.height-49, kCScreenWidth, 49)];
    [self.v_foot setHidden:YES];
    [self.v_foot addSubview:self.btnAddBookShelf];
    [self.v_foot addSubview:self.btnReadNow];
    [self.v_foot addSubview:self.btnDownLoad];
    [self.view addSubview:self.v_foot];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.adType = [[Common shareCommon] randomADTypeWithKey:@"Details"];
    //self.title=kAPP_Name;
    self.lineNumberCount=3;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.mainTableView];
    [self addFooterView];
    self.navigationItem.rightBarButtonItem = [self shareButttonItem];
    
    
    // 初始化激励视频
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postRewardVideo) name:@"postRewardVideo" object:nil];
    NSString* type = [self.paramentOnline objectForKey:[NSString stringWithFormat:@"好评弹窗%@", kAPP_VERSION]];
    if ([Common shareCommon].zs_Open && [type isEqualToString:@"1"]) {
        BURewardedVideoModel *model = [[BURewardedVideoModel alloc] init];
        self.rewardedVideoAd = [[BURewardedVideoAd alloc] initWithSlotID:TouTiao_Reward rewardedVideoModel:model];
        self.rewardedVideoAd.delegate = self;
        [self.rewardedVideoAd loadAdData];
    } else if ([Common shareCommon].zs_Open && [type isEqualToString:@"2"]) {
        [[GADRewardBasedVideoAd sharedInstance] loadRequest:[GADRequest request]
                                               withAdUnitID:@"ca-app-pub-9664827263749384/1659834675"];
    }
    
    NSString *adtype = self.adType;
    AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:adtype];
    
    // 先强制谷歌,测试谷歌广告
    if ([Common shareCommon].zs_Open && [[ZY_ADHelper shareADHelper]getOpenStatusForAD:@"Details"]) {
        self.adEntity = [[ADModel alloc] init];
        self.adEntity.platform = adplatform_Google;
    }
    
    
    if ([adtype isEqualToString:@"5"] && [Common shareCommon].zs_Open && [[ZY_ADHelper shareADHelper]getOpenStatusForAD:@"Details"]) {
        [[ZY_ADHelper shareADHelper] requestLomiGif];
        self.adEntity = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_Gif adPlatform:platform];
    } else if ([[ZY_ADHelper shareADHelper]getOpenStatusForAD:@"Details"] && [adtype isEqualToString:@"4"]) {
        self.adEntity = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_Feed adPlatform:platform];
    } else if ([[ZY_ADHelper shareADHelper]getOpenStatusForAD:@"Details"] && [adtype isEqualToString:@"3"]) {
        NSDictionary *gdtKeyAndID = [[ZY_ADHelper shareADHelper] getGDTKeyWithADPlace:@"Details"];
        self.GDTAd = [[GDTNativeAd alloc] initWithAppkey:gdtKeyAndID[@"ID"] placementId:gdtKeyAndID[@"KEY"]];
        self.GDTAd.delegate = self;
        self.GDTAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
        [self.GDTAd loadAd:3];
    }
    
    __weak __typeof(self)weakSelf = self;
    
    [weakSelf.mainTableView jr_configureWithPlaceHolderBlock:^UIView * _Nonnull(UITableView * _Nonnull sender) {
        UIView *errorView = [ZY_Common createLoadingErrorViewWithFrame:weakSelf.mainTableView.bounds withTarget:weakSelf.mainTableView.mj_header action:@selector(beginRefreshing)];
        [errorView setHidden:YES];
        
        return errorView;
    } normalBlock:^(UITableView * _Nonnull sender) {
        
    }];
    
    [self.mainTableView.mj_header beginRefreshing];
    if ([[ZY_DBHelper shareDBHelper] queryBookIsOnTheBookShelf:self.w_novel.novelId]) {
        [self setAddButtonBookShelfStateWithShowHud:NO];
    }
}


-(void)goPopBack{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addLeftBar{
    
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 44)];
    [button addTarget:self action:@selector(goPopBack) forControlEvents:UIControlEventTouchUpInside];
    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 44)];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (40-16)/2, 10,16)];
    [imageView setImage:[UIImage imageNamed:@"返回"]];
    
    if (self.spetialBackImageName) {
        [imageView setImage:[UIImage imageNamed:self.spetialBackImageName]];
    }
    
    [button addSubview:imageView];
    [barView addSubview:button];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:barView];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - PrivateFunction

- (void)pushToMoreSameAuthurList {
    ZY_SubListController *subVC = [[ZY_SubListController alloc] init];
    
    NSDictionary *dic = @{
                          @"title" : self.novelInfo.novelAuthor,
                          @"books" : self.allSameAuthurBook
                          };

    subVC.booksDic = dic;
    subVC.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:subVC animated:YES];
}

-(void)novelShare{
    if (self.novelInfo) {
   
        NSString *textToShare = self.novelInfo.novelName;
        NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
        NSString *icon = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
        UIImage *imageToShare = [UIImage imageNamed:icon];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        NSString* key = [manager cacheKeyForURL:[NSURL URLWithString:self.novelInfo.novelCoverUrl]];
        SDImageCache* cache = [SDImageCache sharedImageCache];
        UIImage *cacheImage = [cache imageFromDiskCacheForKey:key];
        
        if (cacheImage!=nil) {
            imageToShare=cacheImage;
        }
        
        NSURL *urlToShare = [NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@?mt=8",kAppId]];
        NSArray *activityItems = @[textToShare, imageToShare, urlToShare];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeMessage,UIActivityTypeMail,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];
        
        if ([activityVC respondsToSelector:@selector(popoverPresentationController)]) {
            activityVC.popoverPresentationController.sourceView = self.view;
        }
        
        [self presentViewController:activityVC animated:YES completion:nil];
    }
}

-(void)loadRequest{
    ZY_WS(weakSelf);
    
    [weakSelf.mainTableView.jr_placeHolderView setHidden:YES];
    NSString *detailUrl = [NSString stringWithFormat: @"https://api.zhuishushenqi.com/book/%@",self.w_novel.novelId];
    [self.downLoader getNovelHtmlDataByUrl:detailUrl];
}
-(void)loadSimilerBooks {
    
    NSString *silimerURL = [NSString stringWithFormat:@"http://api03icqj.zhuishushenqi.com/book/%@/recommend?packageName=com.zsreader.sdk",self.novelInfo.novelId];
    
    [[NWS_httpClient sharedInstance] getWithURLString:silimerURL parameters:nil success:^(id responseObject) {
       
        NSMutableArray *AllBooksArray = responseObject[@"books"];
        NSInteger randomCount = AllBooksArray.count;
        randomCount = randomCount > 6 ? 6 : randomCount;
        self.showMoreSimilerBook = randomCount > 6 ? YES : NO;
        self.novelInfo.sameNovelList = [self randomAccessData:AllBooksArray WithSpecifiedNumber:randomCount];
        if (self.novelInfo.sameNovelList.count > 0) {
            [self.dataSource addObject:@[recommendCellIdentifier,@([self getCellHeight:self.novelInfo.sameNovelList]),self.novelInfo]];
            [self.mainTableView reloadData];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)loadSameAurhurBooks {
    NSString *sameAuthurURL = [NSString stringWithFormat:@"http://api03icqj.zhuishushenqi.com/book/accurate-search?author=%@&packageName=com.zsreader.sdk",self.novelInfo.novelAuthor];
    NSString *urt8Str = [sameAuthurURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[NWS_httpClient sharedInstance] getWithURLString:urt8Str parameters:nil success:^(id responseObject) {
        
        NSMutableArray *AllBooksArray = responseObject[@"books"];
        self.allSameAuthurBook = AllBooksArray;
        NSInteger randomCount = AllBooksArray.count;
        randomCount = randomCount > 6 ? 6 : randomCount;
        self.showMoreSameAurhurBook = AllBooksArray.count > 6 ? YES : NO;
        self.novelInfo.sameUserNovelList = [self randomAccessData:AllBooksArray WithSpecifiedNumber:randomCount];
        if (self.novelInfo.sameUserNovelList.count > 0) {
            [self.dataSource addObject:@[sameUserCellIdentifier,@([self getCellHeight:self.novelInfo.sameUserNovelList]),self.novelInfo]];
            [self.mainTableView reloadData];
        }
        
        [self loadSimilerBooks];
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)gotoRead{
    
    NSDictionary *paramentDic = [[NSUserDefaults standardUserDefaults] objectForKey:@"parameterOnline"];
    NSString *typeString = paramentDic[[NSString stringWithFormat:@"好评弹窗%@", kAPP_VERSION]];
    // 绿色或者娶不到开始阅读参数，允许直接阅读
    if (![Common shareCommon].zs_Open || !typeString) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoReadByNovel" object:self.novelInfo];
        return;
    }
    
    // 非绿色要好评、视频
    bool type = typeString.boolValue;
    NSString *jiesuo = [[NSUserDefaults standardUserDefaults]objectForKey:@"jiesuo"];
    
    // 判断上次激励视频时间
    bool adayAfter = NO;
    NSDate *lastDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"showedRewardADTime"];
    NSDate *nowDate = [NSDate date];
    NSTimeInterval a_day = 24*60*60;
    NSTimeInterval secondsInterval= [nowDate timeIntervalSinceDate:lastDate];
    if (secondsInterval >= a_day || !lastDate) {
        adayAfter = YES;
    }
    
    
    if ((type == 0 && ![jiesuo isEqualToString:@"1"]) || (type != 0 && adayAfter && self.rewardedVideoAd.isAdValid) || (type != 0 && adayAfter && [[GADRewardBasedVideoAd sharedInstance] isReady]) ) {
        ZY_NeedFiveStarView *fiveStarView = [[ZY_NeedFiveStarView alloc] init];
        fiveStarView.fiveStarOrVideo = type;
        [fiveStarView setFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight)];
        fiveStarView.alpha = 0;
        [[UIApplication sharedApplication].keyWindow addSubview:fiveStarView];
        
        [UIView animateWithDuration:0.3 animations:^{
            fiveStarView.alpha = 1;
        }];
    } else {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoReadByNovel" object:self.novelInfo];
    }
    
}

// M广告的回调

- (void)onVideoAdDismissed:(NSString *)unitId withConverted:(BOOL)converted withRewardInfo:(MTGRewardAdInfo *)rewardInfo {
    if (converted) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate new] forKey:@"showedRewardADTime"];
    }
}
- (void)rewardedVideoAdDidClose:(BURewardedVideoAd *)rewardedVideoAd {
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate new] forKey:@"showedRewardADTime"];
}
- (void)rewardedVideoAdDidClickDownload:(BURewardedVideoAd *)rewardedVideoAd {
   
}

- (void)postRewardVideo {
    
    NSString* type = [self.paramentOnline objectForKey:[NSString stringWithFormat:@"好评弹窗%@", kAPP_VERSION]];
    if ([Common shareCommon].zs_Open && [type isEqualToString:@"1"]) {
        if (self.rewardedVideoAd.isAdValid) {
            [self.rewardedVideoAd showAdFromRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
           
        } else {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoReadByNovel" object:self.novelInfo];
        }
    } else if ([Common shareCommon].zs_Open && [type isEqualToString:@"2"]) {
        if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
            [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
        }
    }
    
    
}

// 随机取数据
-(NSMutableArray *)randomAccessData:(NSMutableArray *)dataArray WithSpecifiedNumber:(NSInteger)numberCount{
    NSMutableArray *indexRandomArray = [[NSMutableArray alloc] init];
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    
    NSInteger totalCount =dataArray.count-numberCount;
    
    while (YES) {
        if (indexRandomArray.count==totalCount) {
            break;
        }
        NSString *nsstrRandom = [NSString stringWithFormat:@"%ld",(long)(arc4random() % dataArray.count)];
        if (![indexRandomArray containsObject:nsstrRandom]) {
            [indexRandomArray addObject:nsstrRandom];
            [indexSet addIndex:[nsstrRandom integerValue]];
        }
    }
    
    [dataArray removeObjectsAtIndexes:indexSet];
    
    return dataArray;
}

#pragma mark AdManger delegate
- (void)nativeAdsLoaded:(NSArray *)nativeAds nativeManager:(nonnull MTGNativeAdManager *)nativeManager
{
    
    if (nativeAds.count > 0) {
        MTGCampaign *campain=nativeAds[0];
        self.compaign = campain;
        [self.mainTableView reloadData];
    }
    
}

#pragma mark - 穿山甲 广点通代理

- (void)rewardedVideoAd:(BURewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error {
    
}

- (void)nativeAdsFailedToLoadWithError:(NSError *)error nativeManager:(nonnull MTGNativeAdManager *)nativeManager
{
    //[self log:[NSString stringWithFormat:@"Failed to load ads, error:%@", error.localizedDescription]];
}
- (void)bannerAdViewDidClick:(BUBannerAdView *)bannerAdView WithAdmodel:(BUNativeAd *)nativeAd {
   
}
- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray {
    self.GDTDataArray = nativeAdDataArray;
    self.adEntity = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_Feed];
    
    [self.mainTableView.mj_header beginRefreshing];
    
}

#pragma mark - ZY_DataDownloaderDelegate

-(void)downloadNavError:(NSError*)error downloadErrorType:(downloadErrorType)type{
    DebugLog(@"错误___%@",error);
    ZY_WS(weakSelf);
    [weakSelf.mainTableView.jr_placeHolderView setHidden:NO];
    [self.mainTableView.mj_header endRefreshing];
}

-(void)htmlDataFinish:(NSData *)htmlData{
    
    self.novelInfo = [[[ZY_JsonParser alloc] init] getNovelDetailByData:htmlData];
    
    if (!self.novelInfo.isForbitToRead) {
        self.v_foot.hidden = NO;
        self.mainTableView.frame = CGRectMake(0, 0, kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height-49);
    }
    NSString *review = [[NSUserDefaults standardUserDefaults] objectForKey:@"isOpenReview"];
    if ([review isEqualToString:@"1"]) {
        self.v_foot.hidden = NO;
        self.mainTableView.frame = CGRectMake(0, 0, kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height-49);
    }
    
    self.dataSource = [NSMutableArray array];
    
    if (![ZY_Common isBlankString:self.novelInfo.novelDes]){
        
        UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [sizeLabel setFont:[UIFont systemFontOfSize:14]];
        [sizeLabel setNumberOfLines:1];
        [sizeLabel setText:self.novelInfo.novelDes];
        [sizeLabel sizeToFit];
        
        CGFloat tempHeight = sizeLabel.frame.size.height*self.lineNumberCount+self.lineNumberCount*kLineSpacing;
        
        if (tempHeight > self.novelInfo.novelContentDescHeight||tempHeight==0) {
            tempHeight = self.novelInfo.novelContentDescHeight;
            self.lineNumberCount = 0;
        }
        
        [self.dataSource addObject:@[novelInfoCellIdentifier,@(242+tempHeight),self.novelInfo,@(self.lineNumberCount>0?sizeLabel.frame.size.height:0)]];
    }else{
        self.lineNumberCount=0;
        [self.dataSource addObject:@[novelInfoCellIdentifier,@(262),self.novelInfo,@(0)]];
    }
    
    if (self.adEntity) {
        
        [Common showDebugHUD:self.adType];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if ([self.adType isEqualToString:@"4"]) {
            [self.dataSource addObject:@[@"MTGVideoCell",@(340)]];
           
        } else if ([self.adType isEqualToString:@"1"]) { // 谷歌
            [self.dataSource addObject:@[adCellIdentifier,@(290)]];
        } else {
            [self.dataSource addObject:@[adCellIdentifier,@(self.adEntity.CellHeight)]];
            if ([self.adType isEqualToString:@"5"] && self.adEntity) {
                // 展示汇报
              
                NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiGif;
                [[ZY_ADHelper shareADHelper] upLoadLomiShowWithURL:kaipingDic[@"count_url"]];
            } else {
               
            }
        }
    }
    
    
    if (self.novelInfo.sameUserNovelList.count > 0) {
        [self.dataSource addObject:@[sameUserCellIdentifier,@([self getCellHeight:self.novelInfo.sameUserNovelList]),self.novelInfo]];
    }
    
    if (self.novelInfo.sameNovelList.count > 0) {
        [self.dataSource addObject:@[recommendCellIdentifier,@([self getCellHeight:self.novelInfo.sameNovelList]),self.novelInfo]];
    }
    
    [self.mainTableView reloadData];
    [self.mainTableView.mj_header endRefreshing];
    
    [self loadSameAurhurBooks];
    
}
-(NSInteger)getCellHeight:(NSMutableArray *)arr{
    NSInteger hang = arr.count%3;
    if (hang == 0)
    {
        hang = arr.count / 3;
    }
    else
    {
        hang = arr.count / 3 +1;
    }
    return hang*206;
}

#pragma mark - ZY_NovelInfoTableViewCellDelegate

-(void)didClickShowAllBookDesc{
    self.lineNumberCount = 0;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    for (NSInteger i=0; i<self.dataSource.count; i++) {
        NSArray *dataArray = [self.dataSource objectAtIndex:i];
        if (dataArray) {
            NSString *identifier = dataArray.firstObject;
            if ([identifier isEqualToString:novelInfoCellIdentifier]) {
                [self.dataSource removeObject:dataArray];
                
                break;
            }
        }
    }
    
    if (![ZY_Common isBlankString:self.novelInfo.novelDes]){
        [self.dataSource insertObject:@[novelInfoCellIdentifier,@(242+self.novelInfo.novelContentDescHeight),self.novelInfo,@(0)] atIndex:0];
    }else{
        [self.dataSource insertObject:@[novelInfoCellIdentifier,@(262),self.novelInfo,@(0)] atIndex:0];
    }
    
    [self.mainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - ZY_NovelRecommendTableViewCellDelegate

-(void)clickRecommendNovel:(ZY_NovelInfo *)currentNovel{
    ZY_BookDetailsViewController *zy_bookDetailViewController = [[ZY_BookDetailsViewController alloc] init];
    zy_bookDetailViewController.w_novel = currentNovel;
    zy_bookDetailViewController.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:zy_bookDetailViewController animated:YES];
   [MobClick event:@"Details" label:@"同类型小说"];
}

-(void)clickSameUserNovel:(ZY_NovelInfo *)currentNovel{
    ZY_BookDetailsViewController *zy_bookDetailViewController = [[ZY_BookDetailsViewController alloc] init];
    zy_bookDetailViewController.w_novel = currentNovel;
    zy_bookDetailViewController.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:zy_bookDetailViewController animated:YES];
   [MobClick event:@"Details" label:@"作者还写过"];
}

#pragma mark - UITableViewDelegate

// 控制导航栏透明度

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *array = self.dataSource[indexPath.row];
    return [[array objectAtIndex:1] floatValue];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *data = [self.dataSource objectAtIndex:indexPath.row];
    NSString *identifier = data.firstObject;
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    tableViewCell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([identifier isEqualToString:novelInfoCellIdentifier]){
        ZY_NovelInfo *info = [data objectAtIndex:2];
        
        if(info){
            [(ZY_NovelInfoTableViewCell*)tableViewCell setDelegate:self];
            [(ZY_NovelInfoTableViewCell*)tableViewCell bindItemWithNovelInfo:info withLineNumberCount:self.lineNumberCount withSingleDesHeight:[[data lastObject] floatValue]];
        }
        
    }else if ([identifier isEqualToString:adCellIdentifier]){
        [(ZY_DetailBookAdTableViewCell*)tableViewCell bindData:self.adEntity];
        if ([self.adType isEqualToString:@"3"]) {
           
            [self.GDTAd attachAd:self.adEntity.data toView:tableViewCell];
        } else if ([self.adType isEqualToString:@"5"]) {
           
        } else if ([self.adType isEqualToString:@"1"]) {
            [MobClick event:@"Google_show" label:@"展示_详情"];
        }
        
    }else if ([identifier isEqualToString:@"MTGVideoCell"]){
        
       
        BUSize *size = [BUSize sizeBy:BUProposalSize_Banner600_500];
        BUBannerAdView *banner = [[BUBannerAdView alloc] initWithSlotID:TouTiao_detail size:size rootViewController:self];
        [banner loadAdData];
        const CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
        banner.delegate = self;
        CGFloat bannerHeight = screenWidth * size.height / size.width;
        banner.frame = CGRectMake(0, 0, screenWidth, bannerHeight);
        //banner.delegate = self;
        [tableViewCell addSubview:banner];
        
    }else if ([identifier isEqualToString:sameUserCellIdentifier]){
        
        ((ZY_SameUserTableViewCell*)tableViewCell).moreButton.hidden = !self.showMoreSameAurhurBook;
        [((ZY_SameUserTableViewCell*)tableViewCell) setDelegate:self];
        ((ZY_SameUserTableViewCell*)tableViewCell).dataArray = self.novelInfo.sameUserNovelList;
        [((ZY_SameUserTableViewCell*)tableViewCell) reloadBookData];
        
    }else if ([identifier isEqualToString:recommendCellIdentifier]){

        [(ZY_NovelRecommendTableViewCell*)tableViewCell setDelegate:self];
        ((ZY_NovelRecommendTableViewCell*)tableViewCell).recommendNovelDataArray = self.novelInfo.sameNovelList;
        [(ZY_NovelRecommendTableViewCell*)tableViewCell createControl];
        [(ZY_NovelRecommendTableViewCell*)tableViewCell bindRecommendNovelWithCurrentNovel];
        [((ZY_NovelRecommendTableViewCell*)tableViewCell).labelTitle setText:@"同类型小说"];
    }
    
    return tableViewCell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSArray *data = [self.dataSource objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier = data.firstObject;
    if ([nsstrIdentifier isEqualToString:adCellIdentifier]){
        if (self.adEntity&&self.adEntity.platform != adplatform_Google) {
            [[ZY_ADHelper shareADHelper] tapAD:self.adEntity CurrentVC:self];
            
            if ([self.adType isEqualToString:@"3"]) {
               
            } else if ([self.adType isEqualToString:@"5"]) {
               
            }
            
        }
    }
}

#pragma mark - Getter or Setter

-(UITableView *)mainTableView{
    if (!_mainTableView) {
        _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height) style:UITableViewStylePlain];
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView registerClass:[ZY_NovelInfoTableViewCell class] forCellReuseIdentifier:novelInfoCellIdentifier];
        [_mainTableView registerClass:[ZY_DetailBookAdTableViewCell class] forCellReuseIdentifier:adCellIdentifier];
        [_mainTableView registerClass:[ZY_SameUserTableViewCell class] forCellReuseIdentifier:sameUserCellIdentifier];
        [_mainTableView registerClass:[ZY_NovelRecommendTableViewCell class] forCellReuseIdentifier:recommendCellIdentifier];
        //[_mainTableView registerClass:[BD_BookInfoCell class] forCellReuseIdentifier:@"bookInfoCell"];
        
        _mainTableView.estimatedRowHeight = 0;
        
        _mainTableView.estimatedSectionHeaderHeight = 0;
        
        _mainTableView.estimatedSectionFooterHeight = 0;
        
        // 视频广告
        [_mainTableView registerNib:[UINib nibWithNibName:@"MTGNativeVideoCell" bundle:nil] forCellReuseIdentifier:@"MTGVideoCell"];
        _mainTableView.separatorStyle = NO;
        _mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadRequest)];
    }
    
    return _mainTableView;
}

-(ZY_DataDownloader *)downLoader{
    if (!_downLoader) {
        _downLoader = [[ZY_DataDownloader alloc] init];
        [_downLoader setDelegate:self];
    }
    return _downLoader;
}

-(UIBarButtonItem *)shareButttonItem{
    UIButton *shareBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 17, 17)];
    [shareBtn addTarget:self action:@selector(novelShare) forControlEvents:UIControlEventTouchUpInside];
    [shareBtn setImage:[UIImage imageNamed:@"分享"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:shareBtn];
    return backButton;
}

-(void)clickAddBookOnTheBookShelf:(id)sender{
    [self setAddButtonBookShelfStateWithShowHud:YES];
   [MobClick event:@"Details" label:@"加入书架"];
    if (![[ZY_DBHelper shareDBHelper] queryBookIsOnTheBookShelf:self.novelInfo.novelId]) {
        [[ZY_DBHelper shareDBHelper] addBookToShelf:self.novelInfo];
    }
}

-(void)setAddButtonBookShelfStateWithShowHud:(BOOL)show{
    
    [self.btnAddBookShelf setTitle:@"已加入书架" forState:UIControlStateNormal];
    [self.btnAddBookShelf setImage:nil forState:UIControlStateNormal];
    [self.btnAddBookShelf setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.btnAddBookShelf setBackgroundColor:RGB(240, 240, 240)];
    self.btnAddBookShelf.layer.borderColor = [kRGBCOLOR(240, 240, 240) CGColor];
    [self.btnAddBookShelf setEnabled:NO];
    
    if (show) {
        [SVProgressHUD setMinimumDismissTimeInterval:2];
        [SVProgressHUD showSuccessWithStatus:@"添加成功"];
    }
    
}


-(void)downLoadAll{
    
    LSYReadModel *model = [LSYReadModel getLocalModelWithNovel:self.novelInfo];
    [MobClick event:@"Details" label:@"缓存全本"];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        if (model.chapters.count == 0) {

            [SVProgressHUD showInfoWithStatus:@"初始化下载器"];
            
            NSString *tmpContent = [LSYReadUtilites encodeWithURL:[NSURL URLWithString:model.novelInfo.novelListUrl]];
            if (tmpContent) {
                NSData* xmlData = [tmpContent dataUsingEncoding:NSUTF8StringEncoding];
                model.chapters = [[[ZY_JsonParser alloc]init]getChapterListByData:xmlData];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (model.chapters.count == 0) {
                [SVProgressHUD showErrorWithStatus:@"获取目录失败!"];
            }else
            {
                [[LS_DownloadInfoManager shareDownloadInfoManager] addItemToDownLoadingArray:model];
                ZY_NovelContentDownLoadInfo *downLoadInfo = [[ZY_NovelContentDownLoadInfo alloc] init];
                [downLoadInfo setDownloadType:downloadTypeAll];
                [downLoadInfo setReadModel:model];
                ZY_NovelDownLoadOperation *downLoadOperation = [[ZY_NovelDownLoadOperation alloc] init];
                [downLoadOperation setDownloadInfo:downLoadInfo];
                [[ZY_BookHelp shareBookHelp].novelDownLoadQueue addOperation:downLoadOperation];
                [SVProgressHUD setMinimumDismissTimeInterval:2];
                
                [SVProgressHUD showSuccessWithStatus:@"加入缓存管理，正在下载中!"];
            }
        });
    });
}
- (NSDictionary *)paramentOnline {
    if (_paramentOnline == nil) {
        _paramentOnline = [[NSUserDefaults standardUserDefaults] objectForKey:@"parameterOnline"];
    }
    return _paramentOnline;
}

@end


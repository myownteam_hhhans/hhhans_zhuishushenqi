//
//  ZY_ShuDanDetailViewController.m
//  ZYNovel

#import "ZY_ShuDanDetailViewController.h"
#import "ZY_BookLIst.h"
#import "ZY_Common.h"


@interface ZY_ShuDanDetailViewController ()

@end

@implementation ZY_ShuDanDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.shudan.ShuDanName;
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    ZY_BookLIst *_bookList = [[ZY_BookLIst alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height)];
    _bookList.BaseMethodUrl = [NSString stringWithFormat: @"https://shuapi.jiaston.com/shudan/detail/%@.html",self.shudan.ShuDanId];
    [self.view addSubview:_bookList];
    [_bookList createHeadView];
    [_bookList createTableView];
    [self.view addSubview:_bookList];
    //self.navigationItem.hidesBackButton = YES;
    //[self addLeftBar];
}

-(void)goPopBack{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addLeftBar{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 65, 44)];
    [button addTarget:self action:@selector(goPopBack) forControlEvents:UIControlEventTouchUpInside];
    UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 44)];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, (40-16)/2, 10,16)];
    [imageView setImage:[UIImage imageNamed:@"返回"]];
    [button addSubview:imageView];
    [barView addSubview:button];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:barView];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

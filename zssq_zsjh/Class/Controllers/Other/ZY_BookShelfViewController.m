//
//  BookShelfViewController.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_BookShelfViewController.h"
#import "MJRefresh.h"
#import "ZY_Common.h"
#import "ZY_BookShelfCollectionViewCell.h"
#import "ZY_BookDetailsViewController.h"
#import "ZY_BookShelfAdCollectionViewCell.h"
#import "ZY_DBHelper.h"
#import "ZY_SearchViewController.h"
#import "ZY_BookHelp.h"
#import "ZY_AdEntity.h"
#import "SHW_ADInterstitial.h"
#import "LSYReadPageViewController.h"
#import "SVProgressHUD.h"
#import "ZY_XPathParser.h"
#import "LSYReadModel.h"
#import "ZY_XPathParserConfig.h"
#import "ZY_SearchResultViewController.h"
#import "ZY_AddNovelCollectionViewCell.h"
#import "CFT_parameterOnline.h"
#import "ZY_CustomNotificationView.h"
#import "ZY_FounctionTableViewCell.h"
#import "ZY_BookShelfCollectionListCell.h"
#import "ZY_AddNovelCollectionViewListCell.h"
#import "ZY_BookShelfListAdCollectionViewCell.h"
#import "M_DownloaderController.h"
#import "ZY_ADHelper.h"
#import "Common.h"
#import <MTGSDK/MTGSDK.h>
#import <IIViewDeckController.h>
#import "GDTNativeAd.h"
#import <GoogleMobileAds/GoogleMobileAds.h>


static NSString *identifier=@"UICollectionViewCellIdentifier";

static NSString *adIdentifier=@"ADUICollectionViewCellIdentifier";

static NSString *searchIdentifier=@"SearchUICollectionViewCellIdentifier";

static NSString *headerReuseIdentifier=@"headerReuseIdentifier";

static NSString *tableViewReuseIdentifier=@"tableViewReuseIdentifier";

static NSString *listIdentifier =@"ZY_BookShelfCollectionListCell";

static NSString *listAddIdentifier =@"ZY_AddNovelCollectionViewListCell";

static NSString *listAdIdentifier =@"ZY_BookShelfListAdCollectionViewCell";

#define kSectionCount ceil(kCScreenWidth/150)
#define kCollectionViewCellWidth  (kCScreenWidth-15*kSectionCount)/kSectionCount
#define kHeight kCollectionViewCellWidth*(125.0/100.0)
#define kCollectionViewCellHeight (kHeight>150?kHeight:150)+20

@interface ZY_BookShelfViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,ZY_BookShelfCollectionViewCellDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,MTGNativeAdManagerDelegate,GDTNativeAdDelegate>

typedef NS_ENUM(NSInteger, showMenueType) {
    showMenueType_more = 0,
    showMenueType_close,
    showMenueType_complete
};


@property(nonatomic,assign) BOOL isEditor;

@property(nonatomic,assign) showMenueType showMenueState;

@property(nonatomic,strong) NSMutableArray *dataArray;

@property(nonatomic,strong) UIButton *btnBookManager;

@property(nonatomic,strong) UICollectionView *collectionView;
//@property(nonatomic,strong) ZY_AdEntity *currentEntity;
@property(nonatomic,strong) ADModel *currentAd;

//@property(nonatomic,strong) NSString *nsstrNotifictionMessage;

@property(nonatomic,strong) NSTimer *currentTimer;

@property(nonatomic,strong) ZY_CustomNotificationView *customNotificationView;

@property(nonatomic,strong) UIButton *maskButtonView;

@property(nonatomic,strong) UITableView *listView;

@property(nonatomic,strong) NSMutableArray *functionDataArray;

@property(nonatomic,strong) NSIndexPath *indexPath;

@property (nonatomic, strong) MTGNativeAdManager *nativeVideoAdManager;
@property (nonatomic, strong) MTGCampaign *campaign;

@property (nonatomic, strong) NSString *adType;

@property (nonatomic, strong) GDTNativeAd *GDTAd;
@property (nonatomic, strong) NSArray *GDTDataArray;

@property(nonatomic, strong) GADBannerView *bannerView;

// 自己做个导航条
@property (nonatomic, strong) UIView *navBackView;
@property (nonatomic, strong) UIButton *getLeftBarButton;

@end

@implementation ZY_BookShelfViewController


#pragma mark - Life cycle

-(void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.adType = [[Common shareCommon] randomADTypeWithKey:@"bookcover"];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.dataArray = [[NSMutableArray alloc] init];
    [self.navBackView addSubview:self.btnBookManager];
    
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.navBackView];
    [self.collectionView.mj_header beginRefreshing];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:ZY_CheckBookUpdateDidFinishedNotification object:nil];
//    self.currentTimer = [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(getUMNotification) userInfo:nil repeats:YES];
    [[UIApplication sharedApplication].keyWindow addSubview:self.maskButtonView];
    
    // 请求广点通广告
    NSDictionary *gdtKeyAndID = [[ZY_ADHelper shareADHelper] getGDTKeyWithADPlace:@"bookcover"];
    self.GDTAd = [[GDTNativeAd alloc] initWithAppkey:gdtKeyAndID[@"ID"] placementId:gdtKeyAndID[@"KEY"]];
    self.GDTAd.delegate = self;
    self.GDTAd.controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    [self.GDTAd loadAd:3];

    
    if ([Common shareCommon].zs_Open && [[ZY_ADHelper shareADHelper]getOpenStatusForAD:@"bookcover"] && [self.adType isEqualToString:@"1"]) {
        self.bannerView = [[GADBannerView alloc]
                           initWithAdSize:kGADAdSizeBanner];
        [self addBannerViewToView:self.bannerView];
        self.bannerView.adUnitID = @"ca-app-pub-9664827263749384/1934510301";
        self.bannerView.rootViewController = self;
        [self.bannerView loadRequest:[GADRequest request]];
        
        CGRect frame = self.collectionView.frame;
        frame.origin.y += 50;
        frame.size.height -= 50;
        self.collectionView.frame = frame;
        [MobClick event:@"Google_show" label:@"展示_书架横幅"];
    }
    
    
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)refreshBookShelf{
    [self.collectionView.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.view.frame = CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight);
}

- (void)viewDidAppear:(BOOL)animated {
    
}

#pragma mark - 头条代理 广点通代理

- (void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray {
    self.GDTDataArray = nativeAdDataArray;
//    self.currentAd = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_BookShelf];
//    if ([self.adType isEqualToString:@"3"]) {
//        [self loadData];
//    }
}
- (void)nativeAdFailToLoad:(NSError *)error {
    
}

#pragma mark - Private Function

// 加谷歌广告
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.topLayoutGuide
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:44],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
}

-(void)editCollectionView{
    [self.btnBookManager setTitle:@"" forState:UIControlStateNormal];
    
    if (self.showMenueState==showMenueType_more) {
         self.showMenueState=showMenueType_close;
        [self.btnBookManager setImage:[UIImage imageNamed:@"取消"] forState:UIControlStateNormal];
        [self.maskButtonView addSubview:self.listView];
        [self.maskButtonView setHidden:NO];
    }else if(self.showMenueState==showMenueType_close){
         self.showMenueState=showMenueType_more;
        [self.btnBookManager setImage:[UIImage imageNamed:@"书籍管理"] forState:UIControlStateNormal];
         //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self btnBookManager]];
        [self.maskButtonView setHidden:YES];
    }else if (self.showMenueState==showMenueType_complete){
         self.isEditor=NO;
         self.showMenueState=showMenueType_more;
        [self.btnBookManager setImage:[UIImage imageNamed:@"书籍管理"] forState:UIControlStateNormal];
        [self.maskButtonView setHidden:YES];
        [self.collectionView reloadData];
    }
    
}

-(void)loadData{
    
    self.adType = [[Common shareCommon] randomADTypeWithKey:@"bookcover"];
//    self.currentEntity = [ZY_AdEntity initWithSHW_AdsModel:[SHW_ADInterstitial pickUpBookAd]];
    [[ZY_ADHelper shareADHelper] requestLomiPic];
    
    if ([Common shareCommon].zs_Open && [[ZY_ADHelper shareADHelper] getOpenStatusForAD:@"bookcover"]) {
        NSString *adtype = self.adType;
        AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:adtype];
        if ([self.adType isEqualToString:@"3"]) {
            self.currentAd = [[ZY_ADHelper shareADHelper] getModelWithGDTData:self.GDTDataArray type:adType_BookShelf];
        } else {
            self.currentAd = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_BookShelf adPlatform:platform];
        }
    }
    
    self.functionDataArray=nil;
    NSMutableArray *dataArray= [[ZY_DBHelper shareDBHelper] queryBookShelfInfoList];
    self.dataArray = [[NSMutableArray alloc] init];
    
    BOOL isListReadModel = [[NSUserDefaults standardUserDefaults] boolForKey:nsstrReadingModel];
    for (NSInteger i=0; i<dataArray.count; i++) {
        if (isListReadModel) {
            [self.dataArray addObject:@[listIdentifier,[dataArray objectAtIndex:i]]];
        }else{
            [self.dataArray addObject:@[identifier,[dataArray objectAtIndex:i]]];
        }
    }
    
    if (self.dataArray&&self.dataArray.count>0 && self.currentAd) {
        if (isListReadModel) {
            [self.dataArray insertObject:@[listAdIdentifier] atIndex:0];
        }else{
            [self.dataArray insertObject:@[adIdentifier] atIndex:0];
        }
        
        if ([self.adType isEqualToString:@"5"]) {
            // 展示汇报
            NSDictionary *kaipingDic = [ZY_ADHelper shareADHelper].lomiPic;
            [[ZY_ADHelper shareADHelper] upLoadLomiShowWithURL:kaipingDic[@"count_url"]];
        }
        
    }
    
    
    if (isListReadModel) {
        [self.dataArray addObject:@[listAddIdentifier]];
    }else{
        [self.dataArray addObject:@[searchIdentifier]];
    }
    
    [self.collectionView reloadData];
    [self performSelector:@selector(collectionEndRefresh) withObject:nil afterDelay:0.1];

    
}
-(void)collectionEndRefresh {
    [self.collectionView.mj_header endRefreshing];
}

-(void)gotoSearchController{
   
    ZY_SearchViewController *searchVC = [[ZY_SearchViewController alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searchVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
}

//- (void)nativeAdsLoaded:(NSArray *)nativeAds nativeManager:(nonnull MTGNativeAdManager *)nativeManager
//{
//    if (nativeAds.count > 0) {
//        MTGCampaign *campain=nativeAds[0];
//        
//        self.campaign = campain;
//        self.currentAd = [[ADModel alloc] init];
//        self.currentAd.adImgPath = self.campaign.iconUrl;
//        self.currentAd.name = self.campaign.appName;
//        [self.dataArray insertObject:@[adIdentifier] atIndex:0];
//        
//        [self.collectionView reloadData];
//    }
//
//}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        [self deleteCollectionViewCell:self.indexPath];
    }
}

#pragma mark - ZY_BookShelfCollectionViewCellDelegate

-(void)deleteCollectionViewCell:(NSIndexPath *)indexPath{
    NSArray *dataArray=[self.dataArray objectAtIndex:indexPath.row];
    
    if ([[dataArray firstObject] isEqualToString:identifier]||[[dataArray firstObject] isEqualToString:listIdentifier]) {
        

        
        [self.collectionView performBatchUpdates:^{
            ZY_NovelInfo *novelInfo=[dataArray  lastObject];
            
            if (novelInfo) {
                [[ZY_DBHelper shareDBHelper] removeBookIsOnTheBookShelfWithNovelId:novelInfo.novelId];
                [self.dataArray removeObjectAtIndex:indexPath.row];
                [LSYReadModel delLocalModelWithNovel:novelInfo];
                [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
            }
            
        }completion:^(BOOL finished){
            [self.collectionView reloadData];
        }];
    }
}

#pragma mark - UICollectionViewDelegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size =CGSizeMake(kCollectionViewCellWidth, kCollectionViewCellHeight);
    BOOL boollistReadModel =[[NSUserDefaults standardUserDefaults] boolForKey:nsstrReadingModel];
    
    if (boollistReadModel&&indexPath.row==self.dataArray.count-1){
        size = CGSizeMake(kCScreenWidth, kBookShelfListAdHeight);
    }else if (boollistReadModel) {
        size = CGSizeMake(kCScreenWidth, kBookShelfListHeight);
    }
    
    return size;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *dataArray = [self.dataArray objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier =[dataArray firstObject];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:nsstrIdentifier forIndexPath:indexPath];
    
    if ([nsstrIdentifier isEqualToString:identifier]) {
        [self bindCollectionViewData:cell withIndexPath:indexPath withArrayData:dataArray];
    }else if ([nsstrIdentifier isEqualToString:adIdentifier]){
        if (self.currentAd) {
            [(ZY_BookShelfAdCollectionViewCell*)cell bindData:self.currentAd];
            [self.nativeVideoAdManager registerViewForInteraction:cell withCampaign:self.campaign];
            // 头条注册点击
            if ([self.adType isEqualToString:@"4"]) {
                BUNativeAd *ad = self.currentAd.data;
                [ad registerContainer:cell withClickableViews:cell.subviews];
              
            }
            if ([self.adType isEqualToString:@"3"]) {
               
                [self.GDTAd attachAd:self.currentAd.data toView:cell];
            }
            if ([self.adType isEqualToString:@"5"]) {
              
            }
            [Common showDebugHUD:self.adType];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
        }
    }else if ([nsstrIdentifier isEqualToString:searchIdentifier]){
        [(ZY_AddNovelCollectionViewCell*)cell  bindDataWithNoMoreData];
    }else if ([nsstrIdentifier isEqualToString:listIdentifier]){
        [self bindCollectionViewData:cell withIndexPath:indexPath withArrayData:dataArray];
    }else if([nsstrIdentifier isEqualToString:listAdIdentifier]){
        if (self.currentAd) {
            [(ZY_BookShelfListAdCollectionViewCell*)cell bindData:self.currentAd];
            [self.nativeVideoAdManager registerViewForInteraction:cell withCampaign:self.campaign];
            // 头条注册点击
            if ([self.adType isEqualToString:@"4"]) {
                BUNativeAd *ad = self.currentAd.data;
                [ad registerContainer:cell withClickableViews:cell.subviews];
              
            }
            if ([self.adType isEqualToString:@"3"]) {
               
                [self.GDTAd attachAd:self.currentAd.data toView:cell];
            }
            if ([self.adType isEqualToString:@"5"]) {
              
            }
        }
        

    }
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //self.ATTarget = [self.collectionView cellForItemAtIndexPath:indexPath];
    ZY_BookShelfCollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    if (![cell isKindOfClass:[ZY_AddNovelCollectionViewListCell class]]) {
        self.ATTarget = cell.bookShelfImageView;
    }
    
    
    NSArray *dataArray = [self.dataArray objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier =[dataArray firstObject];
    
    if (([nsstrIdentifier isEqualToString:identifier]&&!self.isEditor)||([nsstrIdentifier isEqualToString:listIdentifier]&&!self.isEditor)) {
        ZY_NovelInfo *novelInfo=[dataArray lastObject];

        ZY_NovelInfo *newstNovelInfo =[[ZY_BookHelp shareBookHelp].dictNovelUpdate objectForKey:novelInfo.novelId];
        newstNovelInfo.novelUrl=novelInfo.novelUrl;
        if (newstNovelInfo) {
            [[ZY_DBHelper shareDBHelper] updateBookToShelf:newstNovelInfo];
            [[ZY_BookHelp shareBookHelp].dictNovelUpdate removeObjectForKey:novelInfo.novelId];
            [UIView performWithoutAnimation:^{
                [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
            }];
        }
        
        if (novelInfo) {
            [[ZY_DBHelper shareDBHelper] updateBookToShelfAndReadTime:novelInfo];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoReadByNovel" object:novelInfo];
        }
        
    }else if ([nsstrIdentifier isEqualToString:adIdentifier]||[nsstrIdentifier isEqualToString:listAdIdentifier]){
        if (self.currentAd) {
         
            [[ZY_ADHelper shareADHelper] tapAD:self.currentAd CurrentVC:self];
            if ([self.adType isEqualToString:@"4"]) {
              
            }
            if ([self.adType isEqualToString:@"3"]) {
              
            }
            if ([self.adType isEqualToString:@"5"]) {
              
            }
        }
    }else if (([nsstrIdentifier isEqualToString:searchIdentifier]||[nsstrIdentifier isEqualToString:listAddIdentifier])&&!self.isEditor){
        UITabBarController *rootViewController =nil;
        IIViewDeckController *iiVC = (IIViewDeckController *)[UIApplication sharedApplication].delegate.window.rootViewController;
        
        if([iiVC.centerViewController isKindOfClass:[UITabBarController class]]){
            rootViewController = (UITabBarController *)iiVC.centerViewController;
            rootViewController.selectedIndex=1;
            [MobClick event:@"bookshelf" label:@"添加小说"];
        }
    }else if([nsstrIdentifier isEqualToString:listIdentifier]&&self.isEditor){
        self.indexPath=indexPath;
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"确定删除?" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alerView show];
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size=CGSizeMake(0, 0);
    
//    if (![ZY_Common isBlankString:self.nsstrNotifictionMessage]) {
//        size=CGSizeMake(kCScreenWidth, 44);
//    }
    
    return size;
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
//    UICollectionReusableView *reusableView = nil;
//    if (kind == UICollectionElementKindSectionHeader) {
//        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerReuseIdentifier forIndexPath:indexPath];
//        [header removeFromSuperview];
//        [header addSubview:self.customNotificationView];
//        //[self.customNotificationView bindNotificationMessage:self.nsstrNotifictionMessage];
//        reusableView =header;
//    }
//
//    return reusableView;
//}

#pragma mark - UITableViewDelegate


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.functionDataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return scaleValue(250)/3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:tableViewReuseIdentifier forIndexPath:indexPath];
    tableViewCell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSArray *data = [self.functionDataArray objectAtIndex:indexPath.row];
    [(ZY_FounctionTableViewCell*)tableViewCell bindData:[data lastObject] withfunctionName:[data firstObject]];
    
    return tableViewCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row==0) {
        DebugLog(@"列表模式");
        [MobClick event:@"bookshelf" label:@"列表模式"];
        BOOL isListReadModel = [[NSUserDefaults standardUserDefaults] boolForKey:nsstrReadingModel];
        
        if (isListReadModel) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:nsstrReadingModel];
            [self.collectionView setCollectionViewLayout:[self getCoverCollectionViewFlowLayout]];
        }else{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:nsstrReadingModel];
            [self.collectionView setCollectionViewLayout:[self getListCollectionViewFlowLayout]];
        }
        
        [self loadData];
        [self.collectionView reloadData];
        [self.collectionView setContentOffset:CGPointZero];
        [self editCollectionView];
        [self.listView reloadData];
    }else if (indexPath.row==1){
        DebugLog(@"编辑书籍");
        [MobClick event:@"bookshelf" label:@"编辑模式"];
        self.isEditor=YES;
        self.showMenueState=showMenueType_complete;
        //[self.btnBookManager setFrame:CGRectMake(0,0, 30, 30)];
        [self.btnBookManager setImage:nil forState:UIControlStateNormal];
        [self.btnBookManager setTitle:@"完成" forState:UIControlStateNormal];
        [self.btnBookManager.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [self.btnBookManager setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self btnBookManager]];
        [self.maskButtonView setHidden:YES];
        [self.collectionView reloadData];
    }else if (indexPath.row==2){
        [MobClick event:@"bookshelf" label:@"搜索小说"];
        [self editCollectionView];
        [self gotoSearchController];
    } else if (indexPath.row == 3) {
        DebugLog(@"下载管理");
       [MobClick event:@"bookshelf" label:@"下载管理"];
        [self editCollectionView];
        M_DownloaderController *downloadVC = [[M_DownloaderController alloc] init];
        downloadVC.downloadedFirst = YES;
        downloadVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:downloadVC animated:YES];
    }
}

#pragma mark - Getter or Setter

-(UIButton*)btnBookManager{
    
    if (!_btnBookManager) {
        _btnBookManager = [[UIButton alloc] initWithFrame:CGRectMake(k_ScreenWidth - 64, self.navBackView.frame.size.height - 44, 44, 44)];
        [_btnBookManager setImage:[UIImage imageNamed:@"书籍管理"] forState:UIControlStateNormal];
        _btnBookManager.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_btnBookManager addTarget:self action:@selector(editCollectionView) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _btnBookManager;
}

/********
 封面模式
 ********/
-(UICollectionViewFlowLayout*)getCoverCollectionViewFlowLayout{
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
     layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [layout setSectionInset:UIEdgeInsetsMake(18,10,10,10)];
    [layout setMinimumInteritemSpacing:10];
    [layout setMinimumLineSpacing:15];
     //layout.itemSize = CGSizeMake(kCollectionViewCellWidth, kCollectionViewCellHeight);
    return layout;
}

/********
 列表模式
 ********/
-(UICollectionViewFlowLayout*)getListCollectionViewFlowLayout{
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
     layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [layout setSectionInset:UIEdgeInsetsMake(0,0,0,0)];
    [layout setMinimumInteritemSpacing:0];
    [layout setMinimumLineSpacing:0];
    //layout.itemSize = CGSizeMake(kCScreenWidth, 82);
    return layout;
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
         UICollectionViewFlowLayout *layout =[[NSUserDefaults standardUserDefaults] boolForKey:nsstrReadingModel]?[self getListCollectionViewFlowLayout]:[self getCoverCollectionViewFlowLayout]; 
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, isIPhoneX ? 84 : 64, kCScreenWidth,kCScreenHeight-JH_TabbarHome_Height-JH_Navigation_Height-49) collectionViewLayout:layout];
         _collectionView.delegate=self;
         _collectionView.dataSource=self;
        [_collectionView setBackgroundColor:[UIColor whiteColor]];
        [_collectionView registerClass:[ZY_BookShelfCollectionViewCell class] forCellWithReuseIdentifier:identifier];
        [_collectionView registerClass:[ZY_AddNovelCollectionViewCell class] forCellWithReuseIdentifier:searchIdentifier];
        [_collectionView registerClass:[ZY_BookShelfAdCollectionViewCell class] forCellWithReuseIdentifier:adIdentifier];
        [_collectionView registerClass:[ZY_BookShelfCollectionListCell class] forCellWithReuseIdentifier:listIdentifier];
        [_collectionView registerClass:[ZY_AddNovelCollectionViewListCell class] forCellWithReuseIdentifier:listAddIdentifier];
        [_collectionView registerClass:[ZY_BookShelfListAdCollectionViewCell class] forCellWithReuseIdentifier:listAdIdentifier];
         _collectionView.showsVerticalScrollIndicator = NO;
         _collectionView.showsHorizontalScrollIndicator = NO;
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader  withReuseIdentifier:headerReuseIdentifier];
         _collectionView.mj_header=[MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    }
    return _collectionView;
}

-(ZY_CustomNotificationView *)customNotificationView{
    if (!_customNotificationView) {
        _customNotificationView = [[ZY_CustomNotificationView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, 44)];
    }
    return _customNotificationView;
}

-(UIButton *)maskButtonView{
    if (!_maskButtonView) {
         _maskButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationController.navigationBar.frame), kCScreenWidth, kCScreenHeight)];
        [_maskButtonView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
        [_maskButtonView setHidden:YES];
        [_maskButtonView addTarget:self action:@selector(editCollectionView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _maskButtonView;
}

-(UITableView *)listView{
    if (!_listView) {
        CGFloat listViewWidth= scaleValue(204);
        CGFloat listViewHeight= scaleValue(250/3*self.functionDataArray.count);
        _listView = [[UITableView alloc] initWithFrame:CGRectMake(kCScreenWidth-listViewWidth, 0, listViewWidth, listViewHeight) style:UITableViewStylePlain];
        [_listView setDelegate:self];
        [_listView setDataSource:self];
        [_listView registerClass:[ZY_FounctionTableViewCell class] forCellReuseIdentifier:tableViewReuseIdentifier];
        _listView.separatorStyle = NO;
    }
    return _listView;
}

-(NSMutableArray *)functionDataArray{
    
    if (!_functionDataArray) {
        NSString *nsstrDisplayName = [[NSUserDefaults standardUserDefaults] boolForKey:nsstrReadingModel] ? @"封面阅读":@"列表模式";
        NSString *nsstrDisplayIcon = [[NSUserDefaults standardUserDefaults] boolForKey:nsstrReadingModel] ? @"书封模式":@"列表模式";
        _functionDataArray = [NSMutableArray arrayWithObjects:[NSArray arrayWithObjects:nsstrDisplayName,nsstrDisplayIcon, nil],[NSArray arrayWithObjects:@"编辑书籍",@"编辑模式", nil],[NSArray arrayWithObjects:@"搜索小说",@"搜索模式", nil],[NSArray arrayWithObjects:@"下载管理",@"bookShelfDownload", nil], nil];
        if ([Common shareCommon].zs_Open) {
            _functionDataArray = [NSMutableArray arrayWithObjects:[NSArray arrayWithObjects:nsstrDisplayName,nsstrDisplayIcon, nil],[NSArray arrayWithObjects:@"编辑书籍",@"编辑模式", nil],[NSArray arrayWithObjects:@"搜索小说",@"搜索模式", nil],[NSArray arrayWithObjects:@"下载管理",@"bookShelfDownload", nil], nil];
        } else {
            _functionDataArray = [NSMutableArray arrayWithObjects:[NSArray arrayWithObjects:nsstrDisplayName,nsstrDisplayIcon, nil],[NSArray arrayWithObjects:@"编辑书籍",@"编辑模式", nil],[NSArray arrayWithObjects:@"搜索小说",@"搜索模式", nil], nil];
        }
    }
    
    return _functionDataArray;
}

-(void)bindCollectionViewData:(UICollectionViewCell*)collectionViewCell withIndexPath:(NSIndexPath*)indexPath withArrayData:(NSArray*)dataArray{
    if (indexPath.row<self.dataArray.count) {
        
        ZY_NovelInfo *novelInfo=[dataArray lastObject];
        
        if(novelInfo){
            
            BOOL isBookUpdate =NO;
            
            if ([[ZY_BookHelp shareBookHelp].dictNovelUpdate objectForKey:novelInfo.novelId]) {
                isBookUpdate=YES;
            }
            
            if ([collectionViewCell isKindOfClass:[ZY_BookShelfCollectionViewCell class]]) {
                [(ZY_BookShelfCollectionViewCell*)collectionViewCell setDelegate:self];
                [(ZY_BookShelfCollectionViewCell*)collectionViewCell bindData:novelInfo withIsEditor:self.isEditor withIndexPath:indexPath withIsUpdate:isBookUpdate];
            }else if ([collectionViewCell isKindOfClass:[ZY_BookShelfCollectionListCell class]]){
            
                [(ZY_BookShelfCollectionListCell*)collectionViewCell bindData:novelInfo withIsEditor:self.isEditor withIndexPath:indexPath withIsUpdate:isBookUpdate];
            }
        }
    }
}

- (UIView *)navBackView {
    if (!_navBackView) {
        _navBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, k_ScreenWidth, isIPhoneX ? 84 : 64)];
        [_navBackView setBackgroundColor:[UIColor whiteColor]];
        [_navBackView addSubview:self.getLeftBarButton];
        [_navBackView addSubview:({
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, _navBackView.frame.size.height, k_ScreenHeight, 0.7)];
            [lineView setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.5]];
            lineView;
        })];
    }
    return _navBackView;
}

- (UIButton *)getLeftBarButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(14.5, JH_Navigation_Height - 44 + (44 - 35)/2, 35, 35)];
    [button setImage:[UIImage imageNamed:@"leftSettingItem"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(showLeftVC) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)showLeftVC {
    IIViewDeckController *iivc = (IIViewDeckController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    [iivc openSide:IIViewDeckSideLeft animated:YES];
    [MobClick event:@"TABbar" label:@"我的"];
    
}

@end

//
//  SettingViewController.m
//  ZYNovel
//
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import "ZY_SettingViewController.h"
#import "ZY_Common.h"
#import "SDImageCache.h"
#import "ZY_DBHelper.h"
#import "CFT_parameterOnline.h"
#import "SVProgressHUD.h"
#import "M_DownloaderController.h"
#import "Common.h"
#import "ZY_Common.h"
#import <IIViewDeckController.h>

@interface ZY_SettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) NSArray *array;

@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) UISwitch *switchBrightness;

@property(nonatomic,strong) UIView *userContainerView;

@property(nonatomic,strong) UILabel *labelReadCountLabel;

@property(nonatomic,strong) UILabel *labelCacheSize;

@property (nonatomic,strong) UIView *tableHeader;

@end

@implementation ZY_SettingViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settingBack"]];
    [imageView setFrame:CGRectMake(0, 0, k_ScreenWidth, k_ScreenHeight)];
    imageView.contentMode = UIViewContentModeScaleToFill;
    
    [self.view addSubview:imageView];
    [self.view addSubview:self.tableView];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     NSMutableArray *bookShelfArray =[[ZY_DBHelper shareDBHelper] queryBookShelfInfoList];
    [self.labelReadCountLabel setText:[NSString stringWithFormat:@"%zd 本",bookShelfArray.count]];
    
    CGFloat cacheSize = [[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
    [self.labelCacheSize setText:[NSString stringWithFormat:@"%.2fMB",cacheSize]];
}

#pragma mark - PrivateFunction

- (void)oneSwitchValueChanged:(UISwitch *) sender {
    
    BOOL isResult=NO;
    
    if ([self.switchBrightness isOn]) {
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }else{
        [UIApplication sharedApplication].idleTimerDisabled = NO;
        isResult=YES;
    }
    
    NSUserDefaults *usd=[NSUserDefaults standardUserDefaults];
    [usd setBool:isResult forKey:kCloseChangLiang];
    [usd synchronize];
}

#pragma mark - UITableViewDelegate


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return self.array.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [Common get414RadiusByMap:54];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *nsstrIdentifier =@"SettingCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nsstrIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nsstrIdentifier];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font = [UIFont systemFontOfSize:[Common get414FontOfSizeByMap:16]];
    
    if (indexPath.row == 0) {
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    }
    
    NSString *info = [self.array objectAtIndex:indexPath.row];
    [cell.textLabel setText:info];
    
    if ([info isEqualToString:@"屏幕常亮"]){
        [cell addSubview:self.switchBrightness];
    }else if ([info isEqualToString:@"清除缓存"]){
        [cell addSubview:self.labelCacheSize];
    }
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, [Common get414RadiusByMap:52], kCScreenWidth-10, 0.5)];
    [lineView setBackgroundColor:[Common colorWithHexString:@"#8FC7FF" alpha:1.0]];
    [cell addSubview:lineView];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *nsstrName = [self.array objectAtIndex:indexPath.row];
    
    [MobClick event:@"mine" label:nsstrName];
    
    if ([nsstrName isEqual:@"清除缓存"]) {

      
        
        CGFloat cacheSize = [[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:[NSString stringWithFormat:@"缓存大小为%.2fM，确认要清理吗？",cacheSize] preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"清理" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[SDImageCache sharedImageCache] clearDisk];
            [self.labelCacheSize setText:@"0 MB"];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }else if ([nsstrName isEqual:@"免责声明"])
    {
       

       UITextView *textView=[[UITextView alloc]initWithFrame:CGRectMake(10, 0, kCScreenWidth-20, kCScreenHeight)];
        [textView setBackgroundColor:[UIColor whiteColor]];
        [textView setFont:[UIFont systemFontOfSize:13]];
        [textView setEditable:NO];
        [textView setTextColor:kRGBCOLOR(153, 153, 153)];
        
        NSString *nsstrDes =@"\n       {appname}是一款聚集小说阅读与交流的移动客户端，因为网络文化传播的地域性、空间性等等限制，{appname}的内容均来自网友分享和上传。由于小说数量较多，{appname} 对非法转载等侵权行为的发生不具备充分监控能力，所以，{appname}对用户在本产品上实施的侵权行为不承担法律责任，侵权的法律责任概由上传侵权内容的用户本人或被链接的网站承担。\n\n\n       如果您是版权作者或机构，对本站网友分享上传您的作品有任何异议，请提供相关作品的版权证明，并E-mail：biqugecom@gmail.com给我们。我们保证在第一时间内回复，待信息核实后做出相应调整，以示{appname}对著作版权的公正和维护。\n\n\n      {appname}秉着一如既往支持维护著作版权的态度和决心，也将尽一切所能在最大范围内保证版权人行使权力，保护版权人的合法利益，{appname}致力于为优秀小说的宣传推广做出应有的贡献!{appname}愿与所有小说作品权利人共同携手，为所有华语小说爱好者打造一个汇聚世界各类小说文化的分享交流平台。欢迎各方洽谈合作！ ";
        
        NSString *nsstrStatement=[nsstrDes stringByReplacingOccurrencesOfString:@"{appname}" withString:kAPP_Name];
        [textView setText:nsstrStatement];
        UIViewController *mianzeController = [[UIViewController alloc] init];
        mianzeController.title=@"免责声明";
        [mianzeController.view setBackgroundColor:[UIColor whiteColor]];
        [mianzeController.view addSubview:textView];
         mianzeController.hidesBottomBarWhenPushed = YES;
        
        IIViewDeckController *deckVC = (IIViewDeckController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        UITabBarController *tabVC = (UITabBarController *)deckVC.centerViewController;
        [deckVC closeSide:YES];
        [(UINavigationController *)tabVC.selectedViewController pushViewController:mianzeController animated:YES];
        
    }else if([nsstrName isEqual:@"分享给朋友"]){
       
      
        
        NSString *textToShare = [NSString stringWithFormat:@"全网百万小说免费随便看",kAPP_Name];
        NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
        NSString *icon = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
        UIImage *imageToShare = [UIImage imageNamed:icon];
        NSURL *urlToShare = [NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@?mt=8",kAppId]];
        
        NSArray *activityItems = @[textToShare, imageToShare, urlToShare];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
        activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeMessage,UIActivityTypeMail,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];
        
        if ([activityVC respondsToSelector:@selector(popoverPresentationController)]) {
            activityVC.popoverPresentationController.sourceView = self.view;
        }
        
        [self presentViewController:activityVC animated:YES completion:nil];
    }else if([nsstrName isEqual:@"去除广告"]){
        if (!_iapManager)
        {
            _iapManager = [[IAPManager alloc] init];
        }
        // iTunesConnect 苹果后台配置的产品ID
        [_iapManager startPurchWithID:@"removead" completeHandle:^(IAPPurchType type,NSData *data) {
            NSString *str = @"购买成功,重启app后无广告!";
            switch (type) {
                case kIAPPurchSuccess:
                    [[NSUserDefaults standardUserDefaults]setObject:@"Success" forKey:@"_iapManager"];
                    break;
                case kIAPPurchFailed:
                    str = @"购买失败";
                    break;
                case kIAPPurchCancle:
                    str = @"用户取消购买";
                    break;
                case KIAPPurchVerFailed:
                    str = @"订单校验失败";
                    break;
                case KIAPPurchVerSuccess:
                    [[NSUserDefaults standardUserDefaults]setObject:@"Success" forKey:@"_iapManager"];
                    break;
                case kIAPPurchNotArrow:
                    str = @"不允许程序内付费";
                    break;
            }
            [SVProgressHUD showInfoWithStatus:str];
        }];
        return;
    } else if ([nsstrName isEqual:@"下载管理"]) {
        
      
        
        IIViewDeckController *deckVC = (IIViewDeckController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        UITabBarController *tabVC = (UITabBarController *)deckVC.centerViewController;
        [deckVC closeSide:YES];
        
        M_DownloaderController *downloadVC = [[M_DownloaderController alloc] init];
        downloadVC.downloadedFirst = YES;
        downloadVC.hidesBottomBarWhenPushed = YES;
        [(UINavigationController *)tabVC.selectedViewController pushViewController:downloadVC animated:YES];
        
    } else if ([nsstrName isEqual:@"意见反馈"]) {

      
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/us/app/twitter/id%@?mt=8&action=write-review",kAppId]]];
        
    }
    
    
}

#pragma mark - Getter or Setter

-(NSArray *)array
{
    if (!_array) {
         _array= [[NSArray alloc] init];
        
        if ([Common shareCommon].zs_Open) {
            _array = @[@"下载管理",@"意见反馈",@"免责声明",@"分享给朋友",@"清除缓存"];
        } else {
            _array = @[@"免责声明",@"清除缓存"];
        }
        
        
    }
    
    return _array;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        CGSize size=[UIScreen mainScreen].bounds.size;
         _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, size.width, size.height) style:UITableViewStylePlain];
        if (@available(iOS 11.0,*)) {
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
        }
         _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
         _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.tableHeaderView = self.tableHeader;
    }
    
    return _tableView;
}



-(UISwitch *)switchBrightness
{
    if (!_switchBrightness) {
        CGSize size=[UIScreen mainScreen].bounds.size;
         _switchBrightness=[[UISwitch alloc]initWithFrame:CGRectMake(size.width*332/414-70, 9, 35, 25)];
        //[_switchBrightness setOnTintColor:[UIColor whiteColor]];
        [_switchBrightness addTarget:self action:@selector(oneSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
        bool isCloseChangLiangScreen = [[NSUserDefaults standardUserDefaults] boolForKey:kCloseChangLiang];
        if (isCloseChangLiangScreen) {
            [_switchBrightness setOn:NO];
        }else{
            [_switchBrightness setOn:YES];
        }
    }
    
    return _switchBrightness;
}

-(UILabel *)labelCacheSize{
    if (!_labelCacheSize) {
         _labelCacheSize=[[UILabel alloc]initWithFrame:CGRectMake(kCScreenWidth*332/414-70, 8, 70, 30)];
        [_labelCacheSize setTextColor:[UIColor whiteColor]];
        [_labelCacheSize setFont:[UIFont systemFontOfSize:14]];
    }
    
    return _labelCacheSize;
}

-(UIView *)userContainerView{
    if (!_userContainerView) {
         _userContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kCScreenWidth, 146)];
        //[_userContainerView setBackgroundColor:kNavigationBgColor];
        NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
        NSString *icon = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
        UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kCScreenWidth-60)/2, 10, 60, 60)];
        [iconImageView setImage:[UIImage imageNamed:icon]];
        iconImageView.layer.cornerRadius = iconImageView.frame.size.width / 2; 
        iconImageView.layer.masksToBounds = YES;
        iconImageView.layer.borderWidth=1;
        iconImageView.layer.borderColor=[UIColor whiteColor].CGColor;
        [_userContainerView addSubview:iconImageView];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_userContainerView.frame)-50, kCScreenWidth, 50)];
        [bottomView setBackgroundColor:kSettingBottomViewBgColor];
        [_userContainerView addSubview:bottomView];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake((kCScreenWidth-1)/2, (CGRectGetHeight(bottomView.frame)-14)/2, 1, 14)];
        [lineView setBackgroundColor:kRGBCOLOR(255, 255, 255)];
        [bottomView addSubview:lineView];
        
        UILabel *labelUseDayCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(((kCScreenWidth-1)/2-40)/2, (CGRectGetHeight(bottomView.frame)-31)/2, 40, 15)];
        [labelUseDayCountLabel setText:[NSString stringWithFormat:@"%zd 天",[ZY_Common getReadBookUseTimeCount]]];
        [labelUseDayCountLabel setFont:[UIFont systemFontOfSize:14]];
        [labelUseDayCountLabel setTextAlignment:NSTextAlignmentCenter];
        [labelUseDayCountLabel setTextColor:[UIColor whiteColor]];
        [bottomView addSubview:labelUseDayCountLabel];
        
        self.labelReadCountLabel = [[UILabel alloc] initWithFrame:CGRectMake((kCScreenWidth-1)/2+((kCScreenWidth-1)/2-40)/2, (CGRectGetHeight(bottomView.frame)-31)/2, 40, 15)];
        NSMutableArray *bookShelfArray =[[ZY_DBHelper shareDBHelper] queryBookShelfInfoList];
        [self.labelReadCountLabel setText:[NSString stringWithFormat:@"%zd 本",bookShelfArray.count]];
        [self.labelReadCountLabel setFont:[UIFont systemFontOfSize:14]];
        [self.labelReadCountLabel setTextColor:[UIColor whiteColor]];
        [self.labelReadCountLabel setTextAlignment:NSTextAlignmentCenter]; 
        [bottomView addSubview:self.labelReadCountLabel];
        
        UILabel *useTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(((kCScreenWidth-1)/2-80)/2, (CGRectGetHeight(bottomView.frame))/2, 80, 15)];
        [useTimeLabel setText:@"使用时长"];
        [useTimeLabel setFont:[UIFont systemFontOfSize:13]];
        [useTimeLabel setTextColor:kRGBCOLOR(255, 255, 255)];
        [useTimeLabel setTextAlignment:NSTextAlignmentCenter];
        [bottomView addSubview:useTimeLabel];
        
        UILabel *readCountLabel = [[UILabel alloc] initWithFrame:CGRectMake((kCScreenWidth-1)/2+((kCScreenWidth-1)/2-80)/2, (CGRectGetHeight(bottomView.frame))/2, 80, 15)];
        [readCountLabel setText:@"已经读过"];
        [readCountLabel setFont:[UIFont systemFontOfSize:13]];
        [readCountLabel setTextColor:kRGBCOLOR(255, 255, 255)];
        [readCountLabel setTextAlignment:NSTextAlignmentCenter];
        [bottomView addSubview:readCountLabel];
    }
    
    return _userContainerView;
}

- (UIView *)tableHeader {
    if (_tableHeader == nil) {
        _tableHeader = [[UIView alloc] init];
        [_tableHeader setFrame:[Common get414FrameByMap:CGRectMake(0, 0, 414, 205-52)]];
        [_tableHeader addSubview:({
            UIImageView *headImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settingHead"]];
            [headImage setFrame:[Common get414FrameByMap:CGRectMake(19, 76, 56, 56)]];
            headImage;
            
        })];
        [_tableHeader addSubview:({
            UILabel *userNameLabel = [[UILabel alloc] init];
            [userNameLabel setFrame:[Common get414FrameByMap:CGRectMake(90, 96, 414, 16)]];
            [userNameLabel setText:@"游客用户"];
            [userNameLabel setTextColor:[UIColor whiteColor]];
            [userNameLabel setTextAlignment:NSTextAlignmentLeft];
            [userNameLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
            userNameLabel;
        })];
    }
    return _tableHeader;
}

@end

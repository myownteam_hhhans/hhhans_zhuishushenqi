//
//  ZY_BookRankViewController.m
//  ZYNovel
//
//  Created by apple on 2018/6/15.
//

#import "ZY_BookRankViewController.h"
#import "ZY_BookLIst.h"
#import "ZY_HeadEntity.h"
#import "ZY_Common.h"


@interface ZY_BookRankViewController ()

@end

@implementation ZY_BookRankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.titleInfo;
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    ZY_BookLIst *_bookList = [[ZY_BookLIst alloc]initWithFrame:CGRectMake(0, 0, kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height-49)];
    _bookList.BaseMethodUrl = [NSString stringWithFormat: @"https://shuapi.jiaston.com/top/%@/top/%@/%%@/%%i.html",self.sex,self.urlKey];
    [self.view addSubview:_bookList];
    
    NSMutableArray *headArr = [[NSMutableArray alloc]init];
    ZY_HeadEntity *head1 = [[ZY_HeadEntity alloc]init];
    head1.title = @"周榜";
    head1.urlKey = @"week";
    [headArr addObject:head1];
    
    ZY_HeadEntity *head2 = [[ZY_HeadEntity alloc]init];
    head2.title = @"月榜";
    head2.urlKey = @"month";
    [headArr addObject:head2];
    
    ZY_HeadEntity *head3 = [[ZY_HeadEntity alloc]init];
    head3.title = @"总榜";
    head3.urlKey = @"total";
    [headArr addObject:head3];
    
    _bookList.head_arr = headArr;
    [_bookList createHeadView];
    [_bookList createTableView];
    [self.view addSubview:_bookList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

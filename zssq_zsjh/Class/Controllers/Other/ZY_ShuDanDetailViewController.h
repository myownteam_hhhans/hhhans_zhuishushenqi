//
//  ZY_ShuDanDetailViewController.h
//  ZYNovel
//

#import <UIKit/UIKit.h>
#import "ZY_ShuDan.h"

@interface ZY_ShuDanDetailViewController : UIViewController

@property (nonatomic,strong) ZY_ShuDan *shudan;
@end

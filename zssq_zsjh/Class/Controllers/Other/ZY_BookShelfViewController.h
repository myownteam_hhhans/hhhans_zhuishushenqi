//
//  BookShelfViewController.h
//  ZYNovel
//  书架
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZY_BookShelfViewController : UIViewController

-(void)refreshBookShelf;

@end

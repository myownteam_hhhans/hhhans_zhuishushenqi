//
//  ABP_GuideController.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/3.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABP_GuideControllerDelegate <NSObject>

@required

-(void)gotoMainInterface;

@end

@interface ABP_GuideController : UIViewController

@property (nonatomic, weak) id<ABP_GuideControllerDelegate> delegate;

@end

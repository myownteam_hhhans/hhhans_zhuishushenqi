//
//  ABP_GuideController.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/3.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "ABP_GuideController.h"
#import "Common.h"
#import "M_UserFavourateButton.h"
#import "NWS_httpClient.h"
#import "ZY_NovelInfo.h"
#import "ZY_RecommentBookCell.h"
#import "ZY_DBHelper.h"
#import <SVProgressHUD.h>
#import "ZY_Common.h"
#import "ZY_JsonParser.h"

#define IS_PAD (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad)

@interface ABP_GuideController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UIScrollView *rootView;

// 男女选择
@property (nonatomic, strong) UILabel *choose_infoLabel;
@property (nonatomic, strong) UIButton *choose_malebButton;
@property (nonatomic, strong) UIButton *choose_femaleButton;
@property (nonatomic, strong) NSString *gender;

// 偏好选择
@property (nonatomic, strong) UILabel *pre_infoLabel;
@property (nonatomic, strong) UILabel *pre_detailInfoLabel;
@property (nonatomic, strong) UIButton *pre_nextButton;

// 推荐
@property (nonatomic, strong) UILabel *rec_infoLabel;
@property (nonatomic, strong) UILabel *rec_detailInfoLabel;
@property (nonatomic, strong) UITableView *rec_table;
@property (nonatomic, strong) UIButton *rec_noNeedButton;
@property (nonatomic, strong) UIButton *rec_addShelfButton;

// 数据源
@property (nonatomic, strong) NSArray *cateArray;
@property (nonatomic, strong) NSMutableArray *selectedCateArray;
@property (nonatomic, strong) NSMutableArray *recommendBookArray;  // 推荐的书

@property (nonatomic, strong) NSMutableArray *selectedIndexArray;  // 选中的cell序号

@end

@implementation ABP_GuideController


#pragma mark - life circle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.rootView];
    
    [self.rootView addSubview:self.choose_infoLabel];
    [self.rootView addSubview:self.choose_malebButton];
    [self.rootView addSubview:self.choose_femaleButton];
    [self.rootView addSubview:self.pre_infoLabel];
    [self.rootView addSubview:self.pre_detailInfoLabel];
    [self.rootView addSubview:self.pre_nextButton];
    [self.rootView addSubview:self.rec_infoLabel];
    [self.rootView addSubview:self.rec_detailInfoLabel];
    [self.rootView addSubview:self.rec_table];
    [self.rootView addSubview:self.rec_noNeedButton];
    [self.rootView addSubview:self.rec_addShelfButton];
    
    [self addCateViews];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - privateFunc

// 选择分类的子控件添加
- (void)addCateViews {
    
    CGFloat spaceY = 24;
    if (IS_PAD) {
        spaceY = 5;
    }
    
    
    // 男生分类label
    [self.rootView addSubview:({
        UILabel *label = [[UILabel alloc] init];
        [label setTextColor:[Common colorWithHexString:@"#7EBAFF" alpha:1.0]];
        [label setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:14]]];
        [label setText:@"男生爱看"];
        [label sizeToFit];
        CGFloat centerY = [Common get414RadiusByMap:149+13/2] - k_StatusBarHeight;
        CGFloat cengerX = [Common get414RadiusByMap:414+75+55/2];
        [label setCenter:CGPointMake(cengerX, centerY)];
        label;
    })];
    [self.rootView addSubview:({
        UIImageView *headImage = [[UIImageView alloc] init];
        [headImage setFrame:[Common get414FrameByMap:CGRectMake(414+43, 142, 19, 26)]];
        [self fixStatusHeightForView:headImage];
        [headImage setImage:[UIImage imageNamed:@"guide_cateMale"]];
        headImage;
    })];
    // 女生分类label
    [self.rootView addSubview:({
        UILabel *label = [[UILabel alloc] init];
        [label setTextColor:[Common colorWithHexString:@"#FF9393" alpha:1.0]];
        [label setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:14]]];
        [label setText:@"女生分类"];
        [label sizeToFit];
        CGFloat centerY = [Common get414RadiusByMap:314+13/2] - k_StatusBarHeight;
        CGFloat cengerX = [Common get414RadiusByMap:414+75+55/2];
        if (IS_PAD) {
            centerY-= 80;
        }
        [label setCenter:CGPointMake(cengerX, centerY)];
        label;
    })];
    [self.rootView addSubview:({
        UIImageView *headImage = [[UIImageView alloc] init];
        
        
        [headImage setFrame:[Common get414FrameByMap:CGRectMake(414+43, 309, 25, 25)]];
        
        if (IS_PAD) {
            CGRect frame = headImage.frame;
            frame.origin.y -= 80;
            headImage.frame = frame;
        }
        [self fixStatusHeightForView:headImage];
        [headImage setImage:[UIImage imageNamed:@"guide_cateFemale"]];
        headImage;
    })];
    // 出版物label
    [self.rootView addSubview:({
        UILabel *label = [[UILabel alloc] init];
        [label setTextColor:[Common colorWithHexString:@"#92D39E" alpha:1.0]];
        [label setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:14]]];
        [label setText:@"出版读物"];
        [label sizeToFit];
        CGFloat centerY = [Common get414RadiusByMap:480+13/2] - k_StatusBarHeight;
        CGFloat cengerX = [Common get414RadiusByMap:414+75+55/2];
        if (IS_PAD) {
            centerY-= 160;
        }
        [label setCenter:CGPointMake(cengerX, centerY)];
        label;
    })];
    [self.rootView addSubview:({
        UIImageView *headImage = [[UIImageView alloc] init];
        [headImage setFrame:[Common get414FrameByMap:CGRectMake(414+44, 474, 22, 25)]];
        if (IS_PAD) {
            CGRect frame = headImage.frame;
            frame.origin.y -= 160;
            headImage.frame = frame;
        }
        [self fixStatusHeightForView:headImage];
        [headImage setImage:[UIImage imageNamed:@"guide_catePublication"]];
        headImage;
    })];
    
    CGFloat buttonWidth = [Common get414RadiusByMap:85];
    CGFloat buttonHeight = [Common get414RadiusByMap:30];
    CGFloat x;
    CGFloat y;
    CGFloat buttonYSpace = 24;
    // 男生分类button
    NSArray *maleSubData = self.cateArray[0];
    for (int i = 0; i < maleSubData.count; i++) {
        x = k_ScreenWidth + [Common get414RadiusByMap:33] + i%3*[Common get414RadiusByMap:85 + 46];
        y = [Common get414RadiusByMap:186] - k_StatusBarHeight + i/3*[Common get414RadiusByMap:(30 + spaceY)];
        NSString *titleStr = maleSubData[i];
        M_UserFavourateButton *btn = [M_UserFavourateButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleStr forState:UIControlStateNormal];
        [btn setFrame:CGRectMake(x, y, buttonWidth, buttonHeight)];
        [btn setTitleColor:[Common colorWithHexString:@"#7EBAFF" alpha:1.0] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:13]]];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn addTarget:self action:@selector(cateButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn.layer setCornerRadius:buttonHeight/2];
        btn.layer.borderColor = btn.currentTitleColor.CGColor;
        btn.layer.borderWidth = 1.0;
        
        
        
        [self.rootView addSubview:btn];
    }
    // 女生分类button
    NSArray *femaleSubData = self.cateArray[1];
    for (int i = 0; i < femaleSubData.count; i++) {
        x = k_ScreenWidth + [Common get414RadiusByMap:33] + i%3*[Common get414RadiusByMap:85 + 46];
        y = [Common get414RadiusByMap:353] - k_StatusBarHeight + i/3*[Common get414RadiusByMap:(30 + spaceY)];
        NSString *titleStr = femaleSubData[i];
        M_UserFavourateButton *btn = [M_UserFavourateButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleStr forState:UIControlStateNormal];
        [btn setFrame:CGRectMake(x, y, buttonWidth, buttonHeight)];
        [btn setTitleColor:[Common colorWithHexString:@"#FF9494" alpha:1.0] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:13]]];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn addTarget:self action:@selector(cateButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn.layer setCornerRadius:buttonHeight/2];
        btn.layer.borderColor = btn.currentTitleColor.CGColor;
        btn.layer.borderWidth = 1.0;
        
        if (IS_PAD) {
            CGRect frame = btn.frame;
            frame.origin.y -= 80;
            btn.frame = frame;
        }
        
        [self.rootView addSubview:btn];
    }
    // 出版分类button
    NSArray *publicationSubData = self.cateArray[2];
    for (int i = 0; i < publicationSubData.count; i++) {
        x = k_ScreenWidth + [Common get414RadiusByMap:33] + i%3*[Common get414RadiusByMap:85 + 46];
        y = [Common get414RadiusByMap:519] - k_StatusBarHeight + i/3*[Common get414RadiusByMap:(30 + spaceY)];
        NSString *titleStr = publicationSubData[i];
        M_UserFavourateButton *btn = [M_UserFavourateButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleStr forState:UIControlStateNormal];
        [btn setFrame:CGRectMake(x, y, buttonWidth, buttonHeight)];
        [btn setTitleColor:[Common colorWithHexString:@"#92D39E" alpha:1.0] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:13]]];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn addTarget:self action:@selector(cateButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [btn.layer setCornerRadius:buttonHeight/2];
        btn.layer.borderColor = btn.currentTitleColor.CGColor;
        btn.layer.borderWidth = 1.0;
        
        if (IS_PAD) {
            CGRect frame = btn.frame;
            frame.origin.y -= 160;
            btn.frame = frame;
        }
        
        [self.rootView addSubview:btn];
    }
}

- (void)cateButtonClick:(UIButton *)sender {
    
    // 判断是否还可以点击
    if (!sender.selected && self.selectedCateArray.count == 4) {
        [SVProgressHUD showErrorWithStatus:@"最多选择四个"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        return;
    }
    // 改变选中状态颜色
    UIColor *borderColor = [UIColor colorWithCGColor:sender.layer.borderColor];
    sender.selected = !sender.selected;
    [sender setBackgroundColor: sender.selected ? borderColor : [UIColor whiteColor]];
    // 修改偏好数据
    if (sender.selected) {
        [self.selectedCateArray addObject:sender.titleLabel.text];
    } else {
        [self.selectedCateArray removeObject:sender.titleLabel.text];
    }
    
  
    
}

- (void)fixStatusHeightForView:(UIView *)view {
    view.transform = CGAffineTransformMakeTranslation(0, -k_StatusBarHeight);
}

- (void)loadBookData {
    
    
    if (self.selectedCateArray.count == 0) {
        [SVProgressHUD showErrorWithStatus:@"至少选择一个分类"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        return;
    }
    
    
    NSMutableString *cateString = [NSMutableString string];
    for (NSString *string in self.selectedCateArray) {
        [cateString appendString:string];
        [cateString appendString:@","];
    }
    
    NSDictionary *paramenters = @{
                                  @"cats" : cateString,
                                  @"packageName" : @"com.ushaqi.zhuishushenqi",
                                  @"gender" : self.gender
                                  };
    
    [[NWS_httpClient sharedInstance] getWithURLString:@"http://api.zhuishushenqi.com/v2/book/recommend" parameters:paramenters success:^(NSDictionary *responseObject) {
        
        [self.recommendBookArray removeAllObjects];
        [self.recommendBookArray removeAllObjects];
        
        NSArray *bookDicArray = responseObject[@"books"];
        
        int index = 0;
        
        for (NSDictionary *bookDic in bookDicArray) {
            
            
            
            ZY_NovelInfo *info = [[[ZY_JsonParser alloc] init] getNovelByDic:bookDic];
            info.novelCategory = bookDic[@"minorCate"];
            info.novelAuthor = bookDic[@"author"];
            id score = [bookDic objectForKey:@"rating"];
            if ([score isKindOfClass:[NSDictionary class]]) {
                info.novelScore = [NSString stringWithFormat:@"%@",[score objectForKey:@"score"]];
            }
            info.novelId = bookDic[@"_id"];
            info.novelName = bookDic[@"title"];
            NSString *string  = bookDic[@"cover"];
            string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            string = [string substringFromIndex:7];
            info.novelCoverUrl = string;
            info.novelDes = bookDic[@"shortIntro"];
            info.novelLatestChapter = bookDic[@"lastChapter"];
            [self.recommendBookArray addObject:info];
            [self.selectedIndexArray addObject:@(index)];
            index ++;
            
        }
        [self.rec_table reloadData];
        
        [self.rootView setContentOffset:CGPointMake(k_ScreenWidth*2, self.rootView.contentOffset.y) animated:YES];
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)sexButtonClick:(UIButton *)sender {
    
    // 推荐书籍
    // 推荐书籍
    NSString *fileName = @"recommend";
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *dictResult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    NSArray *booksArray;
    
    if (sender.tag == 0) {
        booksArray = dictResult[@"女频"];
        [MobClick event:@"pages" label:@"女生"];
    } else {
        booksArray = dictResult[@"男频"];
        [MobClick event:@"pages" label:@"男生"];
    }
    
    for (NSDictionary *dic in booksArray) {
        ZY_NovelInfo *info = [self getNoelInfoFromDic:dic];
        [self.recommendBookArray addObject:info];
    }
    
    if (self.recommendBookArray.count > 6) {
        self.recommendBookArray = [Common randomAccessData:self.recommendBookArray WithSpecifiedNumber:6];
    }
    // 进入主界面
    [self closeGuide:nil];
    self.gender = sender.tag == 0 ? @"male" : @"female";
    
}

- (ZY_NovelInfo *)getNoelInfoFromDic:(NSDictionary *)bookDic {
    ZY_NovelInfo *info = [[ZY_NovelInfo alloc] init];
    info.novelCategory = bookDic[@"cat"];
    info.novelAuthor = bookDic[@"author"];
    info.novelStatus = bookDic[@"isSerial"];
    info.novelLatestChapter = bookDic[@"lastChapter"];
    id score = [bookDic objectForKey:@"rating"];
    if ([score isKindOfClass:[NSDictionary class]]) {
        info.novelScore = [NSString stringWithFormat:@"%@",[score objectForKey:@"score"]];
    }
    info.novelId = bookDic[@"_id"];
    info.novelName = bookDic[@"title"];
    NSString *string  = bookDic[@"cover"];
    string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if (string && string.length > 7) {
        string = [string substringFromIndex:7];
        info.novelCoverUrl = string;
    }
    
    info.novelDes = bookDic[@"shortIntro"];
    info.WordCountFormat = [bookDic[@"wordCount"] stringValue];
    
    info.novelListUrl = [NSString stringWithFormat:@"https://api.zhuishushenqi.com/mix-toc/%@/",info.novelId];
    
    return info;
}

- (void)closeGuide:(UIButton *)sender {
    
    for (ZY_NovelInfo *info in self.recommendBookArray) {
        [[ZY_DBHelper shareDBHelper] addBookToShelf:info];
    }
    
    if ([self.delegate respondsToSelector:@selector(gotoMainInterface)]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"guideVCOpened"];
        [self.delegate gotoMainInterface];
    }
    
}

#pragma mark - tableView Delegate & DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recommendBookArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [Common get414RadiusByMap:148];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZY_RecommentBookCell *cell = [self.rec_table dequeueReusableCellWithIdentifier:@"bookCell"];
    [cell bindNovelItem:self.recommendBookArray[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectedButton.selected = NO;
    
    NSNumber *selectNumber = @(indexPath.row);
    for (NSNumber *addedNumber in self.selectedIndexArray) {
        if (selectNumber.integerValue == addedNumber.integerValue) {
            cell.selectedButton.selected = YES;
            return cell;
        }
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *selectNumber = @(indexPath.row);
    for (NSNumber *addedNumber in self.selectedIndexArray) {
        if (selectNumber.integerValue == addedNumber.integerValue) {
            [self.selectedIndexArray removeObject:addedNumber];
            
            [self.rec_table reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            return;
        }
    }
    [self.selectedIndexArray addObject:selectNumber];
    [self.rec_table reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - lazyLoad

-(UILabel *)choose_infoLabel {
    if (_choose_infoLabel == nil) {
        _choose_infoLabel = [[UILabel alloc] init];
        [_choose_infoLabel setFrame:[Common get414FrameByMap:CGRectMake(0, 138, 414, 26)]];
        [self fixStatusHeightForView:_choose_infoLabel];
        [_choose_infoLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:26]]];
        [_choose_infoLabel setText:@"选个爱看的"];
        [_choose_infoLabel setTextAlignment:NSTextAlignmentCenter];
        [_choose_infoLabel setTextColor:[Common colorWithHexString:@"#333333" alpha:1.0]];
    }
    return _choose_infoLabel;
}
- (UIButton *)choose_malebButton {
    if (_choose_malebButton == nil) {
        _choose_malebButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [_choose_malebButton setFrame:[Common get414FrameByMap:CGRectMake(53, 199, 312, 140)]];
        [self fixStatusHeightForView:_choose_malebButton];
        [_choose_malebButton setBackgroundImage:[UIImage imageNamed:@"guide_maleButton"] forState:UIControlStateNormal];
        [_choose_malebButton addTarget:self action:@selector(sexButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_choose_malebButton setTag:0];
    }
    return _choose_malebButton;
}
- (UIButton *)choose_femaleButton {
    if (_choose_femaleButton == nil) {
        _choose_femaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_choose_femaleButton setFrame:[Common get414FrameByMap:CGRectMake(53, 366, 312, 140)]];
        [self fixStatusHeightForView:_choose_femaleButton];
        [_choose_femaleButton setBackgroundImage:[UIImage imageNamed:@"guide_femaleButton"] forState:UIControlStateNormal];
        [_choose_femaleButton addTarget:self action:@selector(sexButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_choose_malebButton setTag:1];
    }
    return _choose_femaleButton;
}
- (UILabel *)pre_infoLabel {
    if (_pre_infoLabel == nil) {
        _pre_infoLabel = [[UILabel alloc] init];
        [_pre_infoLabel setFrame:[Common get414FrameByMap:CGRectMake(414, 65, 414, 24)]];
        [self fixStatusHeightForView:_pre_infoLabel];
        [_pre_infoLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:24]]];
        [_pre_infoLabel setText:@"选择您的阅读偏好"];
        [_pre_infoLabel setTextAlignment:NSTextAlignmentCenter];
        [_pre_infoLabel setTextColor:[Common colorWithHexString:@"#292929" alpha:1.0]];
    }
    return _pre_infoLabel;
}
- (UILabel *)pre_detailInfoLabel {
    if (_pre_detailInfoLabel == nil) {
        _pre_detailInfoLabel = [[UILabel alloc] init];
        [_pre_detailInfoLabel setFrame:[Common get414FrameByMap:CGRectMake(414, 98, 414, 12)]];
        [self fixStatusHeightForView:_pre_detailInfoLabel];
        [_pre_detailInfoLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:12]]];
        [_pre_detailInfoLabel setText:@"让我们更好的为您服务（最多四个）"];
        [_pre_detailInfoLabel setTextAlignment:NSTextAlignmentCenter];
        [_pre_detailInfoLabel setTextColor:[Common colorWithHexString:@"#999999" alpha:1.0]];
    }
    return _pre_detailInfoLabel;
}

- (UIButton *)pre_nextButton {
    if (_pre_nextButton == nil) {
        _pre_nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_pre_nextButton setFrame:[Common get414FrameByMap:CGRectMake(112+414, 736-39-40, 190, 40)]];
        
        if (CGRectGetMaxY(_pre_nextButton.frame)> k_ScreenHeight) {
            CGRect frame = _pre_nextButton.frame;
            frame.origin.y = k_ScreenHeight - 70;
            _pre_nextButton.frame = frame;
        }
        
        [self fixStatusHeightForView:_pre_nextButton];
        [_pre_nextButton setTitle:@"开启阅读之旅" forState:UIControlStateNormal];
        [_pre_nextButton setBackgroundColor:[Common colorWithHexString:@"#FF9393" alpha:1.0]];
        [_pre_nextButton.layer setCornerRadius:[Common get414RadiusByMap:20]];
        [_pre_nextButton addTarget:self action:@selector(loadBookData) forControlEvents:UIControlEventTouchUpInside];
    }
    return _pre_nextButton;
}

- (UILabel *)rec_infoLabel {
    if (_rec_infoLabel == nil) {
        _rec_infoLabel = [[UILabel alloc] init];
        [_rec_infoLabel setFrame:[Common get414FrameByMap:CGRectMake(828, 45, 414, 24)]];
        [self fixStatusHeightForView:_rec_infoLabel];
        [_rec_infoLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:24]]];
        [_rec_infoLabel setText:@"为您推荐以下好书"];
        [_rec_infoLabel setTextAlignment:NSTextAlignmentCenter];
        [_rec_infoLabel setTextColor:[Common colorWithHexString:@"#292929" alpha:1.0]];
    }
    return _rec_infoLabel;
}
- (UILabel *)rec_detailInfoLabel {
    if (_rec_detailInfoLabel == nil) {
        _rec_detailInfoLabel = [[UILabel alloc] init];
        [_rec_detailInfoLabel setFrame:[Common get414FrameByMap:CGRectMake(828, 78, 414, 16)]];
        [self fixStatusHeightForView:_rec_detailInfoLabel];
        [_rec_detailInfoLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
        [_rec_detailInfoLabel setText:@"希望您能喜欢"];
        [_rec_detailInfoLabel setTextAlignment:NSTextAlignmentCenter];
        [_rec_detailInfoLabel setTextColor:[Common colorWithHexString:@"#BDBDBD" alpha:1.0]];
    }
    return _rec_detailInfoLabel;
}
-(UITableView *)rec_table {
    if (_rec_table == nil) {
        _rec_table = [[UITableView alloc] initWithFrame:CGRectMake([Common get414RadiusByMap:828], [Common get414RadiusByMap:118], [Common get414RadiusByMap:414], k_ScreenHeight - [Common get414RadiusByMap:178]) style:UITableViewStylePlain];
        [self fixStatusHeightForView:_rec_table];
        [_rec_table setDelegate:self];
        [_rec_table setDataSource:self];
        [_rec_table registerClass:[ZY_RecommentBookCell class] forCellReuseIdentifier:@"bookCell"];
        [_rec_table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _rec_table;
}
-(UIButton *)rec_noNeedButton {
    if (_rec_noNeedButton == nil) {
        _rec_noNeedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rec_noNeedButton setFrame:[Common get414FrameByMap:CGRectMake(23 + 828, 686, 153, 40)]];
        [_rec_noNeedButton setBackgroundColor:[Common colorWithHexString:@"#B1E0F9" alpha:1.0]];
        
        [self fixStatusHeightForView:_rec_noNeedButton];
        [_rec_noNeedButton setTitleColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0] forState:UIControlStateNormal];
        [_rec_noNeedButton setTitle:@"暂不需要" forState:UIControlStateNormal];
        [_rec_noNeedButton.layer setCornerRadius:_rec_noNeedButton.frame.size.height/2];
        [_rec_noNeedButton setTag:0];
        [_rec_noNeedButton addTarget:self action:@selector(closeGuide:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect rect = _rec_noNeedButton.frame;
        rect.origin.y = k_ScreenHeight - k_StatusBarHeight - [Common get414RadiusByMap:50];
        _rec_noNeedButton.frame = rect;
    }
    return _rec_noNeedButton;
}
- (UIButton *)rec_addShelfButton {
    if (_rec_addShelfButton == nil) {
        _rec_addShelfButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rec_addShelfButton setFrame:[Common get414FrameByMap:CGRectMake(238 + 828, 686, 153, 40)]];
        [self fixStatusHeightForView:_rec_addShelfButton];
        [_rec_addShelfButton setBackgroundColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0]];
        [_rec_addShelfButton setTitleColor:[Common colorWithHexString:@"#F9F9F9" alpha:1.0] forState:UIControlStateNormal];
        [_rec_addShelfButton setTitle:@"加入书架" forState:UIControlStateNormal];
        [_rec_addShelfButton.layer setCornerRadius:_rec_addShelfButton.frame.size.height/2];
        [_rec_addShelfButton setTag:1];
        [_rec_addShelfButton addTarget:self action:@selector(closeGuide:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect rect = _rec_addShelfButton.frame;
        rect.origin.y = k_ScreenHeight - k_StatusBarHeight - [Common get414RadiusByMap:50];
        _rec_addShelfButton.frame = rect;
    }
    return _rec_addShelfButton;
}

-(UIScrollView *)rootView{
    if (!_rootView) {
        _rootView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0, k_ScreenWidth, k_ScreenHeight)];
        [_rootView setContentSize:CGSizeMake(k_ScreenWidth*3,1)];
        [_rootView setShowsHorizontalScrollIndicator:NO];
        [_rootView setPagingEnabled:YES];
        [_rootView setBounces:NO];
        [_rootView setScrollEnabled:NO];
        _rootView.backgroundColor = [UIColor whiteColor];
        [_rootView setDelegate:self];
    }
    return _rootView;
}

#pragma mark - data array
- (NSArray *)cateArray {
    if (_cateArray == nil) {
        _cateArray = @[
                       @[@"玄幻",@"都市",@"仙侠",@"历史",@"游戏",@"灵异"],
                       @[@"耽美百合",@"幻想言情",@"古代言情",@"现代言情",@"科幻空间",@"青春校园"],
                       @[@"生活时尚",@"人文社科",@"文学艺术",@"商业与经济",@"心理与励志",@"出版小说"]
                       ];
    }
    return _cateArray;
}
- (NSMutableArray *)selectedCateArray {
    if (_selectedCateArray == nil) {
        _selectedCateArray = [NSMutableArray array];
    }
    return _selectedCateArray;
}

- (NSMutableArray *)recommendBookArray {
    if (!_recommendBookArray) {
        _recommendBookArray = [NSMutableArray array];
    }
    return _recommendBookArray;
}
- (NSMutableArray *)selectedIndexArray {
    if (_selectedIndexArray == nil) {
        _selectedIndexArray = [NSMutableArray array];
    }
    return _selectedIndexArray;
}



@end

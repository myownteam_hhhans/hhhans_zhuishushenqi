//
//  ZY_ShuDanViewController.m
//  ZYNovel
//
//

#import "ZY_ShuDanViewController.h"
#import "ZY_Common.h"
#import "ZY_DataDownloader.h"
#import "ZY_ADHelper.h"
#import "CFT_parameterOnline.h"
#import "UITableView+JRTableViewPlaceHolder.h"
#import "MJRefresh.h"
#import "ZY_JsonParser.h"
#import "RNChosenTableViewCell.h"
#import "ZY_ShuDanADTableViewCell.h"
#import "ZY_ShuDanDetailViewController.h"
#import "Common.h"

@interface ZY_ShuDanViewController ()<ZY_DataDownloaderDelegate,UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) NSMutableArray *dataArray;

@property(nonatomic,strong) ZY_DataDownloader *downLoader;

@property(nonatomic,strong) ADModel *adEntity;

@property(nonatomic,assign) NSInteger nsintPage;

@property(nonatomic,assign) BOOL isRefresh;

@property(nonatomic,assign) BOOL isPulldownRefresh;

@end


static NSString *nsstrShuDanIdentifier = @"BookCellIdentifier";

static NSString *nsstrShuDanADIdentifier= @"BookStoreAdIdentifier";

@implementation ZY_ShuDanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"推荐男生书单";
    if ([self.sex isEqualToString:@"lady"]) {
        self.title = @"推荐女生书单";
    }
    self.dataArray = [[NSMutableArray alloc] init];
    
    ZY_WS(weakSelf);
    [weakSelf.tableView jr_configureWithPlaceHolderBlock:^UIView * _Nonnull(UITableView * _Nonnull sender) {
        UIView *errorView =[ZY_Common createLoadingErrorViewWithFrame:weakSelf.tableView.bounds withTarget:weakSelf.tableView.mj_header action:@selector(beginRefreshing)];
        [errorView setHidden:YES];
        
        return errorView;
        
    } normalBlock:^(UITableView * _Nonnull sender) {
        
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PrivateFunction

-(void)loadData{
    self.nsintPage=1;
    self.isPulldownRefresh=YES;
    ZY_WS(weakSelf);
    [weakSelf.tableView.jr_placeHolderView setHidden:YES];
    [self.downLoader getNovelHtmlDataByUrl:[NSString stringWithFormat:@"https://shuapi.jiaston.com/shudan/%@/all/commend/1.html",self.sex]];
}
-(void)loadNextData{
    
    [self.downLoader getNovelHtmlDataByUrl:[NSString stringWithFormat:@"https://shuapi.jiaston.com/shudan/%@/all/commend/%i.html",self.sex,(int)self.nsintPage]];
    self.isRefresh=YES;
}

#pragma mark - ZY_DataDownloaderDelegate
-(void)downloadNavError:(NSError*)error downloadErrorType:(downloadErrorType)type
{
    NSLog(@"Error:%@",error);
    __weak __typeof(self)weakSelf = self;
    [weakSelf.tableView.jr_placeHolderView setHidden:NO];
    DebugLog(@"错误___%@",error);
    [self.tableView.mj_header endRefreshing];
    if (self.isRefresh) {
        [self.tableView.mj_footer endRefreshing];
        self.isRefresh=NO;
    }
}

-(void)htmlDataFinish:(NSData *)htmlData{
    
    NSMutableArray *array = [[[ZY_JsonParser alloc] init] getShuDanListByData:htmlData];
    int cellHeight = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)?385:285;
    if (array&&array.count > 0) {
        if (self.isPulldownRefresh) {
            [self.dataArray removeAllObjects];
            self.isPulldownRefresh=NO;
        }
        int randx = arc4random() % 4;
        for (NSInteger i = 0; i<array.count; i++) {
            if (self.dataArray.count == randx) {
                
                
                    NSString *adtype = [[Common shareCommon] getADType];
                    AdPlatform platform = [ZY_ADHelper platformWithOnlineParameter:adtype];
                    if (platform != adplatform_Google) {
                        self.adEntity = [[ZY_ADHelper shareADHelper] obtainADInstance:adType_Feed adPlatform:platform];
                    }
                
                
                if (self.adEntity) {
                    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"_iapManager"]) {
                        [self.dataArray insertObject:@[nsstrShuDanADIdentifier,@(cellHeight),self.adEntity] atIndex:randx];
                    }
                }
            }
            
            [self.dataArray addObject:@[nsstrShuDanIdentifier,@(cellHeight),[array objectAtIndex:i]]];
        }
        if (self.nsintPage <= 10) {
            self.nsintPage++;
        }
    }
    if (self.isRefresh) {
        [self.tableView.mj_footer endRefreshing];
        self.isRefresh = NO;
    }
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[[self.dataArray objectAtIndex:indexPath.row] objectAtIndex:1] floatValue];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dataArray = [self.dataArray objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier=[dataArray firstObject];
    
    UITableViewCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:nsstrIdentifier forIndexPath:indexPath];
    tableViewCell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if ([nsstrIdentifier isEqualToString:nsstrShuDanIdentifier]){
        ZY_ShuDan *novelInfo = [dataArray lastObject];
        
        if (novelInfo) {
            ((RNChosenTableViewCell*)tableViewCell).chosenMode = novelInfo;
        }
        
    }else if ([nsstrIdentifier isEqualToString:nsstrShuDanADIdentifier]){
        if (self.adEntity&&self.adEntity.platform != adplatform_Google) {
            ((ZY_ShuDanADTableViewCell*)tableViewCell).data = self.adEntity;
        }
    }
    
    return tableViewCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *dataArray = [self.dataArray objectAtIndex:indexPath.row];
    NSString *nsstrIdentifier=[dataArray firstObject];
    
    if ([nsstrIdentifier isEqualToString:nsstrShuDanIdentifier]){
        ZY_ShuDan *novelInfo = [dataArray lastObject];
        
        if (novelInfo) {
          
            ZY_ShuDanDetailViewController *zyDetailViewController = [[ZY_ShuDanDetailViewController alloc] init];
            zyDetailViewController.shudan = novelInfo;
            self.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:zyDetailViewController animated:YES];
            self.hidesBottomBarWhenPushed = NO;
        }
        
    }else if ([nsstrIdentifier isEqualToString:nsstrShuDanADIdentifier]){
        if (self.adEntity&&self.adEntity.platform != adplatform_Google) {
 
            [[ZY_ADHelper shareADHelper] tapAD:self.adEntity CurrentVC:self];
        }
    }
}

#pragma mark - Getter or Setter

-(ZY_DataDownloader *)downLoader{
    if (!_downLoader) {
        _downLoader = [[ZY_DataDownloader alloc] init];
        [_downLoader setDelegate:self];
    }
    
    return _downLoader;
}

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,kCScreenWidth, kCScreenHeight-JH_Navigation_Height-JH_TabbarHome_Height) style:UITableViewStylePlain];
        if(@available(iOS 11.0, *)) {
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        _tableView.backgroundColor =  RGB(246, 247, 249);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView setBackgroundColor:[UIColor whiteColor]];
        [_tableView registerClass:[RNChosenTableViewCell class] forCellReuseIdentifier:nsstrShuDanIdentifier];
        [_tableView registerClass:[ZY_ShuDanADTableViewCell class] forCellReuseIdentifier:nsstrShuDanADIdentifier];
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadNextData)];
    }
    
    return _tableView;
}

@end

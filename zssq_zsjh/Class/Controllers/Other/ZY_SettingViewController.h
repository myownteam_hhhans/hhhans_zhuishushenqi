//
//  SettingViewController.h
//  ZYNovel
//  设置
//  Created by S on 2017/3/13.
//  Copyright © 2017年 S. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPManager.h"

@interface ZY_SettingViewController : UIViewController

@property(nonatomic,strong)IAPManager * iapManager;
@end

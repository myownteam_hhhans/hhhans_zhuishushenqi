//
//  B_HeaderView.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BS_HeaderViewDelegate <NSObject>

@optional
//0=排行榜,1=限免,2=推荐,3=完结
-(void)clickFunctionButton:(NSInteger)type;
-(void)clickCycleView:(NSInteger)index;

@end

@interface BS_HeaderView : UIView

@property (nonatomic, weak) id<BS_HeaderViewDelegate> delegate;

// 轮播视图绑定数据
- (void)bindData:(NSArray *)bookArr;
- (void)showInGreenMode;
@end

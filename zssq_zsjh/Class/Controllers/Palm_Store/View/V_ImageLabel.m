//
//  FN_ImageLabel.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/2.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "V_ImageLabel.h"
#import "Common.h"

@interface V_ImageLabel ()

@property(nonatomic,strong) UIImageView *headImage;

@property(nonatomic,strong) UILabel *labelTitle;

@end

@implementation V_ImageLabel

-(instancetype)initWithFrame:(CGRect)frame withTitle:(NSString *)title{
    self = [super initWithFrame:frame];
    
    if (self) {
       //[self addSubview:self.headImage];
       [self addSubview:self.labelTitle];
       [self.labelTitle setText:title];
    }
    
    return self;
}

#pragma mark - Getter or Setter

-(UIImageView *)headImage{
    if (!_headImage) {
         _headImage = [[UIImageView alloc] initWithFrame:[Common getFrameByMap:CGRectMake(10, 0, 4, 13)]];
         //_headImage.dk_imagePicker = DKImagePickerWithNames(@"FN_HeadImg",@"FN_HeadImg");
    }
    
    return _headImage;
}

-(UILabel *)labelTitle{
    if (!_labelTitle) {
         _labelTitle = [[UILabel alloc] initWithFrame:[Common getFrameByMap:CGRectMake(19,0, 100, 25)]];
         //_labelTitle.dk_textColorPicker = DKColorPickerWithKey(FN_ImageLabel_labelTitleTextColor);
        [_labelTitle setFont:[UIFont systemFontOfSize:[Common get414FontOfSizeByMap:17]weight:UIFontWeightSemibold]];
        [_labelTitle setTextAlignment:NSTextAlignmentLeft];
    }
    
    return _labelTitle;
}

-(void)setTitle:(NSString *)title{
    [self.labelTitle setText:title];
}

@end

//
//  BS_BookButton.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@interface BS_BookButton : UIButton

-(void)bindData:(ZY_NovelInfo *)bookInfo;

@end

//
//  V_LogoButton.m
//  Palm_Novel
//
//  Created by shiwei on 2018/3/27.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "V_ImageButton.h"

@implementation V_ImageButton

-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    
    return CGRectMake(0, 0, contentRect.size.width, contentRect.size.height);
}

@end

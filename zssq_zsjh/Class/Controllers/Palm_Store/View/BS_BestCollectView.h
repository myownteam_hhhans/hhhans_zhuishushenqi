//
//  BS_BestCollectView.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/6/13.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BS_BestCollectView : UIButton

@property (nonatomic, strong) UIImageView *novelCoverImageView;
@property (nonatomic, strong) UILabel *bookNameLabel;
@property (nonatomic, strong) UILabel *authorNameLabel;
@property (nonatomic, strong) UILabel *statusLabel;

- (void)bindStatusText:(NSString *)text;

@end

//
//  BD_SearchHeavyBookView.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/9/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BD_SearchHeavyBookView.h"
#import <UIImageView+WebCache.h>
#import "Common.h"

@interface BD_SearchHeavyBookView()

@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *tempLabel;

@end

@implementation BD_SearchHeavyBookView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.coverImageView];
        [self addSubview:self.nameLabel];
        [self addSubview:self.tempLabel];
    }
    return self;
}

- (void)bindDataWithCover:(NSString *)cover name:(NSString *)name temp:(NSString *)temp {
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:cover]];
    [self.nameLabel setText:name];
    [self.tempLabel setText:temp];
    
    if ([temp containsString:@"热搜度"]) {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:temp];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(3, 3)];
        [self.tempLabel setAttributedText:string];
    }
    
}

#pragma mark ==== 懒加载 ====
- (UIImageView *)coverImageView {
    if (_coverImageView == nil) {
        _coverImageView = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 84, 112)]];
        _coverImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        _coverImageView.layer.shadowOffset = CGSizeMake(0, 0);
        _coverImageView.layer.shadowRadius = 2;
        _coverImageView.layer.shadowOpacity = 0.3;
    }
    return _coverImageView;
}
- (UILabel *)nameLabel {
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        [_nameLabel setTextColor:[Common colorWithHexString:@"#000000" alpha:1.0]];
        [_nameLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
        [_nameLabel setFrame:[Common get414FrameByMap:CGRectMake(0, 120, 85, 12)]];
    }
    return _nameLabel;
}
- (UILabel *)tempLabel {
    if (_tempLabel == nil) {
        _tempLabel = [[UILabel alloc] init];
        [_tempLabel setTextColor:[Common colorWithHexString:@"#A1A1B3" alpha:1.0]];
        [_tempLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
        [_tempLabel setFrame:[Common get414FrameByMap:CGRectMake(0, 120 + 17, 85, 12)]];
        [_tempLabel setText:@"热搜度82°"];
    }
    return _tempLabel;
}

@end

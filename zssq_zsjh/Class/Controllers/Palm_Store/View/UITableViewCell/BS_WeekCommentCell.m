//
//  BS_WeekCommentCell.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/8/1.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_WeekCommentCell.h"
#import <UIImageView+WebCache.h>
#import "Common.h"

@interface BS_WeekCommentCell()

@property (nonatomic, strong) UIImageView *coverImage;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *authorLabel;
@property (nonatomic, strong) UILabel *detailLabel;

@end

@implementation BS_WeekCommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.coverImage];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.authorLabel];
        [self.contentView addSubview:self.detailLabel];
    }
    return self;
}

- (void)bindData:(ZY_NovelInfo *)info {
    self.coverImage.contentMode = UIViewContentModeScaleAspectFill;
    [self.coverImage sd_setImageWithURL:[NSURL URLWithString:info.novelCoverUrl]];
    
    [self.titleLabel setText:info.novelName];
    [self.titleLabel sizeToFit];
    [self.titleLabel setFrame:CGRectMake([Common get414RadiusByMap:113], [Common get414RadiusByMap:10], self.titleLabel.frame.size.width, self.titleLabel.frame.size.height)];
    
    [self.authorLabel setText:[NSString stringWithFormat:@"%@/%@",info.novelAuthor,info.novelCategory]];
    [self.authorLabel sizeToFit];
    [self.authorLabel setFrame:CGRectMake([Common get414RadiusByMap:113], [Common get414RadiusByMap:41],self.authorLabel.frame.size.width, self.authorLabel.frame.size.height)];
    
    
    
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:info.novelDes];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:8];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [info.novelDes length])];
    [self.detailLabel setAttributedText:attributedString1];
    
    [self.detailLabel setFrame:[Common get414FrameByMap:CGRectMake(113, 68, 259, 39 + 24)]];
    
}

#pragma mark ==== 懒加载 ====
- (UIImageView *)coverImage {
    if (_coverImage == nil) {
        _coverImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(10, 10, 89, 125)]];
        _coverImage.layer.shadowOpacity = 0.5;
        _coverImage.layer.shadowRadius = 2;
        _coverImage.layer.shadowColor = [UIColor blackColor].CGColor;
        _coverImage.layer.shadowOffset = CGSizeMake(0, 0);
        _coverImage.clipsToBounds = YES;
    }
    return _coverImage;
}
- (UILabel *)titleLabel {
    if (_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setTextColor:[Common colorWithHexString:@"#222222" alpha:1.0]];
        [_titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:16]]];
        
    }
    return _titleLabel;
}
- (UILabel *)authorLabel {
    if (_authorLabel == nil) {
        _authorLabel = [[UILabel alloc] init];
        [_authorLabel setTextColor:[Common colorWithHexString:@"#676767" alpha:1.0]];
        [_authorLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
    }
    return _authorLabel;
}
- (UILabel *)detailLabel {
    if (_detailLabel == nil) {
        _detailLabel = [[UILabel alloc] init];
        [_detailLabel setTextColor:[Common colorWithHexString:@"#676767" alpha:1.0]];
        [_detailLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_detailLabel setNumberOfLines:0];
        
    }
    return _detailLabel;
}

@end

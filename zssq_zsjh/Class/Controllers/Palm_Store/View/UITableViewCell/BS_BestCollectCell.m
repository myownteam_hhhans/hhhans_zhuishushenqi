//
//  BS_BestCollectCell.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/6/13.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_BestCollectCell.h"
#import "BS_BestCollectView.h"
#import <UIImageView+WebCache.h>
#import "Common.h"

@implementation BS_BestCollectCell

- (void)bindDataWithBookModelArray:(NSArray *)array {
    
    if (self.bookArr == array) {
        return;
    }
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.bookArr = array;
    
    for (int i = 0; i < array.count; i++) {
        ZY_NovelInfo *info = array[i];
        
        
        
        CGFloat x = 20 + i%2*180;
        CGFloat y = i/2*120;
        
        BS_BestCollectView *view = [[BS_BestCollectView alloc] initWithFrame:[Common getFrameByMap:CGRectMake(x, y, 151, 100)]];
        [view.novelCoverImageView sd_setImageWithURL:[NSURL URLWithString:info.novelCoverUrl]];
        
        [view.bookNameLabel setText:info.novelName];
        CGRect frameNew = [view.bookNameLabel.text boundingRectWithSize:CGSizeMake([Common getRadiusByMap:99], MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:view.bookNameLabel.font} context:nil];  // 指定为width，通常都是控件的width都是固定调整height
        CGRect rect = view.bookNameLabel.frame;
        rect.size = frameNew.size;
        view.bookNameLabel.frame = rect;
        
        [view.authorNameLabel setText:info.novelCategory];
        CGRect frame = view.authorNameLabel.frame;
        frame.origin.y = view.bookNameLabel.frame.size.height + 8;
        view.authorNameLabel.frame = frame;
        
        view.tag = i;
        [view addTarget:self action:@selector(bookClick:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *status = @"完结";
        if (info.Finished != 1) {
            status = @"连载";
        }
        
        [view bindStatusText:status];
        [self addSubview:view];
        
    }
}


- (void)bookClick:(UIButton *)sender {
    
     
    
    if ([self.delegate respondsToSelector:@selector(collectionButtonClick:)]) {
        ZY_NovelInfo *info = self.bookArr[sender.tag];
        [self.delegate collectionButtonClick:info];
    }
}

@end

//
//  BS_FreeTableViewCell.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_FreeTableViewCell.h"
#import "BS_BookButton.h"
#import "Common.h"

@interface BS_FreeTableViewCell ()

@property(nonatomic,strong) NSMutableArray *dataSource;

@property(nonatomic,strong) NSMutableArray *controlArray;

@property(nonatomic,strong) UIView *lineView;

@end

@implementation BS_FreeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        self.controlArray = [[NSMutableArray alloc] init];
        BS_BookButton *bookOne = [self createBookButtonWithFrame:CGRectMake(10, 13, 85, 119)];
        bookOne.tag=0;
        [self.controlArray addObject:bookOne];
        [self addSubview:bookOne];
        
        BS_BookButton *bookTwo = [self createBookButtonWithFrame:CGRectMake(111, 13, 85, 119)];
        bookTwo.tag=1;
        [self.controlArray addObject:bookTwo];
        [self addSubview:bookTwo];
        
        BS_BookButton *bookThree = [self createBookButtonWithFrame:CGRectMake(210, 13, 85, 119)];
        bookThree.tag=2;
        [self.controlArray addObject:bookThree];
        [self addSubview:bookThree];
        
        BS_BookButton *bookFour = [self createBookButtonWithFrame:CGRectMake(310, 13, 85, 119)];
        bookFour.tag=3;
        [self.controlArray addObject:bookFour];
        [self addSubview:bookFour];
        
        //[self addSubview:self.lineView];
        self.clipsToBounds = YES;
    }
    
    return self;
}

-(void)clickBook:(BS_BookButton *)sender{
    ZY_NovelInfo *bookInfo = [self.dataSource objectAtIndex:sender.tag];
    
    if (bookInfo&&[self.delegate respondsToSelector:@selector(clickBookToDetailPageController:)]) {
        [self.delegate clickBookToDetailPageController:bookInfo];
    }
}

-(UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectZero];
        //_lineView.dk_backgroundColorPicker = DKColorPickerWithKey(BS_TitleTableViewCell_LineColor);
        _lineView.alpha=0.3;
    }
    
    return _lineView;
}

-(BS_BookButton *)createBookButtonWithFrame:(CGRect)frame{
    BS_BookButton *bookInfoBtn = [[BS_BookButton alloc] initWithFrame:[Common get414FrameByMap:frame]];
    [bookInfoBtn addTarget:self action:@selector(clickBook:) forControlEvents:UIControlEventTouchUpInside];
    return bookInfoBtn;
}

-(void)bindData:(NSMutableArray *)dataArray{
    self.dataSource = dataArray;
    
    if (dataArray.count < self.controlArray.count) {
        return;
    }
    
    for (NSInteger i=0; i<self.controlArray.count; i++) {
        ZY_NovelInfo *bookInfo = [dataArray objectAtIndex:i];
        BS_BookButton *bookButton = [self.controlArray objectAtIndex:i];
        [bookButton bindData:bookInfo];
    }
    
    [self.lineView setFrame:CGRectMake(0, CGRectGetHeight(self.frame)-0.5, k_ScreenWidth, 0.5)];
}

@end

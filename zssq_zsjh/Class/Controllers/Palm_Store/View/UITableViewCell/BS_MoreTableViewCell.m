//
//  BS_MoreTableViewCell.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_MoreTableViewCell.h"
#import "Common.h"
@interface BS_MoreTableViewCell ()


@property (nonatomic, strong) UIImageView *nextImage;
@property(nonatomic,strong) UIView *lineView;

@end

@implementation BS_MoreTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
       [self.contentView addSubview:self.labelTitle];
       [self.contentView addSubview:self.lineView];
        [self.contentView addSubview:self.nextImage];
    }
    
    return self;
}

-(UILabel *)labelTitle{
    if (!_labelTitle) {
         _labelTitle = [[UILabel alloc] initWithFrame:[Common getFrameByMap:CGRectMake(144,20,120, 14)]];
         //_labelTitle.dk_textColorPicker = DKColorPickerWithKey(BS_MoreTableViewCellTitleColor);
        [_labelTitle setFont:[UIFont systemFontOfSize:[Common getFontOfSizeByMap:14]]];
        [_labelTitle setTextAlignment:NSTextAlignmentLeft];
        [_labelTitle setText:@"查看榜单"];
    }
    return _labelTitle;
}

-(UIView *)lineView{
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectZero];
       // _lineView.dk_backgroundColorPicker = DKColorPickerWithKey(BS_TitleTableViewCell_LineColor);
        _lineView.alpha=0.3;
    }
    
    return _lineView;
}

- (UIImageView *)nextImage {
    if (_nextImage == nil) {
        _nextImage = [[UIImageView alloc] init];
        [_nextImage setImage:[UIImage imageNamed:@"BS_More"]];
        _nextImage.frame = [Common getFrameByMap:CGRectMake(206, 11, 13, 13)];
        CGPoint center = _nextImage.center;
        center.y = self.labelTitle.center.y;
        _nextImage.center = center;
    }
    return _nextImage;
}

-(void)bind{ 
    [self.lineView setFrame:CGRectMake(0, CGRectGetHeight(self.frame)-0.5, k_ScreenWidth, 0.5)];
}


@end

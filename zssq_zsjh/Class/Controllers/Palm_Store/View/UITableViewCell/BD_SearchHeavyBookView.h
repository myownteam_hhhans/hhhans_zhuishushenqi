//
//  BD_SearchHeavyBookView.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/9/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BD_SearchHeavyBookView : UIView

@property (nonatomic, assign) BOOL isTempCell;

- (void)bindDataWithCover:(NSString *)cover name:(NSString *)name temp:(NSString *)temp;

@end

//
//  BD_SearchHeavyCell.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/9/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BD_SearchHeavyCell.h"
#import "BD_SearchHeavyBookView.h"
#import "Common.h"
#import "ZY_NovelInfo.h"

@interface BD_SearchHeavyCell()

@property (nonatomic, strong) NSMutableArray *bookViewArray;
@property (nonatomic, strong) NSArray *bookModelArray;

@end

@implementation BD_SearchHeavyCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        // 添加八个View
        for (NSInteger i = 0; i < 8; i++) {
            BD_SearchHeavyBookView *heavyView = [[BD_SearchHeavyBookView alloc] init];
            CGFloat x =16 + i%4*(84+16);
            CGFloat y = i/4*172 + 20;
            [heavyView setFrame:[Common get414FrameByMap:CGRectMake(x, y, 84, 172)]];
            [self.bookViewArray addObject:heavyView];
            UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bookButtonClick:)];
            [heavyView addGestureRecognizer:ges];
            heavyView.tag = i;
            heavyView.isTempCell = YES;
            [self addSubview:heavyView];
        }
        
    }
    return self;
}

- (void)bindDataWithArray:(NSArray *)arr {
    
    self.bookModelArray = arr;
    NSInteger bookCount = arr.count;
    
    for (NSInteger i = 0; i < bookCount; i++) {
        BD_SearchHeavyBookView *view = self.bookViewArray[i];
        ZY_NovelInfo *info = (ZY_NovelInfo *)arr[i];
        NSInteger temp = 82 + (long)(arc4random() % 10);
        [view bindDataWithCover:info.novelCoverUrl name:info.novelName temp:[NSString stringWithFormat:@"热搜度%ld°",temp]];
    }
    
}

- (void)bookButtonClick:(UIGestureRecognizer *)ges {
    if ([self.delegate respondsToSelector:@selector(clickBookToDetailPageController:)]) {
        [self.delegate clickBookToDetailPageController:self.bookModelArray[ges.view.tag]];
    }
}

#pragma mark === 懒加载 ====
- (NSMutableArray *)bookViewArray {
    if (_bookViewArray == nil) {
        _bookViewArray = [NSMutableArray array];
    }
    return _bookViewArray;
}

@end

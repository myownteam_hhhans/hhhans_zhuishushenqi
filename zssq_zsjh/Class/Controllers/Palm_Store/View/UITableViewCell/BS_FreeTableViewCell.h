//
//  BS_FreeTableViewCell.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BS_FreeTableViewCellDelegate <NSObject>

@optional
-(void)clickBookToDetailPageController:(ZY_NovelInfo *)bookInfo;

@end
 
@interface BS_FreeTableViewCell : UITableViewCell

@property (nonatomic, weak) id<BS_FreeTableViewCellDelegate> delegate;

-(void)bindData:(NSMutableArray *)dataArray;

@end

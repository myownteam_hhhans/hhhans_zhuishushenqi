//
//  BS_TitleTableViewCell.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BS_TitleTableViewCellDelegate <NSObject>

@optional
-(void)jumpToCategoryListWithID:(NSString *)ID name:(NSString *)name;
-(void)refreshBookWithIndex:(NSInteger)index;
@end


@interface BS_TitleTableViewCell : UITableViewCell

@property (nonatomic, weak) id<BS_TitleTableViewCellDelegate> delegate;

@property (nonatomic, assign) NSInteger categoryID;

@property (nonatomic, assign) NSInteger cellIndex;

@property (nonatomic, strong) NSString *nodeID;

-(void)bindTitle:(NSString *)title sex:(NSInteger)sex showFresh:(BOOL)show moreBook:(BOOL)more;

@end

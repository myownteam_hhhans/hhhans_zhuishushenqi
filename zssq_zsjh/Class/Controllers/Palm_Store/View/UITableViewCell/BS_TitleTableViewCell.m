//
//  BS_TitleTableViewCell.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_TitleTableViewCell.h"
#import "V_ImageLabel.h"
#import "UIButton+EnlargeTouchArea.h"
#import "Common.h"

@interface BS_TitleTableViewCell ()

@property(nonatomic,assign) CGFloat angle;//角度

@property(nonatomic,strong) V_ImageLabel *titleImgLabel;

@property(nonatomic,strong) UIView *lineView;
@property (nonatomic, strong) UIImageView *Vline;

@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UIImageView *moreImage;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) UIButton *refreshButton;
@property (nonatomic, strong) UIImageView *refreshImage;

@end

@implementation BS_TitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleImgLabel];
        [self.contentView addSubview:self.moreButton];
        [self.contentView addSubview:self.moreImage];
        [self.contentView addSubview:self.Vline];
        [self.contentView addSubview:self.refreshButton];
        [self.contentView addSubview:self.refreshImage];
    }
    
    return self;
}

-(void)prepareForReuse{
    [super prepareForReuse];
}

#pragma mark - PrivateFunction



#pragma mark - Event
- (void)moreButtonClick {
    if ([self.delegate respondsToSelector:@selector(jumpToCategoryListWithID:name:)]) {
        [self.delegate jumpToCategoryListWithID:self.nodeID name:self.title];
    }
}
- (void)refreshButtonClick {
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 4.0 * 0.5];
    rotationAnimation.duration = 0.5;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1.0;
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    [self.refreshImage.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.51 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([self.delegate respondsToSelector:@selector(refreshBookWithIndex:)]) {
            [self.delegate refreshBookWithIndex:self.cellIndex];
        }
    });
    
}


#pragma mark - Getter or Setter

-(V_ImageLabel *)titleImgLabel{
    if (!_titleImgLabel) {
         _titleImgLabel =[[V_ImageLabel alloc] initWithFrame:[Common getFrameByMap:CGRectMake(0, 17, 375, 25)] withTitle:@""];
    }
    
    return _titleImgLabel;
}


-(UIView *)lineView{
    if (!_lineView) {
         _lineView = [[UIView alloc] initWithFrame:CGRectZero];
         //_lineView.dk_backgroundColorPicker = DKColorPickerWithKey(BS_TitleTableViewCell_LineColor);
         _lineView.alpha=0.3;
    }
    
    return _lineView;
}

-(void)bindTitle:(NSString *)title sex:(NSInteger)sex showFresh:(BOOL)show moreBook:(BOOL)more{
    [self.titleImgLabel setTitle:title];
    self.title = title;
    [self.lineView setFrame:CGRectMake(0, CGRectGetHeight(self.frame)-0.5, k_ScreenWidth, 0.5)];
    self.moreButton.hidden = !more;
    self.moreImage.hidden = !more;
    if (sex == 1) {
        [_Vline setImage:[UIImage imageNamed:@"BS_Vline_Female"]];
    }
    else {
        [_Vline setImage:[UIImage imageNamed:@"BS_Vline"]];
    }
    
    self.refreshButton.hidden =  !show;
    self.refreshImage.hidden = !show;
    
}



- (UIImageView *)Vline {
    if (_Vline == nil) {
        _Vline = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(8, 24, 8, 19)]];
        [_Vline setImage:[UIImage imageNamed:@"BS_Vline"]];
    }
    return _Vline;
}
- (UIButton *)moreButton {
    if (_moreButton == nil) {
        _moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_moreButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:13]]];
        [_moreButton setTitleColor:[Common colorWithHexString:@"#404047" alpha:1.0] forState:UIControlStateNormal];
        [_moreButton setTitle:@"查看更多" forState:UIControlStateNormal];
        [_moreButton sizeToFit];
        [_moreButton setCenter:CGPointMake(k_ScreenWidth-[Common get414RadiusByMap:55], self.Vline.center.y)];
        [_moreButton setHidden:YES];
        [_moreButton addTarget:self action:@selector(moreButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}
- (UIImageView *)moreImage {
    if (_moreImage == nil) {
        _moreImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BD_Morebook"]];
        [_moreImage setFrame:[Common get414FrameByMap:CGRectMake(391, 0, 6, 11)]];
        CGPoint center = _moreImage.center;
        center.y = self.moreButton.center.y;
        _moreImage.center = center;
        [_moreButton setHidden:YES];
    }
    return _moreImage;
}
- (void)setCategoryID:(NSInteger)categoryID {
    _categoryID = categoryID;
    self.moreButton.hidden = NO;
    self.moreImage.hidden = NO;
}
- (UIButton *)refreshButton {
    if (_refreshButton == nil) {
        _refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_refreshButton setTitle:@"换分类" forState:UIControlStateNormal];
        //[_refreshButton setImage:[UIImage imageNamed:@"BS_RefreshButton"] forState:UIControlStateNormal];
        [_refreshButton setTitleColor:[Common colorWithHexString:@"#404047" alpha:1.0] forState:UIControlStateNormal];
        [_refreshButton sizeToFit];
        [_refreshButton setFrame:[Common get414FrameByMap:CGRectMake(341, 1, 414-41-32, 20)]];
        [_refreshButton setTitleEdgeInsets:UIEdgeInsetsMake(0, [Common get414RadiusByMap:-5], 0, 0)];
        [_refreshButton.titleLabel setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_refreshButton setCenter:CGPointMake(k_ScreenWidth-[Common get414RadiusByMap:56], self.Vline.center.y)];
        [_refreshButton setHidden:YES];
        [_refreshButton addTarget:self action:@selector(refreshButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _refreshButton;
}
- (UIImageView *)refreshImage {
    if (_refreshImage == nil) {
        _refreshImage = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 13, 14)]];
        [_refreshImage setCenter:CGPointMake(k_ScreenWidth-[Common get414RadiusByMap:23], self.Vline.center.y)];
        [_refreshImage setImage:[UIImage imageNamed:@"BS_RefreshButton"]];
        [_refreshImage setHidden:YES];
    }
    return _refreshImage;
}


@end

//
//  BS_MoreTableViewCell.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BS_MoreTableViewCell : UITableViewCell

@property(nonatomic,strong) UILabel *labelTitle;

-(void)bind;
- (void)bindSex:(NSInteger)sex;

@end

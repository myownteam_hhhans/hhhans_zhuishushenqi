//
//  BS_BestCollectCell.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/6/13.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@protocol BS_BestCollectionBookDelegate <NSObject>

- (void)collectionButtonClick:(ZY_NovelInfo *)bookInfo; // 点击图书的代理

@end



@interface BS_BestCollectCell : UITableViewCell

@property (nonatomic, weak) id<BS_BestCollectionBookDelegate> delegate;
@property (nonatomic, strong) NSArray *bookArr;

- (void)bindDataWithBookModelArray:(NSArray *)array;


@end

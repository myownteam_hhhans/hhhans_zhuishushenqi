//
//  BD_SearchHeavyCell.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/9/10.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"
#import "BS_FreeTableViewCell.h"

@interface BD_SearchHeavyCell : UITableViewCell

@property (nonatomic, weak) id<BS_FreeTableViewCellDelegate> delegate;
- (void)bindDataWithArray:(NSArray *)arr;

@end

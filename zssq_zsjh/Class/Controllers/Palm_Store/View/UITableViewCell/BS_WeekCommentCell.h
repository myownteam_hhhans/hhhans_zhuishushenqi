//
//  BS_WeekCommentCell.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/8/1.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@interface BS_WeekCommentCell : UITableViewCell

- (void)bindData:(ZY_NovelInfo *)info;

@end

//
//  BS_BestCollectView.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/6/13.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_BestCollectView.h"
#import <UIImageView+WebCache.h>
#import "Common.h"

@interface BS_BestCollectView()




@end

@implementation BS_BestCollectView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.novelCoverImageView];
        [self addSubview:self.authorNameLabel];
        //[self addSubview:self.statusLabel];
        [self addSubview:self.bookNameLabel];
    }
    return self;
}

- (void)bindStatusText:(NSString *)text {
    
    [self.statusLabel setText:text];
    
    if ([text isEqualToString:@"连载"]) {
        [self.statusLabel setTextColor:[Common colorWithHexString:@"#5eddb7" alpha:1.0]];
        self.statusLabel.layer.borderColor = [Common colorWithHexString:@"#c3f3e4" alpha:1.0].CGColor;
    } else {
        [self.statusLabel setTextColor:[Common colorWithHexString:@"#AFAFAF" alpha:1.0]];
        self.statusLabel.layer.borderColor = [Common colorWithHexString:@"#B6B6B6" alpha:1.0].CGColor;
    }
}

#pragma mark ==== 懒加载 ====
- (UIImageView *)novelCoverImageView {
    if (_novelCoverImageView == nil) {
        _novelCoverImageView = [[UIImageView alloc] initWithFrame:[Common getFrameByMap:CGRectMake(0, 0, 74, 103)]];
        [_novelCoverImageView setContentMode:UIViewContentModeScaleAspectFill];
        _novelCoverImageView.layer.shadowOpacity = 0.3;
        _novelCoverImageView.layer.shadowRadius = 2;
        _novelCoverImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        _novelCoverImageView.layer.shadowOffset = CGSizeMake(0, 0);
    }
    return _novelCoverImageView;
}
- (UILabel *)bookNameLabel {
    if (_bookNameLabel == nil) {
        _bookNameLabel = [[UILabel alloc] init];
        [_bookNameLabel setFrame:[Common getFrameByMap:CGRectMake(85, 0, 84, 40)]];
        [_bookNameLabel setNumberOfLines:2];
        [_bookNameLabel setFont:[UIFont systemFontOfSize:[Common getFontOfSizeByMap:14]]];
    }
    return _bookNameLabel;
}

- (UILabel *)authorNameLabel {
    if (_authorNameLabel == nil) {
        _authorNameLabel = [[UILabel alloc] init];
        [_authorNameLabel setFrame:[Common getFrameByMap:CGRectMake(85, 44, 60, 17)]];
        [_authorNameLabel setTextColor:[Common colorWithHexString:@"#B4B4B4" alpha:1.0]];
        [_authorNameLabel setFont:[UIFont systemFontOfSize:[Common getFontOfSizeByMap:12]]];
    }
    return _authorNameLabel;
}

- (UILabel *)statusLabel {
    if (_statusLabel == nil) {
        _statusLabel = [[UILabel alloc] initWithFrame:[Common getFrameByMap:CGRectMake(90, 84, 32, 16)]];
        [_statusLabel setFont:[UIFont systemFontOfSize:[Common getFontOfSizeByMap:10]]];
        [_statusLabel setTextColor:[UIColor whiteColor]];
        _statusLabel.layer.cornerRadius = 2.0;
        _statusLabel.layer.masksToBounds = YES;
        _statusLabel.layer.borderWidth = 1;
        [_statusLabel setTextAlignment:NSTextAlignmentCenter];
        //[_statusLabel setBackgroundColor:[Common colorWithHexString:@"#8CD7C6" alpha:1.0]];
    }
    return _statusLabel;
}

@end

//
//  BS_BookButton.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_BookButton.h"
#import "UIImageView+WebCache.h"
#import "Common.h"

@interface BS_BookButton ()

//背景图
@property(nonatomic,strong) UIImageView *bgImageView;
//角标图片
@property(nonatomic,strong) UIImageView *cornerImg;

@property(nonatomic,strong) UIImageView *coverMapImg;

@property(nonatomic,strong) UILabel *labelBookName;

@property(nonatomic,strong) UILabel *labelPrice;

@property (nonatomic, strong) UILabel *authorNameLabel;

@end

@implementation BS_BookButton

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
       [self addSubview:self.bgImageView];
        [self addSubview:self.authorNameLabel];
       [self.bgImageView addSubview:self.coverMapImg];
       //[self.coverMapImg addSubview:self.cornerImg];
       [self.bgImageView addSubview:self.labelBookName];


    }
    
    return self;
}

#pragma mark - Getter or Setter



-(UIImageView *)bgImageView{
    if (!_bgImageView) {
         _bgImageView = [[UIImageView alloc] initWithFrame:[Common getFrameByMap:CGRectMake(0,0,104, 173)]];
         //_bgImageView.dk_imagePicker = DKImagePickerWithNames(@"BS_BookButton_bg",@"BS_BookButton_bg");
    }
    
    return _bgImageView;
}

-(UIImageView *)coverMapImg{
    if (!_coverMapImg) {
         _coverMapImg = [[UIImageView alloc] initWithFrame:[Common get414FrameByMap:CGRectMake(0,0, 85, 119)]];
        //[_coverMapImg setBackgroundColor:[UIColor redColor]];
        [_coverMapImg setContentMode:UIViewContentModeScaleAspectFill];
        _coverMapImg.layer.shadowOpacity = 0.3;
        _coverMapImg.layer.shadowRadius = 2;
        _coverMapImg.layer.shadowColor = [UIColor blackColor].CGColor;
        _coverMapImg.layer.shadowOffset = CGSizeMake(0, 0);
    }
    
    return _coverMapImg;
}

-(UIImageView *)cornerImg{
    if (!_cornerImg) {
         _cornerImg = [[UIImageView alloc] initWithFrame:[Common getFrameByMap:CGRectMake(0,0, 41, 37)]];
         //_cornerImg.dk_imagePicker = DKImagePickerWithNames(@"BS_Corner",@"BS_Corner");
    }
    
    return _cornerImg;
}

-(UILabel *)labelBookName{
    if (!_labelBookName) {
         _labelBookName = [[UILabel alloc] initWithFrame:[Common getFrameByMap:CGRectMake(0,120, 85, 13)]];
         //_labelBookName.dk_textColorPicker = DKColorPickerWithKey(BS_BookButton_labelBookNameTextColor);
        [_labelBookName setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:15]]];
        [_labelBookName setTextAlignment:NSTextAlignmentLeft];
        [_labelBookName setNumberOfLines:2];
    }
    
    return _labelBookName;
}

-(UILabel *)labelPrice{
    if (!_labelPrice) {
         _labelPrice = [[UILabel alloc] initWithFrame:[Common getFrameByMap:CGRectMake(3,154, 97, 12)]];
         //_labelPrice.dk_textColorPicker = DKColorPickerWithKey(BS_BookButton_labelPriceTextColor);
        [_labelPrice setFont:[UIFont systemFontOfSize:[Common getFontOfSizeByMap:10]]];
        [_labelPrice setTextAlignment:NSTextAlignmentCenter];
    }
    
    return _labelPrice;
}

- (UILabel *)authorNameLabel {
    if (_authorNameLabel == nil) {
        _authorNameLabel = [[UILabel alloc] init];
        [_authorNameLabel setFont:[UIFont systemFontOfSize:12]];
        [_authorNameLabel setTextColor:[Common colorWithHexString:@"#B4B4B4" alpha:1.0]];
    }
    return _authorNameLabel;
}

-(void)bindData:(ZY_NovelInfo *)bookInfo{
    [self.coverMapImg sd_setImageWithURL:[NSURL URLWithString:bookInfo.novelCoverUrl] placeholderImage:[UIImage imageNamed:@"moren-shujia"]];
    [self.labelBookName setText:bookInfo.novelName];
    [self.labelBookName sizeToFit];
//    NSString *nsstrPrice = [NSString stringWithFormat:@"%ld书币",(long)bookInfo.BookPrice];
//    NSUInteger length = [nsstrPrice length];
//    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:nsstrPrice];
//    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
//    [attri addAttribute:NSStrikethroughColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, length)];
//    [self.labelPrice setAttributedText:attri];
    [self.authorNameLabel setText:bookInfo.novelAuthor];
    [self.authorNameLabel sizeToFit];
    [self.authorNameLabel setFrame:CGRectMake(0, self.labelBookName.frame.origin.y + self.labelBookName.frame.size.height + 4, 98, 17)];
}

@end

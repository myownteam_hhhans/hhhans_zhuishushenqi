//
//  B_HeaderView.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_HeaderView.h"
#import "Common.h"
#import "SDCycleScrollView.h"
#import "ZY_NovelInfo.h"

@interface BS_HeaderView () <SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *headScrollView;
@property (nonatomic, strong) NSMutableArray *coverBookArray;


@property (nonatomic, strong) UIView *lineView;

@end

@implementation BS_HeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {
    
        [self addSubview:self.headScrollView];
        //[self addSubview:self.lineView];

    }
    
    return self;
}


#pragma mark - Event
// 绑定数据，图片url和书籍bookID
- (void)bindData:(NSArray *)bookArr {
    
    if (!bookArr || bookArr.count == 0) {
        return;
    }
    
    NSMutableArray *coverURLArr = [NSMutableArray array];
    
    for (NSDictionary *infoDic in bookArr) {
        
        NSString *bookID = [infoDic objectForKey:@"link"];
        NSString *urlStr = [infoDic objectForKey:@"img"];
        NSString *type = [infoDic objectForKey:@"type"];
        
        if ([type isEqualToString:@"c-bookdetail"]) {
            [coverURLArr addObject:urlStr];
            [self.coverBookArray addObject:@([bookID integerValue])];
        }
        
        
    }
    
    self.headScrollView.imageURLStringsGroup = coverURLArr;
    
}


- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    

    
    if ([self.delegate respondsToSelector:@selector(clickCycleView:)]) {
        [self.delegate clickCycleView:index];
    }
}

#pragma mark - Getter or Setter

- (SDCycleScrollView *)headScrollView {
    if (_headScrollView == nil) {
        _headScrollView = [SDCycleScrollView cycleScrollViewWithFrame:[Common get414FrameByMap:CGRectMake(0, 0, 414, 148)] delegate:self placeholderImage:nil];
        _headScrollView.infiniteLoop = YES;
        _headScrollView.clipsToBounds = NO;
        _headScrollView.autoScrollTimeInterval = 8.0;
    }
    return _headScrollView;
}


- (NSMutableArray *)coverBookArray {
    if (_coverBookArray == nil) {
        _coverBookArray = [NSMutableArray array];
    }
    return _coverBookArray;
}

- (UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 1, k_ScreenWidth, 0.3)];
        [_lineView setBackgroundColor:[UIColor colorWithRed:217/255.0 green:217/255.0 blue:217/255.0 alpha:1]];
    }
    return _lineView;
}

@end

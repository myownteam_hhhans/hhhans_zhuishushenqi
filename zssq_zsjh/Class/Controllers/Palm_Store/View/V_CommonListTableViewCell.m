//
//  V_CommonListTableViewCell.m
//  Palm_Novel
//
//  Created by shiwei on 2018/3/20.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "V_CommonListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Common.h"
#import "BookScoreView.h"

@interface V_CommonListTableViewCell ()

@property(nonatomic,strong) UIImageView *bookCoverShadowMap;

//封面图
@property(nonatomic,strong) UIImageView *bookCoverMapImg;

//标题
@property(nonatomic,strong) UILabel *labelBookName;

//简介
@property(nonatomic,strong) UILabel *labelBookDescribe;

//角标图片
@property(nonatomic,strong) UIImageView *cornerImg;

//作者
@property(nonatomic,strong) UILabel *labelBookAuthor;

//字数图片
@property(nonatomic,strong) UIImageView *imgWordCount;

//字数
@property(nonatomic,strong) UILabel *labelWordCount;

//分类名字
@property(nonatomic,strong) UILabel *labelCategoryName;

//分数
@property(nonatomic,strong) BookScoreView *scoreView;



//火热图标
@property(nonatomic,strong) UIImageView *hotImgView;

@end


@implementation V_CommonListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
         //self.contentView.dk_backgroundColorPicker = DKColorPickerWithKey(BG);
         self.selectedBackgroundView=[[UIView alloc]initWithFrame:self.frame];
         //self.selectedBackgroundView.dk_backgroundColorPicker = DKColorPickerWithKey(CommonListCellselectedBackgroundViewColor);
       // [self.contentView addSubview:self.bookCoverShadowMap];
        [self.contentView addSubview:self.bookCoverMapImg];
        [self.contentView addSubview:self.labelBookName];
        [self.contentView addSubview:self.hotImgView];
        [self.contentView addSubview:self.cornerImg];
        [self.contentView addSubview:self.labelBookDescribe];
        [self.contentView addSubview:self.labelBookAuthor];
        //[self.contentView addSubview:self.imgWordCount];
       // [self.contentView addSubview:self.labelWordCount];
        [self.contentView addSubview:self.labelBookStatus];
        [self.contentView addSubview:self.labelCategoryName];
        //[self.contentView addSubview:self.lineView];
    }
    
    return  self;
}

-(void)prepareForReuse{
    [super prepareForReuse];
    [self.cornerImg setHidden:YES];
    [self.hotImgView setHidden:YES];
}

#pragma mark - getter or setter

-(UIImageView *)bookCoverMapImg{
    if (!_bookCoverMapImg) {
         CGRect frame=[Common get414FrameByMap:CGRectMake(12,10,75,113)];
         _bookCoverMapImg = [[UIImageView alloc] initWithFrame:frame];
         //_bookCoverMapImg.dk_imagePicker = DKImagePickerWithNames(@"moren-shujia",@"moren-shujia");
        _bookCoverMapImg.contentMode = UIViewContentModeScaleAspectFit;
        //_bookCoverMapImg.contentMode = UIViewContentModeScaleAspectFill;
        _bookCoverMapImg.layer.shadowColor = [UIColor blackColor].CGColor;
        _bookCoverMapImg.layer.shadowOffset = CGSizeMake(0,0);
        _bookCoverMapImg.layer.shadowRadius = 2;
        _bookCoverMapImg.layer.shadowOpacity = 0.3;
    }
    
    return _bookCoverMapImg;
}



-(UILabel *)labelBookName{
    if (!_labelBookName) {
          CGRect frame=[Common get414FrameByMap:CGRectMake(100,12,221.5, 17.5)];
         _labelBookName = [[UILabel alloc] initWithFrame:frame];
        [_labelBookName setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:17]]];
        //_labelBookName.dk_textColorPicker = DKColorPickerWithKey(CommonListCellBookNameTitleColor);
        [_labelBookName setTextAlignment:NSTextAlignmentLeft];
        [_labelBookName setText:@"武侠时代的皇帝修炼日记"];
    }
    return _labelBookName;
}
 
-(UILabel *)labelBookDescribe{
    if (!_labelBookDescribe) {
         CGRect frame=[Common get414FrameByMap:CGRectMake(100,39,278, 42)];
         _labelBookDescribe = [[UILabel alloc] initWithFrame:frame];
        [_labelBookDescribe setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:14]]];
        [_labelBookDescribe setTextColor:[Common colorWithHexString:@"#666666" alpha:1.0]];
        [_labelBookDescribe setTextAlignment:NSTextAlignmentLeft];
        [_labelBookDescribe setNumberOfLines:2];
        [_labelBookDescribe setText:@"少年天子坐正明堂，左右狐狸两旁，阴诡的披金戴紫，跋扈的持节封王；上有那端庄的圣母养闺臣，下跪着衣冠的世家藏心肠，殿悬着血泪忠良，这斑斑青竹泪几行，沙场白发眺天狼，沦落在胡尘望帝乡，说一句楚虽三户三闾强。"];
    }
    
    return _labelBookDescribe;
}

-(UILabel *)labelBookAuthor{
    if (!_labelBookAuthor) {
        CGRect frame=[Common get414FrameByMap:CGRectMake(100, 105,100, 13)];
         _labelBookAuthor = [[UILabel alloc] initWithFrame:frame];
        [_labelBookAuthor setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:12]]];
        // _labelBookAuthor.dk_textColorPicker = DKColorPickerWithKey(CommonListCellBookAuthorColor);
        [_labelBookAuthor setTextAlignment:NSTextAlignmentLeft];
        [_labelBookAuthor setText:@"总裁夫人"];
    }
    
    return _labelBookAuthor;
}

-(UIImageView *)imgWordCount{
    if (!_imgWordCount) {
         CGRect frame=[Common getFrameByMap:CGRectMake(175.5, 118.5,9, 9)];
         _imgWordCount= [[UIImageView alloc] initWithFrame:frame];
         //_imgWordCount.dk_imagePicker = DKImagePickerWithNames(@"字数",@"字数");
    }
    
    return _imgWordCount;
}

-(UILabel *)labelWordCount{
    if (!_labelWordCount) {
        CGRect frame=[Common getFrameByMap:CGRectMake(187.5, 117.5,50, 12)];
         _labelWordCount = [[UILabel alloc] initWithFrame:frame];
        [_labelWordCount setFont:[UIFont systemFontOfSize:12]];
        // _labelWordCount.dk_textColorPicker = DKColorPickerWithKey(CommonListCellBookWordCountColor);
        [_labelWordCount setTextAlignment:NSTextAlignmentLeft];
        [_labelWordCount setText:@"19万字"];
    }
    
    return _labelWordCount;
}

-(UILabel *)labelCategoryName{
    if (!_labelCategoryName) {
         CGRect frame =[Common get414FrameByMap:CGRectMake(345, 106,52, 18)];
         _labelCategoryName = [[UILabel alloc] initWithFrame:frame];
        [_labelCategoryName setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:10]]];
         [_labelCategoryName setTextColor:[Common colorWithHexString:@"#A1A1B3" alpha:1.0]];
        [_labelCategoryName setTextAlignment:NSTextAlignmentCenter];
        [_labelCategoryName setText:@"言情"];
        [_labelCategoryName.layer setMasksToBounds:YES];
        [_labelCategoryName.layer setCornerRadius:2.0];
        //[_labelCategoryName.layer setBorderWidth:1.0];
        _labelCategoryName.layer.borderWidth = 1;
        _labelCategoryName.layer.borderColor = [Common colorWithHexString:@"#F0F0F5" alpha:1.0].CGColor;
    }
    return _labelCategoryName;
}

-(UILabel *)labelBookStatus{
    if (!_labelBookStatus) {
        CGRect frame=[Common get414FrameByMap:CGRectMake(306,106,33, 18)];
         _labelBookStatus = [[UILabel alloc] initWithFrame:frame];
        [_labelBookStatus setFont:[UIFont systemFontOfSize:[Common get414RadiusByMap:10]]];
        [_labelBookStatus setTextAlignment:NSTextAlignmentCenter];
        [_labelBookStatus setText:@"完结"];
        [_labelBookStatus.layer setMasksToBounds:YES];
        [_labelBookStatus.layer setCornerRadius:2.0];
        _labelBookStatus.layer.borderWidth = 0.7;
       // _labelBookStatus.dk_textColorPicker = DKColorPickerWithKey(CommonListCellBookCategoryColor);
    }
    
    return _labelBookStatus;
}

//#eeeeee  #eeeeee CommonListCellBookStatusColor
//#b6b7b9  #b6b7b9 CommonListCellBookStatusBorderColor
//
//#5eddb7  #5eddb7  CommonListCellBookStatusSecondColor
//#c3f3e4  #c3f3e4  CommonListCellBookStatusSecondBounderColor

-(void)settingBookStatusStyle:(BOOL)title{
    if (title) {
        
        [self.labelBookStatus setTextColor:[Common colorWithHexString:@"#23B383" alpha:1.0]];
        self.labelBookStatus.layer.borderColor = [Common colorWithHexString:@"#23B383" alpha:1.0].CGColor;
        
    }else{
        
        [self.labelBookStatus setTextColor:[Common colorWithHexString:@"#70A7E3" alpha:1.0]];
        self.labelBookStatus.layer.borderColor = [Common colorWithHexString:@"#70A7E3" alpha:1.0].CGColor;
    }
}

-(UIView *)lineView{
    if (!_lineView) {
         CGRect frame=[Common getFrameByMap:CGRectMake(0, 152, 375, 0.5)];
         _lineView=[[UIView alloc] initWithFrame:frame];
        [_lineView setAlpha:0.4];
        // _lineView.dk_backgroundColorPicker = DKColorPickerWithKey(CommonListCellLineColor);
    }
    
    return _lineView;
}


-(void)bindData:(ZY_NovelInfo *)bookInfo{
    [self.bookCoverMapImg sd_setImageWithURL:[NSURL URLWithString:bookInfo.novelCoverUrl] placeholderImage:[UIImage imageNamed:@"moren-shujia"]];
    [self.labelBookName setText:bookInfo.novelName];
    [Common setLabelSpace:self.labelBookDescribe withValue:bookInfo.novelDes withFont:[UIFont systemFontOfSize:13] withlineSpace:2];
    [self.labelBookAuthor setText:bookInfo.novelAuthor];
    [self.labelCategoryName setText:bookInfo.novelCategory];
    [self settingBookStatusStyle:bookInfo.Finished];
    
    if (bookInfo.Finished) {
        [self.labelBookStatus setText:@"完结"];
    } else {
        [self.labelBookStatus setText:@"连载"];
    }
    
    [self.labelWordCount setText:bookInfo.WordCountFormat];
    
    
}




@end

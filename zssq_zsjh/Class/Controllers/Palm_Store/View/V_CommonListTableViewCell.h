//
//  V_CommonListTableViewCell.h
//  Palm_Novel
//
//  Created by shiwei on 2018/3/20.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZY_NovelInfo.h"

@interface V_CommonListTableViewCell : UITableViewCell

-(void)bindData:(ZY_NovelInfo *)bookInfo withLineNumber:(NSInteger)number;

-(void)bindData:(ZY_NovelInfo *)bookInfo;

-(void)bindData:(ZY_NovelInfo *)bookInfo withShowHot:(BOOL)isShow;

//连载状态
@property(nonatomic,strong) UILabel *labelBookStatus;
//线
@property(nonatomic,strong) UIView *lineView;

@end

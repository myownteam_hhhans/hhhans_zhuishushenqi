//
//  BS_NavTitleView.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/31.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_NavTitleView.h"
#import "Common.h"

@interface BS_NavTitleView()

@property (nonatomic, strong) UIView *searchBackView;
@property (nonatomic, strong) UILabel *searchLabel;
@property (nonatomic, strong) UIImageView *searchImageView;

@end

@implementation BS_NavTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self addSubview:self.searchBackView];
        [self.searchBackView addSubview:self.searchLabel];
        [self.searchBackView addSubview:self.searchImageView];
    }
    return self;
}

- (void)searchBackClick {
    if ([self.delegate respondsToSelector:@selector(jumpToSearchController)]) {
        [self.delegate jumpToSearchController];
    }
}



- (CGSize)intrinsicContentSize {
    return UILayoutFittingExpandedSize;
}

#pragma mark ==== 懒加载 ====

- (UIView *)searchBackView {
    if (_searchBackView == nil) {
        _searchBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 7, k_ScreenWidth - 0 - 5 - 70, 30)];
        [_searchBackView setBackgroundColor:[Common colorWithHexString:@"" alpha:1.0]];
        _searchBackView.layer.cornerRadius = 15;
        [_searchBackView setBackgroundColor:[Common colorWithHexString:@"#EBEBEB" alpha:1.0]];
        _searchBackView.layer.masksToBounds = YES;
        [_searchBackView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchBackClick)]];
    }
    return _searchBackView;
}
- (UILabel *)searchLabel {
    if (_searchLabel == nil) {
        _searchLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 1, 1)];
        [_searchLabel setFont:[UIFont systemFontOfSize:14]];
        [_searchLabel setText:@"搜索书名、作者"];
        [_searchLabel setTextColor:[Common colorWithHexString:@"#CCCCCC" alpha:1.0]];
        [_searchLabel sizeToFit];
        [_searchLabel setCenter:CGPointMake(_searchLabel.center.x, 15)];
    }
    return _searchLabel;
}
- (UIImageView *)searchImageView {
    if (_searchImageView == nil) {
        _searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.searchBackView.frame.size.width - 29, 8, 14, 14)];
        [_searchImageView setImage:[UIImage imageNamed:@"BS_Search"]];
    }
    return _searchImageView;
}

@end

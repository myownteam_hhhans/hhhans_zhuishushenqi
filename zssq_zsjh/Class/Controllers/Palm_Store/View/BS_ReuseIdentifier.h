//
//  B_ReuseIdentifier.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BS_ReuseIdentifier : NSObject

extern NSString *const  BS_TitleCellIdentifier;

extern NSString *const  BS_ZhongBangListCellIdentifier;

extern NSString *const  BS_FreeTableViewCellIdentifier;

extern NSString *const  BS_BestCollectionCellIdentifier;

extern NSString *const  BS_CompleteAndPotentialCellIdentifier;

extern NSString *const  BS_BestSaleCellIdentifier;

extern NSString *const  BS_ListCellIdentifier;

extern NSString *const  BS_MoreCellIdentifier;

extern NSString *const  BS_WeekCommentCellIdentifier;

extern NSString *const  BS_PreferCellIdentifier;
extern NSString *const  BS_PresentCellIdentifier;
extern NSString *const  BS_SearchHeavyIdentifier;

@end

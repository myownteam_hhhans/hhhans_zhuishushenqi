//
//  BS_NavTitleView.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/7/31.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BS_NavTitleDelegate <NSObject>

- (void)jumpToSearchController;

@end

@interface BS_NavTitleView : UIView


@property (nonatomic, strong) id<BS_NavTitleDelegate> delegate;
- (void)changeSex:(BOOL)sex; // 0男生 1女生

@end

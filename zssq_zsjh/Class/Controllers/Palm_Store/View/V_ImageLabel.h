//
//  FN_ImageLabel.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/2.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V_ImageLabel : UILabel

-(instancetype)initWithFrame:(CGRect)frame withTitle:(NSString *)title;

-(void)setTitle:(NSString *)title;

@end

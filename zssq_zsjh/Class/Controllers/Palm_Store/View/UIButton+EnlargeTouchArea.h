//
//  UIButton+EnlargeTouchArea.h
//  Palm_Novel
//
//  Created by shiwei on 2018/4/9.
//  Copyright © 2018年 shiwei. All rights reserved.
//
 
#import <UIKit/UIKit.h>

@interface UIButton (EnlargeTouchArea)

- (void)setEnlargeEdgeWithTop:(CGFloat) top right:(CGFloat) right bottom:(CGFloat) bottom left:(CGFloat) left;

- (void)setEnlargeEdge:(CGFloat) size;

@end

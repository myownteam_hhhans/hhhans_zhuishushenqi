//
//  B_ReuseIdentifier.m
//  Palm_Novel
//
//  Created by shiwei on 2018/4/8.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_ReuseIdentifier.h"

@implementation BS_ReuseIdentifier

NSString *const  BS_TitleCellIdentifier=@"BS_TitleCellIdentifier";

NSString *const  BS_ZhongBangListCellIdentifier=@"BS_ZhongBangListCellIdentifier";

NSString *const  BS_FreeTableViewCellIdentifier= @"BS_FreeTableViewCell";

NSString *const  BS_BestCollectionCellIdentifier = @"BS_BestCollectionCell";

NSString *const  BS_CompleteAndPotentialCellIdentifier = @"BS_CompleteAndPotentialCell";

NSString *const  BS_BestSaleCellIdentifier = @"BS_BestSaleCell";

NSString *const  BS_ListCellIdentifier=@"BS_ListCellIdentifier";

NSString *const  BS_MoreCellIdentifier=@"BS_MoreTableViewCell";

NSString *const  BS_WeekCommentCellIdentifier=@"BS_WeekCommentCellIdentifier";

NSString *const  BS_PreferCellIdentifier=@"BS_PreferCellIdentifier";

NSString *const  BS_PresentCellIdentifier =@"BS_PresentCellIdentifier";

NSString *const  BS_SearchHeavyIdentifier =@"BS_SearchHeavyIdentifier";

@end

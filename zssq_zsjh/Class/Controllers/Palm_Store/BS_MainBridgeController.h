//
//  BS_MainBridgeController.h
//  Palm_Novel
//
//  Created by 包涵 on 2018/6/19.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BS_MainBridgeController : UIViewController

@property (nonatomic, strong) UIScrollView *mainScrollView;
// 切换左右的两个button
@property (nonatomic, strong) UIButton *maleButton;
@property (nonatomic, strong) UIButton *femaleButton;
@property (nonatomic, strong) UIButton *publicationButton;
// 滑动view
@property (nonatomic, strong) UIView *sliderView;
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, assign) BOOL isHiddenNavigationBar;

@end

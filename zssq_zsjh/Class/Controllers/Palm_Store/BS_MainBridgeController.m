//
//  BS_MainBridgeController.m
//  Palm_Novel
//
//  Created by 包涵 on 2018/6/19.
//  Copyright © 2018年 shiwei. All rights reserved.
//
#import "BS_MainBridgeController.h"
#import "BS_NavTitleView.h"
#import "BS_BookStoreController.h"
#import "ZY_SearchViewController.h"
#import <MJRefresh.h>
#import <UserNotifications/UserNotifications.h>
#import "Common.h"
#import "ZY_Common.h"

@interface BS_MainBridgeController () <UIScrollViewDelegate, BS_NavTitleDelegate>

@property (nonatomic, strong) BS_NavTitleView *storeTitle;
@property (nonatomic, assign) BOOL isLogIn;

@end

@implementation BS_MainBridgeController

- (void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBar.translucent = NO;
    
    if (@available(iOS 11.0, *)) {
        self.mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //self.extendedLayoutIncludesOpaqueBars = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    if (self.isHiddenNavigationBar) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    } else {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }


}

- (void)viewDidAppear:(BOOL)animated {
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview: self.mainScrollView];
    
    self.navigationItem.titleView = self.storeTitle;
    [self.view addSubview:self.femaleButton];
    [self.view addSubview:self.maleButton];
    [self.view addSubview:self.publicationButton];
    [self.view addSubview:self.sliderView];
    [self.view addSubview:self.lineView];
    
    BS_BookStoreController *maleStore = [[BS_BookStoreController alloc] init];
    maleStore.mainTableView.frame = CGRectMake(0, 0, k_ScreenWidth, self.mainScrollView.frame.size.height);
    maleStore.gender = 2;

    BS_BookStoreController  *femaleStore = [[BS_BookStoreController alloc] init];
    femaleStore.mainTableView.frame = CGRectMake(k_ScreenWidth, 0, k_ScreenWidth, self.mainScrollView.frame.size.height);
    femaleStore.gender = 1;

    BS_BookStoreController  *publicationStore = [[BS_BookStoreController alloc] init];
    publicationStore.mainTableView.frame = CGRectMake(k_ScreenWidth*2, 0, k_ScreenWidth, self.mainScrollView.frame.size.height);
    publicationStore.gender = 0;
    
    [self.mainScrollView addSubview:maleStore.mainTableView];
    [self.mainScrollView addSubview:femaleStore.mainTableView];
    [self.mainScrollView addSubview:publicationStore.mainTableView];
    
    [self addChildViewController:maleStore];
    [self addChildViewController:femaleStore];
    [self addChildViewController:publicationStore];
    
    [self changeSex:self.maleButton];
    
    [maleStore.mainTableView.mj_header beginRefreshing];
    [femaleStore.mainTableView.mj_header beginRefreshing];
    [publicationStore.mainTableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ==== Private Func ====

- (void)loadData {
    BS_BookStoreController *male = self.childViewControllers[0];
    [male loadData];
    BS_BookStoreController *female = self.childViewControllers[1];
    [female loadData];
}

- (void)jumpToSearchController{
    ZY_SearchViewController *searchVC = [[ZY_SearchViewController alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
    self.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:searchVC animated:YES];
    self.hidesBottomBarWhenPushed=NO;
    [MobClick event:@"Book_city" label:@"搜索"];
}

- (void)changeSex:(UIButton *)sender {

    
    sender.selected = YES;
    CGPoint center= self.sliderView.center;
    center.x = sender.center.x;
    self.sliderView.center = center;

    [UIView animateWithDuration:0.3 animations:^{
        if (sender.tag == 0) {
            self.femaleButton.selected = NO;
            self.publicationButton.selected = NO;
            self.mainScrollView.contentOffset  =  CGPointMake(0, 0);
        } else if(sender.tag == 1) {
            self.mainScrollView.contentOffset  =  CGPointMake(k_ScreenWidth, 0);
            self.maleButton.selected = NO;
            self.publicationButton.selected = NO;
        } else if(sender.tag == 2) {
            self.mainScrollView.contentOffset  =  CGPointMake(k_ScreenWidth*2, 0);
            self.maleButton.selected = NO;
            self.femaleButton.selected = NO;
        }
    }];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x < 1) {
        [self changeSex:self.maleButton];
    } else if (scrollView.contentOffset.x > k_ScreenWidth-1 && scrollView.contentOffset.x < k_ScreenWidth+1){
        [self changeSex:self.femaleButton];
    } else if (scrollView.contentOffset.x > k_ScreenWidth*2-1){
        [self changeSex:self.publicationButton];
    }
}

#pragma mark ==== 懒加载 ====

- (UIScrollView *)mainScrollView {
    if (_mainScrollView == nil) {
        _mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 55, k_ScreenWidth, k_ScreenHeight - k_TabBarHeight - k_StatusBarHeight - 55)];
        _mainScrollView.contentSize = CGSizeMake(k_ScreenWidth * 3, 1);
        [_mainScrollView setDelegate:self];
        //_mainScrollView.bounces =  NO;
        _mainScrollView.pagingEnabled = YES;
        [_mainScrollView setShowsHorizontalScrollIndicator:NO];
        //[_mainScrollView setScrollEnabled:NO];
    }
    return _mainScrollView;
}

- (BS_NavTitleView *)storeTitle {
    if (_storeTitle == nil) {
        _storeTitle = [[BS_NavTitleView alloc] initWithFrame:CGRectMake(0, 20-k_StatusBarHeight, k_ScreenWidth, 44)];
        _storeTitle.delegate = self;
    }
    return _storeTitle;
}

- (UIButton *)maleButton {
    if (_maleButton == nil) {
        _maleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_maleButton setTitle:@"男生" forState:UIControlStateNormal];
        [_maleButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_maleButton setTitleColor:[Common colorWithHexString:@"#333333" alpha:1.0] forState:UIControlStateNormal];
        [_maleButton setTitleColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0] forState:UIControlStateSelected];
        [_maleButton sizeToFit];
        [_maleButton setTag:0];
        [_maleButton setCenter:CGPointMake(34, 79-64+7)];
        [_maleButton addTarget:self action:@selector(changeSex:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _maleButton;
}

- (UIButton *)femaleButton {
    if (_femaleButton == nil) {
        _femaleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_femaleButton setTitle:@"女生" forState:UIControlStateNormal];
        [_femaleButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_femaleButton setTitleColor:[Common colorWithHexString:@"#333333" alpha:1.0] forState:UIControlStateNormal];
        [_femaleButton setTitleColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0] forState:UIControlStateSelected];
        [_femaleButton sizeToFit];
        [_femaleButton setTag:1];
        [_femaleButton setCenter:CGPointMake(88 + 14, 79-64+7)];
        [_femaleButton addTarget:self action:@selector(changeSex:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _femaleButton;
}

- (UIButton *)publicationButton {
    if (_publicationButton == nil) {
        _publicationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_publicationButton setTitle:@"出版" forState:UIControlStateNormal];
        [_publicationButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [_publicationButton setTitleColor:[Common colorWithHexString:@"#333333" alpha:1.0] forState:UIControlStateNormal];
        [_publicationButton setTitleColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0] forState:UIControlStateSelected];
        [_publicationButton sizeToFit];
        [_publicationButton setTag:2];
        [_publicationButton setCenter:CGPointMake(157 + 14, 79-64+7)];
        [_publicationButton addTarget:self action:@selector(changeSex:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _publicationButton;
}

- (UIView *)sliderView {
    if (_sliderView == nil) {
        _sliderView = [[UIView alloc] initWithFrame:CGRectMake(25, 98-64, 18, 3)];
        _sliderView.layer.cornerRadius = 1.5;
        [_sliderView setBackgroundColor:[Common colorWithHexString:@"#48BEFE" alpha:1.0]];
    }
    return _sliderView;
}

- (UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 119-64-0.5, k_ScreenWidth, 0.5)];
        _lineView.backgroundColor = [Common colorWithHexString:@"#D8D8D8" alpha:1.0];
        _lineView.layer.shadowOpacity = 0.5;
        _lineView.layer.shadowRadius = 2;
        _lineView.layer.shadowColor = [UIColor blackColor].CGColor;
        _lineView.layer.shadowOffset = CGSizeMake(0, 2);
    }
    return _lineView;
}

@end

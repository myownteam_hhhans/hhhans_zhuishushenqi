//
//  B_BookStoreController.m
//  Palm_Novel
//
//  Created by shiwei on 2018/3/20.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import "BS_BookStoreController.h"
#import "V_CommonListTableViewCell.h"
#import "BS_HeaderView.h"
#import "MJRefresh.h"
#import "NWS_httpClient.h"
#import "BS_ReuseIdentifier.h"
#import "BS_FreeTableViewCell.h"
#import "BS_TitleTableViewCell.h"
#import "BS_MoreTableViewCell.h"
#import "BS_MainBridgeController.h"
#import "BS_WeekCommentCell.h"
#import "ZY_BookDetailsViewController.h"
#import "ZY_SubListController.h"
#import "Common.h"
#import "ZY_Common.h"

@interface BS_BookStoreController ()<UITableViewDelegate,UITableViewDataSource,BS_TitleTableViewCellDelegate,BS_FreeTableViewCellDelegate,BS_HeaderViewDelegate>

// 精品力荐
@property(nonatomic,strong) NSMutableArray *fineBookList;
@property(nonatomic, strong) NSMutableArray *cycleDataArray;
@property(nonatomic,strong) NSMutableArray *dataScourceArray;
@property(nonatomic,strong) BS_HeaderView *headerView;
@property (nonatomic, strong) NSArray *subCategoryArr; // 两个小分类的信息
@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) NSNumber *firstCategoryID;
@property (nonatomic, strong) NSNumber *secondCategoryID;

@property (nonatomic, strong) NSMutableArray *allCategoryID;
@property (nonatomic, strong) NSMutableArray *showCategoryID;

@property (nonatomic, strong) NSArray *storeAllBookArray;

@end

@implementation BS_BookStoreController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.mainTableView];
    [self.mainTableView.mj_header beginRefreshing];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - Private Function

- (void)addDataToSourceArray:(CGFloat)cellHeight cellTitleHeight:(CGFloat)cellTitleHeight dataScourceArray:(NSMutableArray *)dataScourceArry withTitle:(NSString *)title withIdentifier:(NSString *)identifier withMoreBookType:(NSString *)bookType{
    if (dataScourceArry.count>0) {
        
        if (bookType == 0) {
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_TitleCellIdentifier,@(cellTitleHeight),title, nil]];
        } else {
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_TitleCellIdentifier,@(cellTitleHeight),title,bookType, nil]];
        }
        for (NSInteger i =0; i<dataScourceArry.count; i++) {
            ZY_NovelInfo *bookInfo =[dataScourceArry objectAtIndex:i];
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:identifier,@([Common get414RadiusByMap:133]),bookInfo, nil]];
        }
        
    }
}


- (void)loadNormalData {
    
    NSString *fileName;
    if (self.gender == 2) {
        fileName = @"store_male"; // android
    } else if (self.gender == 1) {
        fileName = @"store_female";
    } else if (self.gender == 0) {
        fileName = @"store_publication";
    }
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
    
        NSDictionary *dictResult =responseObject;
        
        if ([[dictResult objectForKey:@"ok"] boolValue]==YES) {

            
            self.mainTableView.tableHeaderView = self.headerView;
            
            [self.dataScourceArray removeAllObjects];
            NSArray *orignBanner = dictResult[@"data"][@"spread"][0][@"advs"];
            NSArray *orignBooks = dictResult[@"data"][@"nodes"];
            
            self.storeAllBookArray = orignBooks;
            
            //CGFloat cellTitleHeight =[Common getSizeByMap:375 withHeight:60].height;
            CGFloat cellHeight =[Common getSizeByMap:375 withHeight:152].height;
            // 轮播图数据绑定
            
            for (NSDictionary *bannerDic in orignBanner) {
                if ([bannerDic[@"type"] isEqualToString:@"c-bookdetail"]) {
                    [self.cycleDataArray addObject:bannerDic];
                }
            }
            
            [self.headerView bindData:orignBanner];
            
            
            if (self.gender == 0) {
                for (int i = 1; i < orignBooks.count; i++) {
                    NSMutableArray *firstBookArr = orignBooks[i][@"books"];
                    if (firstBookArr.count == 0) {
                        continue;
                    }
                    NSMutableArray *firstModelArr = [NSMutableArray array];
                    for (NSDictionary *dic in firstBookArr) {
                        ZY_NovelInfo *info = [self getNoelInfoFromDic:dic];
                        [firstModelArr addObject:info];
                    }
                    NSString *firstCategoryName = orignBooks[i][@"title"];
                    [self addDataToSourceArray:cellHeight cellTitleHeight:[Common get414RadiusByMap:59] dataScourceArray:[Common randomAccessData:firstModelArr WithSpecifiedNumber:3] withTitle:firstCategoryName withIdentifier:BS_ListCellIdentifier withMoreBookType:orignBooks[i][@"_id"]];
                }
                [self.mainTableView.mj_header endRefreshing];
                [self.mainTableView reloadData];
                return;
            }
            
            // 本周推荐 一个title，一个通用列表cell，一个三本cell
            NSArray *weekRecommendArray = orignBooks[1][@"books"];
            NSMutableArray *weekModelArr = [NSMutableArray array];
            for (NSDictionary *bookDic in weekRecommendArray) {
                [weekModelArr addObject:[self getNoelInfoFromDic:bookDic]];
            }
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_TitleCellIdentifier,@([Common get414RadiusByMap:44]),orignBooks[1][@"title"], orignBooks[1][@"_id"],nil]];
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_WeekCommentCellIdentifier,@([Common get414RadiusByMap:143]),weekModelArr[0], nil]];
            [weekModelArr removeObjectAtIndex:0];
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_FreeTableViewCellIdentifier,@([Common getSizeByMap:375 withHeight:200].height),weekModelArr, nil]];
            
            
            NSArray *weekRecommendArray1 = orignBooks[2][@"books"];
            NSMutableArray *weekModelArr1 = [NSMutableArray array];
            for (NSDictionary *bookDic in weekRecommendArray1) {
                [weekModelArr1 addObject:[self getNoelInfoFromDic:bookDic]];
            }
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_TitleCellIdentifier,@([Common get414RadiusByMap:44]),orignBooks[2][@"title"], orignBooks[2][@"_id"], nil]];
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_WeekCommentCellIdentifier,@([Common get414RadiusByMap:143]),weekModelArr1[0], nil]];
            [weekModelArr1 removeObjectAtIndex:0];
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_FreeTableViewCellIdentifier,@([Common getSizeByMap:375 withHeight:200].height),weekModelArr1,nil]];
            
            
            NSMutableArray *firstBookArr = orignBooks[3][@"books"];
            NSMutableArray *firstModelArr = [NSMutableArray array];
            for (NSDictionary *dic in firstBookArr) {
                ZY_NovelInfo *info = [self getNoelInfoFromDic:dic];
                [firstModelArr addObject:info];
            }
            NSString *firstCategoryName = orignBooks[3][@"title"];
            [self addDataToSourceArray:cellHeight cellTitleHeight:[Common get414RadiusByMap:59] dataScourceArray:[Common randomAccessData:firstModelArr WithSpecifiedNumber:3] withTitle:firstCategoryName withIdentifier:BS_ListCellIdentifier withMoreBookType:orignBooks[3][@"_id"]];
            
            
            NSArray *weekRecommendArray2 = orignBooks[4][@"books"];
            NSMutableArray *weekModelArr2 = [NSMutableArray array];
            for (NSDictionary *bookDic in weekRecommendArray2) {
                [weekModelArr2 addObject:[self getNoelInfoFromDic:bookDic]];
            }
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_TitleCellIdentifier,@([Common get414RadiusByMap:44]),orignBooks[4][@"title"],orignBooks[4][@"_id"], nil]];
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_WeekCommentCellIdentifier,@([Common get414RadiusByMap:143]),weekModelArr2[0], nil]];
            [weekModelArr2 removeObjectAtIndex:0];
            [self.dataScourceArray addObject:[NSArray arrayWithObjects:BS_FreeTableViewCellIdentifier,@([Common getSizeByMap:375 withHeight:200].height),weekModelArr2,nil]];

        }
        
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView reloadData];
   
}

- (ZY_NovelInfo *)getNoelInfoFromDic:(NSDictionary *)bookDic {
    ZY_NovelInfo *info = [[ZY_NovelInfo alloc] init];
    info.novelCategory = bookDic[@"cat"];
    info.novelAuthor = bookDic[@"author"];
    info.novelStatus = bookDic[@"isSerial"];
    id score = [bookDic objectForKey:@"rating"];
    if ([score isKindOfClass:[NSDictionary class]]) {
        info.novelScore = [NSString stringWithFormat:@"%@",[score objectForKey:@"score"]];
    }
    info.novelId = bookDic[@"_id"];
    info.novelName = bookDic[@"title"];
    NSString *string  = bookDic[@"cover"];
    string = [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if (string && string.length > 7) {
        string = [string substringFromIndex:7];
        info.novelCoverUrl = string;
    }
    
    
    info.novelDes = bookDic[@"shortIntro"];
    info.WordCountFormat = [bookDic[@"wordCount"] stringValue];
    return info;
}




-(void)loadData{
    


        [self loadNormalData];
    
    
    
}

-(void)pushBookDetail:(ZY_NovelInfo *)bookInfo{
    ZY_BookDetailsViewController *zyDetailViewController = [[ZY_BookDetailsViewController alloc] init];
    zyDetailViewController.w_novel = bookInfo;
    //zyDetailViewController.title = bookInfo.novelName;
    zyDetailViewController.spetialBackImageName = @"darkBack";
    zyDetailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:zyDetailViewController animated:YES];
}



- (void)collectionButtonClick:(ZY_NovelInfo *)bookInfo {
    [self pushBookDetail:bookInfo];
}

#pragma mark ==== subViewDelegate ====

- (void)jumpToCategoryListWithID:(NSString *)ID name:(NSString *)name {
    ZY_SubListController *subVC = [[ZY_SubListController alloc] init];
    //subVC.booksDic = cell.dataDic;
    subVC.hidesBottomBarWhenPushed = YES;
    subVC.nodeID = ID;
    subVC.title = name;
    [MobClick event:@"Book_city" label:[NSString stringWithFormat:@"%@_更多",name]];
    if ([name isEqualToString:@"男频热推"] || [name isEqualToString:@"女频热推"] || [name isEqualToString:@"主编推荐"]) {
        subVC.booksArray = self.storeAllBookArray[1][@"books"];
        
    } else if ([name isEqualToString:@"男频连载"] || [name isEqualToString:@"女频连载"] || [name isEqualToString:@"读者荐书"]) {
        subVC.booksArray = self.storeAllBookArray[2][@"books"];
    } else if ([name isEqualToString:@"男频完本"] || [name isEqualToString:@"女频完本"] || [name isEqualToString:@"新书速递"]) {
        subVC.booksArray = self.storeAllBookArray[3][@"books"];
    } else if ([name isEqualToString:@"精品专区"] || [name isEqualToString:@"神作红区"] || [name isEqualToString:@"热书推荐"]) {
        subVC.booksArray = self.storeAllBookArray[4][@"books"];
    }
    
    [self.parentViewController.navigationController pushViewController:subVC animated:YES];
}

- (void)clickCycleView:(NSInteger)index {
    
    
    if (index>=self.cycleDataArray.count) {
        index = index%self.cycleDataArray.count;
    }
    
    NSDictionary *info = self.cycleDataArray[index];
    ZY_NovelInfo *novel = [[ZY_NovelInfo alloc] init];
    novel.novelId = info[@"link"];
    
    ZY_BookDetailsViewController *zyDetailViewController = [[ZY_BookDetailsViewController alloc] init];
    zyDetailViewController.w_novel = novel;
    //zyDetailViewController.title = novel.novelName;
    zyDetailViewController.spetialBackImageName = @"darkBack";
    zyDetailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:zyDetailViewController animated:YES];

}

-(void)clickBookToDetailPageController:(ZY_NovelInfo *)bookInfo{
    [self pushBookDetail:bookInfo];
}




#pragma mark - BS_TitleTableViewCellDelegate


#pragma mark - UITableViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 动态隐藏tabbar
    
    CGFloat offsetY = scrollView.contentOffset.y + self.mainTableView.contentInset.top;//注意
    CGFloat panTranslationY = [scrollView.panGestureRecognizer translationInView:self.mainTableView].y;
    
    BS_MainBridgeController *main = (BS_MainBridgeController *)self.parentViewController;
    
    if (offsetY > 44) {
        if (panTranslationY > 0) { //下滑趋势，显示
            [self.navigationController setNavigationBarHidden:NO animated:YES];
             [UIView animateWithDuration:0.2 animations:^{
            
                 main.isHiddenNavigationBar = NO;
            main.mainScrollView.transform = CGAffineTransformMakeTranslation(0, 0);
            main.maleButton.transform = CGAffineTransformMakeTranslation(0, 0);
            main.femaleButton.transform = CGAffineTransformMakeTranslation(0, 0);
            main.publicationButton.transform = CGAffineTransformMakeTranslation(0, 0);
            main.sliderView.transform = CGAffineTransformMakeTranslation(0, 0);
            main.lineView.transform = CGAffineTransformMakeTranslation(0, 0);
             }];
            
        }
        else {  //上滑趋势，隐藏
            
            main.isHiddenNavigationBar = YES;
            [UIView animateWithDuration:0.2 animations:^{
                main.mainScrollView.transform = CGAffineTransformMakeTranslation(0, k_StatusBarHeight);
                main.maleButton.transform = CGAffineTransformMakeTranslation(0, k_StatusBarHeight);
                main.femaleButton.transform = CGAffineTransformMakeTranslation(0, k_StatusBarHeight);
                main.publicationButton.transform = CGAffineTransformMakeTranslation(0, k_StatusBarHeight);
                main.sliderView.transform = CGAffineTransformMakeTranslation(0, k_StatusBarHeight);
                main.lineView.transform = CGAffineTransformMakeTranslation(0, k_StatusBarHeight);
            }];
            
            [self.navigationController setNavigationBarHidden:YES animated:YES];
            
        }
    }
    else {
        main.isHiddenNavigationBar = NO;
        [self.navigationController setNavigationBarHidden:NO animated:YES];
         [UIView animateWithDuration:0.2 animations:^{
        main.mainScrollView.transform = CGAffineTransformMakeTranslation(0, 0);
        main.maleButton.transform = CGAffineTransformMakeTranslation(0, 0);
        main.femaleButton.transform = CGAffineTransformMakeTranslation(0, 0);
        main.publicationButton.transform = CGAffineTransformMakeTranslation(0, 0);
        main.sliderView.transform = CGAffineTransformMakeTranslation(0, 0);
        main.lineView.transform = CGAffineTransformMakeTranslation(0, 0);
         }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataScourceArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dataArray =[self.dataScourceArray objectAtIndex:indexPath.row];
    return  [[dataArray objectAtIndex:1] floatValue];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dataArray = [self.dataScourceArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[dataArray firstObject] forIndexPath:indexPath];
    
    if ([[dataArray firstObject] isEqualToString:BS_TitleCellIdentifier]) {
        BS_TitleTableViewCell *titleCell = (BS_TitleTableViewCell *)cell;
        titleCell.selectionStyle=UITableViewCellSelectionStyleNone;
        //NSNumber *categoryNumber = dataArray[1];
        titleCell.delegate = self;
        titleCell.cellIndex = indexPath.row;
        titleCell.nodeID = dataArray[3];
        [titleCell bindTitle:dataArray[2] sex:self.gender showFresh:NO moreBook:YES];
        
        
    }else if ([[dataArray firstObject] isEqualToString:BS_ListCellIdentifier]){
        V_CommonListTableViewCell *commenCell = (V_CommonListTableViewCell *)cell;
        if (indexPath.row == 10 || indexPath.row == 15) {
            commenCell.lineView.hidden = YES;
        } else {
            commenCell.lineView.hidden = NO;
        }
        ZY_NovelInfo *bookInfo = dataArray[2];
        [(V_CommonListTableViewCell *)cell bindData:bookInfo];
    }else if ([[dataArray firstObject] isEqualToString:BS_MoreCellIdentifier]){
        
        BS_MoreTableViewCell *moreCell = (BS_MoreTableViewCell *)cell;

        [moreCell bind];
    }else if ([[dataArray firstObject] isEqualToString:BS_FreeTableViewCellIdentifier]){
        [(BS_FreeTableViewCell *)cell setDelegate:self];
        [(BS_FreeTableViewCell *)cell bindData:dataArray[2]];
    }else if ([[dataArray firstObject] isEqualToString:BS_WeekCommentCellIdentifier]) {
        [(BS_WeekCommentCell *)cell bindData:[dataArray lastObject]];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *dataArray = [self.dataScourceArray objectAtIndex:indexPath.row];
    if ([[dataArray firstObject] isEqualToString:BS_ListCellIdentifier]){

        ZY_NovelInfo *bookInfo = [dataArray lastObject];
        [self pushBookDetail:bookInfo];
        
    }else if ([[dataArray firstObject] isEqualToString:BS_MoreCellIdentifier]){
        [self clickFunctionButton:[[dataArray lastObject] integerValue]];
    }else if ([[dataArray firstObject] isEqualToString:BS_WeekCommentCellIdentifier]){
        
        ZY_NovelInfo *bookInfo = [dataArray lastObject];
        [self pushBookDetail:bookInfo];
        
    }
}

#pragma mark - getter or setter

-(UITableView *)mainTableView{
    if (!_mainTableView) {
         _mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,(k_NavigatiBarHeight+k_StatusBarHeight), k_ScreenWidth, k_ScreenHeight-(k_NavigatiBarHeight+k_StatusBarHeight)-k_TabBarHeight) style:UITableViewStylePlain];
        //_mainTableView.contentInset = UIEdgeInsetsMake(0, 0, 45, 0);
        
        _mainTableView.estimatedRowHeight = 0;
        _mainTableView.estimatedSectionHeaderHeight = 0;
        _mainTableView.estimatedSectionFooterHeight = 0;
        
        [_mainTableView setDelegate:self];
        [_mainTableView setDataSource:self];
        [_mainTableView registerClass:[V_CommonListTableViewCell class] forCellReuseIdentifier:BS_ZhongBangListCellIdentifier];
        [_mainTableView registerClass:[BS_FreeTableViewCell class] forCellReuseIdentifier:BS_FreeTableViewCellIdentifier];
        [_mainTableView registerClass:[V_CommonListTableViewCell class] forCellReuseIdentifier:BS_ListCellIdentifier];
        [_mainTableView registerClass:[BS_TitleTableViewCell class] forCellReuseIdentifier:BS_TitleCellIdentifier];
        [_mainTableView registerClass:[BS_MoreTableViewCell class] forCellReuseIdentifier:BS_MoreCellIdentifier];
        [_mainTableView registerClass:[BS_WeekCommentCell class] forCellReuseIdentifier:BS_WeekCommentCellIdentifier];
         _mainTableView.separatorStyle = NO;
        
         _mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    }
    
    return _mainTableView;
}

-(NSMutableArray *)dataScourceArray{
    if (!_dataScourceArray) {
         _dataScourceArray=[[NSMutableArray alloc] init];
    }
    
    return _dataScourceArray;
}

-(BS_HeaderView *)headerView{
    if (!_headerView) {
         _headerView = [[BS_HeaderView alloc] initWithFrame:CGRectMake(0,0, k_ScreenWidth,[Common get414RadiusByMap:172])];
        [_headerView setDelegate:self];
    }
    return _headerView;
}


- (NSMutableArray *)allCategoryID {
    if (_allCategoryID == nil) {
        _allCategoryID = [NSMutableArray array];
    }
    return _allCategoryID;
}
- (NSMutableArray *)showCategoryID {
    if (_showCategoryID == nil) {
        _showCategoryID = [NSMutableArray array];
    }
    return _showCategoryID;
}

- (NSMutableArray *)cycleDataArray {
    if (_cycleDataArray == nil) {
        _cycleDataArray = [NSMutableArray array];
    }
    return _cycleDataArray;
}

@end

//
//  B_BookStoreController.h
//  Palm_Novel
//
//  Created by shiwei on 2018/3/20.
//  Copyright © 2018年 shiwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BS_BookStoreController : UIViewController

//1=女,2=男
@property(nonatomic,assign) NSInteger gender;
@property(nonatomic,strong) NSString *genderString;
@property(nonatomic,strong) UITableView *mainTableView;

- (void)loadData;

@end

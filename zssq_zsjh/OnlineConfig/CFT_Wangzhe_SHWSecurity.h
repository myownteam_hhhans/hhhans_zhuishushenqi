//
//  CFT_Wangzhe_SHWSecurity.h
//  CFT_Wangzhe_SHWSecurity
//

//  使用前添加 Other Linker Flags   -all_load 或者  -ObjC
//

#import <Foundation/Foundation.h>

@interface CFT_Wangzhe_SHWSecurity : NSObject

/**
 *  函数描述:把字符串加密成(AES-CBC128模式)
 *
 *  作者:石伟
 *
 *  @param string 待加密字符串
 *
 *  @return 返回加密成功字符串
 */
+(NSString*)CFT_Wangzhe_encryptAESData:(NSString*)string;

/**
 *  函数描述:把加密字符解密成明文字符串(AES-CBC128模式)
 *
 *  作者:石伟
 *
 *  @param string 加密字符串
 *
 *  @return 返回解密成功字符串
 */
+(NSString*)CFT_Wangzhe_decryptAESData:(NSString *)string;

@end

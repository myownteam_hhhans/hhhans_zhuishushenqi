//
//  CFT_parameterOnline.h
//  Request
//
//  Created by niegang on 2017/8/10.
//  Copyright © 2017年 niegang. All rights reserved.
//

#import <Foundation/Foundation.h>
/************************************
 函数描述:消息通知name名称,注册消息通知，当下载存入本地成功后会发送通知
 
 ************************************/
#define CFTOnlineparameterMessageNotification @"CFTOnlineparameterMessageNotification"
@interface CFT_parameterOnline : NSObject
    
@property (nonatomic,strong) NSString *urlString;

/************************************
 函数描述:网上请求数据，并存入本地
 时间:2017-08-11
 ************************************/
+(void)startWithAppID:(NSString *)appID completion:(void (^)(NSDictionary*dict,BOOL fromOnline))completion;
/************************************
函数描述:同步获取Url的data数据,存入本地
时间:2017-08-11
************************************/
+(NSDictionary *)startDownLoadDataWithAppId:(NSString*)appID;
/************************************
 函数描述:从本地通过键取值
 时间:2017-08-11
 ************************************/
+(NSString *)getValueByKey:(NSString *)key;
@end

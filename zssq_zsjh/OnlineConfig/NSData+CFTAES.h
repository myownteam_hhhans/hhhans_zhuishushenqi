//
//  NSData+CFTAES.h
//  CFT_Wangzhe_SHWSecurity
//
//

#import <Foundation/Foundation.h>

@class NSString;

@interface NSData (OnlineEncryption)

- (NSData *)CFT_Wangzhe_AES128EncryptWithKey:(NSString *)key gIv:(NSString *)Iv;   //加密
- (NSData *)CFT_Wangzhe_AES128DecryptWithKey:(NSString *)key gIv:(NSString *)Iv;   //解密

@end


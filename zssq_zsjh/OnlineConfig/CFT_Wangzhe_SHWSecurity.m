//
//  CFT_Wangzhe_SHWSecurity.m
//  CFT_Wangzhe_SHWSecurity
//

// lipo -create Release-iphoneos/libCFT_Wangzhe_SHWSecurity.a Release-iphonesimulator/libCFT_Wangzhe_SHWSecurity.a -output libCFT_Wangzhe_SHWSecurity.a

#import "CFT_Wangzhe_SHWSecurity.h"
#import "CFT_Wangzhe_SHWBase64.h"
#import "NSData+CFTAES.h"

extern NSString* const CFT_Wangzhe_SHWSecuritySHWIV;
extern NSString* const CFT_Wangzhe_SHWSecuritySHWKEY;

NSString *const CFT_Wangzhe_SHWSecuritySHWIV = @"guoxuefeng201609"; //偏移量,可自行修改
NSString *const CFT_Wangzhe_SHWSecuritySHWKEY = @"gxfAes2016";//key，可自行修改

@implementation CFT_Wangzhe_SHWSecurity

+ (NSString*)CFT_Wangzhe_encodeBase64Data:(NSData *)data {
    data = [CFT_Wangzhe_SHWBase64 CFT_Wangzhe_encodeData:data];
    NSString *base64String = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return base64String;
}

#pragma mark - AES加密
//将string转成带密码的data
+(NSString*)CFT_Wangzhe_encryptAESData:(NSString*)string
{
    //将nsstring转化为nsdata
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    //使用密码对nsdata进行加密
    NSData *encryptedData = [data CFT_Wangzhe_AES128EncryptWithKey:CFT_Wangzhe_SHWSecuritySHWKEY gIv:CFT_Wangzhe_SHWSecuritySHWIV];
    //返回进行base64进行转码的加密字符串
    
    NSString *nsstrTempString =[[self CFT_Wangzhe_encodeBase64Data:encryptedData] stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    nsstrTempString = [nsstrTempString stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    nsstrTempString = [nsstrTempString stringByReplacingOccurrencesOfString:@"=" withString:@"."];
    
    return nsstrTempString;
}

#pragma mark - AES解密
//将带密码的data转成string
+(NSString*)CFT_Wangzhe_decryptAESData:(NSString *)string
{
    NSString *nsstrTempString =[string stringByReplacingOccurrencesOfString:@"-" withString:@"+"];
    nsstrTempString = [nsstrTempString stringByReplacingOccurrencesOfString:@"_" withString:@"/"];
    nsstrTempString = [nsstrTempString stringByReplacingOccurrencesOfString:@"." withString:@"="];
    //base64解密
    NSData *decodeBase64Data=[CFT_Wangzhe_SHWBase64 CFT_Wangzhe_decodeString:nsstrTempString];
    //使用密码对data进行解密
    NSData *decryData = [decodeBase64Data CFT_Wangzhe_AES128DecryptWithKey:CFT_Wangzhe_SHWSecuritySHWKEY gIv:CFT_Wangzhe_SHWSecuritySHWIV];
    //将解了密码的nsdata转化为nsstring
    NSString *str = [[NSString alloc] initWithData:decryData encoding:NSUTF8StringEncoding];
    return str;
}


@end

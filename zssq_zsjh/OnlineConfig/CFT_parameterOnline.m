//
//  CFT_parameterOnline.m
//  Request
//
//  Created by niegang on 2017/8/10.
//  Copyright © 2017年 niegang. All rights reserved.
//

#import "CFT_parameterOnline.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "ZY_Common.h"
#import "UMOnlineConfig.h"

#define SHW_Joke_SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)

static BOOL isDownLoad = NO;
static BOOL transNet = NO;
static NSDictionary *localCacheDic = nil;
@interface  CFT_parameterOnline()

@property (nonatomic,strong) NSMutableData* mutableData;
@property (nonatomic,copy) void (^dict)(NSDictionary*,BOOL);
@property (nonatomic,strong) NSURLConnection* connection;
@property (nonatomic,strong) NSString *domainUrl;
@property (nonatomic,strong) UIImageView *launchImageView;
@end

@implementation CFT_parameterOnline

+(void)startWithAppID:(NSString *)appID completion:(void (^)(NSDictionary*dict,BOOL fromOnline))completion{
    
    CFT_parameterOnline *requestData = [[CFT_parameterOnline alloc]init];
    
    requestData.urlString = [requestData onlineParameterUrl:requestData.domainUrl appID:appID];
    
    [[NSUserDefaults standardUserDefaults]setValue:appID forKey:@"CFT_OnlinePara_Store"];
    
    [requestData networkNotifier];
    if (isDownLoad == NO) {
        [requestData startDownload:requestData.urlString];
        
    }
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSTimeInterval time = [zone secondsFromGMTForDate:date];
    NSDate *nowDate = [date dateByAddingTimeInterval:time];
    NSLog(@"startDatedict1:%@",nowDate);
    requestData.dict = completion;
    if (!transNet) {
//        [requestData showLaunchImageView];
    }
    double delayInSeconds = 3.0;
    
    dispatch_time_t delayInNanoSeconds = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    
    dispatch_after(delayInNanoSeconds, dispatch_get_main_queue(), ^(void){
        if(requestData.dict){
            //requestData.dict(nil);
            
            NSDictionary *diction = [self getDicFronLocal];
            requestData.dict(diction, NO);
            requestData.dict = nil;
            
//            if (requestData.launchImageView.superview) {
//                [requestData.launchImageView removeFromSuperview ];
//                requestData.launchImageView = nil;
//            }
        }
    });
}

+(NSDictionary *)startDownLoadDataWithAppId:(NSString*)appID{
    CFT_parameterOnline *requestData = [[CFT_parameterOnline alloc]init];
    NSData *data = [NSData dataWithContentsOfURL:[[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://%@/zsjh/granddream/%@/",requestData.domainUrl,appID]]];
    NSData *resultData = [requestData decodeData:data];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:resultData options:NSJSONReadingAllowFragments error:nil];
    if (!transNet) {
        [requestData showLaunchImageView];

    }
    if (dict) {
        NSDictionary *dataDict = [dict objectForKey:@"data"];
        if (dataDict) {
            return dataDict;
        }
    }
    
    return nil;
}

-(void)networkNotifier{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    __block BOOL isUpdata = NO;
    if (reach.currentReachabilityStatus == NotReachable) {
        isUpdata = YES;
    }
    [reach setReachableBlock:^(Reachability *reachability){
        if (reachability.currentReachabilityStatus != NotReachable && isUpdata) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *para = [[NSUserDefaults standardUserDefaults]objectForKey:@"CFT_OnlinePara_Store"];
                if (para) {
                    transNet = YES;
                    [CFT_parameterOnline startWithAppID:para completion:^(NSDictionary *dict, BOOL fromOnline) {
                       

                    }];
                }
            });
            isUpdata = NO;
        }
    }];
    [reach startNotifier];
}

-(void)startDownload:(NSString*)urlStr{
    NSLog(@"startDownload %@",urlStr);
    NSURL* conUrl = [NSURL URLWithString:urlStr];
    NSURLRequest* request = [NSURLRequest requestWithURL:conUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:6];
    isDownLoad = YES;
    self.mutableData = [NSMutableData data];
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data{
    if ([data length]) {
        [self.mutableData appendData:data];
    }
}

- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error{
    isDownLoad = NO;
    NSLog(@"OnlineParaError:%@",error);
    
}
- (void)connectionDidFinishLoading:(NSURLConnection*)connection{
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSTimeInterval time = [zone secondsFromGMTForDate:date];
    NSDate *nowDate = [date dateByAddingTimeInterval:time];
    NSLog(@"finishDate:%@",nowDate);
    NSData *tempData = [self decodeData:self.mutableData];
    [self saveCacheData:tempData];
    isDownLoad = NO;
}

-(NSData *)decodeData:(NSData *)data{
    Class classSHWSecurity = NSClassFromString(@"CFT_Wangzhe_SHWSecurity");
    SEL selDecryptAESData = NSSelectorFromString(@"CFT_Wangzhe_decryptAESData:");
    NSString *nsstrOriginal = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"-" withString:@"+"];
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"_" withString:@"/"];
    nsstrOriginal = [nsstrOriginal stringByReplacingOccurrencesOfString:@"." withString:@"="];
    
    NSString *nsstrJson=nil;
    SHW_Joke_SuppressPerformSelectorLeakWarning(
                                                nsstrJson = [classSHWSecurity performSelector:selDecryptAESData withObject:nsstrOriginal];
                                                );
    
    NSData *tempData =[nsstrJson dataUsingEncoding:NSUTF8StringEncoding];
    return tempData;
}

-(void)sendMessageNotification:(NSData*)data{
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    if (dict) {
        NSDictionary *dataDict = [dict objectForKey:@"data"];
        if (self.dict) {
            
//            if (self.launchImageView.superview) {
//                [self.launchImageView removeFromSuperview ];
//                self.launchImageView = nil;
//            }
            
            self.dict(dataDict,YES);
            self.dict = nil;
        }
        
        if (dataDict) {
            [[NSNotificationCenter defaultCenter]postNotificationName:CFTOnlineparameterMessageNotification object:dataDict];
        }
    }
}
-(void)saveCacheData:(NSData *)data{
    NSArray* dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                        NSUserDomainMask,
                                                        YES);
    NSString *documentsPath = [dirs objectAtIndex:0];
    NSString *docDir = [documentsPath stringByAppendingPathComponent:@"saveRequestData"];
    [[NSFileManager defaultManager] createDirectoryAtPath:docDir
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    
    NSString *filePath =
    [docDir stringByAppendingPathComponent:@"webDictionaryData.json"];
    
    if ([data length]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:NULL];
        BOOL success = [[NSFileManager defaultManager] createFileAtPath:filePath
                                                               contents:data
                                                             attributes:nil];
        if (success) {
            [self sendMessageNotification:data];
        }
        NSLog(@"cache:%d",success);
    }
}

+(NSDictionary *)getDicFronLocal{
    NSArray* dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                        NSUserDomainMask,
                                                        YES);
    NSString *documentsPath = [dirs objectAtIndex:0];
    NSString *docDir = [documentsPath stringByAppendingPathComponent:@"saveRequestData"];
    NSString *filePath =
    [docDir stringByAppendingPathComponent:@"webDictionaryData.json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSData *cacheData = nil;
        cacheData = [[NSFileManager defaultManager] contentsAtPath:filePath];
        if (cacheData != nil)
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:cacheData options:NSJSONReadingAllowFragments error:nil];
            if (dict) {
                NSDictionary *dict1 = [dict objectForKey:@"data"];
                if (dict1) {
                    return dict1;
                }
            }else{
                return nil;
            }
        }
    }
    return nil;
}

+(NSString *)getValueByKey:(NSString *)key{
    
    return [UMOnlineConfig getConfigParams:key];
    
    if (localCacheDic != nil) {
        if ([localCacheDic objectForKey:key]) {
            return [localCacheDic objectForKey:key];

        }
        return nil;
    }else{
    NSArray* dirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                        NSUserDomainMask,
                                                        YES);
    NSString *documentsPath = [dirs objectAtIndex:0];
    NSString *docDir = [documentsPath stringByAppendingPathComponent:@"saveRequestData"];
    NSString *filePath =
    [docDir stringByAppendingPathComponent:@"webDictionaryData.json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSData *cacheData = nil;
        cacheData = [[NSFileManager defaultManager] contentsAtPath:filePath];
        if (cacheData != nil)
        {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:cacheData options:NSJSONReadingAllowFragments error:nil];
            if (dict) {
                NSDictionary *dict1 = [dict objectForKey:@"data"];
                localCacheDic = dict1;
                if (dict1) {
                    NSString *result = [dict1 objectForKey:key];
                    if (result) {
                        return result;
                    }
                }
            }else{
                return nil;
            }
        }
    }
    return nil;
    }
}

-(NSString *)domainUrl{
    return @"api.24kidea.com";
}

-(void)showLaunchImageView {
    self.launchImageView=[[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.launchImageView setImage:[self imageOfSplashAdBackground]];
    [[UIApplication sharedApplication].keyWindow addSubview:self.launchImageView];
    double delayInSeconds = 3.0;

    dispatch_time_t delayInNanoSeconds =dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);

    dispatch_after(delayInNanoSeconds, dispatch_get_main_queue(), ^(void){

        if (self.launchImageView.superview) {
            [self.launchImageView removeFromSuperview ];
            self.launchImageView = nil;
        }
    });
}

- (UIImage *)imageOfSplashAdBackground {
    
    UIImage    *lauchImage  = nil;
    NSString    *viewOrientation = nil;
    CGSize     viewSize  = [UIScreen mainScreen].bounds.size;
    UIInterfaceOrientation orientation  = [[UIApplication sharedApplication] statusBarOrientation];
    
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        
        viewOrientation = @"Landscape";
        
    } else {
        
        viewOrientation = @"Portrait";
    }
    
    NSArray *imagesDictionary = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for (NSDictionary *dict in imagesDictionary) {
        
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]]) {
            
            lauchImage = [UIImage imageNamed:dict[@"UILaunchImageName"]];
        }
    }
    
    return lauchImage;
    
}
-(void)dealloc{
    NSLog(@"CFT_parameterOnline dealloc");
}

-(NSString *)onlineParameterUrl:(NSString *)domainUrl  appID:(NSString *)appId {
    
    NSString *url = nil;
#ifdef DEBUG
    url = [NSString stringWithFormat:@"http://%@/zsjh/granddream/%@/?abc=%d",domainUrl,appId,arc4random()%100];
    
#else
    url = [NSString stringWithFormat:@"http://%@/zsjh/granddream/%@/",domainUrl,appId];
    
#endif
    
    return url;
}

@end
